<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                Редактировать коментарий
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Имя</label>
                            <div class="form-control form-control-sm rounded-0"><?php echo $comment['comment_username'];?></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Email</label>
                            <div class="form-control form-control-sm rounded-0"><?php echo $comment['comment_user_email'];?></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Коментарий</label>
                            <textarea name="text" class="form-control form-control-sm rounded-0" rows="3"><?php echo $comment['comment_text'];?></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Ответ</label>
                            <textarea name="text_reply" class="form-control form-control-sm rounded-0" rows="3"><?php echo $comment['comment_text_reply'];?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
                    <input type="hidden" name="comment" value="<?php echo $comment['id_comment'];?>">
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/comments/ajax_operations/edit',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
