<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Currency extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
		$this->data = array();
		
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("currency_model", "currency");

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		return_404($this->data);
	}

	function _get_all($params = array()){
		return $this->currency->handler_get_all($params);
	}

	function _get_default_apanel(){
		return $this->currency->handler_get_default_admin();
	}
}
