<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Comments_model extends CI_Model{
	var $items_comments = "items_comments";
	var $items = "items";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->items_comments, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_comment, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_comment', $id_comment);
		$this->db->update($this->items_comments, $data);
	}

	function handler_delete($id_comment){
		$this->db->where('id_comment', $id_comment);
		$this->db->delete($this->items_comments);
	}

	function handler_get($id_comment){
		$this->db->where('id_comment', $id_comment);
		return $this->db->get($this->items_comments)->row_array();
	}

	function handler_get_by_item($id_item){
		$this->db->where('id_item', $id_item);
		return $this->db->get($this->items_comments)->row_array();
	}

	function handler_delete_by_item($id_item){
		$this->db->where('id_item', $id_item);
		$this->db->delete($this->items_comments);
	}

	function handler_get_all($conditions = array()){
		$order_by = " {$this->items_comments}.comment_date DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->select("{$this->items_comments}.*");
		$this->db->select("{$this->items}.item_url");
		$this->db->from("{$this->items_comments}");
		$this->db->join("{$this->items}", "{$this->items}.id_item = {$this->items_comments}.id_item", "left");

        if(isset($id_item)){
			$this->db->where("{$this->items_comments}.id_item", $id_item);
        }
		
        if(isset($comment_date_from)){
			$this->db->where("DATE({$this->items_comments}.comment_date) >= DATE('{$comment_date_from}')");
        }

        if(isset($comment_date_to)){
			$this->db->where("DATE({$this->items_comments}.comment_date) <= DATE('{$comment_date_to}')");
        }

        if(isset($comment_date_reply_from)){
			$this->db->where("DATE({$this->items_comments}.comment_date_reply) >= DATE('{$comment_date_reply_from}')");
        }

        if(isset($comment_date_reply_to)){
			$this->db->where("DATE({$this->items_comments}.comment_date_reply) <= DATE('{$comment_date_reply_to}')");
        }

		if (isset($keywords)) {
			$this->db->where("({$this->items_comments}.comment_username LIKE '%{$keywords}%' OR {$this->items_comments}.comment_user_email LIKE '%{$keywords}%' OR {$this->items_comments}.comment_text LIKE '%{$keywords}%' OR {$this->items_comments}.comment_text_reply LIKE '%{$keywords}%')");
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_item)){
			$this->db->where("id_item", $id_item);
        }
		
        if(isset($comment_date_from)){
			$this->db->where("DATE(comment_date) >= DATE('{$comment_date_from}')");
        }

        if(isset($comment_date_to)){
			$this->db->where("DATE(comment_date) <= DATE('{$comment_date_to}')");
        }

        if(isset($comment_date_reply_from)){
			$this->db->where("DATE(comment_date_reply) >= DATE('{$comment_date_reply_from}')");
        }

        if(isset($comment_date_reply_to)){
			$this->db->where("DATE(comment_date_reply) <= DATE('{$comment_date_reply_to}')");
        }

		if (isset($keywords)) {
			$this->db->where(' (comment_username LIKE "%'.$keywords.'%" OR comment_user_email LIKE "%'.$keywords.'%" OR comment_text LIKE "%'.$keywords.'%" OR comment_text_reply LIKE "%'.$keywords.'%") ');
		}

		return $this->db->count_all_results($this->items_comments);
	}
}
?>
