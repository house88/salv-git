<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view($this->theme->public_view('includes/head')); ?>
</head>
<body>
	<?php $this->load->view($this->theme->public_view('includes/navigation'));?>

	<?php $this->load->view($this->theme->public_view('includes/breadcrumbs'));?>

	<?php $this->load->view($this->theme->public_view($main_content));?>
		
	<?php $this->load->view($this->theme->public_view('includes/footer'));?>
</body>
</html>