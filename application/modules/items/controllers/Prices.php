<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Prices extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/items/prices/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'prices';

		$this->data = array();

		$this->load->model("Prices_model", "prices");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		checkPermision('manage_price_variants', '/admin');

		$this->data['page_header'] = 'Ценовые категорий';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'edit':
				checkPermisionAjax('manage_price_variants');


				$id_price_variant = (int) $this->uri->segment(5);
				$this->data['price_variant'] = $this->prices->handler_get($id_price_variant);
				if(empty($this->data['price_variant'])){
					jsonResponse('Ценовая категория не найдена.');
				}

				$this->data['currencies'] = Modules::run('currency/_get_all', array(
					'active' => 1
				));

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'edit':
				checkPermisionAjax('manage_price_variants');

				$this->form_validation->set_rules('price', 'Ценовая категория', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[30]');
				$this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_price = (int) $this->input->post('price');
				$price = $this->prices->handler_get($id_price);
				if(empty($price)){
					jsonResponse('Ценовая категория не найдена.');
				}

				$update = array(
					'price_variant_title' => $this->input->post('title', true),
					'price_variant_currency' => (int) $this->input->post('currency'),
					'price_variant_active' => 0
				);

				if($this->input->post('active')){
					if(empty($update['price_variant_title'])){
						jsonResponse('Нельзя активировать категорию без названия.');
					}

					$update['price_variant_active'] = 1;
				}

				$this->prices->handler_update($id_price, $update);

				jsonResponse('Сохранено','success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_price_variants');

				$this->form_validation->set_rules('price', 'Ценовая категория', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_price = (int) $this->input->post('price');
				$price = $this->prices->handler_get($id_price);
				if(empty($price)){
					jsonResponse('Ценовая категория не найдена.');
				}

				if(empty($price['price_variant_title'])){
					jsonResponse('Нельзя активировать Ценовую категорию без названия.');
				}

				$this->prices->handler_update($id_price, array(
					'price_variant_active' => (int) $price['price_variant_active'] === 1 ? 0 : 1
				));

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_price_variants');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);
		
				$records = $this->prices->handler_get_all($params);
				$records_total = $this->prices->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_id'			=>  $record['id_price_variant'],
							'dt_name'		=>  !empty($record['price_variant_title']) ? $record['price_variant_title'] : 'Без названия',
							'dt_status'		=> '<a href="#" class="custom-font-size-34 '. get_choice('fad fa-check-square text-success', (int) $record['price_variant_active'] === 1, 'fad fa-minus-square text-gray') .' call-function" data-callback="change_status" data-price="'.$record['id_price_variant'].'"></a>',
							'dt_actions'	=> '<button type="button" class="btn btn-primary btn-flat btn-sm call-popup" data-popup="#general_popup_form" data-href="'.base_url('admin/prices/popup/edit/' . $record['id_price_variant']).'" title="Редактировать">
													<i class="fad fa-pencil"></i>
												</button>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
