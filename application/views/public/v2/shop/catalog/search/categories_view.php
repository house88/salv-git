<?php if(!empty($search_categories)){?>
    <ul class="categories-list">
        <?php foreach($search_categories as $search_category){?>
            <li>
                <a class="parent-title <?php if(!empty($category) && $category['category_id'] == $search_category['detail']->category_id){echo 'active';}?>" href="<?php echo base_url('search/catalog?category='.$search_category['detail']->url.'&keywords='.$keywords);?>">
                    <?php echo $search_category['detail']->category_title;?>
                </a>
                <?php if(!empty($search_category['sub_categories'])){?>
                    <ul>
                        <?php foreach($search_category['sub_categories'] as $search_subcategory){?>
                            <li>
                                <a class="<?php if(!empty($category) && $category['category_id'] == $search_subcategory['category']->category_id){echo 'active';}?>" href="<?php echo base_url('search/catalog?category='.$search_subcategory['category']->url.'&keywords='.$keywords);?>">
                                    <span class="title">
                                        <?php echo $search_subcategory['category']->category_title;?>
                                    </span>
                                    <span class="products-count">(<?php echo $search_subcategory['search_count'];?>)</span>
                                </a>
                            </li>
                        <?php }?>
                    </ul>
                <?php } else{?>
                    <span class="products-count-single">(<?php echo $search_category['search_count'];?>)</span>
                <?php }?>
            </li>
        <?php }?>
    </ul>
<?php }?>