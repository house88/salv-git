<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/pages/';
		$this->active_menu = 'special_pages';
		$this->active_submenu = 'pages';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Pages_model", "pages");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_pages', '/admin');

		$this->data['page_header'] = 'Страницы';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_pages');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_pages');

				$id_page = (int) $this->uri->segment(5);
				$this->data['page'] = $this->pages->handler_get($id_page);
				if(empty($this->data['page'])){
					jsonResponse('Страница не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_pages');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('url', 'Url', 'required|xss_clean|alpha_dash|max_length[100]|is_unique[pages.url]');
				$this->form_validation->set_rules('description', 'Текст', 'required');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$insert = array(
					'page_title' => $this->input->post('title'),
					'page_description' => $this->input->post('description'),
					'url' => $this->input->post('url'),
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'page_visible' => $this->input->post('visible') ? 1 : 0
				);

				$this->pages->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_pages');

                $this->form_validation->set_rules('page', 'Страница', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('description', 'Текст', 'required');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean|max_length[250]');
				
				$id_page = (int) $this->input->post('page');
				$page = $this->pages->handler_get($id_page);
				if(empty($page)){
					jsonResponse('Нет такой страницы!');
				}

				$url = $this->input->post('url');
				if($page['url'] != $url){
					$this->form_validation->set_rules('url', 'Url', 'required|xss_clean|alpha_dash|max_length[100]|is_unique[pages.url]');
				} else{
					$this->form_validation->set_rules('url', 'Url', 'required|xss_clean|alpha_dash|max_length[100]');
				}

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$update = array(
					'page_title' => $this->input->post('title'),
					'page_description' => $this->input->post('description'),
					'url' => $this->input->post('url'),
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'page_visible' => $this->input->post('visible') ? 1 : 0
				);

				$this->pages->handler_update($id_page, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_pages');

				$this->form_validation->set_rules('page', 'Страница', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_page = (int) $this->input->post('page');
				$page = $this->pages->handler_get($id_page);
				if(empty($page)){
					jsonResponse('Страница не найдена.');
				}

				$this->pages->handler_delete($id_page);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_pages');
				 
				$this->form_validation->set_rules('page', 'Страница', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_page = (int) $this->input->post('page');
				$page = $this->pages->handler_get($id_page);
				if(empty($page)){
					jsonResponse('Страница не найдена.');
				}
				
				$this->pages->handler_update($id_page, array(
					'page_visible' => (int) !((int) $page['page_visible'])
				));

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_pages');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'id_brand-desc' )
				);

				$params['sort_by'] = flat_dt_ordering($_POST, array(
					'dt_id'     => 'id_page',
					'dt_name'   => 'page_title'
				));

				$records = $this->pages->handler_get_all($params);
        		$records_total = $this->pages->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){			
						return array(
							'dt_id'			=>  $record['id_page'],
							'dt_name'		=>  '<a href="'.base_url('page/'.$record['url']).'" target="_blank">'.$record['page_title'].'</a>',
							'dt_url'		=>  'page/'.$record['url'],
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['page_visible'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-page="'.$record['id_page'].'"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/pages/popup/edit/' . $record['id_page']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить бренд?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-page="'.$record['id_page'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
