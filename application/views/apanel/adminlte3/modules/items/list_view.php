<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header pl-2 pl-sm-3 pr-2 pr-sm-3">
					<h3 class="card-title">
						<?php echo $page_header;?>, <small>найдено: <span id="total_dtTable-counter">0</span></small>
					</h3>
					<div class="card-tools">
						<a class="btn btn-tool call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/items/popup/add');?>" tooltip="Добавить товар"><i class="fas fa-plus"></i> Добавить</a>
						<button type="button" class="btn btn-tool" data-toggle="modal" data-target="#dtFilter" data-toggle-class="modal-open-aside">
							<i class="fas fa-filter"></i>
						</button>
					</div>
				</div>
				<div class="card-body pl-2 pl-sm-3 pr-2 pr-sm-3">
					<div class="dtfilter-list">
						<div class="filter-plugin-list d-flex justify-content-start flex-wrap"></div>
					</div>
					
					<?php if(!empty($dt_filter)){?>
						<?php $this->load->view($this->theme->apanel_view('includes/filter'));?>
					<?php }?>

					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>                                    
                                    <th class="dt_photo">Фото</th>
									<th class="dt_name">Название</th>
									<th class="dt_quantity">Количество</th>
									<th class="dt_price">Цена (<?php echo $currency['currency_symbol'];?>)</th>
                                    <th class="dt_create">Создан</th>
                                    <th class="dt_update">Обновлен</th>
                                    <th class="dt_actions"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>