<form method="GET" action="<?php echo current_url();?>">
	<?php if(!empty($firstLevelCategories)){?>			
		<div class="filters-card">
			<div class="filters-card-header border-bottom">
				<h6 class="mb-0">Catalog</h6>
			</div>
			<div class="filters-card-body card-shop-filters">
				<?php foreach ($firstLevelCategories as $firstLevelCategory) {?>											
					<?php $categoryUrl = (!empty($firstLevelCategory['category_special_url']))? $firstLevelCategory['category_special_url'] :'catalog/'.$firstLevelCategory['category_url'];?>
					<a href="<?php echo base_url($categoryUrl);?>" class="filter-link">
						<?php if($category['category_id'] == $firstLevelCategory['category_id'] || (int) $firstLevelCategory['category_id'] === (int) $category['category_parent']){?>
							<strong><?php echo $firstLevelCategory['category_title'];?></strong>
						<?php } else{?>
							<span><?php echo $firstLevelCategory['category_title'];?></span>
						<?php }?>
					</a>

					<?php 
						if(
							(int) $category['category_parent'] > 0 
							&& (int) $firstLevelCategory['category_id'] !== (int) $category['category_parent']
							|| 0 === (int) $category['category_parent']
							&& (int) $firstLevelCategory['category_id'] !== (int) $category['category_id']
						){continue;}
					?>
					<?php foreach($subCategories as $subCategory){?>
						<?php $subCategoryUrl = (!empty($subCategory['category_special_url']))? $subCategory['category_special_url'] : 'catalog/'.$subCategory['category_url'];?>
						<a href="<?php echo base_url($subCategoryUrl);?>" class="custom-padding-left-15 sub-filter-link">
							<?php if($category['category_id'] == $subCategory['category_id']){?>
								<strong><?php echo $subCategory['category_title'];?></strong>
							<?php } else{?>
								<span><?php echo $subCategory['category_title'];?></span>
							<?php }?>
						</a>
					<?php }?>
				<?php }?>
			</div>
		</div>
	<?php }?>
	<div class="filters-card">
		<div class="filters-card-header border-bottom">
			<h6 class="mb-0">Preț</h6>
		</div>
		<div class="filters-card-body card-shop-filters">
			<div class="filter-range">
				<input type="text" class="form-control" name="priceFrom" placeholder="De la" value="<?php echo $properties_selected['priceFrom'] ?? '';?>">
				<span>&mdash;</span>
				<input type="text" class="form-control" name="priceTo" placeholder="Pînă la" value="<?php echo $properties_selected['priceTo'] ?? '';?>">
			</div>
		</div>
	</div>
	<div class="filters-card">
		<div class="filters-card-footer">
			<a class="btn btn-light btn-sm" href="<?php echo current_url();?>">Anulează</a>
			<button class="btn btn-primary btn-sm" type="submit">Aplică</button>
		</div>
	</div>
</form>