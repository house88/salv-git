<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lauth
{
	var $password_salt = 'simpla';
	public function __construct(){
		$this->ci =& get_instance();
	}
	
	function set_sessiondata($user){
		$session_data = array(
			'id_user' => $user->id,
			'group_type' => $user->group_type,
			'group_price_variant' => $user->group_price_variant,
			'group_price_type' => $user->group_price_type,
			'logged_in' => true,
			'rights' => $user->user_rights,
			'signin_type' => $user->signin_type,
			'group_view_decimals' => $user->group_view_decimals,
			'group_view_stocks' => $user->group_view_stocks,
			'group_view_stocks_settings' => $user->group_view_stocks_settings,
			'group_view_suppliers' => $user->group_view_suppliers,
			'group_view_suppliers_settings' => $user->group_view_suppliers_settings,
			'group_view_cashback' => $user->group_view_cashback,
			'user_cashback_balance' => $user->user_cashback_balance,
			'callapi_sip_id' => $user->callapi_sip_id,
			'user_city' => $user->user_city
		);
		
		$this->ci->session->set_userdata($session_data);
	}
	
	function logged_in(){
		return (bool) $this->ci->session->userdata('logged_in');
	}
	
	function logout(){
		$this->ci->session->sess_destroy();
	}
	
	function group_view_decimals(){
		if($this->logged_in())
		{
			return (bool) $this->ci->session->userdata('group_view_decimals');
		}
		else
		{
			return FALSE;
		}
	}
	
	function group_view_cashback(){
		if($this->logged_in())
		{
			return (bool) $this->ci->session->userdata('group_view_cashback');
		}
		else
		{
			return TRUE;
		}
	}
	
	function group_has_cashback(){
		if($this->logged_in() && $this->group_view_cashback())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function user_cashback_balance(){
		if($this->logged_in() && $this->group_view_cashback())
		{
			return $this->user_data()->user_cashback_balance;
		}
		else
		{
			return 0;
		}
	}
	
	function user_data(){
		if($this->logged_in())
		{
			return $this->ci->auth_model->handler_get_by_id($this->ci->session->userdata('id_user'));
		}
		else
		{
			return FALSE;
		}
	}

	function group_view_stocks(){
		if($this->logged_in())
		{
			return (int) $this->ci->session->userdata('group_view_stocks');
		}
		else
		{
			return 0;
		}
	}

	function group_get_stock_setting($quantity = 0){
		$stock_settings = json_decode($this->ci->session->userdata('group_view_stocks_settings'));
		$options = array();
		foreach ($stock_settings as $stock_setting) {
			if($stock_setting->to < $quantity)
			{
				continue;
			}

			if($stock_setting->to >= $quantity)
			{
				$options = array('color' => $stock_setting->color, 'lines' => $stock_setting->lines);
				break;
			}
		}

		return $options;
	}

	function group_view_suppliers(){
		if($this->logged_in())
		{
			return (int) $this->ci->session->userdata('group_view_suppliers');
		}
		else
		{
			return 0;
		}
	}
	
	function get_user_data($id = 0){
		if($id)
		{
			return $this->ci->auth_model->handler_get_by_id($id);
		}
		else
		{
			return FALSE;
		}
	}

	function id_user(){
		if($this->logged_in())
		{
			return (int) $this->ci->session->userdata('id_user');
		}
		else
		{
			return 0;
		}
	}

	function callapi_sip_id(){
		if($this->logged_in())
		{
			return (int) $this->ci->session->userdata('callapi_sip_id');
		}
		else
		{
			return 0;
		}
	}

	function user_city(){
		if($this->logged_in())
		{
			return (int) $this->ci->session->userdata('user_city');
		}
		else
		{
			return 0;
		}
	}

	function user_group_type(){
		if($this->logged_in())
		{
			return (int) $this->ci->session->userdata('group_type');
		}
		else
		{
			return '';
		}
	}
	
	function is_admin(){
		if(in_array($this->ci->session->userdata('group_type'), array('admin', 'content_menedger', 'moderator')))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function signin_type(){
		if(!$this->logged_in())
		{
			return FALSE;
		}

		if($this->ci->session->userdata('signin_type') == 'site')
		{
			return 'site';
		}
		else
		{
			return 'social';
		}
	}

	function have_right($rights){
		if(!$this->logged_in()){
			return FALSE;
		}

		$all = TRUE;

		$search_rights = explode(',', $rights);
		$user_rights = $this->ci->session->userdata('rights');
		foreach($search_rights as $right){
			if(is_array($user_rights) && !in_array(trim($right), $this->ci->session->userdata('rights')))
			{
				$all = FALSE;
			}
		}

		return $all;
	}

	function have_right_or($rights){
		if(!$this->logged_in()){
			return FALSE;
		}
		
		$search_rights = explode(',', $rights);
		$user_rights = $this->ci->session->userdata('rights');
		foreach($search_rights as $right){
			if(is_array($user_rights) && in_array(trim($right), $this->ci->session->userdata('rights')))
			{
				return TRUE;
				exit();
			}

		}

		return FALSE;
	}

	function hash_password($password = ''){
		return md5($password.$this->password_salt);
	}
}

//region Lauth Helpers
function checkPermision($rights, $redirect_url = null){
	$_ci_instance =& get_instance();
	if(!$_ci_instance->lauth->have_right_or($rights)){
		$_ci_instance->session->set_flashdata('flash_message', json_encode(
			array(
				'message' => 'Ошибка: Нет прав!',
				'type' => 'error'
			)
		));

		if(null !== $redirect_url){
			redirect($redirect_url);
		}
	}

	return;
}

function checkPermisionAjax($rights){
	$_CI =& get_instance();

	if(!$_CI->lauth->have_right_or($rights)){
		jsonResponse('Ошибка: Нет прав!');
	}

	return;
}

function checkPermisionAjaxDT($rights){
	$_CI =& get_instance();

	if(!$_CI->lauth->have_right_or($rights)){
		jsonDTResponse('Ошибка: Нет прав!');
	}

	return;
}
//endregion Lauth Helpers