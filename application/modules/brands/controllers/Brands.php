<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Brands extends MX_Controller{
	function __construct(){
		parent::__construct();

        $this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->load->model("Brands_model", "brands");
	}

	function index(){
		return_404($this->data);
	}

	function _get_all($params = array()){
		return $this->brands->handler_get_all($params);
	}
}
