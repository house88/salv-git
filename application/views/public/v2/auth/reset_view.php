<div class="static-form-container mb-15">
    <div class="collapsed in">
        <h1>Изменить пароль</h1><br>
        <form id="static_reset_password_form">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="ca-icon ca-icon_password"></span>
                    </div>
                    <input type="password" name="password" class="form-control mb-0" placeholder="Новый пароль">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="ca-icon ca-icon_password"></span>
                    </div>
                    <input type="password" name="confirm_password" class="form-control mb-0" placeholder="Повторите пароль">
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger btn-lg btn-block mb-15">Сохранить</button>
                <input type="hidden" name="token" value="<?php echo $token;?>">
            </div>
            <div class="wr-help">
                <a href="<?php echo base_url();?>" class="pull-left">Авторизация</a>
                <a href="<?php echo base_url('register');?>" class="pull-right">Регистрация</a>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $('#static_reset_password_form').on('submit', function(e){
            var $form = $(this);
            var fdata = $form.serialize();
            
            $.ajax({
				type: 'POST',
				url: base_url+'auth/ajax_operations/reset_password',
				data: fdata,
				dataType: 'JSON',
                beforeSend: function(){
                    showLoader('body');
                },
				success: function(resp){
					if(resp.mess_type == 'success'){
                        window.location.href = base_url;
					} else{
						systemMessages(resp.message, resp.mess_type);
                        hideLoader('body');
					}
				}
			});
            return false;
        });
    });
</script>