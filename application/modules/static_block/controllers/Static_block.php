<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Static_block extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model("Static_block_model", "static_block");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		return_404($this->data);
	}

	function page(){
		$url = $this->uri->segment(2);
		if(empty($url)){
			return_404($this->data);
		}

		$this->data['static_page'] = $this->static_block->handler_get_by_alias($url);
		if(empty($this->data['static_page'])){
			return_404($this->data);
		}

		$block_breadcrumbs = json_decode($this->data['static_page']['block_breadcrumbs'], true);
		if(!empty($block_breadcrumbs)){
			foreach ($block_breadcrumbs as $block_bread) {
				$this->breadcrumbs[] = array(
					'title' => $block_bread['link_title'],
					'link' => base_url($block_bread['link_url'])
				);				
			}
		}

		$this->breadcrumbs[] = array(
			'title' => $this->data['static_page']['block_title'],
			'link' => base_url('spage/'.$url)
		);

		$this->data['stitle'] = $this->data['static_page']['block_title'].' - '.$this->data['stitle'];
		$this->data['skeywords'] = $this->data['static_page']['block_title'].' '.$this->data['skeywords'];
		$this->data['sdescription'] = $this->data['static_page']['block_title'].' - '.$this->data['sdescription'];

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		if((int) $this->data['static_page']['is_landing'] === 1){
			$this->data['main_content'] = 'static_block/full_page_view';
			$this->load->view($this->theme->public_view('landing_view'), $this->data);
		} else{
			$this->data['main_content'] = 'static_block/page_view';
			$this->load->view($this->theme->public_view('shop_view'), $this->data);
		}
		
	}

	function landing(){
		$url = $this->uri->segment(1);
		if(empty($url)){
			return_404($this->data);
		}

		$this->data['static_page'] = $this->static_block->handler_get_by_alias($url);
		if(empty($this->data['static_page'])){
			return_404($this->data);
		}

		$block_breadcrumbs = json_decode($this->data['static_page']['block_breadcrumbs'], true);
		if(!empty($block_breadcrumbs)){
			foreach ($block_breadcrumbs as $block_bread) {
				$this->breadcrumbs[] = array(
					'title' => $block_bread['link_title'],
					'link' => base_url($block_bread['link_url'])
				);				
			}
		}

		$this->breadcrumbs[] = array(
			'title' => $this->data['static_page']['block_title'],
			'link' => base_url('spage/'.$url)
		);

		$this->data['stitle'] = $this->data['static_page']['block_title'].' - '.$this->data['stitle'];
		$this->data['skeywords'] = $this->data['static_page']['block_title'].' '.$this->data['skeywords'];
		$this->data['sdescription'] = $this->data['static_page']['block_title'].' - '.$this->data['sdescription'];

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		if((int) $this->data['static_page']['is_landing'] === 1){
			$this->data['main_content'] = 'static_block/full_page_view';
			$this->load->view($this->theme->public_view('landing_view'), $this->data);
		} else{
			$this->data['main_content'] = 'static_block/page_view';
			$this->load->view($this->theme->public_view('shop_view'), $this->data);
		}
		
	}

	function _view($params = array()){
		extract($params);

		if(!isset($menu)){
			return;
		}

		$this->data['static_block'] = $this->static_block->handler_get_by_alias($menu);
		return $this->load->view($this->theme->public_view('static_block/item_view'), $this->data, true);
	}

	function _get_all($params = array()){
		return $this->static_block->handler_get_all($params);
	}

	function _get($alias){
		return $this->static_block->handler_get_by_alias($alias);
	}
}
