<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		
		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/admin/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'settings';

		$this->data = array();

		$this->load->model("categories/Categories_model", "categories");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		$this->settings();
	}	
	
	function settings(){
		$this->data['page_header'] = 'Настройки';
		$this->data['categories'] = Modules::run('categories/_get_tree_tokens');
		$this->data['home_settings'] = arrayByKey($this->settings->get_settings_home(), 'setting_alias');
		
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'settings_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'settings_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'save_settings':
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('settings[]', 'Настройки', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$settings = $this->input->post('settings');
				$update = array();
				foreach($settings as $key => $setting){
					$update[] = array(
						'setting_alias' => $key,
						'setting_value' => $setting
					);
				}
				
				$this->settings->update_settings($update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'save_recomended_block':
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$recomended_blocks = $this->input->post('recomended_blocks');
				$blocks_data = array_filter(array_map(function($recomended_block){
					if(!empty($recomended_block['block_name']) && !empty($recomended_block['block_categories'])){
						return $recomended_block;
					}
				}, $recomended_blocks));

				if(empty($blocks_data)){
					$blocks_data = '';
				}

				$setting_data = array(
					'limit' => (int) $this->input->post('recomended_blocks_limit'),
					'blocks' => $blocks_data
				);

				$block_active = 0;
				if($this->input->post('recomended_blocks_active')){
					$block_active = 1;
				}

				$update = array(
					'setting_data' => json_encode($setting_data),
					'setting_active' => $block_active
				);
				
				$this->settings->update_setting_home('recomended_products', $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'save_popular_block':
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$block_data = array(
					'block_name' => $this->input->post('block_name', true),
					'limit' => (int)$this->input->post('popular_block_limit')
				);

				$block_active = 0;
				if($this->input->post('popular_block_active')){
					$block_active = 1;
				}

				$update = array(
					'setting_data' => json_encode($block_data),
					'setting_active' => $block_active
				);
				
				$this->settings->update_setting_home('popular_products', $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'save_newest_block':
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$block_data = array(
					'block_name' => $this->input->post('block_name', true),
					'limit' => (int)$this->input->post('newest_block_limit')
				);

				$block_active = 0;
				if($this->input->post('newest_block_active')){
					$block_active = 1;
				}

				$update = array(
					'setting_data' => json_encode($block_data),
					'setting_active' => $block_active
				);
				
				$this->settings->update_setting_home('newest_products', $update);
				jsonResponse('Сохранено.', 'success');
			break;
		}
	}
}
