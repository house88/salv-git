<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['admin'] 	= "Admin";
$route['admin/(:any)'] 	= "admin/$1";
