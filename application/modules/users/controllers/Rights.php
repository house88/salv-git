<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rights extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/users/rights/';
		$this->active_menu = 'users';
		$this->active_submenu = 'rights';

        $this->data = array();
        
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("User_model", 'user_model');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        checkPermision('manage_rights');

		$this->data['page_header'] = 'Права пользователей';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
            case 'list':
                checkPermisionAjaxDT('manage_rights');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength', true),
					'start' => (int) $this->input->post('iDisplayStart', true)
				);
		
				$records = $this->user_model->handler_get_rights_all($params);
                $records_total = $this->user_model->handler_get_rights_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
                        return array(
							'dt_id'		=>  $record['id_right'],
                            'dt_name'	=>  $record['right_name'],
                            'dt_alias'	=>  $record['right_alias']
						);
					}, $records)
				);

				jsonResponse('', 'success', $output);
			break;
		}
    }
}
