<div class="modal fade modal-open-aside" id="dtFilter" data-backdrop="true" data-class="modal-open-aside" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-right modal-sm">
		<div class="modal-content rounded-0 border-0 d-flex flex-column h-100">
			<div class="modal-header">
				<h4 class="modal-title">Фильтры</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body dt-filter-form custom-width-300 overflow-auto">
				<?php $this->load->view($dt_filter);?>
			</div>
		</div>
	</div>
</div>