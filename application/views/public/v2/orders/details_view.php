<article class="p-15">
    <?php if($order['order_user_viewed'] <= 1){?>
        <p class="fs-18">Спасибо за ваш заказ - в ближайшее время с вами свяжутся наши менеджеры для подтверждения заказа.</p>    
    <?php }?>
    <h3 class="fs-16">Заказ номер <?php echo orderNumber($order['id_order']);?></h3>
    <div class="dashboard-order-content">
        <div class="products-order">
            <?php 
                $total_cashback = $this->lauth->user_cashback_balance();
                $total_bonus = 0;
                $can_apply_bonus = false;
            ?>
            <?php $ordered_items = json_decode($order['order_ordered_items'], true);?>
            <?php foreach($ordered_items as $ordered_item){?>
                <div class="row row-eq-height cart_product-wr">
                    <div class="col-xs-12 col-sm-2 col-md-1">
                        <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$ordered_item['item_photo']));?>" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-10 col-md-7">
                        <a class="fs-13" href="<?php echo base_url('products/'.$ordered_item['item_url']);?>"><?php echo clean_output($ordered_item['item_title']);?></a>
                    </div>
                    <div class="col-sm-12 visible-sm hidden-md mb-15"></div>
                    <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                        <div class="w-45pr pull-left h-30 lh-30 text-right">
                            <?php echo $ordered_item['item_quantity'];?>
                        </div>
                        <div class="w-10pr pull-left h-30 lh-30">x</div>
                        <div class="w-45pr pull-left h-30 lh-30 text-left">
                            <strong><?php echo numberFormat($ordered_item['item_price'], $order['order_price_decimals']);?> <?php echo $ordered_item['item_currency'];?></strong>
                        </div>
                    </div>
                    <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                </div>
                <?php 
                    if (isset($ordered_item['item_cashback_price'])) {
                        $total_cashback += $ordered_item['item_cashback_price'] * $ordered_item['item_quantity'];
                        if($ordered_item['item_cashback_price'] == 0 && $this->lauth->group_view_cashback()){
                            $total_bonus = $ordered_item['item_price'] * $ordered_item['item_quantity'];
                            $can_apply_bonus = true;
                        }
                    }
                ?>
            <?php }?>
            <?php $cart_cashback = $total_cashback - $this->lauth->user_cashback_balance();?>
        </div>
        <div class="row order-footer__blocks-order">
            <div class="order-2 col-sm-7">
                <div class="row">
                    <?php $order_status = get_order_status($order['order_status']);?>
                    <div class="col-xs-12 col-sm-4 col-md-3">Состояние заказа</div>
                    <div class="col-xs-12 col-sm-8 col-md-9"><div class="label label-<?php echo $order_status['color_class'];?>"><?php echo $order_status['title'];?></div></div>
                </div>
                            
                <?php if((int) $order['order_payment_card'] === 1 && $status_payment_procesing === true){?>
                    <div class="row">
                        <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                        <div class="col-xs-12 col-sm-4 col-md-3">Состояние оплаты</div>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                            <div class="label label-warning">Оплата оформлена. Ждет подтверждения.</div>
                        </div>
                    </div>
                <?php }?>
                <?php if((int) $order['order_payment_card'] === 1 && $order['order_payment_card_status'] === 'paid' && !empty($transaction_details = json_decode($order['order_payment_card_details'], true))){?>
                    <div class="row">
                        <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                        <div class="col-xs-12 col-sm-4 col-md-3">Состояние оплаты</div>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                            <div class="label label-success">Оплачено <?php echo numberFormat($transaction_details['context']['transaction']['Data'][0]['Amount'] / 100, 2) . ' ' . $order['order_currency'];?></div> 
                            <i class="fa fa-calendar"></i> <?php echo date('d.m.Y H:i:s',strtotime($transaction_details['context']['transaction']['Data'][0]['Confirmed']));?> 
                        </div>
                    </div>
                <?php }?>

                <div class="row">
                    <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                    <div class="col-xs-12 col-sm-4 col-md-3">Дата</div>
                    <div class="col-xs-12 col-sm-8 col-md-9"><i class="fa fa-calendar"></i> <?php echo formatDate($order['order_created'], 'd.m.Y H:i');?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                    <div class="col-xs-12 col-sm-4 col-md-3">Покупатель</div>
                    <div class="col-xs-12 col-sm-8 col-md-9"><i class="ca-icon ca-icon_user"></i> <?php echo $order['order_user_name'];?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                    <div class="col-xs-12 col-sm-4 col-md-3">Email</div>
                    <div class="col-xs-12 col-sm-8 col-md-9"><i class="ca-icon ca-icon_envelope"></i> <?php echo $order['order_user_email'];?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                    <div class="col-xs-12 col-sm-4 col-md-3">Телефон</div>
                    <div class="col-xs-12 col-sm-8 col-md-9"><i class="ca-icon ca-icon_phone"></i> <?php echo $order['order_user_phone'];?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                    <div class="col-xs-12 col-sm-4 col-md-3">Адрес доставки</div>
                    <div class="col-xs-12 col-sm-8 col-md-9"><i class="fa fa-map-marker"></i> <?php echo $order['order_user_address'];?></div>
                </div>
                <?php if(!empty($delivery_option)){?>
                    <div class="row">
                        <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                        <div class="col-xs-12 col-sm-4 col-md-3">Метод доставки</div>
                        <div class="col-xs-12 col-sm-8 col-md-9"><i class="ca-icon ca-icon_truck"></i> <?php echo $delivery_option['delivery_title'];?></div>
                    </div>
                <?php }?>
                <?php if(!empty($payment_option)){?>
                    <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3">Способ оплаты</div>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                            <?php echo $payment_option['payment_title'];?>
                        </div>
                    </div>
                <?php }?>
                <?php if(!empty($order['order_user_comment'])){?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-3">Пояснение</div>
                        <div class="col-xs-12 col-sm-8 col-md-9"><i class="fa fa-comment"></i> <?php echo $order['order_user_comment'];?></div>
                    </div>
                <?php }?>
            </div>
            <div class="order-1 col-sm-5">
                <div class="row">
                    <?php if($this->lauth->group_view_cashback() && $cart_cashback > 0){?>
                        <div class="col-xs-12 text-right">
                            <div class="order-footer">
                                <span class="order-footer__text cashback-text"><sup>*</sup>Подарок на следующую покупку:</span>
                                <span class="order-footer__value">
                                    <span class="value cashback-text">
                                        + <?php echo numberFormat($cart_cashback, $this->lauth->group_view_decimals());?> лей
                                    </span>
                                </span>
                            </div>                      
                        </div>                        
                    <?php }?>
                    <div class="col-xs-12 text-right">
                        <div class="order-footer">
                            <span class="order-footer__text">Доставка:</span>
                            <span class="order-footer__value">
                                <span class="value">
                                    <strong>
                                        <?php echo numberFormat($order['order_delivery_price'], $order['order_price_decimals']);?>
                                        <span class="currency"><?php echo $order['order_currency'];?></span>
                                    </strong>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-12 text-right">
                        <div class="order-footer">
                            <span class="order-footer__text">Сумма заказа:</span>
                            <span class="order-footer__value">
                                <span class="value">
                                    <strong>
                                        <?php echo numberFormat($order['order_price_final'] + $order['order_delivery_price'], $order['order_price_decimals']);?>
                                        <span class="currency"><?php echo $order['order_currency'];?></span>
                                    </strong>
                                </span>
                            </span>
                        </div>
                    </div>
                    <?php if($order['order_cashback'] > 0){?>
                        <div class="col-xs-12 text-right">
                            <div class="order-footer">
                                <span class="order-footer__text">Бонусные баллы:</span>
                                <span class="order-footer__value">
                                    <span class="value">
                                        <strong>
                                            - <?php echo numberFormat($order['order_cashback'], $order['order_price_decimals']);?>
                                            <span class="currency"><?php echo $order['order_currency'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                    <?php }?>
                    <div class="col-xs-12 text-right">
                        <div class="order-footer order-footer__border-top">
                            <span class="order-footer__text">Итого к оплате:</span>
                            <span class="order-footer__value">
                                <span class="value">
                                    <strong>
                                        <?php echo numberFormat($order['order_price_final'] + $order['order_delivery_price'] - $order['order_cashback'], $order['order_price_decimals']);?>
                                        <span class="currency"><?php echo $order['order_currency'];?></span>
                                    </strong>
                                </span>
                            </span>
                        </div>
                    </div>
                    <?php if($this->lauth->group_view_cashback() && $cart_cashback > 0 && in_array($order['order_status'], array('1','2','3'))){?>
                        <?php if(!$this->lauth->group_has_cashback()){?>
                            <div class="col-xs-12 text-right">
                                <span class="order-footer__info text-danger"><sup>*</sup>Чтобы воспользоваться остатком бонусных балов при следуюших покупках просим вас <a href="#" class="call-function" data-callback="show_auth_popup" data-target="#auth-modal" data-action=".login-form-link">авторизоваться</a> или <a href="#" class="call-function" data-callback="show_auth_popup" data-target="#auth-modal" data-action=".register-form-link">зарегистрироваться</a>!</span>
                            </div>                            
                        <?php } else{?>
                            <div class="col-xs-12 text-right">
                                <span class="order-footer__info text-primary"><sup>*</sup>Остаток бонусных балов попадет в личный кабинет и будет использован при следующих заказах.</span>
                            </div>
                        <?php }?>
                    <?php }?>
                    
                    <?php if((int) $order['order_payment_card'] === 1 && $show_payment_button === true){?>
                        <div class="col-xs-12 col-sm-7 col-sm-offset-5 text-right">
                            <p class="order-footer__info text-danger pt-30">Перед тем как оплатить, просим вас дождаться звонка от менеджера.</p>
                            <a href="#" class="btn btn-danger btn-block mt-15 call-function" data-callback="order_card_pay" data-order="<?php echo $order['order_hash'];?>">Оплатить заказ</a>
                            <div id="js-payment-form-container" style="display: none;"></div>
                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</article>