<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model('Cart_model', 'cart');
		$this->load->model('items/Items_Model', 'items');
		$this->load->model("delivery/Delivery_model", "delivery");
		$this->load->model("payment/Payment_model", "payment");
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		$this->breadcrumbs[] = array(
			'title' => 'Корзина',
			'link' => base_url('cart')
		);

		$basket_hash = get_cookie('_cart_products');
		if(!empty($basket_hash)){
			$basket = $this->cart->handler_get($basket_hash);
			if(!empty($basket)){
				$items = json_decode($basket['basket_data'], true);
				$this->data['count_oversize_items'] = 0;

				if(!empty($items)){
					$this->data['items'] = Modules::run('items/_get_all', array('items_list' => array_keys($items)));
					
					$this->load->model('categories/Categories_Model', 'categories');
					$basket_items_categories_ids = array_column($this->data['items'], 'id_category', 'id_category');
					$basket_items_categories = array_column(Modules::run('categories/_get_all', array('id_category' => $basket_items_categories_ids)), null, 'category_id');
					
					$total_cashback = $this->lauth->user_cashback_balance();
					$total_basket_price = 0;
					$total_bonus = 0;
					foreach($this->data['items'] as $key => $item){
						$items_quantity = (int) $items[$item['id_item']]['quantity'];

						$this->data['items'][$key]['item_cart_qty'] = $items_quantity;
						if($basket_items_categories[$item['id_category']]['category_is_oversized']){
							$this->data['count_oversize_items'] += $items_quantity;
						}

						$group_view_cashback = $this->lauth->group_view_cashback();
						if($group_view_cashback){
							$total_cashback += $item['item_cashback_price'] * $items_quantity;
						}

						$price = getPrice($item);
						if($item['item_cashback_price'] == 0 && $group_view_cashback){
                            $total_bonus += $price['display_price'] * $items_quantity;
						}
						
						$basket_currency = $price['currency_symbol'];
						$total_basket_price += $price['display_price'] * $items_quantity;
					}

					$total_bonus_price = ($total_cashback <= $total_bonus)?$total_cashback:$total_bonus;
					$total_basket_price -= $total_bonus_price;

					$this->data['delivery_options'] = $this->delivery->handler_get_all(array('active' => 1));
					$this->data['payment_options'] = $this->payment->handler_get_all(array('active' => 1));
					
					if($this->lauth->logged_in()){
						$this->data['user_info'] = $this->lauth->user_data();
					}
				}

				$this->data['basket'] = $basket;
			}
		}

		$this->data['payment_delivery_relation'] = array();
		$this->data['payment_delivery_methods'] = array();
		$this->data['delivery_payment_methods'] = array();
		$payment_delivery_relation = Modules::run('payment/_get_delivery_relations');

		if(!empty($payment_delivery_relation)){
			foreach ($payment_delivery_relation as $relation) {
				$relation_key = "{$relation['id_payment']}_{$relation['id_delivery']}";
				$this->data['payment_delivery_relation'][$relation_key] = $relation['relation_text'];
				
				$this->data['payment_delivery_methods'][$relation['id_payment']][] = $relation['id_delivery'];
				$this->data['delivery_payment_methods'][$relation['id_delivery']][] = $relation['id_payment'];
			}
		}
		$this->data['payment_delivery_relation'] = json_encode($this->data['payment_delivery_relation']);
		$this->data['payment_delivery_methods'] = $this->data['payment_delivery_methods'];
		$this->data['delivery_payment_methods'] = $this->data['delivery_payment_methods'];

		// dump($this->data);
		// exit;
		
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'cart/index_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('id_item', 'Produs', 'required|xss_clean');
				$this->form_validation->set_rules('quantity', 'Cantitatea', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$basket_hash = get_cookie('_cart_products');
				$item = (int) $this->input->post('id_item');				
				$quantity = (int) $this->input->post('quantity');
				$product = $this->items->handler_get($item);
				if(empty($product)){
					jsonResponse('Produsul nu exista.');
				}

				$price = getPrice($product);
				if($price['display_price'] <= 0){
					jsonResponse('Produsul nu poate fi adaugat in cos.');
				}

				if(!empty($basket_hash)){
					$basket = $this->cart->handler_get($basket_hash);
					if(empty($basket)){
						$basket_hash = md5(uniqid('salv6asket').session_id());
						$insert = array(
							'basket_hash' => $basket_hash,
							'basket_data' => json_encode(array($item => array('item' => $item, 'quantity' => $quantity)))
						);
						$basket = $this->cart->handler_insert($insert);
						set_cookie('_cart_products', $basket_hash, 604800);
					} else{
						$items = json_decode($basket['basket_data'], true);
						if(!array_key_exists($item, $items)){
							$items[$item] = array(
								'item' => $item, 
								'quantity' => $quantity
							);
						} else{
							$items[$item]['quantity'] += $quantity;
						}
						
						$update = array(
							'basket_data' => json_encode($items)
						);
						$this->cart->handler_update($basket_hash, $update);
					}
				} else{
					$basket_hash = md5(uniqid('salv6asket').session_id());
					$insert = array(
						'basket_hash' => $basket_hash,
						'basket_data' => json_encode(array($item => array('item' => $item, 'quantity' => $quantity)))
					);
					$basket = $this->cart->handler_insert($insert);
					set_cookie('_cart_products', $basket_hash, 604800);
				}

				$basket = $this->cart->handler_get($basket_hash);
				if(empty($basket_hash) || empty($basket = $this->cart->handler_get($basket_hash))){
					jsonResponse('Datele nu sunt corecte.');
				}

				$cartData = $this->_getSmallCartContent($basket);
				jsonResponse('Produsul a fost adaugat in cos.', 'success', $cartData);
			break;
			case 'change_quantity':
				$this->form_validation->set_rules('item', 'Produs', 'required|xss_clean');
				$this->form_validation->set_rules('quantity', 'Cantitate', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$basket = [];
				$basket_hash = get_cookie('_cart_products');
				$item = (int)$this->input->post('item');
				$quantity = (int)$this->input->post('quantity');
				if(!empty($basket_hash)){
					$basket = $this->cart->handler_get($basket_hash);
					if(!empty($basket)){
						$items = json_decode($basket['basket_data'], true);
						if(array_key_exists($item, $items)){
							$items[$item]['quantity'] = $quantity;

							$update = array(
								'basket_data' => json_encode($items)
							);
							$this->cart->handler_update($basket_hash, $update);
							$basket['basket_data'] = $update['basket_data'];
						}
					}
				}

				$cartData = $this->_getSmallCartContent($basket);
				jsonResponse('', 'success', $cartData);
			break;
			case 'delete':
				$this->form_validation->set_rules('item', 'Produs', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$basket = [];
				$basket_hash = get_cookie('_cart_products');
				$item = (int)$this->input->post('item');
				if(!empty($basket_hash)){
					$basket = $this->cart->handler_get($basket_hash);
					if(!empty($basket)){
						$items = json_decode($basket['basket_data'], true);
						if(array_key_exists($item, $items)){
							unset($items[$item]);

							if(empty($items)){
								$this->cart->handler_delete($basket_hash);
								delete_cookie('_cart_products');
								$basket = [];
							} else{
								$update = array(
									'basket_data' => json_encode($items)
								);
								$this->cart->handler_update($basket_hash, $update);
								$basket['basket_data'] = $update['basket_data'];
							}
						}
					}
				}

				$cartData = $this->_getSmallCartContent($basket);
				jsonResponse('', 'success', $cartData);
			break;
			case 'update':
				$basket_hash = get_cookie('_cart_products');
				if(empty($basket_hash) || empty($basket = $this->cart->handler_get($basket_hash))){
					jsonResponse('Datele nu sunt corecte.');
				}

				$cartData = $this->_getSmallCartContent($basket);
				jsonResponse('', 'success', $cartData);
			break;
		}
	}

	function _get(){
		$result = [];
		$basket = [];
		$basket_hash = get_cookie('_cart_products');
		if(!empty($basket_hash)){
			$basket = $this->cart->handler_get($basket_hash);
		}

		$result = $this->_getSmallCartContent($basket);

		return $result;
	}

	private function _getSmallCartContent($basket){
		$cart_count = 0;
		$cart_price = 0;
		$cart_currency = '';
		if(!empty($basket)){
			$items = json_decode($basket['basket_data'], true);
			$items_list = array_keys($items);
			if(!empty($items_list)){
				$this->data['items'] = $this->items->handler_get_all(['items_list' => $items_list]);
				foreach($this->data['items'] as $key => $item){
					$cart_count += (int) $items[$item['id_item']]['quantity'];
	
					$this->data['items'][$key]['item_cart_qty'] = $items[$item['id_item']]['quantity'];
					$price = getPrice($item);
					$cart_price += $price['display_price'] * ((int) $items[$item['id_item']]['quantity']);
					$cart_currency = $price['currency_symbol'];
				}
			}
		}

		$this->data['cartAmount'] = $cart_price.' '.$cart_currency;
		$this->data['cartProductsCounter'] = $cart_count;

		return [
			'cartProductsContent' => $this->load->view($this->theme->public_view('cart/small_view'), $this->data, true),
			'cartProductsCounter' => $cart_count
		];
	}
}
