<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/delivery/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'delivery';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Delivery_model", "delivery");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_delivery', '/admin');

		$this->data['page_header'] = 'Доставка';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'delivery_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_delivery');

				$this->data['currencies'] = Modules::run('currency/_get_all', array('active' => 1));
				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_delivery');

				$id_delivery = (int)$this->uri->segment(5);
				$this->data['delivery'] = $this->delivery->handler_get($id_delivery);
				if(empty($this->data['delivery'])){
					jsonResponse('Способ доставки не найден.');
				}

				$this->data['payments_relations'] = $this->delivery->handler_get_payments_relation($id_delivery);
				$this->data['payments_selected'] = array_column($this->data['payments_relations'], 'id_payment');

				$this->data['currencies'] = Modules::run('currency/_get_all', array('active' => 1));
				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_delivery');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext', 'Краткое описание', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description', 'Текст', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$price_options = $this->input->post('price');
				$price_options_free = $this->input->post('free_price');
				$price_options_for_oversize = $this->input->post('price_for_oversize');
				$currencies = Modules::run('currency/_get_all', array('active' => 1));
				$delivery_price_options = array();
				foreach ($currencies as $currency) {
					$delivery_price_options[$currency['currency_code']] = array(
						'price' => numberFormat((float)$price_options[$currency['currency_code']], true),
						'price_free' => (int)$price_options_free[$currency['currency_code']],
						'price_for_oversize' => numberFormat((float)$price_options_for_oversize[$currency['currency_code']], true),
						'currency' => $currency['currency_symbol']
					);
				}
				
				$insert = array(
					'delivery_title' => $this->input->post('title'),
					'delivery_price_options' => json_encode($delivery_price_options),
					'delivery_sdescription' => $this->input->post('stext'),
					'delivery_description' => $this->input->post('description')
				);

				if($this->input->post('active')){
					$insert['delivery_active'] = 1;
				}

				$id_delivery = $this->delivery->handler_insert($insert);

				$payment_relation = array();
				$payments = $this->input->post('payments');
				if(is_array($payments) && !empty($payments)){
					foreach ($payments as $payment_id => $payment_info) {
						$delivery_relation[] = array(
							'id_delivery' => (int) $id_delivery,
							'id_payment' => $payment_id,
							'relation_text' => $payment_info['text']
						);
					}
				}

				if(!empty($payment_relation)){
					$this->delivery->handler_insert_payments_relation($payment_relation);
				}

				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_delivery');

                $this->form_validation->set_rules('delivery', 'Способ доставки', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext', 'Краткое описание', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description', 'Текст', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_delivery = (int)$this->input->post('delivery');
				$price_options = $this->input->post('price');
				$price_options_free = $this->input->post('free_price');
				$price_options_for_oversize = $this->input->post('price_for_oversize');
				$currencies = Modules::run('currency/_get_all', array('active' => 1));
				$delivery_price_options = array();
				foreach ($currencies as $currency) {
					$delivery_price_options[$currency['currency_code']] = array(
						'price' => numberFormat((float)$price_options[$currency['currency_code']], true),
						'price_free' => (int)$price_options_free[$currency['currency_code']],
						'price_for_oversize' => numberFormat((float)$price_options_for_oversize[$currency['currency_code']], true),
						'currency' => $currency['currency_symbol']
					);
				}

				$update = array(
					'delivery_title' => $this->input->post('title'),
					'delivery_price_options' => json_encode($delivery_price_options),
					'delivery_sdescription' => $this->input->post('stext'),
					'delivery_description' => $this->input->post('description'),
					'delivery_active' => 0
				);

				if($this->input->post('active')){
					$update['delivery_active'] = 1;
				}

				$this->delivery->handler_update($id_delivery, $update);

				$payment_relation = array();
				$payments = $this->input->post('payments');
				if(is_array($payments) && !empty($payments)){
					foreach ($payments as $payment_id => $payment_info) {
						$payment_relation[] = array(
							'id_delivery' => (int) $id_delivery,
							'id_payment' => $payment_id,
							'relation_text' => $payment_info['text']
						);
					}
				}

				$this->delivery->handler_delete_payments_relation($id_delivery);
				if(!empty($payment_relation)){
					$this->delivery->handler_insert_payments_relation($payment_relation);
				}

				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_delivery');

                $this->form_validation->set_rules('delivery', 'Способ доставки', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_delivery = (int)$this->input->post('delivery');
				$delivery = $this->delivery->handler_get($id_delivery);
				if(empty($delivery)){
					jsonResponse('Способ доставки не существует.');
				}

				$this->delivery->handler_delete($id_delivery);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_delivery');

                $this->form_validation->set_rules('delivery', 'Способ доставки', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_delivery = (int)$this->input->post('delivery');
				$delivery = $this->delivery->handler_get($id_delivery);
				if(empty($delivery)){
					jsonResponse('Способ доставки не существует.');
				}

				if($delivery['delivery_active'] == 1){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->delivery->handler_update($id_delivery, array('delivery_active' => $status));

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_delivery');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'id_delivery-asc' )
				);

				$records = $this->delivery->handler_get_all($params);
        		$records_total = $this->delivery->handler_get_count($params);
				
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_id'			=>  $record['id_delivery'],
							'dt_name'		=>  $record['delivery_title'],
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['delivery_active'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-delivery="'.$record['id_delivery'].'"></a>',
							'dt_actions'	=>  '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/delivery/popup/edit/' . $record['id_delivery']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить метод доставки?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-delivery="'.$record['id_delivery'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}