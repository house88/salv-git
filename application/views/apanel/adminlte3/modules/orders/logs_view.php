<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Заказ <?php echo orderNumber($order['id_order']);?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body direct-chat direct-chat-primary">
            <?php if(!empty($order_logs)){?>
                <div class="direct-chat-messages custom-padding-left-0">
                    <?php foreach($order_logs as $log){?>
                        <div class="direct-chat-msg">
                            <div class="direct-chat-infos d-flex justify-content-between">
                                <span class="direct-chat-name"><?php echo $log['name'];?></span>
                                <span class="direct-chat-timestamp text-right"><?php echo $log['date'];?></span>
                            </div>
                            <!-- /.direct-chat-infos -->
                            <img class="direct-chat-img" src="<?php echo file_modification_time($this->theme->apanel_assets('img/avatar3.png')); ?>" alt="Менеджер">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                                <?php echo is_array($log['notes']) ? implode('<br>', $log['notes']) : $log['notes'];?>
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                        <!-- /.direct-chat-msg -->
                    <?php }?>
                </div>
            <?php } else{?>
                <div class="callout callout-info">
                    <h5>Нет данных</h5>

                    <p>Для данного заказа нет логов.</p>
                </div>
            <?php }?>
        </div>
        <div class="modal-footer d-flex justify-content-end">
            <div class="d-flex">
                <button type="button" class="btn btn-default btn-flat btn-sm" data-dismiss="modal" aria-label="Close">
                    Закрыть
                </button>
            </div>
        </div>
    </div>
</div>