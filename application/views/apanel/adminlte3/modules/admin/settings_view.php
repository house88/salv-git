<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-xs-12">
				<h1 class="m-0 text-dark"><?php echo $page_header;?></h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-lg">
				<div class="card shadow-lg bg-white card-primary card-outline card-outline-tabs">
					<div class="card-header p-0 border-0">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="pill" href="#custom-tabs-home_page" role="tab" aria-controls="custom-tabs-home_page" aria-selected="true">Главная страница</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#custom-tabs-settings" role="tab" aria-controls="custom-tabs-settings" aria-selected="false">Другие настройки</a>
							</li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="custom-tabs-three-tabContent">
							<div class="tab-pane fade show active" id="custom-tabs-home_page" role="tabpanel" aria-labelledby="home_page-tab">
								<?php $recommended_blocks = json_decode($home_settings['recomended_products']['setting_data']);?>
								<div class="row">
									<div class="col-12 col-md-6">
										<?php $popular_block = json_decode($home_settings['popular_products']['setting_data']);?>
										<form id="popular_block">
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Блок Популярные товары</h3>
												</div>
												<!-- /.card-header -->
												<div class="card-body" style="display: block;">
													<div class="form-group">
														<label>Название блока</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. Популярные товары" name="block_name" value="<?php echo $popular_block->block_name;?>">
													</div>
													<div class="form-group">
														<label>Количество товаров в блоке</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. 4" name="popular_block_limit" value="<?php echo $popular_block->limit;?>">
													</div>
												</div>
												<!-- /.card-body -->
												<div class="card-footer d-flex justify-content-between">
													<div class="icheck-primary">
														<input name="popular_block_active" type="checkbox" id="js-checkbox-popular-block-active" <?php echo set_checkbox('popular_block_active', 1, $home_settings['popular_products']['setting_active'] == 1);?>>
														<label for="js-checkbox-popular-block-active">
															Активный
														</label>
													</div>
													<button class="btn btn-success btn-flat btn-sm float-right call-function" data-callback="save_popular_block"><i class="fad fa-check"></i> Сохранить</button>
												</div>
											</div>											
										</form>
									</div>
									
									<div class="col-12 col-md-6">
										<?php $newest_block = json_decode($home_settings['newest_products']['setting_data']);?>
										<form id="newest_block">
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Блок Новинки</h3>
												</div>
												<!-- /.card-header -->
												<div class="card-body" style="display: block;">
													<div class="form-group">
														<label>Название блока</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. Популярные товары" name="block_name" value="<?php echo $newest_block->block_name;?>">
													</div>
													<div class="form-group">
														<label>Количество товаров в блоке</label>
														<input class="form-control form-control-sm rounded-0" placeholder="ex. 4" name="newest_block_limit" value="<?php echo $newest_block->limit;?>">
													</div>
												</div>
												<!-- /.card-body -->
												<div class="card-footer d-flex justify-content-between">
													<div class="icheck-primary">
														<input name="newest_block_active" type="checkbox" id="js-checkbox-newest-block-active" <?php echo set_checkbox('newest_block_active', 1, $home_settings['newest_products']['setting_active'] == 1);?>>
														<label for="js-checkbox-newest-block-active">
															Активный
														</label>
													</div>
													<button class="btn btn-success btn-flat btn-sm float-right call-function" data-callback="save_newest_block"><i class="fad fa-check"></i> Сохранить</button>
												</div>
											</div>
										</form>	
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="custom-tabs-settings" role="tabpanel" aria-labelledby="settings-tab">
								<div class="row">
									<div class="col-12">
										<form class="form-horizontal" id="settings_form">
											<div class="card card-primary">
												<div class="card-body">
													<?php foreach($settings as $setting){?>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label"><?php echo $setting['setting_title'];?></label>
															<div class="col-sm-8">
																<input type="text" class="form-control form-control-sm rounded-0" name="settings[<?php echo $setting['setting_alias'];?>]" value="<?php echo $setting['setting_value'];?>">
															</div>
														</div>
													<?php }?>
												</div>
												<!-- /.card-body -->
												<div class="card-footer d-flex justify-content-end">
													<button class="btn btn-success btn-flat btn-sm" type="submit"><i class="fad fa-check"></i> Сохранить</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>