<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_model extends CI_Model{
	var $basket = "basket";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->basket, $data);
		return $this->db->insert_id();
	}

	function handler_update($basket_hash, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('basket_hash', $basket_hash);
		$this->db->update($this->basket, $data);
	}

	function handler_delete($basket_hash){
		$this->db->where('basket_hash', $basket_hash);
		$this->db->delete($this->basket);
	}

	function handler_get($basket_hash){
		$this->db->where('basket_hash', $basket_hash);
		return $this->db->get($this->basket)->row_array();
	}
}
?>
