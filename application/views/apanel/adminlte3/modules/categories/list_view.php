<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header">
					<h3 class="card-title">
						<?php echo $page_header;?>
					</h3>
					<div class="card-tools">
						<a class="btn btn-tool call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/categories/popup/add');?>" data-toggle="tooltip" title="Добавить категорию">
							<i class="fas fa-plus"></i>
						</a>
						<a class="btn btn-tool confirm-dialog" href="#" data-callback="actualize_caregories" data-message="Вы уверены что хотите актуализировать категорий?" data-title="Актуализировать категорий" data-type="bg-warning" data-toggle="tooltip" title="Актуализировать">
							<i class="fas fa-sync-alt"></i>
						</a>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="cTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="custom-width-50 text-center">
										<button type="button" class="btn btn-default btn-flat btn-sm call-function" data-callback="toggle_all">
											<i class="fad fa-plus"></i>
										</button>
									</th>
									<th class="custom-width-100 text-center"># ID</th>
									<th>Название</th>
									<th class="custom-width-200">Родительская Категория</th>
									<th class="custom-width-300">URL</th>
									<th class="custom-width-150 text-center">Вес</th>
									<th class="custom-width-150 text-center">К-во товаров</th>
									<th class="custom-width-50 text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($categories)){?>
									<?php echo $categories;?>
								<?php } else{?>
									<tr>
										<td colspan="6">
											<div class="alert alert-info mb-0 ml-5 mr-5">
												Нет категорий.
											</div>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
