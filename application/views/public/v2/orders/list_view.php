<div class="table-responsive mt-10 mb-10">
	<table class="table table-striped table-bordered w-100pr" id="dtTable">
		<thead>
			<tr>
				<th class="dt_toggle">#</th>
				<th class="dt_name">Номер заказа</th>
				<th class="dt_amount">Сумма</th>
				<th class="dt_created">Создан</th>
				<th class="dt_status">Статус</th>
			</tr>
		</thead>
	</table>
</div>

<script>
	var dtTable; //obj of datatable

    /* Formating function for row details */
    function fnFormatDetails(nTr){
		var aData = dtTable.fnGetData(nTr);
		var sOut = aData['dt_detail'];
		return sOut;
    }

	$(function(){
		$('body').on('click', 'a.order_details', function(e) {
            e.preventDefault();
			var $thisBtn = $(this);
		    var nTr = $thisBtn.parents('tr')[0];

			if (dtTable.fnIsOpen(nTr))
				dtTable.fnClose(nTr);
		    else
				dtTable.fnOpen(nTr, fnFormatDetails(nTr), 'details');

			$thisBtn.find('.ca-icon').toggleClass('ca-icon_plus ca-icon_minus');
		});

		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/accent/plugins/datatable/js/Russian.json"
			},
			"sDom": 'tp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "orders/ajax_list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 50,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_toggle"], "mData": "dt_toggle", "bSortable": false},
				{ "sClass": "text-left vam", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "text-center vam", "aTargets": ["dt_amount"], "mData": "dt_amount"},
				{ "sClass": "text-center vam", "aTargets": ["dt_created"], "mData": "dt_created"},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_status"], "mData": "dt_status", "bSortable": false}
			],
			"aaSorting" : [[1,'desc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_paginate.dataTables_paginate').css("display", "block"); 
                    $('#dtTable_paginate.dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_paginate.dataTables_paginate').css("display", "none");
                    $('#dtTable_paginate.dataTables_filter').css("display", "none");
                }
			}
		});
	});
</script>
