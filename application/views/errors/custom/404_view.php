<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view($this->theme->public_view('includes/head')); ?>
</head>
<body>
	<?php $this->load->view($this->theme->public_view('includes/header')); ?>
	<section id="wr-body">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php $this->load->view($this->theme->public_view('includes/breadcrumbs')); ?>
				</div>
				<div class="col-xs-12">
                    <div class="col-sm-6 col-sm-offset-3 text-center">
                        <h1 class="text-danger alt-font" style="font-size: 10em;">404</h1>
                        <h2>Страница не найдена!</h2>
                        <p class="lead">Извините, запрошенная вами страница не найдена. Вернитесь на домашнюю страницу.</p>
                    </div>
				</div>
			</div>
		</div>
	</section>
    <?php $this->load->view($this->theme->public_view('includes/footer')); ?>
</body>
</html>
