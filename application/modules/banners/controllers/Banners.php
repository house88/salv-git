<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Banners extends MX_Controller{
	function __construct(){
		parent::__construct();
        $this->data = array();
		$this->load->model("Banners_model", "banners");
	}

	function index(){
		return_404($this->data);
	}

	function _get_all(){
		return $this->banners->handler_get_all();
	}

	function _get($id_banner = 0){
		return $this->banners->handler_get($id_banner);
	}

	function _get_by($params = array()){
		if(empty($params)){
			return;
		}

		return $this->banners->handler_get_by($params);
	}
}
