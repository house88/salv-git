<?php $this->load->view($this->theme->apanel_view('includes/plugins/tinymce'));?>

<script>
	var toggle_all = function(element){
		var $this = $(element);
		var $icon = $this.find('i.fad');

		if($icon.hasClass('fa-plus')){
			$icon.removeClass('fa-plus').addClass('fa-minus');
			$('#cTable > tbody > tr').show().find('.js-toggle-categories > i').removeClass('fa-plus').addClass('fa-minus');;
		} else{
			$icon.removeClass('fa-minus').addClass('fa-plus');
			$('#cTable > tbody > tr[data-parent="0"]').find('.js-toggle-categories > i').removeClass('fa-minus').addClass('fa-plus');
			$('#cTable > tbody > tr:not([data-parent="0"])').hide().find('.js-toggle-categories > i').removeClass('fa-minus').addClass('fa-plus');
		}
	}

	var toggle_one = function(element){
		var $this = $(element);
		var $tr = $this.closest('tr');
		var $icon = $this.find('i.fad');

		if($icon.hasClass('fa-plus')){
			$icon.removeClass('fa-plus').addClass('fa-minus');
			$('#cTable tr[data-parent="'+ $tr.data('category') +'"]').show();
		} else{
			$icon.removeClass('fa-minus').addClass('fa-plus');
			$('#cTable tr[data-parents*="'+ $this.data('toggle') +'-"]').hide().find('.js-toggle-categories > i').removeClass('fa-minus').addClass('fa-plus');
		}
	}

	var change_status = function(btn){
		var $this = $(btn);
		var category = $this.data('category');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/categories/ajax_operations/change_status',
			data: {category:category},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				hideLoader('body');
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					if(resp.status == 1){
						var template = '<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите сделать категорию невидимой?" data-title="<i class=\'fad fa-eye\'></i> Скрыть категорию" data-callback="change_status" data-category="'+ category +'" data-type="bg-warning">\
											<i class="fad fa-eye-slash"></i> Скрыть категорию\
										</a>';
					} else{
						var template = '<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите сделать категорию видимой?" data-title="<i class=\'fad fa-eye-slash\'></i> Показать категорию" data-callback="change_status" data-category="'+ category +'" data-type="bg-warning">\
											<i class="fad fa-eye"></i> Показать категорию\
										</a>'
					}
					$this.replaceWith(template);
				}
			}
		});
		return false;
	}

	var change_weight = function(btn){
		var $this = $(btn);
		var $input = $this.closest('.js-category-weight').find('input[name="category_weight"]');
		var category = $this.data('category');
		var order_num = $input.val();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/categories/ajax_operations/change_weight',
			data: {category:category,order_num:order_num},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				hideLoader('body');
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}

	var actualize_caregories = function(btn){
		var $this = $(btn);
		$.ajax({
			type: 'POST',
			url: base_url+'admin/categories/ajax_operations/actualize_caregories',
			data: {},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				hideLoader('body');
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}
</script>