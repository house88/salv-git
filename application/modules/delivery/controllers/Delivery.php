<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Delivery extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model("Delivery_model", "delivery");
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		$this->breadcrumbs[] = array(
			'title' => 'Доставка',
			'link' => base_url('delivery')
		);

        $params = array(
            'active' => 1
        );

        $this->data['delivery_options'] = $this->delivery->handler_get_all($params);

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'delivery/index_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}

	function _get_all($params = array()){
		return $this->delivery->handler_get_all($params);
	}
}
