<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{
	
	function __construct(){
		parent::__construct();
				
		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/properties/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'properties';

		$this->data = array();
		
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("categories/Categories_model", "categories");
		$this->load->model("Properties_model", "properties");

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		checkPermision('manage_properties', '/admin');

		$this->data['categories'] = Modules::run('categories/_get_tree_tokens');

		$this->data['page_header'] = 'Свойства товаров';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_properties');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_properties');

				$id_property = (int) $this->uri->segment(5);
				$this->data['property'] = $this->properties->handler_get($id_property);
				if(empty($this->data['property'])){
					jsonResponse('Свойство не найдено.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'categories':
				checkPermisionAjax('manage_properties');

				$id_property = (int) $this->uri->segment(5);
				$this->data['property'] = $this->properties->handler_get($id_property);
				if(empty($this->data['property'])){
					jsonResponse('Свойство не найдено.');
				}

				$this->data['categories'] = Modules::run('categories/_get_tree_tokens');
				$property_categories = arrayByKey($this->properties->handler_get_relation_categories($id_property), 'id_category');
				$property_categories_tree = array();
				foreach ($property_categories as $property_category) {
					if(isset($property_categories[$property_category['category_parent']])){
						$property_categories_tree[(int) $property_category['category_parent']][(int) $property_category['category_id']] = $property_category;
					} else{
						$property_categories_tree[0][(int) $property_category['category_id']] = $property_category;
					}
				}
				$this->data['property_categories_table'] = $this->tree_table($property_categories_tree, 0, 0, $property_categories);

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'categories_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_properties');

				$validator_rules = array(
					array(
						'field' => 'title_property',
						'label' => 'Название',
						'rules' => 'required|xss_clean|max_length[50]',
					),
					array(
						'field' => 'type_property',
						'label' => 'Тип значений',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'faq_text',
						'label' => 'FAQ',
						'rules' => 'xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$insert_properties = array(
					'title_property' => $this->input->post('title_property'),
					'type_property' => $this->input->post('type_property'),
					'faq_text' => $this->input->post('faq_text'),
				);
				
				if($this->input->post('status_property')){
					$insert_properties['status_property'] = 1;
				}
				
				if($this->input->post('show_faq')){
					$insert_properties['show_faq'] = 1;
				}
				
				if($this->input->post('on_item')){
					$insert_properties['on_item'] = 1;
				}
				
				if($this->input->post('in_compare')){
					$insert_properties['in_compare'] = 1;
				}
				
				if($this->input->post('in_filter')){
					$insert_properties['in_filter'] = 1;
				}
				
				$id_property = $this->properties->handler_insert($insert_properties);
				
				$insert_property_values = array();
				switch($insert_properties['type_property']){
					case 'simple':
						$property_values = array();
					break;
					case 'range' :
						$property_value_from = $this->input->post('property_value_from');
						$property_value_to = $this->input->post('property_value_to');
						$property_value_unit = $this->input->post('property_value_unit');
						$property_values = array(
							'min_number' => $property_value_from,
							'max_number' => $property_value_to,
							'unit' => $property_value_unit
						);
					break;
					case 'select' :
					case 'multiselect' :
						$property_values = $this->input->post('property_values');
						if(!empty($property_values)){
							foreach($property_values as $value){
								if(!empty($value)){
									$insert_property_values[] = array(
										'id_property' => $id_property,
										'value' => $value
									);
								}
							}

							if(!empty($insert_property_values)){
								$this->properties->handler_insert_property_values($insert_property_values);
							}
						}

						$property_values = $this->properties->handler_get_property_values($id_property);
					break;
				}
				
				$update_property = array(
					'property_values' => json_encode($property_values)
				);				
				$this->properties->handler_update($id_property, $update_property);
				
				jsonResponse('Сохранено','success');
			break;
			case 'edit':
				checkPermisionAjax('manage_properties');

				$validator_rules = array(
					array(
						'field' => 'id_property',
						'label' => 'Свойство',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'title_property',
						'label' => 'Название',
						'rules' => 'required|xss_clean|max_length[50]',
					),
					array(
						'field' => 'type_property',
						'label' => 'Тип значений',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'faq_text',
						'label' => 'FAQ',
						'rules' => 'xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$update_property = array(
					'title_property' => $this->input->post('title_property'),
					'type_property' => $this->input->post('type_property'),
					'faq_text' => $this->input->post('faq_text'),
					'status_property' => $this->input->post('status_property') ? 1 : 0,
					'show_faq' => $this->input->post('show_faq') ? 1 : 0,
					'on_item' => $this->input->post('on_item') ? 1 : 0,
					'in_compare' => $this->input->post('in_compare') ? 1 : 0,
					'in_filter' => $this->input->post('in_filter') ? 1 : 0
				);

				$id_property = (int) $this->input->post('id_property');

				$property = $this->properties->handler_get($id_property);
				switch($update_property['type_property']){
					case 'simple':
						$property_values = array();
					break;
					case 'range' :
						$property_value_from = $this->input->post('property_value_from');
						$property_value_to = $this->input->post('property_value_to');
						$property_value_unit = $this->input->post('property_value_unit');
						$property_values = array(
							'min_number' => $property_value_from,
							'max_number' => $property_value_to,
							'unit' => $property_value_unit
						);
						$this->properties->handler_remove_property_values($id_property);
					break;
					case 'select' :
					case 'multiselect' :
						$values_list = array_filter($this->input->post('values_list'));
						$property_values = $this->input->post('property_values');
						if(!empty($property_values)){
							foreach($property_values as $weight_index => $value){
								if(!empty($value)){
									if(isset($values_list[$weight_index])){
										$update_property_values[] = array(
											'id_value' => $values_list[$weight_index],
											'id_property' => $id_property,
											'value' => $value,
											'value_weight' => $weight_index
										);
									} else{
										$insert_property_values[] = array(
											'id_property' => $id_property,
											'value' => $value,
											'value_weight' => $weight_index
										);
									}
								}
							}
							
							if(!empty($update_property_values)){
								$this->properties->handler_update_property_values($update_property_values);
							}
							
							if(!empty($insert_property_values)){
								$this->properties->handler_insert_property_values($insert_property_values);
							}
						}

						$delete_values = $this->input->post('delete_values');
						if(!empty($delete_values)){
							$this->properties->handler_remove_property_values($id_property, $delete_values);
						}

						$property_values = $this->properties->handler_get_property_values($id_property);
					break;
				}

				$update_property['property_values'] = json_encode($property_values);
				$this->properties->handler_update($id_property, $update_property);

				jsonResponse('Сохранено','success');
			break;
			case 'delete':
				checkPermisionAjax('manage_properties');

				$validator_rules = array(
					array(
						'field' => 'property',
						'label' => 'Свойство',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$property_id = (int) $this->input->post('property');
				$this->properties->handler_remove_property_values($property_id);
				$this->properties->handler_delete_relation_category($property_id);
				$this->properties->handler_delete($property_id);
				
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_properties');

				$validator_rules = array(
					array(
						'field' => 'property',
						'label' => 'Свойство товара',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$property_id = (int) $this->input->post('property');
				$property = $this->properties->handler_get($property_id);
				if(empty($property)){
					jsonResponse('Свойство не найдена.');
				}
				
				$action = $this->uri->segment(5);
				switch($action){
					default:
					case 'status' :
						$update = array(
							'status_property' => (int) $property['status_property'] === 1 ? 0 : 1
						);
					break;
					case 'on_item' :
						$update = array(
							'on_item' => (int) $property['on_item'] === 1 ? 0 : 1
						);
					break;
					case 'in_compare' :
						$update = array(
							'in_compare' => (int) $property['in_compare'] === 1 ? 0 : 1
						);
					break;
					case 'in_filter' :
						$update = array(
							'in_filter' => (int) $property['in_filter'] === 1 ? 0 : 1
						);
					break;
					case 'show_faq' :
						$update = array(
							'show_faq' => (int) $property['show_faq'] === 1 ? 0 : 1
						);
					break;
				}

				$this->properties->handler_update($property_id, $update);
				
				jsonResponse('Сохранено', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_properties');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'admin_select' => true
				);

				$params['sort_by'] = flat_dt_ordering($_POST, array(
					'dt_id'     	=> 'id_property',
					'dt_cweight'    => 'property_category_weight'
				));

				if ($this->input->post('category')){
					$params['id_category'] = (int) $this->input->post('category');
				}

				$records = $this->properties->handler_get_all($params);
				$records_total = $this->properties->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($params){	
						$category_weight = '&mdash;';
						if (isset($params['id_category'])){
							$id_category = (int) $params['id_category'];
							$category_weight = '<div class="input-group input-group-sm">
													<input class="form-control rounded-0" name="property_category_weight" value="'.$record['property_category_weight'].'" />
													<span class="input-group-append">
														<button type="button" class="btn btn-success btn-flat rounded-0 call-function" data-callback="change_property_category_weight" title="Применить" data-category="'.$id_category.'" data-property="'.$record['id_property'].'"><i class="fad fa-check"></i></button>
													</span>
												</div>';
						}
						
						return array(
							'dt_id'			=>  $record['id_property'],
							'dt_name'		=>  $record['title_property'],
							'dt_cweight'	=>  $category_weight,
							'dt_status'		=> '<a href="#" class="custom-font-size-34 '. get_choice('fad fa-check-square text-success', (int) $record['status_property'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-property="'.$record['id_property'].'" data-action="status"></a>',
							'dt_on_item'	=> '<a href="#" class="custom-font-size-34 '. get_choice('fad fa-check-square text-success', (int) $record['on_item'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-property="'.$record['id_property'].'" data-action="on_item"></a>',
							'dt_in_compare'	=> '<a href="#" class="custom-font-size-34 '. get_choice('fad fa-check-square text-success', (int) $record['in_compare'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-property="'.$record['id_property'].'" data-action="in_compare"></a>',
							'dt_in_filter'	=> '<a href="#" class="custom-font-size-34 '. get_choice('fad fa-check-square text-success', (int) $record['in_filter'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-property="'.$record['id_property'].'" data-action="in_filter"></a>',
							'dt_show_faq'	=> '<a href="#" class="custom-font-size-34 '. get_choice('fad fa-check-square text-success', (int) $record['show_faq'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-property="'.$record['id_property'].'" data-action="show_faq"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/properties/popup/categories/' . $record['id_property']).'">
															<i class="fad fa-folder-tree"></i> Категории
														</a>
														<div class="dropdown-divider"></div>
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/properties/popup/edit/' . $record['id_property']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить свойство?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-property="'.$record['id_property'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
			case 'get_category_properties':
				$validator_rules = array(
					array(
						'field' => 'category',
						'label' => 'Категория товара',
						'rules' => 'xss_clean',
					)
				);
				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$params['id_category'] = (int)$this->input->post('category');
				$this->data['properties'] = $this->properties->handler_get_all($params);
				
				if($this->input->post('item')){
					$id_item = (int)$this->input->post('item');
					$item_properties_values = $this->properties->handler_get_items_properties_values($id_item);
					$this->data['item_properties_values'] = array();
					if(!empty($item_properties_values)){
						foreach($item_properties_values as $item_property_value){
							$this->data['item_properties_values'][$item_property_value['id_property']][] = $item_property_value['value'];
						}
					}
				}
				
				$this->data['properties_required'] = ($this->input->post('properties_required'))?true:false;
				$properties = $this->load->view($this->theme->apanel_view($this->view_module_path . 'item_properties_view'), $this->data, true);
				jsonResponse('', 'success', array('properties' => $properties));
			break;
			case 'get_filter_category_properties':
				$validator_rules = array(
					array(
						'field' => 'category',
						'label' => 'Категория товара',
						'rules' => 'xss_clean',
					)
				);
				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$params['id_category'] = (int)$this->input->post('category');
				$this->data['properties'] = $this->properties->handler_get_all($params);
				$properties = $this->load->view($this->theme->apanel_view($this->view_module_path . 'filter_category_view'), $this->data, true);
				jsonResponse('', 'success', array('properties' => $properties));
			break;
			case 'set_category_property_relation':
				if(!$this->lauth->have_right('manage_properties')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$validator_rules = array(
					array(
						'field' => 'id_property',
						'label' => 'Свойство',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_property = (int) $this->input->post('id_property');
				$category = (int) $this->input->post('category');
				$exist_relation = $this->properties->handler_exist_relation_category($id_property, $category);
				if($exist_relation){
					jsonResponse('Ошибка: Привязка к категорий уже сушествует.');
				}

				$insert_relation_category = array(
					array(
						'id_property' => $id_property,
						'id_category' => $category
					)
				);
				$this->properties->handler_insert_relation_category($insert_relation_category);

				$property_categories = arrayByKey($this->properties->handler_get_relation_categories($id_property), 'id_category');
				$property_categories_tree = array();
				foreach ($property_categories as $property_category) {
					if(isset($property_categories[$property_category['category_parent']])){
						$property_categories_tree[(int) $property_category['category_parent']][(int) $property_category['category_id']] = $property_category;
					} else{
						$property_categories_tree[0][(int) $property_category['category_id']] = $property_category;
					}
				}
				$property_categories_table = $this->tree_table($property_categories_tree, 0, 0, $property_categories);
				jsonResponse('Категория добавлена.', 'success', array('property_categories_table' => $property_categories_table));
			break;
			case 'remove_category_property_relation':
				if(!$this->lauth->have_right('manage_properties')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$validator_rules = array(
					array(
						'field' => 'id_property',
						'label' => 'Свойство',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_property = (int) $this->input->post('id_property');
				$id_category = (int) $this->input->post('category');
				$category = $this->categories->handler_get($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Категория не найдена.');
				}

				$exist_relation = $this->properties->handler_exist_relation_category($id_property, $id_category);
				if(!$exist_relation){
					jsonResponse('Ошибка: Категория не привязана к этому свойству.');
				}

				$this->properties->handler_delete_relation_category($id_property, $id_category);

				$property_categories = arrayByKey($this->properties->handler_get_relation_categories($id_property), 'id_category');
				$property_categories_tree = array();
				foreach ($property_categories as $property_category) {
					if(isset($property_categories[$property_category['category_parent']])){
						$property_categories_tree[(int) $property_category['category_parent']][(int) $property_category['category_id']] = $property_category;
					} else{
						$property_categories_tree[0][(int) $property_category['category_id']] = $property_category;
					}
				}
				$property_categories_table = $this->tree_table($property_categories_tree, 0, 0, $property_categories);
				jsonResponse('Привязка категорий к свойству удалена.', 'success', array('property_categories_table' => $property_categories_table));
			break;
			case 'set_category_children_to_property':
				if(!$this->lauth->have_right('manage_properties')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$validator_rules = array(
					array(
						'field' => 'id_property',
						'label' => 'Свойство',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_property = (int) $this->input->post('id_property');
				$id_category = (int) $this->input->post('category');
				$category = $this->categories->handler_get($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Категория не найдена.');
				}

				$exist_relation = $this->properties->handler_exist_relation_category($id_property, $id_category);
				if(!$exist_relation){
					jsonResponse('Ошибка: Категория не привязана к этому свойству.');
				}
				
				$property_category_relation = $this->properties->handler_get_relation_category($id_property, $id_category);
				$update_relation_category = array(
					'property_category_to_children' => 1
				);
				$this->properties->handler_update_relation_category($id_property, $id_category, $update_relation_category);
				
				if(!empty($category['category_children'])){
					$category_children = array_filter(explode(',', $category['category_children']));
					if(!empty($category_children)){
						$insert_relation_category = array();
						foreach ($category_children as $category_child) {
							if(!$this->properties->handler_exist_relation_category($id_property, $category_child)){
								$insert_relation_category[] = array(
									'id_property' => $id_property,
									'id_category' => $category_child,
									'property_category_weight' => $property_category_relation['property_category_weight'],
									'property_category_to_children' => 1
								);
							} else{
								$update_relation_category = array(
									'property_category_weight' => $property_category_relation['property_category_weight'],
									'property_category_to_children' => 1
								);
								$this->properties->handler_update_relation_category($id_property, $category_child, $update_relation_category);
							}
						}

						if(!empty($insert_relation_category)){
							$this->properties->handler_insert_relation_category($insert_relation_category);
						}
					}
				}

				$property_categories = arrayByKey($this->properties->handler_get_relation_categories($id_property), 'id_category');
				$property_categories_tree = array();
				foreach ($property_categories as $property_category) {
					if(isset($property_categories[$property_category['category_parent']])){
						$property_categories_tree[(int) $property_category['category_parent']][(int) $property_category['category_id']] = $property_category;
					} else{
						$property_categories_tree[0][(int) $property_category['category_id']] = $property_category;
					}
				}
				$property_categories_table = $this->tree_table($property_categories_tree, 0, 0, $property_categories);
				jsonResponse('Передача свойства вложеным категориям установлено.', 'success', array('property_categories_table' => $property_categories_table));
			break;
			case 'remove_category_children_from_property':
				if(!$this->lauth->have_right('manage_properties')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$validator_rules = array(
					array(
						'field' => 'id_property',
						'label' => 'Свойство',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_property = (int) $this->input->post('id_property');
				$id_category = (int) $this->input->post('category');
				$category = $this->categories->handler_get($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Категория не найдена.');
				}

				$exist_relation = $this->properties->handler_exist_relation_category($id_property, $id_category);
				if(!$exist_relation){
					jsonResponse('Ошибка: Категория не привязана к этому свойству.');
				}

				$update_relation_category = array(
					'property_category_to_children' => 0
				);
				$this->properties->handler_update_relation_category($id_property, $id_category, $update_relation_category);

				$property_categories = arrayByKey($this->properties->handler_get_relation_categories($id_property), 'id_category');
				$property_categories_tree = array();
				foreach ($property_categories as $property_category) {
					if(isset($property_categories[$property_category['category_parent']])){
						$property_categories_tree[(int) $property_category['category_parent']][(int) $property_category['category_id']] = $property_category;
					} else{
						$property_categories_tree[0][(int) $property_category['category_id']] = $property_category;
					}
				}
				$property_categories_table = $this->tree_table($property_categories_tree, 0, 0, $property_categories);
				jsonResponse('Передача свойства вложеным категориям снято.', 'success', array('property_categories_table' => $property_categories_table));
			break;
			case 'change_property_category_weight':
				if(!$this->lauth->have_right('manage_properties')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$validator_rules = array(
					array(
						'field' => 'property',
						'label' => 'Свойство',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'property_category_weight',
						'label' => 'Вес',
						'rules' => 'xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_property = (int) $this->input->post('property');
				$id_category = (int) $this->input->post('category');
				$property_category_weight = (int) $this->input->post('property_category_weight');
				$category = $this->categories->handler_get($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Категория не найдена.');
				}

				$exist_relation = $this->properties->handler_exist_relation_category($id_property, $id_category);
				if(!$exist_relation){
					jsonResponse('Ошибка: Категория не привязана к этому свойству.');
				}
				
				$property_category_relation = $this->properties->handler_get_relation_category($id_property, $id_category);
				$update_relation_category = array(
					'property_category_weight' => $property_category_weight
				);
				$this->properties->handler_update_relation_category($id_property, $id_category, $update_relation_category);
				
				if($property_category_relation['property_category_to_children'] == 1){
					$category_children = array_filter(explode(',', $category['category_children']));
					if(!empty($category_children)){
						foreach ($category_children as $category_child) {
							if($this->properties->handler_exist_relation_category($id_property, $category_child)){
								$update_relation_category = array(
									'property_category_weight' => $property_category_weight
								);
								$this->properties->handler_update_relation_category($id_property, $category_child, $update_relation_category);
							}
						}
					}
				}
				jsonResponse('Вес свойства в категорий установлен.', 'success');
			break;
		}
	}
	
	private function tree_table($tree, $category_parent = 0, $level, $categories_list) {
        if (empty($tree[$category_parent]))
            return;
		
        $str = '';
        foreach ($tree[$category_parent] as $k => $row) {
			$parent_category = (!empty($categories_list[$row['category_parent']]))?$categories_list[$row['category_parent']]:array();
			$str .= $this->load->view($this->theme->apanel_view($this->view_module_path . 'tree_category_row'), array('category' => $row, 'level' => $level, 'category_parent' => $parent_category), true);
			
            if (isset($tree[$row['category_id']]))
                $str .= $this->tree_table($tree, $row['category_id'], $level + 1, $categories_list);
        }

        return $str;
    }
}
