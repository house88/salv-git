<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Socials_auth
{
	var $errors = null;

	private $vk_client_id = '6382792';
	private $vk_client_secret = 'su38V9niBaRdCl9Ume9I';

	private $ok_client_id = '1263325184';
	private $ok_public_key = 'CBABNNEMEBABABABA';
	private $ok_secret_key = 'E2F3AB105B7762C07ECB1C94';

	public function __construct(){
		$this_ci =& get_instance();
	}    
	
	function get_fb_user($access_token = null){
		require_once APPPATH.'/third_party/Socials/Facebook/autoload.php';
		$fb = new Facebook\Facebook(array(
			'app_id' => '176649066443991',
			'app_secret' => 'f7a6f78054124a9b606395510d08bf45',
			'default_graph_version' => 'v3.0',
		));
		  
		try {
			$response = $fb->get('/me?fields=id,name,email', $access_token);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$this->errors[] = 'Graph returned an error: ' . $e->getMessage();
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$this->errors[] = 'Facebook SDK returned an error: ' . $e->getMessage();
		}

		if($this->errors){
			return array('status' => 'error', 'message' => $this->errors);
		} else{
			$user = $response->getGraphUser();
			$user_data = array(
				'name' => $user->getName(),
				'email' => $user->getEmail(),
			);

			return array('status' => 'success', 'user' => $user_data);
		}
		  
	}

	function get_vk_callback_url(){
		$params = array(
			'client_id' => $this->vk_client_id,
			'response_type' => 'code',
			'scope' => 'email',
			'redirect_uri' => site_url('auth/vk_callback')
		);
		
		return 'https://oauth.vk.com/authorize?'.urldecode(http_build_query($params));
	}

	function get_vk_user(){
		if ($_GET['code']) {
			$params = array(
				'client_id' => $this->vk_client_id,
				'client_secret' => $this->vk_client_secret,
				'code' => $_GET['code'],
				'scope' => 'email',
				'redirect_uri' => site_url('auth/vk_callback')
			);
			
			$token = json_decode(@file_get_contents('https://oauth.vk.com/access_token?'.urldecode(http_build_query($params))), true);
			if(isset($token['access_token'],$token['user_id'])){
				$params = array(
					'uids'         => $token['user_id'],
					'fields'       => 'uid,first_name,last_name,screen_name,email',
					'access_token' => $token['access_token']
				);
		
				$userInfo = json_decode(@file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
				if (isset($userInfo['response'][0]['uid'])) {
					$userInfo = $userInfo['response'][0];
					$userInfo['email'] = $token['email'];
					
					return array('status' => 'success', 'user' => $userInfo);
				}
			}
		}

		$this->errors[] = 'Нет данных.';
		return array('status' => 'error', 'message' => $this->errors);
	}

	function get_ok_callback_url(){
		$params = array(
			'client_id' => $this->ok_client_id,
			'response_type' => 'code',
			'scope' => 'GET_EMAIL',
			'redirect_uri' => site_url('auth/ok_callback')
		);
		
		return 'https://connect.ok.ru/oauth/authorize?'.urldecode(http_build_query($params));
	}

	function get_ok_user(){
		if ($_GET['code']) {
			$params = array(
				'code' => $_GET['code'],
				'client_id' => $this->ok_client_id,
				'client_secret' => $this->ok_secret_key,
				'grant_type' => 'authorization_code',
				'redirect_uri' => site_url('auth/ok_callback')
			);
			
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'https://api.ok.ru/oauth/token.do');
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$result = curl_exec($curl);
			curl_close($curl);

			$tokenInfo = json_decode($result, true);
			if (isset($tokenInfo['access_token'])) {
				$sign = md5("application_key={$this->ok_public_key}format=jsonmethod=users.getCurrentUser".md5("{$tokenInfo['access_token']}{$this->ok_secret_key}"));
				$params = array(
					'method'          => 'users.getCurrentUser',
					'access_token'    => $tokenInfo['access_token'],
					'application_key' => $this->ok_public_key,
					'format'          => 'json',
					'sig'             => $sign
				);
		
				$userInfo = json_decode(@file_get_contents('http://api.odnoklassniki.ru/fb.do' . '?' . urldecode(http_build_query($params))), true);
				if (!empty($userInfo['email'])) {					
					return array('status' => 'success', 'user' => $userInfo);
				}
			}
		}

		$this->errors[] = 'Нет данных.';
		return array('status' => 'error', 'message' => $this->errors);
	}

	function get_errors(){
		return $this->errors;
	}
}
