<?php if(!empty($message)){?>
	<div class="alert alert-<?php echo $type;?> mb-0 mt-10" role="alert"><?php echo $message?></div>

	<?php if(isset($refresh)){ ?>
        <meta http-equiv="refresh" content="<?php echo $refresh?>"/>
    <?php }?>
<?php }?>
