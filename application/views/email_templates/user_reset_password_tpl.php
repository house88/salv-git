<p style="font: 16px Arial, sans-serif; color: #555555;margin:0 0 10px 0;padding:0;">Bine v-am găsit,</p>
<p style="font: 16px Arial, sans-serif; color: #555555;margin:0 0 10px 0;padding:0;">
    Pentru a modifica parola vă rugăm să apăsati <a href="<?php echo base_url('reset_password/'.@$reset_key);?>" style="color:#555555;">aici</a>.
</p>
<p style="font: 16px Arial, sans-serif; color: #555555;text-align:center;">
    (Dacă link-ul de mai sus nu lucrează, copiati linkul de mai jos:) <br>
    <?php echo base_url('reset_password/'.@$reset_key);?>
</p>