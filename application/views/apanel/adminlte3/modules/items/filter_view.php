<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat custom-width-40"><i class="fad fa-search"></i></button>
        </span>
        <input class="form-control form-control-sm rounded-0 dt_filter" type="text" name="keywords" data-title="Поиск" placeholder="Поиск">
    </div>
</div>

<div class="form-group">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <button type="button" class="btn btn-default btn-flat custom-width-40"><i class="fad fa-folder-tree"></i></button>
        </span>	
        <select name="id_category" id="category_parent" data-title="Категория" class="form-control form-control-sm rounded-0 dt_filter js-parent-category">
            <option value="">Выберите категорию</option>
            <?php if(!empty($categories)){?>
                <?php foreach($categories as $category){?>
                    <option value="<?php echo $category['category_id'];?>"><?php echo $category['category_title'];?></option>
                <?php }?>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group" id="subcategories_block"></div>

<?php if(!empty($brands)){?>
    <div class="form-group">
        <div class="input-group input-group-sm">
            <span class="input-group-prepend">
                <button type="button" class="btn btn-default btn-flat custom-width-40"><i class="fad fa-copyright"></i></button>
            </span>	
            <select name="brand" data-title="Бренд" class="form-control form-control-sm rounded-0 dt_filter">
                <option value="">Выберите бренд</option>
                <?php foreach($brands as $brand){?>
                    <option value="<?php echo $brand['id_brand'];?>"><?php echo $brand['brand_title'];?></option>
                <?php }?>
            </select>
        </div>
    </div>
<?php }?>

<div class="form-group">
    <label class="text-gray custom-font-size-12">Цена, <?php echo $currency['currency_symbol'];?></label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text">от</span>
        </div>
        <input type="text" class="form-control float-right rounded-0 dt_filter" data-title="Цена, от" name="item_price_from" placeholder="0.00">
        <div class="input-group-append rounded-0">
            <span class="input-group-text">до</span>
        </div>
        <input type="text" class="form-control float-right rounded-0 dt_filter" data-title="Цена, до" name="item_price_to" placeholder="0.00">
    </div>
</div>

<div class="form-group">
    <label class="text-gray custom-font-size-12">Обновлен</label>
    <div class="input-group input-group-sm">
        <div class="input-group-prepend rounded-0">
            <span class="input-group-text"><i class="fad fa-calendar-week"></i></span>
        </div>
        <input type="text" name="item_last_update" class="form-control float-right rounded-0 dt_filter" data-title="Обновлен" placeholder="ex. 21/01/2020 - 31/01/2020" id="js-item-updated-date">
    </div>
</div>

<div class="form-group">
    <label class="text-gray custom-font-size-12">Маркетинг</label>
    <a href="#" class="d-none btn btn-danger dt_filter" data-name="item_visible" title="Признак виден" data-title="Признак виден" data-value-text="" data-value="" data-default="true"><span class="ca-icon ca-icon_remove"></span></a>
    <a href="#" class="d-none btn btn-danger dt_filter" data-name="item_hit" title="Признак хит" data-title="Признак хит" data-value-text="" data-value="" data-default="true"><span class="ca-icon ca-icon_remove"></span></a>
    <a href="#" class="d-none btn btn-danger dt_filter" data-name="item_newest" title="Признак новинка" data-title="Признак новинка" data-value-text="" data-value="" data-default="true"><span class="ca-icon ca-icon_remove"></span></a>
    <a href="#" class="d-none btn btn-danger dt_filter" data-name="item_action" title="Признак акция" data-title="Признак акция" data-value-text="" data-value="" data-default="true"><span class="ca-icon ca-icon_remove"></span></a>
    <a href="#" class="d-none btn btn-danger dt_filter" data-name="item_popular" title="Признак популярен" data-title="Признак популярен" data-value-text="" data-value="" data-default="true"><span class="ca-icon ca-icon_remove"></span></a>
    <a href="#" class="d-none btn btn-danger dt_filter" data-name="item_on_home" title="Признак показать на главной" data-title="Признак показать на главной" data-value-text="" data-value="" data-default="true"><span class="ca-icon ca-icon_remove"></span></a>
    <a href="#" class="d-none btn btn-danger dt_filter" data-name="item_commented" title="Признак обсуждаемый" data-title="Признак обсуждаемый" data-value-text="" data-value="" data-default="true"><span class="ca-icon ca-icon_remove"></span></a>
    <div class="btn-toolbar">
        <div class="btn-group btn-group-xs custom-margin-bottom-5 custom-margin-right-5">
            <a href="#" class="btn btn-primary btn-xs btn-flat custom-width-30 dt_filter" data-name="item_visible" title="Виден" data-title="Признак виден" data-value-text="Да" data-value="1" data-current="false">
                <span class="fad fa-eye"></span>
            </a>
            <a href="#" class="btn btn-warning btn-xs btn-flat custom-width-30 dt_filter" data-name="item_visible" title="Не виден" data-title="Признак виден" data-value-text="Нет" data-value="0" data-current="false">
                <span class="fad fa-eye-slash"></span>
            </a>
        </div>
        <div class="btn-group btn-group-xs custom-margin-bottom-5 custom-margin-right-5">
            <a href="#" class="btn btn-primary btn-xs btn-flat custom-width-30 dt_filter" data-name="item_hit" title="Хит" data-title="Признак хит" data-value-text="Да" data-value="1">
                <span class="fad fa-fire"></span>
            </a>
            <a href="#" class="btn btn-warning btn-xs btn-flat custom-width-30 dt_filter" data-name="item_hit" title="Не хит" data-title="Признак хит" data-value-text="Нет" data-value="0">
                <span class="fad fa-fire"></span>
            </a>
        </div>
        <div class="btn-group btn-group-xs custom-margin-bottom-5 custom-margin-right-5">
            <a href="#" class="btn btn-primary btn-xs btn-flat custom-width-30 dt_filter" data-name="item_newest" title="Новинка" data-title="Признак новинка" data-value-text="Да" data-value="1">
                <span class="fad fa-gift"></span>
            </a>
            <a href="#" class="btn btn-warning btn-xs btn-flat custom-width-30 dt_filter" data-name="item_newest" title="Не новинка" data-title="Признак новинка" data-value-text="Нет" data-value="0">
                <span class="fad fa-gift"></span>
            </a>
        </div>
        <div class="btn-group btn-group-xs custom-margin-bottom-5 custom-margin-right-5">
            <a href="#" class="btn btn-primary btn-xs btn-flat custom-width-30 dt_filter" data-name="item_action" title="Акция" data-title="Признак акция" data-value-text="Да" data-value="1">
                <span class="fad fa-stars"></span>
            </a>
            <a href="#" class="btn btn-warning btn-xs btn-flat custom-width-30 dt_filter" data-name="item_action" title="Не акция" data-title="Признак акция" data-value-text="Нет" data-value="0">
                <span class="fad fa-star-exclamation"></span>
            </a>
        </div>
        <div class="btn-group btn-group-xs custom-margin-bottom-5 custom-margin-right-5">
            <a href="#" class="btn btn-primary btn-xs btn-flat custom-width-30 dt_filter" data-name="item_popular" title="Популярен" data-title="Признак популярен" data-value-text="Да" data-value="1">
                <span class="fad fa-heart"></span>
            </a>
            <a href="#" class="btn btn-warning btn-xs btn-flat custom-width-30 dt_filter" data-name="item_popular" title="Не популярен" data-title="Признак популярен" data-value-text="Нет" data-value="0">
                <span class="fad fa-heart-broken"></span>
            </a>
        </div>
        <div class="btn-group btn-group-xs custom-margin-bottom-5 custom-margin-right-5">
            <a href="#" class="btn btn-primary btn-xs btn-flat custom-width-30 dt_filter" data-name="item_on_home" title="Показать на главной" data-title="Признак показать на главной" data-value-text="Да" data-value="1">
                <span class="fad fa-home"></span>
            </a>
            <a href="#" class="btn btn-warning btn-xs btn-flat custom-width-30 dt_filter" data-name="item_on_home" title="Не показывать на главной" data-title="Признак показать на главной" data-value-text="Нет" data-value="0">
                <span class="fad fa-home"></span>
            </a>
        </div>
        <div class="btn-group btn-group-xs custom-margin-bottom-5 custom-margin-right-5">
            <a href="#" class="btn btn-primary btn-xs btn-flat custom-width-30 dt_filter" data-name="item_commented" title="Обсуждаемый" data-title="Признак обсуждаемый" data-value-text="Да" data-value="1">
                <span class="fad fa-comment-alt-dots"></span>
            </a>
            <a href="#" class="btn btn-warning btn-xs btn-flat custom-width-30 dt_filter" data-name="item_commented" title="Не обсуждаемый" data-title="Признак обсуждаемый" data-value-text="Нет" data-value="0">
                <span class="fad fa-comment-alt"></span>
            </a>
        </div>
    </div>
</div>

<div class="form-group">
    <label>
        <span class="text-gray custom-font-size-12">Свойства</span>
        <button class="btn btn-default btn-xs call-function" data-callback="getCategoryProperties" type="button"><i class="ca-icon ca-icon_list "></i></button>
    </label>
    <div id="properties_block"></div>
</div>