<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Categories extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
		$this->data = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("categories_model", "categories");

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		return_404($this->data);
	}

	function _get($id_category){
		return $this->categories->handler_get($id_category);
	}

	function _get_all($params = array()){
		return $this->categories->handler_get_all($params);
	}

	function _get_children($params = array()){
		$categories = $this->categories->handler_get_all($params);
		$children = array();

		if(!empty($categories)){
			foreach ($categories as $category) {
				$_temp_children = explode(',', $category['category_children']);
				$_temp_children[] = $category['category_id'];
				$children = array_merge($children, $_temp_children);
			}
		}

		return array_filter($children);
	}

	function _getTree(){
		$categories = $this->categories->handler_get_tree();
		return $categories;
	}

	function _get_tree(){
		$categories = $this->categories->handler_get_tree();
		return $this->categories->tree_select($categories, 0);
	}

	function _get_tree_tokens($params = array()){
		$categories = $this->categories->handler_get_tree(array(
			'category_children_title' => true
		));

		$selected_categories = array();
		extract($params);
		
		return $this->categories->tree_select($categories, 0, $selected_categories);
	}
}
