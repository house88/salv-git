<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use libphonenumber\PhoneNumberUtil;
use libphonenumber\NumberParseException;

function formatPhoneNumber($phone){
	$phoneNumberUtil = PhoneNumberUtil::getInstance();
	$raw_number = trim($phone);
	$result = array(
		'is_valid' => true,
		'formated' => null
	);

	try {
		if(!$phoneNumberUtil->isViablePhoneNumber($raw_number)){
			$result['is_valid'] = false;
			$result['error'][] = 'Phone number is not viable.';
		}
		
		$phone_number = $phoneNumberUtil->parse($raw_number, 'MD');
		if(!$phoneNumberUtil->isValidNumber($phone_number) && !$phoneNumberUtil->isValidNumberForRegion($phone_number, 'MD')){
			$result['is_valid'] = false;
			$result['error'][] = 'Phone number is not valid.1';
		}
	} catch (NumberParseException $exception) {
		$result['is_valid'] = false;
		$result['error'][] = 'Phone number is not valid.2';
	}

	if(false === $result['is_valid']){
		return $result;
	}

	$phoneNumberObject = $phoneNumberUtil->parse($raw_number, 'MD');
	$result['formated'] = str_replace(' ', '', $phoneNumberUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::NATIONAL));

	return $result;
}

function get_choice($value1 = null, $condition, $value2 = null)
{
	if (true === $condition) {
		return $value1;
	}

	if (false === $condition) {
		return $value2;
	}
}

function file_modification_time($link, $forced = false){
	$version = md5(filemtime( $link ));
	if($forced === true){
		$version = md5(uniqid());
	}

    return base_url($link).'?v='.$version;
}

function clean_output($str = ''){
	// return $str;
	// return htmlentities($str, ENT_QUOTES, "UTF-8");
	return htmlspecialchars($str, ENT_QUOTES, 'UTF-8', false);
}
function delete_files_not_used($target) {
    if(is_dir($target)){
        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

        foreach( $files as $file )
        {
            delete_files_not_used( $file );
        }

        rmdir( $target );
    } elseif(is_file($target)) {
        unlink( $target );
    }
}

function get_embed_video_link($link){
    $components = explode('=', $link);
    $video_id = $components[1];
    return 'https://youtube.com/embed/'.$video_id;
}

function getImage($img_path, $location = 'public'){
	$path_parts = pathinfo($img_path);
	if(!empty($path_parts['extension']) && file_exists($img_path)){
		return $img_path;
	}else{
		$CI = &get_instance();

		if($location !== 'public'){
			return $CI->theme->apanel_assets('img/no_image.png');
		}

		return $CI->theme->public_assets('img/no_image.png');
	}
}

function capitalWord($string, $delimiter_on = '_', $delimiter_off = ' '){
	$filtered = array();
	$conjunctions = array('of','the','and','or','not','but','yet','so','nor','as','for');
	$words = explode($delimiter_on, strtolower($string));

	foreach($words as $word)
		if(!in_array($word, $conjunctions)){
			$filtered[] = ucfirst($word);
		}else{
			$filtered[] = $word;
		}
	$filtered = array_filter($filtered);
	return implode($delimiter_off, $filtered);
}

function get_words($sentence, $count = 10) {
	preg_match("/(?:\w+(?:\W+|$)){0,$count}/", $sentence, $matches);
	return $matches[0];
}

function orderNumber($id){
	return "#{$id}";
}
function orderNumberOnly($id){
	return $id;
}

function round_up($value, $precision) {
    $value = (float)$value;
	$precision = (int)$precision;
	
    if ($precision < 0) { 
        $precision = 0;
	}
	
    $decPointPosition = strpos($value, '.');
    if ($decPointPosition === false) { 
        return $value;
	}
	
    $floorValue = (float)substr($value, 0, $decPointPosition + $precision + 1);
	$followingDecimals = (int)substr($value, $decPointPosition + $precision + 1);
	
    if ($followingDecimals) {
        $ceilValue = $floorValue + pow(10, -$precision); // does this give always right result?
    } else{
        $ceilValue = $floorValue;
	}
	
	return $ceilValue;                
}

function numberFormat($number = 0, $use_decimals = false){
	if ($number < 0) {
		$number = 0;
	}

	if ($use_decimals) {
		return number_format(round_up($number, 2), 2, '.', '');
	} else{
		return number_format(ceil($number), 0, '.', '');
	}
}

function formatPrice($number = 0, $decimals = 0){
	if ($number < 0) {
		$number = 0;
	}

	$ci = & get_instance();
	$group_view_decimals = (int) $ci->session->userdata('group_view_decimals');

	if ($group_view_decimals) {
		return number_format(round_up($number, 2), 2, '.', '');
	} else{
		return number_format(ceil($number), 0, '.', '');
	}
}

function formatPriceInverse($number = 0, $decimals = 0){	
	return number_format(floor($number), $decimals, '.', '');
}

function getPriceColumn(){
	$ci = & get_instance();
	$group_price_variant = (int) $ci->session->userdata('group_price_variant');
	$price_array = array(
		'db_price_column' => 'item_price'
	);

	if($ci->lauth->logged_in() && $group_price_variant > 0){
		switch ($ci->session->userdata('group_price_type')) {
			default:
			case 'static':
				$ci->load->model("items/Prices_model", "prices");
				$static_price = $ci->prices->handler_get($group_price_variant);
				$price_array['db_price_column'] = 'item_price_'.$static_price['id_price_variant'];
			break;				
			case 'dynamic':
				$ci->load->model("items/Dynamic_prices_model", "dynamic_prices");
				$dynamic_price = $ci->dynamic_prices->handler_get($group_price_variant);
				$price_array['db_price_column'] = 'item_price_'.$dynamic_price['id_price_variant'];
			break;
		}
	}

	return $price_array;
}

function getPriceInverse($input_price = 0){
	$ci = & get_instance();
	$ci->load->model('currency/Currency_model', 'currency');
	$user_currency = $currency_default = $ci->currency->handler_get_default();
	$group_price_variant = (int) $ci->session->userdata('group_price_variant');
	$price_array = array(
		'db_price' => 0,
		'db_price_column' => 'item_price',
		'currency_code' => $user_currency['currency_code'],
		'currency_symbol' => $user_currency['currency_symbol'],
		'currency_rate' => $user_currency['currency_rate']
	);

	if(!$ci->lauth->logged_in() || $group_price_variant == 0){
		$user_currency = $ci->currency->handler_get_default_user();
		if($currency_default['id_currency'] != $user_currency['id_currency']){
			$price_array['currency_code'] = $user_currency['currency_code'];
			$price_array['currency_symbol'] = $user_currency['currency_symbol'];
			$price_array['currency_rate'] = $user_currency['currency_rate'];
		}
	} else{
		switch ($ci->session->userdata('group_price_type')) {
			default:
			case 'static':
				$ci->load->model("items/Prices_model", "prices");
				$static_price = $ci->prices->handler_get($group_price_variant);
				$price_array['db_price_column'] = 'item_price_'.$static_price['id_price_variant'];

				if($static_price['price_variant_currency'] != $currency_default['id_currency']){
					$user_currency = $ci->currency->handler_get($static_price['price_variant_currency']);
					$price_array['currency_code'] = $user_currency['currency_code'];
					$price_array['currency_symbol'] = $user_currency['currency_symbol'];
					$price_array['currency_rate'] = $user_currency['currency_rate'];
				}
			break;				
			case 'dynamic':
				$ci->load->model("items/Dynamic_prices_model", "dynamic_prices");
				$dynamic_price = $ci->dynamic_prices->handler_get($group_price_variant);
				$price_array['db_price_column'] = 'item_price_'.$dynamic_price['id_price_variant'];

				if($dynamic_price['id_price_currency'] != $currency_default['id_currency']){
					$user_currency = $ci->currency->handler_get($dynamic_price['id_price_currency']);
					$price_array['currency_code'] = $user_currency['currency_code'];
					$price_array['currency_symbol'] = $user_currency['currency_symbol'];
					$price_array['currency_rate'] = $user_currency['currency_rate'];
				}
			break;
		}
	}

	$price_array['db_price'] = formatPriceInverse($input_price / $price_array['currency_rate']);
	return $price_array;
}

function getPrice($item = array()){
	$ci = & get_instance();
	$ci->load->model('currency/Currency_model', 'currency');
	$user_currency = $currency_default = $ci->currency->handler_get_default();
	$group_price_variant = (int) $ci->session->userdata('group_price_variant');
	$group_view_decimals = (int) $ci->lauth->group_view_decimals();
	$compare_with_temp_price = $item['item_temp_price'] > 0 && $item['item_temp_price'] < $item['item_price'];
	$price_array = array(
		'display_price' => 0,
		'display_price_old' => 0,
		'currency_code' => $user_currency['currency_code'],
		'currency_symbol' => $user_currency['currency_symbol'],
		'currency_number' => $user_currency['currency_number'],
		'currency_rate' => $user_currency['currency_rate']
	);
	
	if(!$ci->lauth->logged_in() || $group_price_variant == 0){
		if($compare_with_temp_price){
			$price_array['display_price'] = $item['item_temp_price'];
			$price_array['display_price_old'] = $item['item_price'];
		} else{
			$price_array['display_price'] = $item['item_price'];
			$price_array['display_price_old'] = 0;
		}

		$user_currency = $ci->currency->handler_get_default_user();
		if($currency_default['id_currency'] != $user_currency['id_currency']){
			$price_array['currency_code'] = $user_currency['currency_code'];
			$price_array['currency_symbol'] = $user_currency['currency_symbol'];
			$price_array['currency_rate'] = $user_currency['currency_rate'];
		}
	} else{
		if($group_price_variant > 0){
			switch ($ci->session->userdata('group_price_type')) {
				default:
				case 'static':
					$ci->load->model("items/Prices_model", "prices");
					$static_price = $ci->prices->handler_get($group_price_variant);
					$item_price = $item['item_price_'.$static_price['id_price_variant']];

					if($static_price['price_variant_currency'] != $currency_default['id_currency']){
						$user_currency = $ci->currency->handler_get($static_price['price_variant_currency']);
						$price_array['currency_code'] = $user_currency['currency_code'];
						$price_array['currency_symbol'] = $user_currency['currency_symbol'];
						$price_array['currency_rate'] = $user_currency['currency_rate'];
					}
				break;				
				case 'dynamic':
					$ci->load->model("items/Dynamic_prices_model", "dynamic_prices");
					$dynamic_price = $ci->dynamic_prices->handler_get($group_price_variant);
					$item_price = $item['item_price_'.$dynamic_price['id_price_variant']] * (100 + $dynamic_price['dinamic_price_add']) / 100;

					if($dynamic_price['id_price_currency'] != $currency_default['id_currency']){
						$user_currency = $ci->currency->handler_get($dynamic_price['id_price_currency']);
						$price_array['currency_code'] = $user_currency['currency_code'];
						$price_array['currency_symbol'] = $user_currency['currency_symbol'];
						$price_array['currency_rate'] = $user_currency['currency_rate'];
					}
				break;
			}

			if($compare_with_temp_price && $item_price > 0 && $item['item_temp_price'] < $item_price){
				$price_array['display_price'] = $item['item_temp_price'];
				$price_array['display_price_old'] = $item_price;
			} else if($item_price > $item['item_price']){
				$price_array['display_price'] = $item['item_price'];
				$price_array['display_price_old'] = 0;
			} else{
				$price_array['display_price'] = $item_price;
				$price_array['display_price_old'] = 0;
			}
		}
	}

	$display_price = $price_array['display_price'] * $price_array['currency_rate'];
	$display_price = numberFormat($display_price, $group_view_decimals);
	$display_price_old = numberFormat($price_array['display_price_old'] * $price_array['currency_rate'], $group_view_decimals);
	if($group_view_decimals == 0){		
		$display_price_divider = 1;
		$display_price_divider_minus = 1;
		$display_price_old_divider_minus = 1;
		if($display_price > 10000){
			$display_price_divider = 100;
		} elseif($display_price > 5000){
			$display_price_divider = 50;
		} elseif($display_price > 500){
			$display_price_divider = 10;
		} else{
			$display_price_divider_minus = 0;
		}
	
		$display_price_old_divider = 1;
		if($display_price_old > 10000){
			$display_price_old_divider = 100;
		} elseif($display_price_old > 5000){
			$display_price_old_divider = 50;
		} elseif($display_price_old > 500){
			$display_price_old_divider = 10;
		} else{
			$display_price_old_divider_minus = 0;
		}
	
		$display_price = numberFormat(ceil($display_price/$display_price_divider) * $display_price_divider - $display_price_divider_minus);
		$display_price_old = numberFormat(ceil($display_price_old/$display_price_old_divider) * $display_price_old_divider - $display_price_old_divider_minus);
	}

	$price_array['display_price'] = $display_price;
	$price_array['display_price_old'] = $display_price_old;

	return $price_array;
}

function getOrderedPrice($item){
	$ci = & get_instance();
	$ci->load->model('currency/Currency_model', 'currency');
	$user_currency = $currency_default = $ci->currency->handler_get_default();
	$group_price_variant = (int) $ci->session->userdata('group_price_variant');
	$group_view_decimals = (int) $ci->lauth->group_view_decimals();
	$price_array = array(
		'display_price' => $item['item_price'],
		'currency_code' => $user_currency['currency_code'],
		'currency_symbol' => $user_currency['currency_symbol'],
		'currency_rate' => $user_currency['currency_rate']
	);

	if(!$ci->lauth->logged_in() || $group_price_variant == 0){
		$user_currency = $ci->currency->handler_get_default_user();
		if($currency_default['id_currency'] != $user_currency['id_currency']){
			$price_array['currency_code'] = $user_currency['currency_code'];
			$price_array['currency_symbol'] = $user_currency['currency_symbol'];
			$price_array['currency_rate'] = $user_currency['currency_rate'];
		}
	} else{
		if($group_price_variant > 0){
			switch ($ci->session->userdata('group_price_type')) {
				default:
				case 'static':
					$ci->load->model("items/Prices_model", "prices");
					$static_price = $ci->prices->handler_get($group_price_variant);
					if($static_price['price_variant_currency'] != $currency_default['id_currency']){
						$user_currency = $ci->currency->handler_get($static_price['price_variant_currency']);
						$price_array['currency_code'] = $user_currency['currency_code'];
						$price_array['currency_symbol'] = $user_currency['currency_symbol'];
						$price_array['currency_rate'] = $user_currency['currency_rate'];
					}
				break;				
				case 'dynamic':
					$ci->load->model("items/Dynamic_prices_model", "dynamic_prices");
					$dynamic_price = $ci->dynamic_prices->handler_get($group_price_variant);
					if($dynamic_price['id_price_currency'] != $currency_default['id_currency']){
						$user_currency = $ci->currency->handler_get($dynamic_price['id_price_currency']);
						$price_array['currency_code'] = $user_currency['currency_code'];
						$price_array['currency_symbol'] = $user_currency['currency_symbol'];
						$price_array['currency_rate'] = $user_currency['currency_rate'];
					}
				break;
			}
		}
	}

	$display_price = numberFormat($price_array['display_price'], $group_view_decimals);
	if(!$group_view_decimals){		
		$display_price_divider = 1;
		if($display_price > 10000){
			$display_price_divider = 100;
		} elseif($display_price > 5000){
			$display_price_divider = 50;
		} elseif($display_price > 500){
			$display_price_divider = 10;
		}
	
		$display_price = numberFormat(ceil($display_price/$display_price_divider) * $display_price_divider - 1);
	}

	$price_array['display_price'] = $display_price;
	
	return $price_array;
}

function displayPrice($input_price = 0, $type = 'ceil'){
	$ci = & get_instance();
	$ci->load->model('currency/Currency_model', 'currency');
	$user_currency = $currency_default = $ci->currency->handler_get_default();
	$group_view_decimals = $ci->lauth->group_view_decimals();
	$group_price_variant = (int) $ci->session->userdata('group_price_variant');
	$price_array = array(
		'price' => 0,
		'currency_code' => $user_currency['currency_code'],
		'currency_symbol' => $user_currency['currency_symbol'],
		'currency_rate' => $user_currency['currency_rate']
	);

	if(!$ci->lauth->logged_in() || $group_price_variant == 0){
		$user_currency = $ci->currency->handler_get_default_user();
		if($currency_default['id_currency'] != $user_currency['id_currency']){
			$price_array['currency_code'] = $user_currency['currency_code'];
			$price_array['currency_symbol'] = $user_currency['currency_symbol'];
			$price_array['currency_rate'] = $user_currency['currency_rate'];
		}
	} else{
		switch ($ci->session->userdata('group_price_type')) {
			default:
			case 'static':
				$ci->load->model("items/Prices_model", "prices");
				$static_price = $ci->prices->handler_get($group_price_variant);

				if($static_price['price_variant_currency'] != $currency_default['id_currency']){
					$user_currency = $ci->currency->handler_get($static_price['price_variant_currency']);
					$price_array['currency_code'] = $user_currency['currency_code'];
					$price_array['currency_symbol'] = $user_currency['currency_symbol'];
					$price_array['currency_rate'] = $user_currency['currency_rate'];
				}
			break;				
			case 'dynamic':
				$ci->load->model("items/Dynamic_prices_model", "dynamic_prices");
				$dynamic_price = $ci->dynamic_prices->handler_get($group_price_variant);

				if($dynamic_price['id_price_currency'] != $currency_default['id_currency']){
					$user_currency = $ci->currency->handler_get($dynamic_price['id_price_currency']);
					$price_array['currency_code'] = $user_currency['currency_code'];
					$price_array['currency_symbol'] = $user_currency['currency_symbol'];
					$price_array['currency_rate'] = $user_currency['currency_rate'];
				}
			break;
		}
	}

	switch ($type) {
		default:
		case 'ceil':
			$display_price = numberFormat($input_price * $price_array['currency_rate']);	
		break;
		case 'floor':
			$display_price = numberFormat($input_price * $price_array['currency_rate']);	
		break;		
	}
	
	if(!$group_view_decimals){
		$display_price_divider = 1;
		if($display_price > 10000){
			$display_price_divider = 100;
		} elseif($display_price > 5000){
			$display_price_divider = 50;
		} elseif($display_price > 500){
			$display_price_divider = 10;
		}

		$display_price = numberFormat($display_price/$display_price_divider) * $display_price_divider - 1;
	}

	if($display_price < 0){
		$display_price = 0;
	}

	$price_array['price'] = $display_price;
	return $price_array;
}

function formatDate($date, $format = "d/m/Y H:i:s"){
    $str_to_time = strtotime($date);
	if($str_to_time && $date != '0000-00-00 00:00:00'){
	   return date($format, $str_to_time);

	} else{
		return '&mdash;';
	} 
}

function is_newest_item($check_date, $settings_newest_days){
	$newest_end_date = date('Y-m-d', strtotime($check_date . "+ {$settings_newest_days}days"));
	$today_date = date('Y-m-d');
	if($newest_end_date >= $today_date){
		return true;
	} else{
		return false;
	}
}

function date_difference($date_from = '', $date_to = '', $format='days'){
	if($date_from == '' || $date_to == ''){
		return 0;
	}

    $datetime_from = strtotime($date_from);
    $datetime_to = strtotime($date_to);
	$total_sec = $datetime_from - $datetime_to;
	switch ($format) {
		case 'days':
		default:
			return ceil($total_sec / 86400);
		break;
	}
}

function date_plus($time_plus, $time_type='days', $from_date=false){
	if(!$from_date)
		$from_date = date('Y-m-d H:i:s');

	$result_date = $from_date;
	switch($time_type){
		default:
		case 'days':
			if($time_plus > 0){
				$result_date = date('Y-m-d H:i:s', strtotime($result_date . '+ '.$time_plus.' days'));
			} elseif($time_plus < 0){
				$result_date = date('Y-m-d H:i:s', strtotime($result_date . '- '.abs($time_plus).' days'));
			}
		break;
	}

	return $result_date;
}

function arrayByKey($arrays, $key, $multiple = false){
	$rez = array();
	if(empty($arrays)){
		return $rez;
	}
	
    foreach($arrays as $one){
		if(isset($one[$key])){
			if($multiple){
				$rez[$one[$key]][] = $one;
			} else{
				$rez[$one[$key]] = $one;
			}
		}
    }
    return $rez;
}

function generate_video_html($id, $w, $h, $autoplay = 0){
	if(empty($id)) return false;

	return  '<iframe class="player bd-none" width="'.$w.'" height="'.$h.'" src="//www.youtube.com/embed/'.$id.'?autoplay='.$autoplay.'" allowfullscreen></iframe>';
}

function getYoutubeID($video){
	if(	preg_match('/[http|https]+:\/\/(?:www\.|)youtube\.com\/watch\?(?:.*)?v=([a-zA-Z0-9_\-]+)/i', $video, $matches) || 
		preg_match('/[http|https]+:\/\/(?:www\.|)youtube\.com\/embed\/([a-zA-Z0-9_\-]+)/i', $video, $matches) || 
		preg_match('/[http|https]+:\/\/(?:www\.|)youtu\.be\/([a-zA-Z0-9_\-]+)/i', $video, $matches))
	{ 
		return $matches[1];
	}
}

function messageInModal($message, $type = 'errors'){
    $CI = &get_instance();
	$data['message'] = $message;
	$data['type'] = $type;
	echo $CI->load->view($this->theme->public_view('includes/modal_systmess_view'), $data, true);
	exit();
}

function jsonResponse($message = '',$type = 'error', $additional_params = array()){
    $resp = $additional_params;
    $resp['mess_type'] = $type;
    $resp['mess_class'] = $type;
    $resp['message'] = $message;

    echo json_encode($resp);
    exit();
}

function jsonDTResponse($message = '',$additional_params = array(), $type = 'error'){
    $output = array(
		"iTotalRecords" => 0,
		"iTotalDisplayRecords" => 0,
		"aaData" => array()
    );

    $output = array_merge($output, $additional_params);
    $output['mess_type'] = $type;
    $output['message'] = $message;

    echo json_encode($output);
    exit();
}

function id_from_link($str, $to_int = true){
    $segments  = explode('-', $str);
	
	if($to_int){
    	return (int)end($segments);
	} else{
    	return xss_clean(end($segments));
	}
}

function lang_title($str='no_title'){
    $CI = get_instance();
	echo $CI->lang->line($str);
}

function lang_line($str='no_title'){
    $CI = get_instance();
	return $CI->lang->line($str);
}

function remove_dir($dir,$action = 'delete') {
	if(empty($dir))
		return false;
	
	if(!is_dir($dir))
		return false;

	foreach(glob($dir . '/*') as $file) {
		@unlink($file);
	}
	
	if($action == 'delete')
		rmdir($dir);
}

function create_dir($dir, $mode = 0755){
    if(!is_dir($dir))
        mkdir ($dir, $mode, true);
}

function checkURI($uri_assoc = array(), $available_uri_segments = array(), $controller_data = array()){
	if(empty($uri_assoc) && empty($available_uri_segments)){
		return true;
	}

	foreach($uri_assoc as $key_uri => $value_uri){
		if(!in_array($key_uri, $available_uri_segments)){
			return_404($controller_data);
		}

		if(empty($value_uri)){
			return_404($controller_data);
		}
	}
}

function arrayToGET($array, $keys = null, $type = 'except'){
	$array = clearArray($array);
	$keys = explode(',', $keys);
	
	if(!count($array))
		return '';
	
	foreach($array as $key=>$value){
		if($type == 'except'){
			if(!in_array($key, $keys))
				$el[] = $key . '=' . $value;
		}elseif($type == 'only'){
			if(in_array($key, $keys))
				$el[] = $key . '=' . $value;
		}
	}	
	return '?' . implode('&', $el);
}

function clearArray($array){
	foreach($array as $key => $value){
		$val = trim($value);
		if(empty($val))
			unset($array[$key]);	
	}
	return $array;
}

function cut_str($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	return mb_substr($trimmed_str, 0, $maxlength, "utf-8");
}

function text_elipsis($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	if(mb_strlen($trimmed_str) > $maxlength){
		return mb_substr($trimmed_str, 0, $maxlength).'...';
	}

	return $trimmed_str;
}

function get_order_status($order_status = '1'){
	$order_statuses = get_order_statuses();

	if(array_key_exists($order_status, $order_statuses)){
		return $order_statuses[$order_status];
	}
	
	$status = array(
		'title' => 'Нейзвестный',
		'color_class' => 'default'
	);

	return $status;
}

function get_order_processing_statuses(){
	return array_filter(
		get_order_statuses(),
		function($status){
			return $status['processing'] === true;
		}
	);
}

function get_order_final_status(){
	return array_filter(
		get_order_statuses(),
		function($status){
			return $status['is_final'] === true;
		}
	);
}

function get_order_statuses(){
	$order_statuses = array(
		'1' => array(
			'title' => 'Nou',
			'color_class' => 'success',
			'icon' => 'fad fa-flag',
			'processing' => true,
			'is_payable' => true,
			'is_final' => false,
			'require_1c_id' => false
		),
		'2' => array(
			'title' => 'În procesare',
			'color_class' => 'warning',
			'icon' => 'fad fa-hourglass-half',
			'processing' => true,
			'is_payable' => true,
			'is_final' => false,
			'require_1c_id' => false
		),
		'3' => array(
			'title' => 'Așteaptă achitarea',
			'color_class' => 'warning',
			'icon' => 'fad fa-comments-alt-dollar',
			'processing' => true,
			'is_payable' => true,
			'is_final' => false,
			'require_1c_id' => false
		),
		'4' => array(
			'title' => 'Completat',
			'color_class' => 'primary',
			'icon' => 'fad fa-box-check',
			'processing' => false,
			'is_payable' => false,
			'is_final' => true,
			'require_1c_id' => false
		),
		'5' => array(
			'title' => 'Anulat',
			'color_class' => 'danger',
			'icon' => 'fad fa-window-close',
			'processing' => false,
			'is_payable' => false,
			'is_final' => false,
			'require_1c_id' => false
		)
	);

	return $order_statuses;
}

function get_keywords_combination($keywords = ''){
	$keywords = trim($keywords);
	$result = array();
	if(empty($keywords)){
		return $result;
	}
	
	if(strlen($keywords) <= 3){
		return '';
	}

	$words = explode(' ', $keywords);
	$total_words = count($words);
	
	if($total_words > 3){
		foreach ($words as $word) {
			$trimed_word = trim($word);
			$result[] = "+{$trimed_word}";
		}
		
		return implode(' ', $result);
	} else{
		switch($total_words){
			case 1:
				return "+{$words[0]}";
			break;
			case 2:
				if(strlen($words[0]) <= 3 || strlen($words[1]) <= 3){				
					return "+({$words[0]} {$words[1]})";
				} else{
					return "+{$words[0]} +{$words[1]}";
				}
			break;
			case 3:
				if(strlen($words[0]) <= 3 || strlen($words[1]) <= 3){
					if(strlen($words[2]) <= 3){
						return "+({$words[0]} {$words[1]} {$words[2]})";
					} else{
						return "+({$words[0]} {$words[1]}) +{$words[2]}";
					}			
				} elseif(strlen($words[1]) <= 3 || strlen($words[2]) <= 3){
					if(strlen($words[2]) <= 3){
						return "+{$words[0]} +(>{$words[1]} >{$words[2]})";
					} else{
						return "+{$words[0]} +>{$words[1]} +>{$words[2]}";
					}
				} else{
					return "+{$words[0]} +{$words[1]} +{$words[2]}";
				}
			break;
		}
	}	
}
           
function write_log($type = 'error', $msg)
{
	$config =& get_config();
	$log_path = ($config['log_path'] != '') ? $config['log_path'] : APPPATH.'logs/';
	$type = strtoupper($type);
	$filepath = $log_path.'log-'.$type.'.php';
	$message  = '';
	
	if ( ! file_exists($filepath))
	{
		$message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
	}
		
	if ( ! $fp = @fopen($filepath, FOPEN_WRITE_CREATE))
	{
		return FALSE;
	}

	$message .= $type.' - '.date('d/m/Y H:i:s'). ' '.$msg."\n";
	
	flock($fp, LOCK_EX);    
	fwrite($fp, $message);
	flock($fp, LOCK_UN);
	fclose($fp);

	@chmod($filepath, FILE_WRITE_MODE);         
	return TRUE;
}

// a function for comparing two float numbers
// float 1 - The first number
// float 2 - The number to compare against the first
// operator - The operator. Valid options are =, <=, <, >=, >, <>, eq, lt, lte, gt, gte, ne
function compare_float_numbers($float1, $float2, $operator="=")
{
	// Check numbers to 5 digits of precision
	$epsilon = 0.01;

	$float1 = (float)$float1;
	$float2 = (float)$float2;

	switch ($operator)
	{
		// equal
		case "=":
		case "eq":
		{
			if (abs($float1 - $float2) < $epsilon) {
				return true;
			}
			break;
		}
		// less than
		case "<":
		case "lt":
		{
			if (abs($float1 - $float2) < $epsilon) {
				return false;
			} else {
				if ($float1 < $float2) {
					return true;
				}
			}
			break;
		}
		// less than or equal
		case "<=":
		case "lte":
		{
			if (compare_float_numbers($float1, $float2, '<') || compare_float_numbers($float1, $float2, '=')) {
				return true;
			}
			break;
		}
		// greater than
		case ">":
		case "gt":
		{
			if (abs($float1 - $float2) < $epsilon) {
				return false;
			}
			else
			{
				if ($float1 > $float2) {
					return true;
				}
			}
			break;
		}
		// greater than or equal
		case ">=":
		case "gte":
		{
			if (compare_float_numbers($float1, $float2, '>') || compare_float_numbers($float1, $float2, '=')) {
				return true;
			}
			break;
		}
		case "<>":
		case "!=":
		case "ne":
		{
			if (abs($float1 - $float2) > $epsilon) {
				return true;
			}
			break;
		}
		default:
		{
			die("Unknown operator '".$operator."' in compareFloatNumbers()");
		}
	}

	return false;
}

function plural_numword($num = 0, $singular_form = '', $plural_forms = '', $lang = 'ru'){
	if($num == 0){
		return $singular_form;
	}

	if(empty($plural_forms)){
		return $singular_form;
	}

	switch ($lang) {
		default:
		case 'ru':
			$cases = array (2, 0, 1, 1, 1, 2);
			$word = array(
				$singular_form, 
				(!empty($plural_forms['2_4']))?$plural_forms['2_4']:$singular_form, 
				(!empty($plural_forms['5_']))?$plural_forms['5_']:$singular_form
			);

			return $word[ ($num%100 > 4 && $num %100 < 20) ? 2 : $cases[min($num%10, 5)] ];
		break;
	}
}

function dt_ordering($source, array $map = array(), \Closure $transformer = null)
{
	$sorting_columns_amount = isset($source['iSortingCols']) ? (int) $source['iSortingCols'] : 0;
	if($sorting_columns_amount <= 0) {
		return array();
	}

	$ordering = array();
	for ($i = 0; $i < $sorting_columns_amount; $i++) {
		$column_direction_key = "sSortDir_{$i}";
		$column_index_key = "iSortCol_{$i}";
		if(
			!isset($source[$column_direction_key]) ||
			!isset($source[$column_index_key])
		) {
			continue;
		}

		$column_direction = mb_strtolower($source[$column_direction_key]);
		$column_index = (int) $source[$column_index_key];
		$column_key = "mDataProp_{$column_index}";
		if(
			!isset($source[$column_key]) ||
			!in_array($column_direction, array('asc', 'desc'), true)
		) {
			continue;
		}

		$column_alias = $source[$column_key];
		if(!isset($map[$column_alias])) {
			continue;
		}

		$ordering[] = array(
			'column'    => $map[$column_alias],
			'direction' => $column_direction,
		);
	}

	return $transformer instanceof \Closure ? array_map($transformer, $ordering) : $ordering;
}

function flat_dt_ordering($source, array $map = array())
{
	return dt_ordering($source, $map, function ($order) {
		return "{$order['column']}-{$order['direction']}";
	});
}

function validateDate($date, $format = "Y-m-d H:i:s"){
    $d = date_create_from_format($format, $date);
    return $d && date_format($d, $format) == $date;
}

function getDateFormat($date, $format = "Y-m-d H:i:s", $return_format = 'j M, Y H:i'){
    $d = date_create_from_format($format, $date);
	if($d && date_format($d, $format) == $date) {
    	return $d->format($return_format);
    }
}

function minus_discount($amount = 0, $discount = 0){
	$discount_amount = $amount - $amount*$discount/100;
	return numberFormat($discount_amount, true);
}

function first_array_element($array)
{
    return reset($array);
}

function first_array_key($array)
{
    return key($array);
}

function encript_uri_hash($str = ""){
	$str = base64_encode($str);
	$str = str_replace('=', '-', $str);
	return $str;
}

function decript_uri_hash($str = ""){
	$str = str_replace('-', '=', $str);
	$str = base64_decode($str);
	return $str;
}

function strForURL($str, $delimeter = '-', $lower = true){
	$rules = array(
		'Any-Latin;',
		'NFD;',
		'[:Nonspacing Mark:] Remove;',
		'NFC;',
		'[:Punctuation:] Remove;'
	);

	if($lower){
		$rules[] = 'Lower();';
	}

	$str = transliterator_transliterate(implode('', $rules), $str);
	$str = preg_replace('/[^a-zA-Z0-9\\/\_\ \-]/', '', $str);
    $str = preg_replace('/[-\s]+/', $delimeter, $str);
    return trim($str,$delimeter);
}

function translit($str, $delimeter = ' ', $lower = true){
	$rules = array(
		'Any-Latin;',
		'NFD;',
		'[:Nonspacing Mark:] Remove;',
		'NFC;',
		'[:Punctuation:] Remove;'
	);

	if($lower){
		$rules[] = 'Lower();';
	}

	$str = transliterator_transliterate(implode('', $rules), $str);
	$str = preg_replace('/[^a-zA-Z0-9\\/_|+ -]/', '', $str);
    $str = preg_replace('/[-\s]+/', $delimeter, $str);
    return trim($str,$delimeter);
}

function check_valid_colorhex($colorCode) {
    $colorCode = ltrim($colorCode, '#');
	
	return ctype_xdigit($colorCode) && (strlen($colorCode) == 6 || strlen($colorCode) == 3);
}

function getHash($str = null, $split_length = 8){
	$token = hash('sha512', $str);

	if ($split_length !== false) {
		$token = implode('-', str_split($token, $split_length));
	}

	return $token;
}

function uniqueId($prefix = '', $lenght = 13) {
    // uniqid gives 13 chars, but you could adjust it to your needs.
    if (function_exists("random_bytes")) {
        $bytes = random_bytes(ceil($lenght / 2));
    } elseif (function_exists("openssl_random_pseudo_bytes")) {
        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
    } else {
        throw new Exception("no cryptographically secure random function available");
    }
    return $prefix . substr(bin2hex($bytes), 0, $lenght);
}