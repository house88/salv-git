<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Theme
{
	private $ci;

	public function __construct(){
		$this->ci =& get_instance();
	}
	
	function apanel_assets($path = null){
		return !empty($path) ? $this->ci->config->item('apanel_assets') . $path : 'The path: ' . $path . ' does not exist.';
	} 
	
	function apanel_view($path = null){
		return !empty($path) ? $this->ci->config->item('apanel_theme') . $path : 'The path: ' . $path . ' does not exist.';
	} 
	
	function public_assets($path = null){
		return !empty($path) ? $this->ci->config->item('public_assets') . $path : 'The path: ' . $path . ' does not exist.';
	} 
	
	function public_view($path = null){
		return !empty($path) ? $this->ci->config->item('public_theme') . $path : 'The path: ' . $path . ' does not exist.';
	}
}
