<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Панель управления</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- ca-icons -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/ca-icons/style.css'));?>">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/fontawesome-pro/css/all.min.css'));?>">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/ionicons/2.0.1/css/ionicons.min.css'));?>">
<!-- Tempusdominus Bbootstrap 4 -->
<link href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/bootstrap-dialog/bootstrap-dialog.css'));?>" rel="stylesheet">
<!-- iCheck -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/icheck-bootstrap/icheck-bootstrap.min.css'));?>">

<!-- Theme style -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('css/adminlte.min.css'));?>">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/overlayScrollbars/css/OverlayScrollbars.min.css'));?>">
<!-- Daterange picker -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/daterangepicker/daterangepicker.css'));?>">
<!-- Toastr -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/toastr/toastr.min.css'));?>">
<!--BOOTSTRAP SELECT-->
<link href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/bootstrap-select/css/bootstrap-select.css'));?>" rel="stylesheet">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/icheck-bootstrap/icheck-bootstrap.min.css'));?>">
<!-- Style -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('css/style.css'));?>">
<!-- Sizes -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('css/custom_sizes.css'));?>">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo file_modification_time($this->theme->apanel_assets('favicon/apple-touch-icon.png'));?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo file_modification_time($this->theme->apanel_assets('favicon/favicon-32x32.png'));?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo file_modification_time($this->theme->apanel_assets('favicon/favicon-16x16.png'));?>">
<link rel="manifest" href="<?php echo file_modification_time($this->theme->apanel_assets('favicon/site.webmanifest'));?>">
<link rel="mask-icon" href="<?php echo file_modification_time($this->theme->apanel_assets('favicon/safari-pinned-tab.svg'));?>" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<script>
    var base_url = '<?php echo base_url();?>';
    var site_url = '<?php echo site_url();?>';
</script>