<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/categories/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'categories';

		$this->data = array();
		
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("categories_model", "categories");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		checkPermision('manage_categories');

		$categories = $this->categories->handler_get_tree();
		$categories_list = arrayByKey($this->categories->handler_get_all(), 'category_id');
		$this->data['categories'] = $this->tree_table($categories, 0, 0, $categories_list);

		$this->data['page_header'] = 'Категории каталога';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_categories');
				
				$categories = $this->categories->handler_get_tree(array(
					'category_children_title' => true
				));
				$this->data['categories'] = $this->categories->tree_select($categories, 0);

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_categories');

				$id_category = (int) $this->uri->segment(5);
				$this->data['category'] = $this->categories->handler_get($id_category);
				if(empty($this->data['category'])){
					jsonResponse('Категория не найдена.');
				}

				if(empty($this->data['category']['category_aditional_fields'])){
					$this->data['parent_aditional_fields'] = $this->categories->handler_get_additional_fields($this->data['category']);
				}
				
				$categories = $this->categories->handler_get_tree(array(
					'category_children_title' => true
				));
				$this->data['categories'] = $this->categories->tree_select($categories, 0, array($this->data['category']['category_parent']));

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'product_options':
				checkPermisionAjax('manage_categories');

				$id_category = (int) $this->uri->segment(5);
				$this->data['category'] = $this->categories->handler_get($id_category);
				if(empty($this->data['category'])){
					jsonResponse('Категория не найдена.');
				}

				$category_aditional_fields = json_decode($this->data['category']['category_aditional_fields'], true);

				$category_aditional_fields = array_filter(array_map(function($option){
					if($option['active'] === 1){
						return $option;
					}
				}, $category_aditional_fields));

				if(empty($category_aditional_fields)){
					$this->data['parent_aditional_fields'] = $this->categories->handler_get_additional_fields($this->data['category']);
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'product_options_form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_categories');

				$validator_rules = array(
					array(
						'field' => 'category_parent',
						'label' => 'Родитель',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'title',
						'label' => 'Название категорий',
						'rules' => 'required|xss_clean|max_length[50]',
					),
					array(
						'field' => 'site_title',
						'label' => 'Название категорий на сайте',
						'rules' => 'required|xss_clean|max_length[50]',
					),
					array(
						'field' => 'mk',
						'label' => 'Meta keywords',
						'rules' => 'xss_clean',
					),
					array(
						'field' => 'md',
						'label' => 'Meta description',
						'rules' => 'xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$id_parent = (int)$this->input->post('category_parent');
				if($id_parent && !empty($category_parent = $this->categories->handler_get($id_parent))){
					$parent_unique_id = $category_parent['category_prog_id'];
				}else{
					$parent_unique_id = uniqid('p');
				}

				$category_unique_id = uniqid('c');

				$insert = array(
					'category_prog_id' => $category_unique_id,
					'category_parent' => $id_parent,
					'category_prog_parent_id' => $parent_unique_id,
					'category_title' => $this->input->post('title'),
					'category_site_title' => $this->input->post('site_title'),
					'category_seo' => $this->input->post('seo_text'),
					'category_mk' => $this->input->post('mk'),
					'category_md' => $this->input->post('md'),
					'category_is_oversized' => $category_parent['category_is_oversized'] ?? 0
				);

				$edit_item = $this->input->post('edit_item');
				$price = $this->input->post('price');
				$aditional_fields = array(
					'item_photo' => array(
						'active' => (isset($edit_item['item_photo']))?1:0,
						'price' => (isset($edit_item['item_photo']))?floatval($price['item_photo']):0
					),
					'item_photos' => array(
						'active' => (isset($edit_item['item_photos']))?1:0,
						'price' => (isset($edit_item['item_photos']))?floatval($price['item_photos']):0,
						'count' => (isset($edit_item['item_photos_count']))?floatval($edit_item['item_photos_count']):0
					),
					'small_description' => array(
						'active' => (isset($edit_item['small_description']))?1:0,
						'price' => (isset($edit_item['small_description']))?floatval($price['small_description']):0
					),
					'full_description' => array(
						'active' => (isset($edit_item['full_description']))?1:0,
						'price' => (isset($edit_item['full_description']))?floatval($price['full_description']):0
					),
					'video' => array(
						'active' => (isset($edit_item['video']))?1:0,
						'price' => (isset($edit_item['video']))?floatval($price['video']):0
					),
					'properties' => array(
						'active' => (isset($edit_item['properties']))?1:0,
						'price' => (isset($edit_item['properties']))?floatval($price['properties']):0
					),
					'mk' => array(
						'active' => (isset($edit_item['mk']))?1:0,
						'price' => (isset($edit_item['mk']))?floatval($price['mk']):0
					),
					'md' => array(
						'active' => (isset($edit_item['md']))?1:0,
						'price' => (isset($edit_item['md']))?floatval($price['md']):0
					),
				);
				$insert['category_aditional_fields'] = json_encode($aditional_fields);
				$id_category = $this->categories->handler_insert($insert);

				$config = array(
					'table' => 'categories',
					'id' => 'category_prog_parent_id',
					'field' => 'category_url',
					'title' => 'category_title',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'category_url' => $this->slug->create_slug($insert['category_title'].'-'.$id_category)
				);
				$this->categories->handler_update($id_category, $update);
				$this->categories->handler_actualize_all();
				jsonResponse('Сохранено','success');
			break;
			case 'edit':
				checkPermisionAjax('manage_categories');

				$validator_rules = array(
					array(
						'field' => 'category_parent',
						'label' => 'Родитель',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'title',
						'label' => 'Название категорий',
						'rules' => 'required|xss_clean|max_length[50]',
					),
					array(
						'field' => 'site_title',
						'label' => 'Название категорий на сайте',
						'rules' => 'required|xss_clean|max_length[50]',
					),
					array(
						'field' => 'mk',
						'label' => 'Meta keywords',
						'rules' => 'xss_clean',
					),
					array(
						'field' => 'md',
						'label' => 'Meta description',
						'rules' => 'xss_clean',
					),
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$current_edited_category_id = (int) $this->input->post('category');
				$current_edited_category = $this->categories->handler_get($current_edited_category_id);
				if(empty($current_edited_category)){
					jsonResponse('Редактируемая категория не была обнаружена. Обновите страницу и повторите попытку.');
				}

				$is_oversized_input_value = $this->input->post('is_oversized') ? 1 : 0;
				$was_changed_param_oversized = $is_oversized_input_value != $current_edited_category['category_is_oversized'];
				
				$id_parent = (int) $this->input->post('category_parent');
				if($id_parent && !empty($category_parent = $this->categories->handler_get($id_parent))){
					$parent_unique_id = $category_parent['category_prog_parent_id'];
				}else{
					$parent_unique_id = uniqid('p');
				}

				$update = array(
					'category_parent' => (int)$this->input->post('category_parent'),
					'category_prog_parent_id' => $parent_unique_id,
					'category_title' => $this->input->post('title'),
					'category_site_title' => $this->input->post('site_title'),
					'category_url' => strForURL($this->input->post('title') . ' ' . $current_edited_category_id),
					'category_special_url' => $this->input->post('special_url', true),
					'category_seo' => $this->input->post('seo_text'),
					'category_mk' => $this->input->post('mk'),
					'category_md' => $this->input->post('md'),
					'category_is_oversized' => $is_oversized_input_value
				);
				
				$this->categories->handler_update($current_edited_category_id, $update);
				$this->categories->handler_actualize_all();

				if ($was_changed_param_oversized && !empty($current_edited_category['category_children'])) {
					$children_list = explode(',', $current_edited_category['category_children']);
					$this->categories->handler_update_same_data(array('id_category' => $children_list), array('category_is_oversized' => $is_oversized_input_value));
				}
				
				jsonResponse('Сохранено','success');
			break;
			case 'product_options':
				checkPermisionAjax('manage_categories');

				$validator_rules = array(
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$category = (int) $this->input->post('category');
				$edit_item = $this->input->post('edit_item');
				$price = $this->input->post('price');
				$aditional_fields = array(
					'item_photo' => array(
						'active' => isset($edit_item['item_photo'])?1:0,
						'price' => isset($edit_item['item_photo']) ? numberFormat(floatval($price['item_photo']), true) : 0
					),
					'item_photos' => array(
						'active' => isset($edit_item['item_photos_count']) && (int) $edit_item['item_photos_count'] > 0 ? 1 : 0,
						'price' => isset($edit_item['item_photos_count']) ? numberFormat(floatval($price['item_photos']), true) : 0,
						'count' => isset($edit_item['item_photos_count']) ? (int) $edit_item['item_photos_count'] : 0
					),
					'small_description' => array(
						'active' => (isset($edit_item['small_description']))?1:0,
						'price' => (isset($edit_item['small_description']))?numberFormat(floatval($price['small_description']), true):0
					),
					'full_description' => array(
						'active' => (isset($edit_item['full_description']))?1:0,
						'price' => (isset($edit_item['full_description']))?numberFormat(floatval($price['full_description']), true):0
					),
					'video' => array(
						'active' => (isset($edit_item['video']))?1:0,
						'price' => (isset($edit_item['video']))?numberFormat(floatval($price['video']), true):0
					),
					'properties' => array(
						'active' => (isset($edit_item['properties']))?1:0,
						'price' => (isset($edit_item['properties']))?numberFormat(floatval($price['properties']), true):0
					),
					'mk' => array(
						'active' => (isset($edit_item['mk']))?1:0,
						'price' => (isset($edit_item['mk']))?numberFormat(floatval($price['mk']), true):0
					),
					'md' => array(
						'active' => (isset($edit_item['md']))?1:0,
						'price' => (isset($edit_item['md']))?numberFormat(floatval($price['md']), true):0
					),
				);

				$aditional_fields = array_filter(array_map(function($option){
					if($option['active'] === 1){
						return $option;
					}
				}, $aditional_fields));
				
				$this->categories->handler_update($category, array(
					'category_aditional_fields' => json_encode($aditional_fields),
					'category_aditional_fields_children' => ($this->input->post('category_aditional_fields_children')) ? 1 : 0
				));
				$this->categories->handler_actualize_all();
				
				jsonResponse('Сохранено','success');
			break;
			case 'delete':
				if(!$this->lauth->have_right('manage_categories')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$validator_rules = array(
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$category_id = (int)$this->input->post('category');
				$category = $this->categories->handler_get($category_id);
				if(empty($category)){
					jsonResponse('Категория не найдена.');
				}
				
				if($this->categories->handler_count_subcategories($category_id)){
					jsonResponse('Категория содержит подкатегорий. Удалите сначало все подкатегорий.');
				}
				
				$this->categories->handler_delete($category_id);
				
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_categories');

				$validator_rules = array(
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$category_id = (int) $this->input->post('category');
				$category = $this->categories->handler_get($category_id);
				if(empty($category)){
					jsonResponse('Категория не найдена.');
				}
				
				$is_visible = (int) $category['category_active'] === 1 ? 0 : 1;
				$this->categories->handler_update($category_id, array(
					'category_active' => $is_visible
				));
				
				jsonResponse('Сохранено', 'success', array('status' => $is_visible));
			break;
			case 'change_weight':
				checkPermisionAjax('manage_categories');

				$validator_rules = array(
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'order_num',
						'label' => 'Вес',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}
				
				$category_id = (int)$this->input->post('category');
				$category = $this->categories->handler_get($category_id);
				if(empty($category)){
					jsonResponse('Категория не найдена.');
				}

				$order_num = (int)$this->input->post('order_num');
				$category_breadcrumbs = json_decode('['.$category['category_breadcrumbs'].']', true);
				if($category_breadcrumbs[0]['category_id'] == $category_id){
					$category_parent_weight = $order_num;
				} else{
					$category_first_parent = $this->categories->handler_get($category_breadcrumbs[0]['category_id']);
					$category_parent_weight = $category_first_parent['category_weight'];
				}
				
				$update = array(
					'category_weight' => $order_num,
					'category_parent_weight' => $category_parent_weight
				);
				$this->categories->handler_update($category_id, $update);

				$category_chilrden = explode(',', $category['category_children']);
				$category_chilrden = array_filter($category_chilrden);
				if(!empty($category_chilrden)){
					$update_children = array(
						'category_parent_weight' => $category_parent_weight
					);
					$this->categories->handler_update_same_data(array('id_category' => $category_chilrden), $update_children);
				}
				
				jsonResponse('Сохранено', 'success');
			break;
			case 'actualize_caregories':
				checkPermisionAjax('manage_categories');
				
				$this->categories->handler_actualize_all();
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'get_all_subcategories':
				$validator_rules = array(
					array(
						'field' => 'category',
						'label' => 'Категория',
						'rules' => 'required|xss_clean',
					)
				);
				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$user_info = $this->lauth->user_data();
				$params = array();
				if(!empty($user_info->user_categories_access_full) && !$this->lauth->have_right('manage_items')){
					$params['id_category'] = explode(',', $user_info->user_categories_access_full);
				}
				$categories = $this->categories->handler_get_tree($params);
				$parent = (int)$this->input->post('category');
				$subcategories = $this->tree_select($categories, $parent, 0, 0);

				jsonResponse('', 'success', array('subcategories' => $subcategories));
			break;
			case 'get_all_categories':
				$categories = $this->categories->handler_get_tree(array('parent' => 0));
				$categories = $this->tree_select($categories, 0);

				jsonResponse('', 'success', array('categories' => $categories));
			break;
			case 'get_user_categories':
				$validator_rules = array(
					array(
						'field' => 'user',
						'label' => 'Пользователь',
						'rules' => 'required|xss_clean',
					)
				);
				$this->form_validation->set_rules($validator_rules);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$user_info = $this->lauth->get_user_data((int) $this->input->post('user'));
				if(empty($user_info->user_categories_access)){
					jsonResponse('', 'success', array('user_categories' => ''));
				}
				
				$categories = $this->categories->handler_get_tree(array('id_category' => explode(',', $user_info->user_categories_access)));
				$parent = (int)$this->input->post('category');
				$user_categories = $this->tree_select($categories, 0);

				jsonResponse('', 'success', array('user_categories' => $user_categories));
			break;
		}
	}

	private function tree_select($tree, $category_parent = 0, $level = 0, $category = 0) {
        if (empty($tree[$category_parent]))
            return;
		
        $str = '';
        foreach ($tree[$category_parent] as $row) {
            $bool = $row['category_id'] == $category;
			$breads = json_decode('['.$row['category_breadcrumbs'].']', true);
			$cat_title = array();
			foreach($breads as $bread){
				$cat_title[] = $bread['category_title'];
			}

            $str .= '<option value="'.$row["category_id"].'" data-value-text="'.implode(' &raquo; ', $cat_title).'" '.set_select('categories', $row["category_id"],$bool).'>'.str_repeat("-", $level).' '.$row['category_title'].'</option>';
            if (isset($tree[$row['category_id']]))
                $str .= $this->tree_select($tree, $row['category_id'],$level + 1, $category);
        }

        return $str;
    }
	
	private function tree_table($tree, $category_parent = 0, $level, $categories_list) {
        if (empty($tree[$category_parent]))
            return;
		
        $str = '';
        foreach ($tree[$category_parent] as $k => $row) {
			$parent_category = (!empty($categories_list[$row['category_parent']]))?$categories_list[$row['category_parent']]:array();
            $str .= $this->load->view($this->theme->apanel_view($this->view_module_path . 'table_tree'), array('category' => $row, 'level' => $level, 'category_parent' => $parent_category), true);
            if (isset($tree[$row['category_id']]))
                $str .= $this->tree_table($tree, $row['category_id'], $level + 1, $categories_list);
        }

        return $str;
    }
}
