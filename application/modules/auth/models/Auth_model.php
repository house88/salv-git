<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model{
	
	var $users_table = "users";
	var $users_groups_table = "users_groups";
	var $users_rights_table = "users_rights";
	var $users_groups_rights_table = "users_groups_rights";
	
	function __construct(){
		parent::__construct();
	}
	
	function handler_insert($data = array()){
		if(empty($data)){
			return false;
		}

		$this->db->insert($this->users_table, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_user, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where("id", $id_user);
		return $this->db->update($this->users_table, $data);
	}

	function handler_get_by_id($id_user){
		$this->db->select('*');
		$this->db->from($this->users_table);
		$this->db->where($this->users_table.'.id', $id_user);
		$this->db->join($this->users_groups_table, $this->users_groups_table.'.id_group = '.$this->users_table.'.id_group', 'left');
		return $this->db->get()->row();
	}

	function handler_get_by_token($token){
		$this->db->select('*');
		$this->db->from($this->users_table);
		$this->db->where($this->users_table.'.activation_key', $token);
		$this->db->join($this->users_groups_table, $this->users_groups_table.'.id_group = '.$this->users_table.'.id_group', 'left');
		return $this->db->get()->row();
	}

	function handler_get_by_reset_token($token){
		$this->db->select('*');
		$this->db->from($this->users_table);
		$this->db->where($this->users_table.'.reset_key', $token);
		$this->db->join($this->users_groups_table, $this->users_groups_table.'.id_group = '.$this->users_table.'.id_group', 'left');
		return $this->db->get()->row();
	}

	function handler_get_by_email($email){
		$this->db->select('*');
		$this->db->from($this->users_table);
		$this->db->where($this->users_table.'.user_email', $email);
		$this->db->join($this->users_groups_table, $this->users_groups_table.'.id_group = '.$this->users_table.'.id_group', 'left');
		return $this->db->get()->row();
	}

	function handler_get_login_info($user_email, $password){
		$this->db->select('*');
		$this->db->from($this->users_table);
		$this->db->where($this->users_table.'.user_email', $user_email);
		$this->db->where($this->users_table.'.user_pass', $password);
		$this->db->join($this->users_groups_table, $this->users_groups_table.'.id_group = '.$this->users_table.'.id_group', 'left');
		$user_info = $this->db->get()->row();
		if($user_info){
			$user_rights = $this->handler_get_rights($user_info->id_group);
			$rights = array();
			if($user_rights){
				foreach ($user_rights as $right) {
					$rights[] = $right->right_alias;
				}
			}

			$user_info->user_rights = $rights;
			return $user_info;
		} else{
			return false;
		}
	}

	function handler_get_social_login_info($user_email){
		$this->db->select('*');
		$this->db->from($this->users_table);
		$this->db->where($this->users_table.'.user_email', $user_email);
		$this->db->join($this->users_groups_table, $this->users_groups_table.'.id_group = '.$this->users_table.'.id_group', 'left');
		$user_info = $this->db->get()->row();
		if($user_info){
			$user_rights = $this->handler_get_rights($user_info->id_group);
			$rights = array();
			if($user_rights){
				foreach ($user_rights as $right) {
					$rights[] = $right->right_alias;
				}
			}

			$user_info->user_rights = $rights;
			return $user_info;
		} else{
			return false;
		}
	}

	function handler_get_rights($id_group){
		$this->db->select('*');
		$this->db->from($this->users_groups_rights_table);
		$this->db->where($this->users_groups_rights_table.'.id_group', $id_group);
		$this->db->join($this->users_rights_table, $this->users_rights_table.'.id_right = '.$this->users_groups_rights_table.'.id_right', 'inner');
		return $this->db->get()->result();
	}

	function handler_get_group_default(){
		$this->db->where('group_default', 1);
		$this->db->limit(1);
		return $this->db->get($this->users_groups_table)->row();
	}
}

?>