<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/items/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'items';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['show_moderator_date_filters'] = false;

		$this->load->model("Items_model", "items");
		$this->load->model("Prices_model", "prices");
		$this->load->model("categories/Categories_model", "categories");
		$this->load->model("properties/Properties_model", "properties");
		$this->load->model("brands/Brands_model", "brands");
		$this->load->model("users/User_model", "users");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];

		$this->config->set_item('language', 'russian');
	}

	function index(){
		checkPermision('manage_items');

		// GET CURRENCIES
		$this->data['currency'] = Modules::run('currency/_get_default_apanel');

		$category_params = array(
			'parent' => 0
		);

		$manager_params = array();

		$this->data['show_moderator_date_filters'] = true;
		if($this->lauth->have_right('manage_items')){
			$manager_params = array(
				'group_type' => array(
					'content_menedger','moderator','admin'
				)
			);
		} else{
			$user_info = $this->lauth->user_data();
			if(!empty($user_info->user_categories_access_full) && !$this->lauth->have_right('manage_items')){
				$category_params['id_category'] = array_filter(array_map('intval', explode(',', $user_info->user_categories_access_full)));
			}
			$this->data['show_moderator_date_filters'] = false;
		}

		$this->data['categories'] = $this->categories->handler_get_all($category_params);		
		$this->data['content_menedgers'] = !empty($manager_params) ? $this->users->handler_get_all($manager_params) : array();
		
		$this->data['show_moderator_date_filters'] = true;
		$this->data['brands'] = Modules::run('brands/_get_all');
		$this->data['page_header'] = 'Список товаров';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function edit(){
		if(!$this->lauth->have_right_or('manage_items')){
			redirect('/admin');
		}

		$id_item = (int)$this->uri->segment(4);
		$this->data['item'] = $this->items->handler_get($id_item);
		if(empty($this->data['item'])){
			redirect('admin/items');
		}

		$this->data['item_category'] = $this->categories->handler_get($this->data['item']['id_category']);
		
		$this->data['aditional_fields'] = $this->categories->handler_get_additional_fields($this->data['item_category']);

		$this->data['brands_list'] = $this->brands->handler_get_all();
		$this->data['supliers'] = arrayByKey($this->items->handler_get_suppliers_all(), 'supplier_prog_id');
		$this->data['price_variants'] = $this->prices->handler_get_all(array('active' => 1));

		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'edit_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'edit_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_items');

				$this->data['categories'] = Modules::run('categories/_get_tree_tokens');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'add_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_items');

				$this->form_validation->set_rules('id_category', 'Категория', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$id_category = (int) $this->input->post('id_category');
				$category = Modules::run('categories/_get', $id_category);
				$categoryChildren = array_filter(array_map('intval', explode(',', $category['category_children'])));
				
				if(!empty($categoryChildren)){
					jsonResponse('Данная категория содержит другие категории.');
				}

				$id_item = $this->items->handler_insert([
					'id_category' => $id_category,
					'item_code' => uniqueId('sku_')
				]);

				jsonResponse('Сохранено.', 'success', [
					'link' => site_url("admin/items/edit/{$id_item}")
				]);
			break;
			case 'edit':
				checkPermisionAjax('manage_items');
	
				$idProduct = (int) $this->input->post('item');
				$productDetails = $this->items->handler_get($idProduct);
				if(empty($productDetails)){
					jsonResponse('Ошибка: Товар не найден.');
				}

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('price', 'Цена', 'required|xss_clean');
				$this->form_validation->set_rules('temp_price', 'Temp price', 'xss_clean');
				$this->form_validation->set_rules('cashback_price', 'Кэшбэк', 'xss_clean');
				$this->form_validation->set_rules('quantity', 'Количество', 'required|xss_clean');
				$this->form_validation->set_rules('photo', 'Главная фото товара', 'xss_clean');
				$this->form_validation->set_rules('brand', 'Брэнд', 'xss_clean');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'xss_clean');
				$this->form_validation->set_rules('md', 'Meta description', 'xss_clean');
				$this->form_validation->set_rules('video', 'Видео', 'valid_url|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$deletePhotos = $this->input->post('remove_photos');
				if(!empty($deletePhotos)){
					foreach($deletePhotos as $deletePhoto){
						@unlink('files/items/'.$id_item.'/'.$deletePhoto);
						@unlink('files/items/'.$id_item.'/thumb_500x500_'.$deletePhoto);
					}
				}

				$photos = $this->input->post('photos');
				$marketting = (array) $this->input->post('marketting');

				$this->load->helper('htmlpurifier');

				$update = array(
					'item_title' => $this->input->post('title'),
					'item_url' => strForURL($this->input->post('title') .' '. $idProduct),
					'id_brand' => (int)$this->input->post('brand'),
					'item_description' => $this->input->post('description'),
					'item_description_cleaned' => html_purify($this->input->post('description'), 'item_description'),
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'item_photo' => $this->input->post('photo'),
					'item_photos' => !empty($photos) && is_array($photos) ? json_encode($photos) : NULL,
					'item_price' => (float) $this->input->post('price'),
					'item_temp_price' => (float) $this->input->post('temp_price'),
					'item_cashback_price' => (float) $this->input->post('cashback_price'),
					'item_quantity' => (int)$this->input->post('quantity'),
					'item_visible' => in_array('visible', $marketting) ? 1 : 0,
					'item_hit' => in_array('hit', $marketting) ? 1 : 0,
					'item_newest' => in_array('newest', $marketting) ? 1 : 0,
					'item_action' => (in_array('action', $marketting) || (float) $this->input->post('cashback_price') > 0) ? 1 : 0,
					'item_popular' => in_array('popular', $marketting) ? 1 : 0,
					'item_on_home' => in_array('on_home', $marketting) ? 1 : 0,
					'item_video_link' => $this->input->post('video', true),
					'item_video_code' => getYoutubeID($this->input->post('video', true))
				);
				
				$vprices = $this->input->post('vprice');
				$price_variants = $this->prices->handler_get_all();
				foreach($price_variants as $price_variant){
					if(isset($vprices[$price_variant['id_price_variant']])){
						$update['item_price_'.$price_variant['id_price_variant']] = (float)$vprices[$price_variant['id_price_variant']];
					} else{
						$update['item_price_'.$price_variant['id_price_variant']] = 0;
					}
				}

				$item_properties = $this->input->post('properties');
				if(!empty($item_properties)){
					$insert_item_properties = array();
					foreach($item_properties as $key=>$properties){
						switch($key){
							case 'simple':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $idProduct,
											'id_property' => $property_key,
											'value' => $property
										);
									}
								}
							break;
							case 'range':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $idProduct,
											'id_property' => $property_key,
											'value' => $property
										);
									}
								}
							break;
							case 'select':
								foreach($properties as $property_key => $property){
									if(!empty($property)){
										$insert_item_properties[] = array(
											'id_item' => $idProduct,
											'id_property' => $property_key,
											'value' => $property
										);
									}
								}
							break;
							case 'multiselect':
								foreach($properties as $property_key => $property_values){
									if(!empty($property_values)){
										foreach($property_values as $property_value){
											$insert_item_properties[] = array(
												'id_item' => $idProduct,
												'id_property' => $property_key,
												'value' => $property_value
											);
										}
									}
								}
							break;
						}
					}

					$this->properties->handler_delete_items_properties_values($idProduct);
					if(!empty($insert_item_properties)){
						$this->properties->handler_insert_items_properties_values($insert_item_properties);
					}
				}
				
				$this->items->handler_update($idProduct, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'copy':
				if(!$this->lauth->have_right('manage_items')){
					jsonResponse('Ошибка: Нет прав!');
				}
	
				$id_item = (int)$this->input->post('id_item');
				$item_info = $this->items->handler_get($id_item);
				if(empty($item_info)){
					jsonResponse('Ошибка: Товар не найден.');
				}
	
				$id_item_copy = (int)$this->input->post('id_item_copy');
				$item_copy_info = $this->items->handler_get($id_item_copy);
				if(empty($item_copy_info)){
					jsonResponse('Ошибка: Товар с которого копировать не найден.');
				}

				$copy_fields = $this->input->post('copy_fields');
				if(!is_array($copy_fields) || empty('copy_fields')){
					jsonResponse('Выберите поля для копирования');
				}

				$item_category = $this->categories->handler_get($item_info['id_category']);
				$copy_item_category = $this->categories->handler_get($item_copy_info['id_category']);
				$update = array();

				foreach ($copy_fields as $field => $is_set) {
					switch ($field) {
						case 'brand':
							$update['id_brand'] = (int) $item_copy_info['id_brand']; 
						break;
						case 'description':
							$update['item_description'] = $item_copy_info['item_description']; 
						break;
						case 'main_photo':
							$path = 'files/items' ;
							$copy_file_name = $item_copy_info['item_photo'];
							if(file_exists("files/items/{$copy_file_name}")){
								$config = array(
									'table' => 'items',
									'id' => 'id_item',
									'field' => 'item_url',
									'title' => 'item_title',
									'replacement' => 'dash'
								);
								$this->load->library('slug', $config);
								$file_name = $this->slug->create_slug(cut_str($item_info['item_title'], 200).'-'.$item_info['id_item']) . '.' . pathinfo("{$path}/{$copy_file_name}", PATHINFO_EXTENSION);
								copy("{$path}/{$copy_file_name}", "{$path}/{$file_name}");

								if(file_exists("{$path}/thumb_500x500_{$copy_file_name}")){
									copy("{$path}/thumb_500x500_{$copy_file_name}", "{$path}/thumb_500x500_{$file_name}");
								} else{
									$config = array(
										'source_image'      => FCPATH . "{$path}/{$copy_file_name}",
										'create_thumb'      => true,
										'thumb_marker'      => 'thumb_500x500_',
										'new_image'         => FCPATH . $path,
										'maintain_ratio'    => true,
										'width'             => 500,
										'height'            => 500
									);
							
									$this->load->library('image_lib');
									$this->image_lib->initialize($config);
									$this->image_lib->resize();
								}

								$update['item_photo'] = $file_name;
							}
						break;
						case 'other_photos':
							$this->load->library('upload');
							$this->load->library('image_lib');
							
							$path = 'files/items/';
							create_dir($path);
							$this->upload->upload_path = FCPATH . $path;
							$this->upload->validate_upload_path();
							$full_path = $this->upload->upload_path;

							$copy_files = array_filter(json_decode($item_copy_info['item_photos']));
							$photos = array();
							if(!empty($copy_files)){
								foreach ($copy_files as $key => $copy_file) {
									$file_index = $key + 1;
									$copy_file_name = $copy_file;
									if(file_exists("{$full_path}{$copy_file_name}")){
										$file_name = strForURL(cut_str($item_info['item_title'], 200).'-'.$item_info['id_item']) . "_{$file_index}." . pathinfo("{$full_path}{$copy_file_name}", PATHINFO_EXTENSION);
										
										copy("{$full_path}{$copy_file_name}", "{$full_path}{$file_name}");
		
										if(file_exists("{$path}/thumb_500x500_{$copy_file_name}")){
											copy("{$path}/thumb_500x500_{$copy_file_name}", "{$path}/thumb_500x500_{$file_name}");
										} else{
											$config = array(
												'source_image'      => "{$full_path}{$file_name}",
												'create_thumb'      => true,
												'thumb_marker'      => 'thumb_500x500_',
												'new_image'         => $full_path,
												'maintain_ratio'    => true,
												'width'             => 500,
												'height'            => 500
											);
									
											$this->image_lib->initialize($config);
											$this->image_lib->resize();
										}
										
										$photos[] = $file_name;
									}
								}
							}
							
							$update['item_photos'] = json_encode($photos);
						break;
						case 'video':
							if(!empty($item_copy_info['item_video_link'])){
								$this->load->library('Videothumb', 'videothumb');
								$video_info = $this->videothumb->process($item_copy_info['item_video_link']);
								
								if(empty($video_info['error'])) {
									$this->load->library('upload');
									$this->load->library('image_lib');

									$path = 'files/video';
									create_dir($path);

									$this->upload->upload_path = FCPATH . $path;
									$this->upload->validate_upload_path();
									$full_path = $this->upload->upload_path;
									
									@unlink($item_info['item_video_image']);
									$config = array(
										'source_image'      => $video_info['image'],
										'create_thumb'      => false,
										'new_image'         => FCPATH . $path,
										'maintain_ratio'    => true,
										'width'             => 800,
										'height'            => 450
									);
									$this->image_lib->initialize($config);
									$this->image_lib->resize();
									
									$update['item_video_source'] = $video_info['type'];
									$update['item_video_image'] = $video_info['image'];
									$update['item_video_link'] = $item_copy_info['item_video_link'];
									$update['item_video_code'] = $video_info['v_id'];
								}
							}
						break;
						case 'properties':
							$properties = $this->properties->handler_get_items_properties_values($item_copy_info['id_item']);
							$insert_properties = array_map(function($element) use ($id_item){
								$element['id_item'] = $id_item;
								return $element;
							}, $properties);
							
							if(!empty($insert_properties)){
								$this->properties->handler_delete_items_properties_values($id_item);
								$this->properties->handler_insert_items_properties_values($insert_properties);
							}
						break;
					}
				}

				if(!empty($update)){
					$this->items->handler_update($id_item, $update);
				}

				$this->session->set_flashdata('flash_message', '<li class="message-success zoomIn">Копирование прошло успешно. <i class="ca-icon ca-icon_remove"></i></li>');
				
				jsonResponse('', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_items');

				$params = array(
					'limit' 			=> (int) $this->input->post('iDisplayLength'),
					'start' 			=> (int) $this->input->post('iDisplayStart'),
					'exclude_bad_price' => false,
					'sort_by' 			=> flat_dt_ordering($_POST, array(
						'dt_name'    => 'item_title',
						'dt_price'   => 'item_price',
						'dt_create'  => 'item_created',
						'dt_update'  => 'item_last_update'
					))
				);

				//region CURENCY
				$currency = Modules::run('currency/_get_default_apanel');
				//endregion CURENCY

				//region FILTERS
				if(isset($_POST['keywords'])){
					$params['keywords'] = $this->input->post('keywords');
				}

				$items_list = array();
				if($this->input->post('id_category')){
					$id_category = (int) $this->input->post('id_category');
					
					if($this->input->post('id_subcategory')){
						$id_category = (int) $this->input->post('id_subcategory');
					}

					$category = $this->categories->handler_get($id_category);
					if(!empty($category)){
						$categories_list = array_map('intval', (array) explode(',', $category['category_children']));
						$categories_list = array_filter($categories_list);
						$categories_list[] = $id_category;
						$params['id_category'] = $categories_list;

						$property_select = array();
						$property_range = array();
						foreach ($_POST as $key => $value) {
							if (preg_match('/^property_select_/', $key)) {
								$prop_id = explode('_', $key);
								$property_select[end($prop_id)] = $value;
							}
							if (preg_match('/^property_range_/', $key)) {
								$components = explode('_', $key);
								$property_range[$components[2]][$components[3]] = $value;
							}
						}

						if (!empty($property_select)){
							$properties_params['properties_select'] = $property_select;
						}

						if (!empty($property_range)){
							$properties_params['properties_range'] = $property_range;
						}

						if(!empty($properties_params)){
							$items_by_properties = $this->properties->handler_get_items_by_properties_and($properties_params);
							if(!empty($items_by_properties)){
								foreach ($items_by_properties as $item_by_properties) {
									$items_list[$item_by_properties['id_item']] = $item_by_properties['id_item'];
								}
							} else{
								$items_list[] = 0;
							}
						}
					}
				}

				if(!empty($items_list)){
					$items_by_properties_inverse = (int) $this->input->post('items_by_properties_inverse');
					if($items_by_properties_inverse){
						$params['not_items_list'] = $items_list;
					} else{
						$params['items_list'] = $items_list;
					}
				}
				
				if($this->input->post('brand')){
					$params['id_brand'] = (int) $this->input->post('brand');
				}
				
				if($this->input->post('item_price_from')){
					$params['item_price_from'] = $this->input->post('item_price_from') / $currency['currency_rate'];
				}

				if($this->input->post('item_price_to')){
					$params['item_price_to'] = $this->input->post('item_price_to') / $currency['currency_rate'];
				}
		
				if($this->input->post('item_last_update')){
					list($item_updated_from, $item_updated_to) = explode(' - ', $this->input->post('item_last_update'));

					if(validateDate($item_updated_from, 'd.m.Y')){
						$params['item_last_update_from'] = getDateFormat($item_updated_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($item_updated_to, 'd.m.Y')){
						$params['item_last_update_to'] = getDateFormat($item_updated_to, 'd.m.Y', 'Y-m-d');
					}
				}

				if($this->input->post('item_visible')){
					$params['item_visible'] = (int)$this->input->post('item_visible');
				}

				if($this->input->post('item_hit')){
					$params['item_hit'] = (int)$this->input->post('item_hit');
				}

				if($this->input->post('item_newest')){
					$params['item_newest'] = (int)$this->input->post('item_newest');
				}

				if($this->input->post('item_action')){
					$params['item_action'] = (int)$this->input->post('item_action');
				}

				if($this->input->post('item_popular')){
					$params['item_popular'] = (int)$this->input->post('item_popular');
				}

				if($this->input->post('item_on_home')){
					$params['item_on_home'] = (int)$this->input->post('item_on_home');
				}

				if($this->input->post('item_commented')){
					$params['item_commented'] = (int)$this->input->post('item_commented');
				}
				//endregion FILTERS

				$records = $this->items->handler_get_all($params);
				$records_total = $this->items->handler_get_count($params);
				
				$records_categories = array_column($records, 'id_category');
				$categories = !empty($records_categories) ? arrayByKey(Modules::run('categories/_get_all', array('id_category' => $records_categories)), 'category_id') : array();

				$currency = Modules::run('currency/_get_default_apanel');

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($categories, $currency){
						$item_url = base_url('products/'.$record['item_url']);
						$item_image_url = base_url(getImage('files/items/'.$record['item_photo']));

						//region CATEGORY
						$category_bread = array();
						$category_link = '';
						if(isset($categories[$record['id_category']])){
							$item_categories = json_decode("[".$categories[$record['id_category']]['category_breadcrumbs']."]", true);
							foreach($item_categories as $category){
								$category_bread[] = $category['category_title'];
							}
							$category_link = '<span class="custom-font-size-11" title="'.implode(' &raquo; ', $category_bread).'"><i class="fad fa-folder-tree"></i> '.$categories[$record['id_category']]['category_title'].'</span>';
						}
						//endregion CATEGORY

						//region MARKETTING
						$item_marketing = '';
						if($record['item_visible']){
							$item_marketing .= '<span class="fad fa-eye" title="Признак виден на сайте"></span> ';
						}

						if($record['item_hit']){
							$item_marketing .= '<span class="fad fa-fire" title="Признак хит"></span> ';
						}

						if($record['item_newest']){
							$item_marketing .= '<span class="fad fa-gift" title="Признак новинка"></span> ';
						}

						if($record['item_action']){
							$item_marketing .= '<span class="fad fa-stars" title="Признак акция"></span> ';
						}

						if($record['item_popular']){
							$item_marketing .= '<span class="fad fa-heart" title="Признак популярен"></span> ';
						}
						
						if($record['item_on_home']){
							$item_marketing .= '<span class="fad fa-home-heart" title="Признак показать на главной"></span> ';
						}
			
						if($record['item_commented']){
							$item_marketing .= '<span class="fad fa-comment-alt-dots" title="Признак обсуждаемый"></span> ';
						}
						//endregion MARKETTING
						
						return array(
							'dt_photo'				=> '<a href="'.$item_image_url.'" data-fancybox="image_'.$record['id_item'].'" data-caption="'.clean_output($record['item_title']).'">
															<img src="'.$item_image_url.'" class="img-thumbnail custom-width-100">
														</a>',
							'dt_name'				=> "<a href=\"{$item_url}\">{$record['item_title']}</a>
														<br><strong class=\"custom-font-size-12\">Маркетинг: </strong>{$item_marketing}
														<br><strong class=\"custom-font-size-12\">Категория: </strong>{$category_link}
														<br><strong class=\"custom-font-size-12\">Артикул: </strong>{$record['item_code']}",
							'dt_price'				=> (!empty($currency))?numberFormat($currency['currency_rate'] * $record['item_price'], true):$record['item_price'],
							'dt_quantity'			=> (int) $record['item_quantity'],
							'dt_create'				=> getDateFormat($record['item_created']),
							'dt_update'				=> getDateFormat($record['item_last_update']),
							'dt_actions'			=> '<div class="dropdown">
															<a data-toggle="dropdown" href="#" aria-expanded="false">
																<i class="fas fa-ellipsis-h"></i>
															</a>
															<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
																<a class="dropdown-item" href="'.base_url('admin/items/edit/' . $record['id_item']).'">
																	<i class="fad fa-pencil"></i> Редактировать
																</a>
															</div>
														</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}

	function upload_photo(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$id_item = (int) $this->input->post('id_item');
		$item = $this->items->handler_get($id_item);
		if(empty($item)){
			jsonResponse('Не могу завершить операцию.');
		}

        $path = 'files/items';
		create_dir($path);

		$file_name = strForURL(get_words(translit($item['item_title']), 6) . ' ' . $id_item);

        $config['upload_path'] = FCPATH . $path;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $file_name;
        $config['min_width']	= '500';
		$config['min_height']	= '500';
		$config['max_size']	= '6000';
		
		$this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('croppedImage')){
			jsonResponse($this->upload->display_errors('',''),'error');
        }
		$data = $this->upload->data();

		$config = array(
			'source_image'      => $data['full_path'],
			'create_thumb'      => true,
			'thumb_marker'      => 'thumb_500x500_',
			'new_image'         => FCPATH . $path,
			'maintain_ratio'    => true,
			'width'             => 500,
			'height'            => 625
		);

		$this->load->library('image_lib');
		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$file = new StdClass;
		$file->filename = $data['file_name'];
		jsonResponse('', 'success', array("file" => $file));
	}
}
