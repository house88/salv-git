<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages_model extends CI_Model{
	var $pages = "pages";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->pages, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_page, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_page', $id_page);
		$this->db->update($this->pages, $data);
	}

	function handler_delete($id_page){
		$this->db->where('id_page', $id_page);
		$this->db->delete($this->pages);
	}

	function handler_get($id_page){
		$this->db->where('id_page', $id_page);
		return $this->db->get($this->pages)->row_array();
	}

	function handler_get_by_url($url){
		$this->db->where('url', $url);
		return $this->db->get($this->pages)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_page ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if(!empty($multi_order_by)){
				$order_by = implode(',', $multi_order_by);
			}
		}

        if(isset($pages_list)){
			$this->db->where_in('id_page', $pages_list);
        }

        if(isset($active)){
			$this->db->where('page_active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->pages)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($pages_list)){
			$this->db->where_in('id_page', $pages_list);
        }

        if(isset($active)){
			$this->db->where('page_active', $active);
        }

		return $this->db->count_all_results($this->pages);
	}
}
