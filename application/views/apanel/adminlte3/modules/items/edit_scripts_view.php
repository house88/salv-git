<?php $this->load->view($this->theme->apanel_view('includes/plugins/tinymce'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/cropper'));?>

<script>
	var id_item = intval('<?php echo $item['id_item'];?>');
	var id_category = intval('<?php echo $item['id_category'];?>');
	var properties_required = intval('<?php echo (!empty($aditional_fields) && $aditional_fields['properties']['active'] == 1)?1:0;?>');
	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

	// Import image
	var $inputLogo = null;
	var $inputImages = null;
	var $cropperImage = null;
	var pasteMainImageWrapper = '#paste-main-image-popup';
	var pasteAdditionalImageWrapper = '#paste-additional-image-popup';
	var pasteFiles = [];
	var upload_images = [];
	var logo_settings = {
		name: "logo_settings",
		title: "Главная фото товара",
		wrapper: "#js-uploaded_photo",
		template: "#js-image-template",
		upload_url: base_url + "admin/items/upload_photo",
		multiple: false,
		input_name: 'photo'
	};

	var pictures_settings = {
		name: "pictures_settings",
		title: "Дополнительные фото товара",
		wrapper: "#js-uploaded_photos",
		template: "#js-image-template",
		upload_url: base_url + "admin/items/upload_photo",
		multiple: true,
		input_name: 'photos[]'
	};

	
	function prepareImagesToUpload(files, settings, callback){
		if (files && files.length) {
			var file_index = 0;
			$.each(files, function(index, file){
				if (/^image\/\w+$/.test(file.type)) {
					file_index++;
					var reader = new FileReader();
					reader.readAsDataURL(file);
					reader.onload = function (e) {
						var image = new Image();
						image.src = e.target.result;

						//Validate the File Height and Width.
						image.onload = function () {
							if(this.width < 500 || this.height < 500){
								systemMessages("Размеры загружаемого изображения ("+ file.name +") не соответствуют допустимым: 500x500px.", 'error');
								return false;
							}

							var options = {
								src: image.src,
								name: file.name,
								type: file.type
							};

							upload_images.push(options);
							if(file_index === files.length){
								callback();
							}
						};
					}
				} else {
					systemMessages("Загрузка файлов данного типа запрещена.", 'error');
				}
			});
		}
		pasteFiles = [];
	}

	const mainImage = document.getElementById('pasteMainImage');
	const additionalImage = document.getElementById('pasteAdditionalImage');

	mainImage.onpaste = function(event){
		pasteFiles = [];
		var items = (event.clipboardData || event.originalEvent.clipboardData).items;
		var iterator = 0;
		$.each(items, function(index, item){
			iterator++;
			if (item.type.indexOf("image") != -1) {
				var file = item.getAsFile();
				pasteFiles.push(file);

				if(iterator === items.length){
					$(pasteMainImageWrapper).modal('hide').on('hidden.bs.modal', function(){
						prepareImagesToUpload(pasteFiles, logo_settings, function(){
							$inputLogo.val('');
							modalCropper(logo_settings, upload_images[0]);
						});
					});
				}
			}
		});
	}

	additionalImage.onpaste = function(event){
		pasteFiles = [];
		var items = (event.clipboardData || event.originalEvent.clipboardData).items;
		var iterator = 0;
		$.each(items, function(index, item){
			iterator++;
			if (item.type.indexOf("image") != -1) {
				var file = item.getAsFile();
				pasteFiles.push(file);

				if(iterator === items.length){
					$(pasteAdditionalImageWrapper).modal('hide').on('hidden.bs.modal', function(){
						prepareImagesToUpload(pasteFiles, pictures_settings, function(){
							$inputImages.val('');
							modalCropper(pictures_settings, upload_images[0]);
						});
					});
				}
			}
		});
	}

	var pasteIntoTextarea = function(element){
		var $this = $(element);
		
		$($this.data('wrapper')).modal('show').on('shown.bs.modal', function(){
			$($this.data('wrapper')).find('textarea').focus();
		});
	}
	
	$(function(){
		'use strict';

		$inputLogo = $('#inputLogo');
		$inputImages = $('#inputImages');
		if (URL) {
			$inputLogo.change(function () {
				var files = this.files;
				
				prepareImagesToUpload(files, logo_settings, function(){
					$inputLogo.val('');
					modalCropper(logo_settings, upload_images[0]);
				});
			});
			
			$inputImages.change(function () {
				var files = this.files;

				prepareImagesToUpload(files, pictures_settings, function(){
					modalCropper(pictures_settings, upload_images[0]);
					$inputImages.val('');
				});

			});
		} else {
			$inputLogo.prop('disabled', true).parent().addClass('disabled');
			$inputImages.prop('disabled', true).parent().addClass('disabled');
		}

		var edit_form = $('#edit_form');
		edit_form.submit(function () {
			tinyMCE.triggerSave();
			var fdata = edit_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/items/ajax_operations/edit',
				data: fdata,
				dataType: 'JSON',
				beforeSend: function(){
					showLoader('body');
				},
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
					hideLoader('body');
				}
			});
			return false;
		});

		<?php if($this->lauth->have_right('manage_items')){?>
			var copy_form = $('#copy_form');
			copy_form.submit(function () {
				var fdata = copy_form.serialize();
				$.ajax({
					type: 'POST',
					url: base_url+'admin/items/ajax_operations/copy',
					data: fdata,
					dataType: 'JSON',
					beforeSend: function(){
						showLoader('body');
					},
					success: function(resp){
						if(resp.mess_type === 'success'){
							location.reload(true);
						} else{
							systemMessages(resp.message, resp.mess_type);
							hideLoader('body');
						}
					}
				});
				return false;
			});
		<?php }?>

		get_properties();
		initTinyMce();
	});

	function closePopup(){
		var $popup = $('#general_popup_form');
		$popup.modal('hide').on('hidden.bs.modal', function(){
			$cropperImage.cropper('destroy');
			upload_images = [];
			$(this).html('');
		});
	}

	function modalCropper(settings, options, replace){
		if(replace === true){
			$cropperImage.cropper('replace', options.src);
			return;
		}

		var $popup = $('#general_popup_form');
		var template = $('#js-image-cropper-template').text();
		var content = {
			settings: settings.name,
			title: settings.title,
			image_src: options.src,
			file_name: options.name,
			file_type: options.type
		};

		for (var key in content) {
			if (content.hasOwnProperty(key)) {
				template = template.replace(new RegExp('{{' + key + '}}', 'g'), content[key]);
			}
		}
		
		$popup.html($(template));
		$popup.find('.modal-dialog').css('opacity', 0);
		$popup.modal('show').on('shown.bs.modal', function(){
			initCropper(function(){
				$popup.find('.modal-dialog').css('opacity', 1);
			});
		});
	}

	function initCropper(__callback){
		$cropperImage = $('.modal-dialog #js-image-cropp');
		$cropperImage.cropper({
			aspectRatio: 1 / 1,
			minCropBoxWidth: 500,
			minCropBoxHeight: 500,
			cropBoxResizable: false,
			dragMode: 'move',
			cropBoxMovable: false,
			scalable:false,
			ready: function(e){
				$(this).cropper('zoomTo', 1);
			},
			zoom: function(e) {
				var canvasData = $(this).cropper('getCanvasData');
				var cropBoxData = $(this).cropper('getCropBoxData');

				if (e.detail.ratio > 1) { // Zoom out
					e.preventDefault(); // Prevent zoom out
					$(this).cropper('zoomTo', 1);
				}
			}
		});

		__callback();
	}

	function cropImage(element){
		var $this = $(element);
		var settings = window[$this.data('settings')];
		$cropperImage.cropper('crop');
		$cropperImage.cropper('getCroppedCanvas', {
			width: 500,
			height: 500,
			minWidth: 500,
			minHeight: 500,
			fillColor: '#fff',
			imageSmoothingEnabled: false,
			imageSmoothingQuality: 'high',
		}).toBlob((blob) => {
			const formData = new FormData();
			formData.append('croppedImage', blob, $this.data('file-name'));
			formData.append('id_item', id_item);

			$.ajax(settings.upload_url, {
				method: "POST",
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				success(resp) {
					if(resp.mess_type === 'success'){
						var file = resp.file;	
						var template = $(settings.template).text();
						var content = {
							image_src: base_url+'files/items/thumb_500x500_'+file.filename,
							file_name: file.filename,
							input_name: settings.input_name
						};

						for (var key in content) {
							if (content.hasOwnProperty(key)) {
								template = template.replace(new RegExp('{{' + key + '}}', 'g'), content[key])
							}
						}
						
						if(settings.multiple === false){
							var $old_image = $(settings.wrapper).find('input[name=photo]');
							if($old_image.length > 0){
								var unused_photo = $old_image.val();
								$(settings.wrapper).closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
							}
	
							$(settings.wrapper).html('');
						}
						
						$(settings.wrapper).append($(template));

						var $popup = $('#general_popup_form');
						upload_images.shift();
						if(upload_images.length > 0){
							modalCropper(settings, upload_images[0], true);
						} else{
							closePopup()
						}

					} else{
						systemMessages( resp.message, resp.mess_class );
					}
				},
				error() {
					systemMessages( 'Ошибка при загрузки.', 'error' );
				},
			});
		}, $this.data('file-type'));
	}

	function get_properties(){
		$.ajax({
			type: 'POST',
			url: base_url+'admin/properties/ajax_operations/get_category_properties',
			data: {item:id_item, category:id_category, properties_required:properties_required},
			dataType: 'JSON',
			success: function(resp){
				if(resp.mess_type == 'success'){
					$('#properties_block').html(resp.properties);
    				$(".bs-select-multiple").selectpicker();
				} else{
					systemMessages(resp.message, resp.mess_type);
				}
			}
		});
		return false;
	}
</script>