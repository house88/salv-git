<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cities_model extends CI_Model{
	var $cities = "cities";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->cities, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_city, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_city', $id_city);
		$this->db->update($this->cities, $data);
	}

	function handler_delete($id_city){
		$this->db->where('id_city', $id_city);
		$this->db->delete($this->cities);
	}

	function handler_get($id_city){
		$this->db->where('id_city', $id_city);
		return $this->db->get($this->cities)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " city_name ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->cities)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->cities);
	}
}
?>
