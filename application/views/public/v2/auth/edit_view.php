<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="edit-profile">EDITARE PROFIL</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="js-popup-manage-account-form">
            <?php $userDetails = $this->lauth->user_data();?>
            <div class="modal-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Nume/Prenume</label>
                        <input name="name" type="text" value="<?php echo $userDetails->user_nicename;?>" class="form-control" placeholder="ex. John Smith">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Email</label>
                        <input name="email" type="text" value="<?php echo $userDetails->user_email;?>" class="form-control" placeholder="ex. example@example.com">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Telefon</label>
                        <input name="phone" type="text" value="<?php echo $userDetails->user_phone;?>" class="form-control" placeholder="0xxxxxxxx">
                    </div>
                    <div class="form-group col-md-12 mb-0">
                        <label>Parola</label>
                        <input name="password" type="password" value="" class="form-control" placeholder="**********">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">Anulare</button>
                <button type="submit" class="btn d-flex w-50 text-center justify-content-center btn-primary">Salveaza</button>
            </div>
        </form>
    </div>
</div>


<script>
    $(function(){
        $('#js-popup-manage-account-form').on('submit', function(e){
            var $form = $(this);
            var fdata = $form.serialize();

            $.ajax({
				type: 'POST',
				url: base_url+'users/ajax_operations/update',
				data: fdata,
				dataType: 'JSON',
                beforeSend: function(){
                    showLoader('body');
                },
				success: function(resp){
                    systemMessages(resp.message, resp.mess_type);
                    hideLoader('body');
					if(resp.mess_type == 'success'){
                        var $popup_parent = $('#general_popup_form');
                        $popup_parent.modal('hide');
					}
				}
			});
            return false;
        });
    });
</script>