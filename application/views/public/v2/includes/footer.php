

	<div class="copyright py-3">
		<div class="container">
			<div class="row">
				<div class="col-md-6 d-flex align-items-center">
					<p class="mb-0">© Copyright <?php echo date('Y');?> <a href="#">Salv</a> . Toate drepturile sunt rezervate.</p>
				</div>
			</div>
		</div>
	</div>
		
	<?php $this->load->view($this->theme->public_view('includes/global_scripts'));?>