<div class="close-dimmer-wr call-function" data-callback="close_dimmer" data-target="body">
	<span class="ca-icon ca-icon_remove"></span>
</div>
<ul class="filters">
	<?php if(!empty($sub_categories)){?>
		<li class="filter">
			<h3 class="filter-heading">Категории</h3>
			<?php $cat_bread_count = (!empty($category_breadcrumbs))?count($category_breadcrumbs):0;?>
			<ul class="filter-list">
				<?php if($cat_bread_count > 0){?>
						<?php $cat_level = 0;?>
						<?php foreach ($category_breadcrumbs as $cbread_key => $category_breadcrumb) {?>
							<?php if(empty($category['category_children']) && $category_breadcrumb->category_id == $category['category_id']){continue;}?>
							<li>
								<?php $category_url = (!empty($category_breadcrumb->special_url))?$category_breadcrumb->special_url:'catalog/'.$category_breadcrumb->url;?>
								<a href="<?php echo base_url($category_url);?>" class="filter-link">
									<?php if($cbread_key > 0){?>
										<span class="fs-8 lh-19 ca-icon ca-icon_arrow-left ml-<?php echo $cat_level*2;?>"></span>
									<?php }?>
									<?php if($category['category_id'] == $category_breadcrumb->category_id){?>
										<strong><?php echo $category_breadcrumb->category_title;?></strong>
									<?php } else{?>
										<span><?php echo $category_breadcrumb->category_title;?></span>
									<?php }?>
								</a>
							</li>
							<?php $cat_level += 2;?>
						<?php }?>
				<?php }?>

				<?php foreach($sub_categories as $sub_category){?>
					<li class="pl-<?php echo ($cat_level*2+15);?>">
						<?php $sub_category_url = (!empty($sub_category['category_special_url']))?$sub_category['category_special_url']:'catalog/'.$sub_category['category_url'];?>
						<a href="<?php echo base_url($sub_category_url);?>" class="filter-link">
							<?php if($category['category_id'] == $sub_category['category_id']){?>
								<strong><?php echo $sub_category['category_title'];?></strong>
							<?php } else{?>
								<span><?php echo $sub_category['category_title'];?></span>
							<?php }?>
						</a>
					</li>
				<?php }?>
			</ul>
		</li>				
	<?php }?>

	<li class="clearfix">
		<div class="jsfilters-container pt-15"></div>
	</li>

	<li class="filter" data-property-type="optional" data-property="foptional">
		<h3 class="filter-heading">Обшие</h3>
		<ul class="filter-list">
			<li data-property-value="actional">
				<label>
					<input class="js_filter" type="checkbox" <?php echo set_checkbox('foptional', 'actional', isset($properties_selected['foptional']) && in_array('actional', $properties_selected['foptional']));?> name="foptional" value="actional" data-title="Обшие" data-value-text="Акции"> 
					Акции
					<span class="filter-counter">(<?php echo (isset($available_properties_values_counters['foptional']['actional']))?$available_properties_values_counters['foptional']['actional']:0;?>)</span>
				</label>
			</li>
			<li data-property-value="newest">
				<label>
					<input class="js_filter" type="checkbox" <?php echo set_checkbox('foptional', 'newest', isset($properties_selected['foptional']) && in_array('newest', $properties_selected['foptional']));?> name="foptional" value="newest" data-title="Обшие" data-value-text="Новинки"> 
					Новинки
					<span class="filter-counter">(<?php echo (isset($available_properties_values_counters['foptional']['newest']))?$available_properties_values_counters['foptional']['newest']:0;?>)</span>
				</label>
			</li>
		</ul>
	</li>

	<?php if($this->lauth->have_right('filter_by_suppliers')){?>
		<li class="filter" data-property-type="suppliers" data-property="fsuppliers">
			<h3 class="filter-heading">Поставщики</h3>
			<ul class="filter-list">
				<?php foreach($suppliers as $supplier){?>
					<?php $sipplier_counter = (int) @$available_properties_values_counters['fsuppliers'][$supplier['id_supplier']];?>
					<li data-property-value="<?php echo $supplier['id_supplier'];?>" style="<?php if($sipplier_counter == 0){echo 'display:none;';}?>">
						<label>
							<input class="js_filter" type="checkbox" <?php echo set_checkbox('fsuppliers', $supplier['id_supplier'], isset($properties_selected['fsuppliers']) && in_array($supplier['id_supplier'], $properties_selected['fsuppliers']));?> name="fsuppliers" value="<?php echo $supplier['id_supplier'];?>" data-title="Поставщик" data-value-text="<?php echo $supplier['supplier_name'];?>"> 
							<?php echo $supplier['supplier_name'];?>
							<span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fsuppliers'][$supplier['id_supplier']]))?$available_properties_values_counters['fsuppliers'][$supplier['id_supplier']]:0;?>)</span>
						</label>
					</li>
				<?php }?>
				<?php $sipplier_counter = (int) @$available_properties_values_counters['fsuppliers']['no'];?>
				<li data-property-value="no" style="<?php if($sipplier_counter == 0){echo 'display:none;';}?>">
					<label>
						<input class="js_filter" type="checkbox" <?php echo set_checkbox('fsuppliers', 'no', isset($properties_selected['fsuppliers']) && in_array('no', $properties_selected['fsuppliers']));?> name="fsuppliers" value="no" data-title="Поставщик" data-value-text="Нет"> 
						Нет
						<span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fsuppliers']['no']))?$available_properties_values_counters['fsuppliers']['no']:0;?>)</span>
					</label>
				</li>
			</ul>
		</li>
	<?php }?>

	<li class="filter">
		<h3 class="filter-heading">Цена</h3>
		<div class="pl-10_i jsfilter-range">
			<input class="range-slider display-n" type="text" name="bprice" value="" data-min="<?php echo $min_price;?>" data-max="<?php echo $max_price;?>" <?php echo ((!empty($properties_selected['fp']))?'data-fvalue="['.implode(',', $properties_selected['fp']).']"':'');?>/>
			<span class="display-n">
				<input class="js_filter" data-jsfilter-type="range" data-title="Цена" type="text" name="fp" value="<?php echo ((!empty($properties_selected['fp']))?implode('-', $properties_selected['fp']):'');?>"/>
			</span>
		</div>
	</li>

	<?php if($this->lauth->group_view_stocks()){?>
		<li class="filter" data-property-type="stock" data-property="fstock">
			<h3 class="filter-heading">Наличие</h3>
			<ul class="filter-list">
				<li data-property-value="yes" style="<?php echo (int) @$available_properties_values_counters['fstock']['yes'] === 0 ? 'display:none;' :'';?>">
					<label>
						<input class="js_filter" type="checkbox" <?php echo set_checkbox('fstock', 'yes', isset($properties_selected['fstock']) && in_array('yes', $properties_selected['fstock']));?> name="fstock" value="yes" data-title="Наличие" data-value-text="Есть"> 
						Есть
						<span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fstock']['yes']))?$available_properties_values_counters['fstock']['yes']:0;?>)</span>
					</label>
				</li>
				<li data-property-value="no" style="<?php echo (int) @$available_properties_values_counters['fstock']['no'] === 0? 'display:none;' :'';?>">
					<label>
						<input class="js_filter" type="checkbox" <?php echo set_checkbox('fstock', 'no', isset($properties_selected['fstock']) && in_array('no', $properties_selected['fstock']));?> name="fstock" value="no" data-title="Наличие" data-value-text="Нет"> 
						Нет
						<span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fstock']['no']))?$available_properties_values_counters['fstock']['no']:0;?>)</span>
					</label>
				</li>
				<li data-property-value="ordering" style="<?php echo (int) @$available_properties_values_counters['fstock']['ordering'] === 0? 'display:none;' :'';?>">
					<label>
						<input class="js_filter" type="checkbox" <?php echo set_checkbox('fstock', 'ordering', isset($properties_selected['fstock']) && in_array('ordering', $properties_selected['fstock']));?> name="fstock" value="ordering" data-title="Наличие" data-value-text="Под заказ"> 
						Под заказ
						<span class="filter-counter">(<?php echo (isset($available_properties_values_counters['fstock']['ordering']))?$available_properties_values_counters['fstock']['ordering']:0;?>)</span>
					</label>
				</li>
			</ul>
		</li>
	<?php }?>

	<li class="filter" data-property-type="brands" data-property="fb" <?php if(empty($brands)){echo 'style="display:none;"';}?>>
		<h3 class="filter-heading">Брэнды</h3>
		<ul class="filter-list">
			<?php $this->load->view($this->theme->public_view('shop/catalog/brands_view'));?>
		</ul>
	</li>

	<?php echo Modules::run('properties/_get_category_properties', array('id_category' => $category['category_id'], 'items_list' => $all_products_list));?>
</ul>