<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/fancybox'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/daterangepicker'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/clipboard'));?>

<script>
	var dtTable; //obj of datatable
	var dtFilter;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/orders/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "text-left custom-width-50 text-nowrap", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "text-left d-block d-md-none d-xl-table-cell", "aTargets": ["dt_user"], "mData": "dt_user", "bSortable": false},
				{ "sClass": "text-center custom-width-100", "aTargets": ["dt_amount"], "mData": "dt_amount"},
				{ "sClass": "text-center custom-width-100 d-block d-md-none d-xl-table-cell", "aTargets": ["dt_created"], "mData": "dt_created"},
				{ "sClass": "text-left custom-width-200", "aTargets": ["dt_status"], "mData": "dt_status", "bSortable": false},
				{ "sClass": "text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions", "bSortable": false}
			],
			"aaSorting" : [[0,'desc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = initDtFilter('orders');
				}

				aoData = aoData.concat(dtFilter.getDTFilter());

				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						var statuses_amounts = [];
						$.each(data.statuses_amount, function(status_key, status_amount){
							var amounts = [];
							if(Object.keys(status_amount.totals_amount).length){
								$.each(status_amount.totals_amount, function(index, total_amount){
									amounts.push('<span class="badge badge-dark">'+number_format(total_amount.orders_price, true)+' '+total_amount.order_currency+'</span>');
								});
							}

							var amounts_str = amounts.length > 0 ? amounts.join(', ') : '<span class="badge badge-dark">0.00</span>';
							
							var template = '<a href="#" class="custom-margin-left-7 custom-margin-right-7 info-box call-function" data-callback="setStatusFilter" data-filter="status" data-value="'+ status_key +'">\
												<div class="info-box-content">\
													<span class="info-box-text">\
														<span class="badge bg-'+ status_amount.status.color_class +' custom-width-100pr">\
															<i class="'+ status_amount.status.icon +'"></i> '+ status_amount.status.title +'\
														</span>\
													</span>\
													<span class="info-box-number custom-font-size-16">'+ amounts_str +'</span>\
												</div>\
											</a>'
							statuses_amounts.push(template);
						});

						$('.statuses_amount').html(statuses_amounts.join(' '));

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});

		$('#js-order-pickup-date').daterangepicker({
			timePicker: false,
			parentEl: '.modal',
			locale: {
				format: 'DD.MM.YYYY'
			},
			autoUpdateInput: false,
			autoApply: true,
			opens: "left",
    		drops: "up"
		}).on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
		}).on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('').trigger('change');
		});

		$('#js-order-created-date').daterangepicker({
			timePicker: false,
			parentEl: '.modal',
			locale: {
				format: 'DD.MM.YYYY'
			},
			autoUpdateInput: false,
			"autoApply": true,
			"opens": "left",
    		"drops": "up"
		}).on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
		}).on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('').trigger('change');
		});

		$('#js-order-updated-date').daterangepicker({
			timePicker: false,
			parentEl: '.modal',
			locale: {
				format: 'DD.MM.YYYY'
			},
			autoUpdateInput: false,
			"autoApply": true,
			"opens": "left",
    		"drops": "up"
		}).on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
		}).on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('').trigger('change');
		});
	});

	var setStatusFilter = function(element){
		var $this = $(element);
		var $select = $('#dtFilter').find('[name="'+ $this.data('filter') +'"]')
		var $option = $select.find('option[value="'+ $this.data('value') +'"]');
		$option.prop('selected', true);
		$select.trigger('change');
	}

	var clipboardBtn = new Clipboard('[data-clipboard-btn]', {
		text: function (trigger) {
			var text_element = $('body').find($(trigger).data('clipboard-target')).html();
			return text_element;
		}
	});
	clipboardBtn.on('success',function(e){
		systemMessages('Скопировано!', 'success');
	});
	clipboardBtn.on('error',function(e){
		systemMessages('Не скопировано!', 'error');
	});
</script>