<script>
    var clearFilemanagerImage = function(btn){
        var $this = $(btn);
        $('body').find($($this.data('target'))).val('');
    }

    var previewFilemanagerImage = function(btn){
        var $this = $(btn);
        var image = $('body').find($($this.data('target'))).val();
        if(image != ''){
            $.fancybox.open([
                {
                    src  : base_url + 'files/uploads/' + image
                }
            ]);
        } else{
            systemMessages( 'Нет картинки.', 'error' );
        }
    }
    
    function openFilemanager(element){
        var h = screen.height - screen.height/5;
        var w = screen.width - screen.width/5;
        var l = Math.floor((screen.width - w) / 2);
        var t = Math.floor((screen.height - h) / 2);
        var win = window.open($(element).data('src'), 'Filemanager', "scrollbars=1,width="+ w +",height="+ h +",top="+ t +",left="+ l);
    }
</script>