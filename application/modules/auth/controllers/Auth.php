<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
        $this->data = array();
        $this->breadcrumbs = array();
		$this->view_module_path = 'auth/';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		return_404($this->data);
	}
	
	function signin(){
		redirect('/');
	}
	
	function register(){
		if($this->lauth->logged_in()){
			if($this->lauth->is_admin()){
				redirect('admin');
			} else{
				redirect('account');
			}
		}

		$this->data['stitle'] = 'Регистрация';

        $this->breadcrumbs[] = array(
			'title' => 'Регистрация',
			'link' => base_url('auth/register')
		);

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['main_content'] = 'auth/register_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}
	
	function forgot_password(){
		if($this->lauth->logged_in()){
			if($this->lauth->is_admin()){
				redirect('admin');
			} else{
				redirect('account');
			}
		}

		$this->data['stitle'] = 'Востановить пароль';

        $this->breadcrumbs[] = array(
			'title' => 'Востановить пароль',
			'link' => base_url('auth/signin')
		);

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'auth/forgot_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}
	
	function reset_password(){
		if($this->lauth->logged_in()){
			if($this->lauth->is_admin()){
				redirect('admin');
			} else{
				redirect('account');
			}
		}
		
		$this->data['token'] = $this->uri->segment(2);
		$user_data = $this->auth_model->handler_get_by_reset_token($this->data['token']);

		if(!$user_data){
			return_404($this->data);
		}

		$this->data['stitle'] = 'Востановить пароль';

        $this->breadcrumbs[] = array(
			'title' => 'Востановить пароль',
			'link' => base_url('auth/signin')
		);

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'auth/reset_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'fb_signin':
				if($this->lauth->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}

				if($this->input->post('status', true) !== 'connected'){
					jsonResponse('Данные для входа неверны.');
				}

				$authResponse = $this->input->post('authResponse');
				$social_data = $this->socials_auth->get_fb_user($authResponse['accessToken']);
				if($social_data['status'] == 'error'){
					jsonResponse('Данные для входа неверны.','error',array('social_data' => $social_data));
				}

				if(empty($social_data['user']['email'])){
					jsonResponse('К вашему акаунту в Facebook не привязан email. Добавите email в настройках акаунта в Facebook и попробуйте снова.');
				}

				$user_email = $social_data['user']['email'];
				$user_data 	= $this->auth_model->handler_get_social_login_info($user_email);				
				if(!$user_data){
					$group = $this->auth_model->handler_get_group_default();
					if(empty($group)){
						jsonResponse('Данные не верны.');
					}

					$insert = array(
						'id_group' => $group->id_group,
						'user_login' => $social_data['user']['name'],
						'user_pass' => $this->lauth->hash_password(uniqid()),
						'user_nicename' => $social_data['user']['name'],
						'user_email' => $user_email,
						'registered_date' => date('Y-m-d H:i:s'),
						'status' => 1,
						'activation_key' => md5($user_email.uniqid().'@cc@ctivate'),
						'signin_type' => 'fb'
					);						
					$id_user = $this->auth_model->handler_insert($insert);
					$user_data 	= $this->auth_model->handler_get_social_login_info($user_email);
				}

				if($user_data->status == 0){
					jsonResponse('Учетная запись не активна.');
				}

				if($user_data->user_banned == 1){
					jsonResponse('Учетная запись заблокирована.');
				}

				$user_data->signin_type = 'fb';
				
				$update = array(
					'signin_type' => 'fb'
				);
				$this->auth_model->handler_update($user_data->id, $update);

				$this->lauth->set_sessiondata($user_data);

				$_city = Modules::run('cities/_get_city', $user_data->user_city);	
				if(!empty($_city)){
					set_cookie('_at_city', $user_data->user_city, 2592000, '/');
				}

				if($this->lauth->is_admin()){
					$redirect = 'admin';
				} else{
					$redirect = 'account';
				}

				jsonResponse('', 'success', array('redirect' => $redirect));				
			break;
			case 'signin':
				if($this->lauth->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}
				
				$config = array(
					array(
						'field' => 'user_email',
						'label' => 'E-mail',
						'rules' => 'required|xss_clean|trim|valid_email',
					),
					array(
						'field' => 'password',
						'label' => 'Password',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$user_email = $this->input->post('user_email', true);
				$password 	= $this->input->post('password', true);
				$user_data 	= $this->auth_model->handler_get_login_info($user_email, $this->lauth->hash_password($password));
				
				if($user_data){
					if($user_data->status == 0){
						jsonResponse('Учетная запись не активна.');
					}

					if($user_data->user_banned == 1){
						jsonResponse('Учетная запись заблокирована.');
					}

					$update = array(
						'signin_type' => 'site'
					);
					$this->auth_model->handler_update($user_data->id, $update);
					$user_data->signin_type = 'site';
					$this->lauth->set_sessiondata($user_data);					

					$_city = Modules::run('cities/_get_city', $user_data->user_city);	
					if(!empty($_city)){
						set_cookie('_at_city', $user_data->user_city, 2592000, '/');
					}

					if($this->lauth->is_admin()){
						$redirect = 'admin';
					} else{
						$redirect = '';
					}

					jsonResponse('', 'success', array('redirect' => $redirect));
				} else{
					jsonResponse('Данные для входа неверны.');
				}
			break;
			case 'signup':
				if($this->lauth->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}
				
				$config = array(
					array(
						'field' => 'username',
						'label' => 'Cum sa ne adresăm Dvs.?',
						'rules' => 'required|trim|xss_clean|max_length[50]',
					),
					array(
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email|is_unique[users.user_email]|xss_clean',
					),
					array(
						'field' => 'password',
						'label' => 'Parola',
						'rules' => 'trim|required|xss_clean|min_length[6]',
					),
					array(
						'field' => 'confirm_password',
						'label' => 'Confirmă Parola',
						'rules' => 'trim|required|xss_clean|min_length[6]|matches[password]',
					)
				);

				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$group = $this->auth_model->handler_get_group_default();
				if(empty($group)){
					jsonResponse('Datele sunt incorecte.');
				}

				$insert = array(
					'id_group' => $group->id_group,
					'user_login' => $this->input->post('username',true),
					'user_pass' => $this->lauth->hash_password($this->input->post('password',true)),
					'user_nicename' => $this->input->post('username',true),
					'user_email' => $this->input->post('email',true),
					'registered_date' => date('Y-m-d H:i:s'),
					'activation_key' => md5($this->input->post('email',true).uniqid().'@cc@ctivate')
				);

				$id_user = $this->auth_model->handler_insert($insert);

				if($id_user){
					$email_data = $insert;
					$email_data['email_content'] = 'user_activation_tpl';

					Modules::run('email/send', array(
						'to' => $insert['user_email'],
						'subject' => 'Înregistrare utilizator nou.',
						'email_data' => $email_data
					));
				}else{
					jsonResponse('Пожалуйста попробуйте поже!');
				}

				jsonResponse('Пожалуйста активируйте ваш кабинет!', 'success');
			break;
			case 'forgot_password':
				if($this->lauth->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}
				
				$config = array(
					array(
						'field' => 'email',
						'label' => 'E-mail',
						'rules' => 'trim|required|valid_email|xss_clean',
					)
				);

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$user_email = $this->input->post('email', true);
				$user_data 	= $this->auth_model->handler_get_by_email($user_email);
				if($user_data){
					if($user_data->status == 0){
						jsonResponse('Учетная запись не активна.');
					} else{
						$update = array(
							'reset_key' => md5($user_email.uniqid().'@ccreset')
						);
						$this->auth_model->handler_update($user_data->id, $update);

						$update['email_content'] = 'user_reset_password_tpl';

						Modules::run('email/send', array(
							'to' => $user_email,
							'subject' => 'Востановление пароля',
							'email_data' => $update
						));

						$this->session->set_flashdata('flash_message', '<li class="message-success zoomIn">Письмо о востановлений пароля отправленно. <i class="ca-icon ca-icon_remove"></i></li>');
						jsonResponse('', 'success');
					}				
				} else{
					jsonResponse('Данные не верны.');
				}
			break;
			case 'reset_password':
				if($this->lauth->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}
				
				$config = array(
					array(
						'field' => 'token',
						'label' => 'Код',
						'rules' => 'trim|required|xss_clean',
					),
					array(
						'field' => 'password',
						'label' => 'Новый пароль',
						'rules' => 'trim|required|xss_clean|min_length[6]',
					),
					array(
						'field' => 'confirm_password',
						'label' => 'Повторите пароль',
						'rules' => 'trim|required|xss_clean|matches[password]',
					)
				);

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				
				$token = $this->input->post('token', true);
				$user_data = $this->auth_model->handler_get_by_reset_token($token);
				if($user_data){
					if($user_data->status == 0){
						jsonResponse('Учетная запись не активна.');
					} else{
						$update = array(
							'reset_key' => md5($user_data->user_email.uniqid().'@ccreset'),
							'user_pass' => $this->lauth->hash_password($this->input->post('password',true))
						);
						$this->auth_model->handler_update($user_data->id, $update);

						$this->session->set_flashdata('flash_message', '<li class="message-success zoomIn">Пароль был изменен. <i class="ca-icon ca-icon_remove"></i></li>');
						jsonResponse('', 'success');
					}				
				} else{
					jsonResponse('Данные не верны.');
				}
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'form':
				if($this->lauth->logged_in()){
					jsonResponse('', 'info', [
						'is_logged' => true
					]);
				}

				$content = $this->load->view($this->theme->public_view($this->view_module_path . 'form_view'), $this->data, true);

				jsonResponse('', 'success', array('popup_content' => $content));				
			break;
			case 'edit':
				if(true !== $this->lauth->logged_in()){
					jsonResponse('Nu sinteti autorizat.');
				}

				$content = $this->load->view($this->theme->public_view($this->view_module_path . 'edit_view'), $this->data, true);

				jsonResponse('', 'success', array('popup_content' => $content));				
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function logout(){
		$this->lauth->logout();
		redirect('');
	}

	function activate(){
		if($this->lauth->logged_in()){
			if($this->lauth->is_admin()){
				redirect('admin');
			} else{
				redirect('account');
			}
		}

		$token = $this->uri->segment(2);
		$user_data = $this->auth_model->handler_get_by_token($token);

		if(!$user_data){
			return_404($this->data);
		}
		
		if($user_data->status === 1){
			redirect('/');
		}

		$update = array(
			'status' => 1
		);
		$this->auth_model->handler_update($user_data->id, $update);
		$this->session->set_flashdata('flash_message', '<li class="message-success zoomIn">Акаунт активирован. <i class="ca-icon ca-icon_remove"></i></li>');
		redirect('/');
	}
}
