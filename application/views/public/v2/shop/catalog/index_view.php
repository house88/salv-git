<section class="py-5 products-listing bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="filters mobile-filters shadow-sm rounded bg-white mb-4 d-none d-block d-md-none">
					<div class="border-bottom">
						<a class="h6 font-weight-bold text-dark d-block m-0 p-3" data-toggle="collapse" href="#mobile-filters" role="button" aria-expanded="false" aria-controls="mobile-filters">Filtru <i class="icofont-arrow-down float-right mt-1"></i></a>
					</div>
					<div id="mobile-filters" class="filters-body collapse multi-collapse">
						<?php $this->load->view($this->theme->public_view('shop/catalog/filter_view'));?>
					</div>
				</div>
				<div class="filters shadow-sm rounded bg-white mb-3 d-none d-sm-none d-md-block">
					<div class="filters-body">
						<?php $this->load->view($this->theme->public_view('shop/catalog/filter_view'));?>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="shop-head mb-3">
					<div class="btn-group float-right mt-2 d-none d-sm-none d-md-block">
						<button type="button" class="btn btn-dark btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="icofont icofont-filter"></span> Sortare dupa: <?php echo $list_sort_by[$sort_by]['title'];?>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<?php foreach($list_sort_by as $sort_key => $sort_item){?>
								<a href="#" class="dropdown-item call-function" data-callback="sort_products" data-sortby="<?php echo $sort_key;?>">
									<?php echo $sort_item['title'];?>
								</a>
							<?php }?>
						</div>
					</div>
					<h5 class="mb-1 text-dark"><?php echo $category['category_title'];?></h5>
					
					<a href="<?php echo base_url();?>">
						<span class="icofont icofont-ui-home"></span> Home
					</a> 							
					<?php foreach($categoryBreadcrumbs as $categoryBreadcrumb){?>
						<span class="icofont icofont-thin-right"></span> 
						<?php if(!empty($categoryBreadcrumb['link'])){?>
							<a href="<?php echo $categoryBreadcrumb['link'];?>"><?php echo $categoryBreadcrumb['title'];?></a>
						<?php } else{?>
							<span><?php echo $categoryBreadcrumb['title'];?></span>
						<?php }?>
					<?php }?>
				</div>

				<?php $this->load->view($this->theme->public_view('shop/catalog/products_view'));?>
			</div>
		</div>
	</div>
</section>