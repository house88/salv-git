<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="js-create-order-popup-title">Plasează comanda</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="js-popup-create-order-form">
            <?php $userDetails = (array) $this->lauth->user_data();?>
            <div class="modal-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Cum sa ne adresăm Dvs.?</label>
                        <input name="name" type="text" value="<?php echo @$userDetails['user_nicename'];?>" class="form-control" placeholder="ex. John Smith">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Email</label>
                        <input name="email" type="text" value="<?php echo @$userDetails['user_email'];?>" class="form-control" placeholder="ex. example@example.com">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Telefon</label>
                        <input name="phone" type="text" value="<?php echo @$userDetails['user_phone'];?>" class="form-control" placeholder="0xxxxxxxx">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Comentariu</label>
                        <textarea class="form-control" name="comment" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">Anulare</button>
                <button type="submit" class="btn d-flex w-50 text-center justify-content-center btn-primary">Trimite</button>
            </div>
        </form>
    </div>
</div>

<script type="text/template" id="js-create-order-success-message-template">
    <div class="modal-body text-center">
        <p>Ați plasat comanda cu numarul <strong>{{orderNumber}}</strong>.</p>
        <p>În curînd veți fi contactat de un operator.</p>
    </div>
    <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn d-flex w-50 text-center justify-content-center btn-outline-primary" data-dismiss="modal">Închide</button>
    </div>
</script>

<script>
    $(function(){
        $('#js-popup-create-order-form').on('submit', function(e){
            var $form = $(this);
            var fdata = $form.serialize();

            $.ajax({
				type: 'POST',
				url: base_url+'orders/ajax_operations/add',
				data: fdata,
				dataType: 'JSON',
                beforeSend: function(){
                    showLoader('body');
                },
				success: function(resp){
                    hideLoader('body');
					if(resp.mess_type == 'success'){
                        $('#js-create-order-popup-title').text('Vă vmulțumim');
                        var template = $('#js-create-order-success-message-template').text();
                        var message = template.replace("{{orderNumber}}", resp.orderNumber);
                        $form.replaceWith(message);
                        $('body').removeClass('toggled');
                        updateSmallCart({
                            'cartProductsCounter': 0,
                            'cartProductsCounter': 0,
                            'cartProductsContent': `<div class="alert alert-primary rounded-0" role="alert">Cosul este gol :(</div>`,
                        });
					} else{
                        systemMessages(resp.message, resp.mess_type);
                    }
				}
			});
            return false;
        });
    });
</script>