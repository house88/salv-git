<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use App\Services\PhoneCodesService;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\NumberParseException;

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/orders/';
		$this->active_menu = 'orders';
		$this->active_submenu = 'orders';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model('Orders_model', 'orders');
		$this->load->model('items/Items_Model', 'items');
		$this->load->model("delivery/Delivery_model", "delivery");
		$this->load->model("payment/Payment_model", "payment");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_orders', '/admin');

		$order_statuses = get_order_statuses();
		$params = array();
		foreach ($order_statuses as $status_key => $order_status) {
			$params['status'] = $status_key;
			$this->data['statuses_amount'][$status_key] = array(
				'totals_amount' => $this->orders->handler_sum_all($params),
				'status' => $order_status
			);
		}

		$this->data['users'] = $this->orders->handler_get_orders_users();
		$this->data['delivery_options'] = $this->delivery->handler_get_all(array('active' => 1));
		$this->data['payment_options'] = $this->payment->handler_get_all(array('active' => 1));

		$this->data['page_header'] = 'Список заказов';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'orders_scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'change':
				checkPermisionAjax('manage_orders');

				$order = $this->input->post('order');
				if(empty($order)){
					jsonResponse('Данные не верны!');
				}

				$id_order = (int) $order['id_order'];
				$order_info = $this->orders->handler_get($id_order);
				if(empty($order_info)){
					jsonResponse('Данные не верны!');
				}
				
				$status = (int) $order['status'];

				$changes = array();
				$admin_changes = array();
				$update_order = array();
				$update_ordered_items = array();

				$ordered_items = arrayByKey(json_decode($order_info['order_ordered_items'], true), 'id_order_item');
				$ordered_items_new = $order['ordered_items'];
				$order_price = 0;
				$order_price_final = 0;
				$items_cashback_price = 0;
				foreach ($ordered_items_new as $item_key => $ordered_item) {
					if(!array_key_exists($item_key, $ordered_items)){
						continue;
					}

					$update_ordered_items[$item_key]['id_order_item'] = $item_key;
					if($ordered_items[$item_key]['item_quantity'] != $ordered_item['quantity']){
						$changes['items_quantity'] = 'Изменилось количество товаров.';
						$admin_changes['items_quantity'] = 'Изменилось количество товаров.';
						$update_ordered_items[$item_key]['item_quantity'] = (int) $ordered_item['quantity'];
					}

					$item_discount = (float) $ordered_item['discount'];
					if($ordered_items[$item_key]['item_discount'] != $item_discount){
						$changes['item_discount'] = 'Изменилась скидка на товары.';
						$admin_changes['item_discount'] = 'Изменилась скидка на товары.';
						$update_ordered_items[$item_key]['item_discount'] = $item_discount;
					}
					
					$update_ordered_items[$item_key]['item_price_final'] = numberFormat(($ordered_items[$item_key]['item_price'] - ($ordered_items[$item_key]['item_price'] * $item_discount / 100)) * $ordered_item['quantity'], $order_info['order_price_decimals']);
					$order_price += numberFormat($ordered_items[$item_key]['item_price'], $order_info['order_price_decimals']) * $ordered_item['quantity'];
					$order_price_final += $update_ordered_items[$item_key]['item_price_final'];
					$items_cashback_price += $ordered_items[$item_key]['item_cashback_price'] * $ordered_item['quantity'];
				}
				
				if(!empty($update_ordered_items)){
					$this->orders->handler_update_orders_items($update_ordered_items);
					$ordered_items = $this->orders->handler_get_orders_items($id_order);
					$update_order['order_ordered_items'] = json_encode($ordered_items);
				}

				// UPDATE USER CASHBACK
				$user_info = $this->auth_model->handler_get_by_id($order_info['id_user']);
				if(!empty($user_info)){
					if(in_array($order_info['order_status'], array_keys(get_order_processing_statuses())) && array_key_exists($status, get_order_final_status())){
						$user_cashback_balance = $user_info->user_cashback_balance + $items_cashback_price - $order_info['order_cashback'];
						$this->auth_model->handler_update($order_info['id_user'], array('user_cashback_balance' => $user_cashback_balance));

						if($user_cashback_balance < 0){
							$update_order['order_cashback'] = $user_cashback_balance;
						}
					}
				}

				// ORDER PRICE
				$update_order['order_price'] = $order_price;

				// ORDER FINAL PRICE
				$update_order['order_price_final'] = $order_price_final;
				if($order_info['order_price_final'] != $order_price_final){
					$changes['price_final'] = 'Изменилась цена заказа.';
					$admin_changes['price_final'] = 'Изменилась цена заказа: ' . $order_price_final;
				}

				// PRICE DISCOUNT
				$discount_by_currecy = $order_price - $order_price_final;
				$update_order['order_discount_by_currecy'] = $discount_by_currecy;

				// PERCENTS DISCOUNT
				$discount_by_percent = $order_price > 0 ? 100 - ($order_price_final * 100/$order_price) : 0;
				$update_order['order_discount_by_percent'] = $discount_by_percent;
				if($order_info['order_discount_by_percent'] != $discount_by_percent){
					$changes['discount'] = 'Изменилась скидка заказа.';
					$admin_changes['discount'] = 'Изменилась скидка заказа: %' . $discount_by_percent;
				}

				if(!isset($order['order_client_not_responding']) && $order_info['order_client_not_responding'] == 1){
					$update_order['order_client_not_responding'] = 0;
					$changes['order_client_not_responding'] = 'Убран признак: Клиент не отвечает.';
					$admin_changes['order_client_not_responding'] = 'Убран признак: Клиент не отвечает.';
				}

				$ordered_items = arrayByKey($ordered_items, 'id_order_item');
				foreach ($order as $order_key => $value) {
					switch ($order_key) {
						case 'status':
							$current_order_status = get_order_status($order_info['order_status']);
							if($current_order_status['is_final'] === false){
								$status = xss_clean($value);
								$order_status = get_order_status($status);
								if($order_info['order_status'] != $status){
									$update_order['order_status'] = $status;
									$changes['status'] = 'Изменился статус заказа: '.$order_status['title'].'.';
									$admin_changes['status'] = 'Изменился статус заказа: '.$order_status['title'];
								}
							}
						break;
						case 'order_client_not_responding':
							if($order_info['order_client_not_responding'] == 0){
								$update_order['order_client_not_responding'] = 1;
								$changes['order_client_not_responding'] = 'Добавлен признак: Клиент не отвечает.';
								$admin_changes['order_client_not_responding'] = 'Добавлен признак: Клиент не отвечает.';
							}
						break;
						case 'user_name':
							$user_name = xss_clean($value);
							if($order_info['order_user_name'] != $user_name){
								$update_order['order_user_name'] = $user_name;
								$changes['user_name'] = 'Изменилось Имя клиента.';
								$admin_changes['user_name'] = 'Изменилось Имя клиента: ' . $user_name;
							}
						break;
						case 'user_email':
							$user_email = xss_clean($value);
							if($order_info['order_user_email'] != $user_email){
								$update_order['order_user_email'] = $user_email;
								$changes['user_email'] = 'Изменился Email клиента.';
								$admin_changes['user_email'] = 'Изменился Email клиента: ' . $user_email;
							}
						break;
						case 'user_phone':
							$user_phone = xss_clean($value);
							$phoneFormatResult = formatPhoneNumber($user_phone);
							if(true === $phoneFormatResult['is_valid']){
								$user_phone = $phoneFormatResult['formated'];
							}

							if($order_info['order_user_phone'] != $user_phone){
								$update_order['order_user_phone'] = $user_phone;
								$changes['user_phone'] = 'Изменился Телефон клиента.';
								$admin_changes['user_phone'] = 'Изменился Телефон клиента: ' . $user_phone;
							}
						break;
						case 'user_comment':
							$user_comment = xss_clean($value);
							if($order_info['order_user_comment'] != $user_comment){
								$update_order['order_user_comment'] = $user_comment;
								$changes['user_comment'] = 'Изменился коментарий клиента.';
								$admin_changes['user_comment'] = 'Изменился коментарий клиента: ' . nl2br($user_comment);
							}
						break;
						case 'admin_comment':
							$admin_comment = xss_clean($value);
							if($order_info['order_admin_comment'] != $admin_comment){
								$update_order['order_admin_comment'] = $admin_comment;
								$admin_changes['admin_comment'] = 'Изменился коментарий администратора: ' . nl2br($admin_comment);
							}
						break;
					}
				}

				if(!empty($admin_changes)){
					$admin_timeline = json_decode($order_info['order_admin_timeline'], true);
					$admin_info = $this->lauth->user_data();
					$admin_timeline[] = array(
						'date' => date('d.m.Y H:i:s'),
						'name' => $admin_info->user_nicename,
						'notes' => $admin_changes
					);
					$update_order['order_admin_timeline'] = json_encode($admin_timeline);
				}

				if(!empty($update_order)){
					$this->orders->handler_update($id_order, $update_order);
					$this->orders->handler_update_search_info($id_order);
				}

				if(!empty($changes)){
					$order = $this->orders->handler_get($id_order);
					$email_data = array(
						'order' => $order,
						'title' => 'Изминения в заказе номер '.orderNumber($id_order).' на сайте salv.md',
						'changes' => $changes,
						'email_content' => 'order_detail_admin_tpl'
					);
					
					Modules::run('email/send', array(
						'to' => $this->data['settings']['order_admin_email']['setting_value'],
						'subject' => 'Изминения в заказе номер '.orderNumber($id_order).' на сайте salv.md',
						'email_data' => $email_data
					));

					if($this->input->post('notify_user')){
						$email_data['email_content'] = 'order_detail_tpl';
						Modules::run('email/send', array(
							'to' => $email_data['order']['order_user_email'],
							'subject' => 'Modificarea comenzii cu numarul '.orderNumber($id_order).' pe salv.md',
							'email_data' => $email_data
						));
					}
				}

				jsonResponse('Сохраненно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_orders');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'id_order-desc' )
				);

				$params['sort_by'] = flat_dt_ordering($_POST, array(
					'dt_name'     => 'id_order',
					'dt_amount'   => 'order_price_final',
					'dt_created'  => 'order_created'
				));
		
				//region Filters
				if($this->input->post('user')){
					$params['id_user'] = (int) $this->input->post('user');
				}
		
				if($this->input->post('id_manager')){
					$params['id_manager'] = (int) $this->input->post('id_manager');
				}
		
				if($this->input->post('status_client')){
					$params['status_client'] = (int) $this->input->post('status_client');
				}
		
				if($this->input->post('status')){
					$params['status'] = (int) $this->input->post('status');
				}
				
				if($this->input->post('order_price_final_from')){
					$params['order_price_final_from'] = (int) $this->input->post('order_price_final_from');
				}
		
				if($this->input->post('order_price_final_to')){
					$params['order_price_final_to'] = (int) $this->input->post('order_price_final_to');
				}
		
				if($this->input->post('order_created')){
					list($order_created_from, $order_created_to) = explode(' - ', $this->input->post('order_created'));

					if(validateDate($order_created_from, 'd.m.Y')){
						$params['order_created_from'] = getDateFormat($order_created_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($order_created_to, 'd.m.Y')){
						$params['order_created_to'] = getDateFormat($order_created_to, 'd.m.Y', 'Y-m-d');
					}
				}
		
				if($this->input->post('order_updated')){
					list($order_updated_from, $order_updated_to) = explode(' - ', $this->input->post('order_updated'));

					if(validateDate($order_updated_from, 'd.m.Y')){
						$params['order_last_update_from'] = getDateFormat($order_updated_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($order_updated_to, 'd.m.Y')){
						$params['order_last_update_to'] = getDateFormat($order_updated_to, 'd.m.Y', 'Y-m-d');
					}
				}
		
				if($this->input->post('keywords')){
					$params['keywords'] = $this->input->post('keywords', true);
				}
				//endregion Filters
				
				$records = $this->orders->handler_get_all($params);
				$records_total = $this->orders->handler_get_count($params);
		
				$order_statuses = get_order_statuses();				
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						$id_manager = (int) $record['id_manager'];
						$status = get_order_status($record['order_status']);

						$client_not_responding = '';
						if((int) $record['order_client_not_responding'] === 1){
							$client_not_responding = '<div><span class="badge bg-warning">Клиент не отвечает</span></div>';
						}
			
						$_actions = array();
						if ($this->lauth->have_right('view_order_logs')) {
							$_actions[] = '<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/orders/popup/logs/'.$record['id_order']).'" >
												<i class="fad fa-clipboard-list"></i> Логи заказа
											</a>';
						}

						$_additional_actions = '';
						if(!empty($_actions)){
							$_additional_actions = '<div class="btn-group" role="group">
														<button id="js-dropdown-order-'.$record['id_order'].'-actions" type="button" class="btn btn-default btn-flat btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<i class="fas fa-ellipsis-h"></i>
														</button>
														<div class="dropdown-menu dropdown-menu-right" aria-labelledby="js-dropdown-order-'.$record['id_order'].'-actions">
															'. implode('', $_actions) .'
														</div>
													</div>';
						}
			
						return array(
							'dt_name'				=> orderNumber($record['id_order']),
							'dt_user'				=> "{$client_not_responding}
														<div>Имя: <strong>{$record['order_user_name']}</strong></div>
														<div>Email: <strong>{$record['order_user_email']}</strong></div>
														<div>Тел: <strong>{$record['order_user_phone']}</strong></div>",
							'dt_amount'				=> numberFormat($record['order_price_final'] + $record['order_delivery_price'], $record['order_price_decimals']) .' '. $record['order_currency'],
							'dt_created'			=> getDateFormat($record['order_created']),
							'dt_status' 			=> '<span class="badge bg-'.$status['color_class'].'">'.$status['title'].'</span>',
							'dt_actions'			=> '<div class="btn-group">
															<button type="button" class="btn btn-secondary btn-flat btn-sm call-popup" data-popup="#general_popup_form" data-href="'.base_url('admin/orders/popup/edit/' . $record['id_order']).'">
																<i class="fad fa-eye"></i>
															</button>
															'. $_additional_actions .'
														</div>'
						);
					}, $records)
				);
				
				foreach ($order_statuses as $status_key => $order_status) {
					$params['status'] = $status_key;
					$output['statuses_amount'][$status_key] = array(
						'totals_amount' => $this->orders->handler_sum_all($params),
						'status' => $order_status
					);
				}
				
				jsonResponse('', 'success', $output);
			break;
		}
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'edit':
				checkPermisionAjax('manage_orders');

				$id_order = (int) $this->uri->segment(5);
				if(empty($id_order)){
					jsonResponse('Данные не верны!');
				}

				$this->data['order'] = $this->orders->handler_get($id_order);
				if(empty($this->data['order'])){
					jsonResponse('Данные не верны!');
				}
				
				$this->data['ordered_items'] = json_decode($this->data['order']['order_ordered_items'], true);
				$this->data['opened_orders'] = (in_array($this->data['order']['order_status'], array('1','2','3')) && $this->data['order']['id_user'] > 0) ? $this->orders->handler_get_count(array('id_user' => $this->data['order']['id_user'], 'status' => array('1','2','3'))) : 0;

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'logs':
				checkPermisionAjax('view_order_logs');

				$id_order = (int) $this->uri->segment(5);
				if(empty($id_order)){
					jsonResponse('Данные не верны!');
				}

				$this->data['order'] = $this->orders->handler_get($id_order);
				if(empty($this->data['order'])){
					jsonResponse('Данные не верны!');
				}

				$logs = json_decode($this->data['order']['order_admin_timeline'], true);
				if(!empty($logs)){
					usort($logs, function($log1, $log2){
						return $log1['date'] <= $log2['date'];
					});
				} else{
					$logs = array();
				}

				$this->data['order_logs'] = $logs;

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'logs_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны!');
			break;
		}
	}
}
