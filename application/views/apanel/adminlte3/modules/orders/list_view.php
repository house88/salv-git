<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header pl-2 pl-sm-3 pr-2 pr-sm-3">
					<h3 class="card-title">
						<?php echo $page_header;?>, <small>найдено: <span id="total_dtTable-counter">0</span></small>
					</h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-toggle="modal" data-target="#dtFilter" data-toggle-class="modal-open-aside">
							<i class="fas fa-filter"></i>
						</button>
					</div>
				</div>
				<div class="card-body pl-2 pl-sm-3 pr-2 pr-sm-3">
					<div class="row d-none d-md-flex flex-wrap justify-content-start statuses_amount">
						<?php foreach($statuses_amount as $status_key => $status_amount){?>
							<a class="custom-margin-left-7 custom-margin-right-7 info-box call-function" data-callback="setStatusFilter" data-filter="status" data-value="<?php echo $status_key;?>" href="#">
								<div class="info-box-content">
									<span class="info-box-text">
										<span class="badge bg-<?php echo $status_amount['status']['color_class'];?> custom-width-100pr">
											<i class="<?php echo $status_amount['status']['icon'];?>"></i> <?php echo $status_amount['status']['title'];?>
										</span>	
									</span>
									<span class="info-box-number custom-font-size-16">											
										<?php $amounts = array();?>
										<?php 
											foreach($status_amount['totals_amount'] as $total_amount){
												$amounts[] = '<span class="badge badge-dark">'.numberFormat($total_amount['orders_price'], true).' '.$total_amount['order_currency'].'</span>';
											}

											echo !empty($amounts) ? implode(', ', $amounts) : '<span class="badge badge-dark">0.00</span>';
										?>
									</span>
								</div>
							</a>
						<?php }?>
					</div>
					<div class="dtfilter-list">
						<div class="filter-plugin-list d-flex justify-content-start flex-wrap"></div>
					</div>
					
					<?php if(!empty($dt_filter)){?>
						<?php $this->load->view($this->theme->apanel_view('includes/filter'));?>
					<?php }?>

					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_name">Номер заказа</th>
									<th class="dt_user">Клиент</th>
									<th class="dt_amount">Сумма</th>
									<th class="dt_created">Создан</th>
									<th class="dt_status">Статус</th>
									<th class="dt_actions"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>