<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/currency/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'currency';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Currency_model", "currency");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		checkPermision('manage_currency', '/admin');

		$this->data['page_header'] = 'Валюты сайта';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'currency_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_currency');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_currency');

				$id_currency = (int)$this->uri->segment(5);
				$this->data['currency'] = $this->currency->handler_get($id_currency);
				if(empty($this->data['currency'])){
					jsonResponse('Валюты не существует.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_currency');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[30]');
				$this->form_validation->set_rules('iso_code', 'ISO Код', 'required|xss_clean|max_length[3]');
				$this->form_validation->set_rules('symbol', 'Символ', 'required|xss_clean|max_length[20]');
				$this->form_validation->set_rules('exchange_rate', 'Курс', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'currency_name' => $this->input->post('title'),
					'currency_code' => $this->input->post('iso_code'),
					'currency_symbol' => $this->input->post('symbol'),
					'currency_rate' => $this->input->post('exchange_rate')
				);

				if($this->input->post('active')){
					$insert['currency_active'] = 1;
				}

				$this->currency->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_currency');

                $this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[30]');
				$this->form_validation->set_rules('iso_code', 'ISO Код', 'required|xss_clean|max_length[3]');
				$this->form_validation->set_rules('symbol', 'Символ', 'required|xss_clean|max_length[20]');
				$this->form_validation->set_rules('exchange_rate', 'Курс', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_currency = (int)$this->input->post('currency');
				$update = array(
					'currency_name' => $this->input->post('title'),
					'currency_code' => $this->input->post('iso_code'),
					'currency_symbol' => $this->input->post('symbol'),
					'currency_rate' => $this->input->post('exchange_rate'),
					'currency_active' => 0
				);

				if($this->input->post('active')){
					$update['currency_active'] = 1;
				}

				$this->currency->handler_update($id_currency, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_currency');

				$this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_currency = (int)$this->input->post('currency');
				$currency = $this->currency->handler_get($id_currency);
				if(empty($currency)){
					jsonResponse('Валюта не существует.');
				}

				if($currency['currency_default'] == 1){
					jsonResponse('Вы не можете удалить главную валюту сайта.');
				}

				$this->currency->handler_delete($id_currency);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_currency');

				$this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_currency = (int)$this->input->post('currency');
				$currency = $this->currency->handler_get($id_currency);
				if(empty($currency)){
					jsonResponse('Валюта не существует.');
				}

				$action = $this->uri->segment(5);
				switch($action){
					case 'default' :
						$this->currency->handler_set_default($id_currency);
					break;
					case 'apanel' :
						$this->currency->handler_set_default_admin($id_currency);
					break;
					case 'public' :
						$this->currency->handler_set_default_user($id_currency);
					break;
					case 'active' :
						if($currency['currency_active'] == 1){
							$status = 0;
						} else{
							$status = 1;
						}
						$this->currency->handler_update($id_currency, array('currency_active' => $status));
					break;
					default:
						jsonResponse('Данные не верны.');
					break;
				}

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_currency');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'id_currency-asc' )
				);

				$records = $this->currency->handler_get_all($params);
				$records_total = $this->currency->handler_get_count($params);
				$default_currency = $this->currency->handler_get_default();
				
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($default_currency){
						$delete_btn = '';
						if($record['currency_default'] == 0){
							$delete_btn = '<div class="dropdown-divider"></div>
											<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить валюту?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-currency="'.$record['id_currency'].'" data-type="bg-danger">
												<i class="fad fa-trash-alt"></i> Удалить
											</a>';
						}

						return array(
							'dt_id'			=>  $record['id_currency'],
							'dt_name'		=>  $record['currency_name'],
							'dt_code'		=>  $record['currency_code'],
							'dt_symbol'		=>  $record['currency_symbol'],
							'dt_rate'		=>  '1 '.$default_currency['currency_symbol'].' = '.$record['currency_rate'].' '.$record['currency_symbol'],
							'dt_default'	=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['currency_default'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-option="default" data-currency="'.$record['id_currency'].'"></a>',
							'dt_adefault'	=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['currency_admin'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-option="apanel" data-currency="'.$record['id_currency'].'"></a>',
							'dt_udefault'	=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['currency_user'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-option="public" data-currency="'.$record['id_currency'].'"></a>',
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['currency_active'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-option="active" data-currency="'.$record['id_currency'].'"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/currency/popup/edit/' . $record['id_currency']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														'. $delete_btn .'
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
