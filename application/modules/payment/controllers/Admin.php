<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/payment/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'payment';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Payment_model", "payment");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_payment', '/admin');

		$this->data['page_header'] = 'Оплата';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'payment_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_payment');

				$this->data['currencies'] = Modules::run('currency/_get_all', array('active' => 1));
				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_payment');

				$id_payment = (int) $this->uri->segment(5);
				$this->data['payment'] = $this->payment->handler_get($id_payment);
				if(empty($this->data['payment'])){
					jsonResponse('Метод оплаты не найден.');
				}
				
				$this->data['delivery_relations'] = $this->payment->handler_get_delivery_relation($id_payment);
				$this->data['delivery_selected'] = array_column($this->data['delivery_relations'], 'id_delivery');
				
				$this->data['currencies'] = Modules::run('currency/_get_all', array('active' => 1));
				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_payment');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('price_add', 'Дополнительный %', 'xss_clean|max[99]');
				$this->form_validation->set_rules('stext', 'Краткое описание', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description', 'Текст', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$price_add = $this->input->post('price_add');
				$payment_add_options = array();
				if(is_array($price_add) && !empty($price_add)){
					foreach ($price_add as $key => $value) {
						$percent = abs((float)$value);
						if($percent > 100){
							jsonResponse('Значение поля Дополнительный %, для '.$key.' не может быть больше 100.');
						}

						$payment_add_options[$key] = numberFormat($percent, true);
					}
				}

				$insert = array(
					'payment_title' => $this->input->post('title'),
					'payment_add_options' => json_encode($payment_add_options),
					'payment_sdescription' => $this->input->post('stext'),
					'payment_description' => $this->input->post('description')
				);

				if($this->input->post('active')){
					$insert['payment_active'] = 1;
				}

				$id_payment = $this->payment->handler_insert($insert);

				$delivery_relation = array();
				$delivery = $this->input->post('delivery');
				if(is_array($delivery) && !empty($delivery)){
					foreach ($delivery as $delivery_id => $delivery_info) {
						$delivery_relation[] = array(
							'id_delivery' => (int) $delivery_id,
							'id_payment' => $id_payment,
							'relation_text' => $delivery_info['text']
						);
					}
				}

				if(!empty($delivery_relation)){
					$this->payment->handler_insert_delivery_relation($delivery_relation);
				}

				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				if(!$this->lauth->have_right('manage_payment')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('payment', 'Метод оплаты', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext', 'Краткое описание', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description', 'Текст', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_payment = (int)$this->input->post('payment');

				$price_add = $this->input->post('price_add');
				$payment_add_options = array();
				if(is_array($price_add) && !empty($price_add)){
					foreach ($price_add as $key => $value) {
						$percent = abs((float)$value);
						if($percent > 100){
							jsonResponse('Значение поля Дополнительный %, для '.$key.' не может быть больше 100.');
						}
						
						$payment_add_options[$key] = numberFormat($percent, true);
					}
				}

				$update = array(
					'payment_title' => $this->input->post('title'),
					'payment_add_options' => json_encode($payment_add_options),
					'payment_sdescription' => $this->input->post('stext'),
					'payment_description' => $this->input->post('description'),
					'payment_active' => 0
				);

				if($this->input->post('active')){
					$update['payment_active'] = 1;
				}

				$this->payment->handler_update($id_payment, $update);
				
				$delivery_relation = array();
				$delivery = $this->input->post('delivery');
				if(is_array($delivery) && !empty($delivery)){
					foreach ($delivery as $delivery_id => $delivery_info) {
						$delivery_relation[] = array(
							'id_delivery' => (int) $delivery_id,
							'id_payment' => $id_payment,
							'relation_text' => $delivery_info['text']
						);
					}
				}

				$this->payment->handler_delete_delivery_relation($id_payment);
				if(!empty($delivery_relation)){
					$this->payment->handler_insert_delivery_relation($delivery_relation);
				}

				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				if(!$this->lauth->have_right('manage_payment')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('payment', 'Метод оплаты', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_payment = (int)$this->input->post('payment');
				$payment = $this->payment->handler_get($id_payment);
				if(empty($payment)){
					jsonResponse('Метод оплаты не существует.');
				}

				$this->payment->handler_delete($id_payment);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_payment');

                $this->form_validation->set_rules('payment', 'Метод оплаты', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_payment = (int)$this->input->post('payment');
				$payment = $this->payment->handler_get($id_payment);
				if(empty($payment)){
					jsonResponse('Метод оплаты не существует.');
				}

				$status = (int) !((int) $payment['payment_active'] === 1);
				$this->payment->handler_update($id_payment, array('payment_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_payment');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'id_payment-asc' )
				);

				$records = $this->payment->handler_get_all($params);
				$records_total = $this->payment->handler_get_count($params);
				
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_id'			=>  $record['id_payment'],
							'dt_name'		=>  $record['payment_title'],
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['payment_active'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-payment="'.$record['id_payment'].'"></a>',
							'dt_actions'	=>  '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/payment/popup/edit/' . $record['id_payment']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить способ оплаты?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-payment="'.$record['id_payment'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
