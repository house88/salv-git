<?php
/*
Author: Daniel Gutierrez
Date: 9/18/12
Version: 1.0
*/

class Menu_model extends CI_Model{
    var $menu = "menu";

    function __construct(){
        parent::__construct();
    }
	
	function handler_apanel_menu(){
		return (object) array(
			'orders' => (object) array(
				'url' => 'orders',
				'icon' => 'fad fa-shopping-cart',
				'name' => 'Заказы',
				'rights' => 'manage_orders'
			),
			'catalog' => (object) array(
				'url' => '',
				'icon' => 'fad fa-store',
				'name' => 'Магазин',
				'rights' => 'manage_categories,manage_items,manage_reviews,manage_price_variants,manage_properties,manage_brands',
				'submenu' => (object) array(
					'categories' => (object) array(
						'url' => 'categories',
						'icon' => 'fad fa-folder-tree',
						'name' => 'Категории товаров',
						'rights' => 'manage_categories'
					),
					'items' => (object) array(
						'url' => 'items',
						'icon' => 'fad fa-th-list',
						'name' => 'Список товаров',
						'rights' => 'manage_items'
					),
					'comments' => (object) array(
						'url' => 'comments',
						'icon' => 'fad fa-comments-alt',
						'name' => 'Отзывы о товарах',
						'rights' => 'manage_reviews'
					),
					'prices' => (object) array(
						'url' => 'prices',
						'icon' => 'fad fa-badge-dollar',
						'name' => 'Ценовые категорий',
						'rights' => 'manage_price_variants'
					),
					'dynamic_prices' => (object) array(
						'url' => 'dynamic_prices',
						'icon' => 'fad fa-badge-percent',
						'name' => 'Динамические цены',
						'rights' => 'manage_price_variants'
					),
					'properties' => (object) array(
						'url' => 'properties',
						'icon' => 'fad fa-filter',
						'name' => 'Свойства товаров',
						'rights' => 'manage_properties'
					),
					'brands' => (object) array(
						'url' => 'brands',
						'icon' => 'fad fa-copyright',
						'name' => 'Бренды',
						'rights' => 'manage_brands'
					)
				)
			),
			'special_pages' => (object) array(
				'url' => '',
				'icon' => 'fad fa-browser',
				'name' => 'Специальные страницы',
				'rights' => 'manage_pages,manage_static_block,manage_banners,manage_site_menu',
				'submenu' => (object) array(
					'pages' => (object) array(
						'url' => 'pages',
						'icon' => 'fad fa-file-alt',
						'name' => 'Страницы',
						'rights' => 'manage_pages'
					),
					'static_block' => (object) array(
						'url' => 'static_block',
						'icon' => 'fad fa-th',
						'name' => 'Статические блоки',
						'rights' => 'manage_static_block'
					),
					'banners' => (object) array(
						'url' => 'banners',
						'icon' => 'fad fa-images',
						'name' => 'Баннеры',
						'rights' => 'manage_banners'
					),
					'menu' => (object) array(
						'url' => 'menu',
						'icon' => 'fad fa-layer-group',
						'name' => 'Меню сайта',
						'rights' => 'manage_site_menu'
					)
				)
			),
			'settings' => (object) array(
				'url' => '',
				'icon' => 'fad fa-cogs',
				'name' => 'Настройки',
				'rights' => 'manage_settings,manage_currency,manage_delivery,manage_payment,manage_cities',
				'submenu' => (object) array(
					'settings' => (object) array(
						'url' => 'settings',
						'icon' => 'fad fa-sliders-v',
						'name' => 'Настройки',
						'rights' => 'manage_settings'
					),
					'currency' => (object) array(
						'url' => 'currency',
						'icon' => 'fad fa-usd-square',
						'name' => 'Валюты сайта',
						'rights' => 'manage_currency'
					),
					'delivery' => (object) array(
						'url' => 'delivery',
						'icon' => 'fad fa-truck',
						'name' => 'Доставка',
						'rights' => 'manage_delivery'
					),
					'payment' => (object) array(
						'url' => 'payment',
						'icon' => 'fad fa-credit-card-front',
						'name' => 'Оплата',
						'rights' => 'manage_payment'
					),
					'cities' => (object) array(
						'url' => 'cities',
						'icon' => 'fad fa-map-marked-alt',
						'name' => 'Города',
						'rights' => 'manage_cities'
					)
				)
			),
			'users' => (object) array(
				'url' => '',
				'icon' => 'fad fa-users-cog',
				'name' => 'Пользователи',
				'rights' => 'manage_users,manage_moderators,manage_groups,manage_rights,manage_group_rights',
				'submenu' => (object) array(
					'users' => (object) array(
						'url' => 'users',
						'icon' => 'fad fa-users',
						'name' => 'Клиенты',
						'rights' => 'manage_users'
					),
					'moderators' => (object) array(
						'url' => 'moderators',
						'icon' => 'fad fa-user-shield',
						'name' => 'Администраторы',
						'rights' => 'manage_moderators'
					),
					'groups' => (object) array(
						'url' => 'groups',
						'icon' => 'fad fa-users-class',
						'name' => 'Группы пользователей',
						'rights' => 'manage_groups'
					),
					'rights' => (object) array(
						'url' => 'rights',
						'icon' => 'fad fa-user-crown',
						'name' => 'Права пользователей',
						'rights' => 'manage_rights'
					),
					'group_rights' => (object) array(
						'url' => 'group_rights',
						'icon' => 'fad fa-users-crown',
						'name' => 'Права по группам',
						'rights' => 'manage_group_rights'
					)
				)
			)
		);
	}
	
	function handler_insert($data = array()){
        if(empty($data)){
            return;
        }

        $this->db->insert($this->menu, $data);
        return $this->db->insert_id();
	}

	function handler_update($id_menu, $data = array()){
        if(empty($data)){
            return;
        }

        $this->db->where('id_menu', $id_menu);
        $this->db->update($this->menu, $data);
	}
	
	function handler_delete($id_menu){
        $this->db->where('id_menu', $id_menu);
        $this->db->delete($this->menu);
	}

	function handler_get($id_menu){
        $this->db->where('id_menu', $id_menu);
        return $this->db->get($this->menu)->row_array();
	}	

	function handler_get_by_title($menu_title){
        $this->db->where('menu_title', $menu_title);
        return $this->db->get($this->menu)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_menu ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->menu)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		return $this->db->count_all_results($this->menu);
	}
}
