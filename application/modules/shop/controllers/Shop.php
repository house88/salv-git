<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shop extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model('items/Items_Model', 'items');
		$this->load->model('categories/Categories_Model', 'categories');
		$this->load->model("properties/Properties_model", "properties");
		$this->load->model('brands/Brands_model', 'brands');
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		$this->data['home_settings'] = arrayByKey($this->settings->get_settings_home(), 'setting_alias');
		$this->data['main_content'] = 'shop/home/index_view';
		$this->load->view($this->theme->public_view('index_view'), $this->data);
	}

	function catalog(){
		// $this->db->cache_on();
		
		$uri = $this->uri->uri_to_assoc(1);
		checkURI($uri, array('catalog', 'brand', 'filter', 'page'), $this->data);

		$id_category = id_from_link($uri['catalog']);
		if(!$id_category){
			return_404($this->data);
		}
		// GET CATEGORY
		$this->data['category'] = $this->categories->handler_get($id_category);
		if(empty($this->data['category'])){
			return_404($this->data);
		}

		$this->data['category_breadcrumbs'] = $category_breadcrumbs = json_decode('['.$this->data['category']['category_breadcrumbs'].']');
		if(!empty($category_breadcrumbs)){
			foreach ($category_breadcrumbs as $category_breadcrumb) {
				$bread_category_url = (!empty($category_breadcrumb->special_url)) ? $category_breadcrumb->special_url : 'catalog/' . $category_breadcrumb->url;
				$this->breadcrumbs[] = array(
					'title' => $category_breadcrumb->category_title,
					'link' => $id_category !== (int) $category_breadcrumb->category_id ? base_url($bread_category_url) : null
				);
			}
		}

		$this->data['suppliers'] = arrayByKey($this->items->handler_get_suppliers_all(), 'id_supplier');

		$link_array = array(
			'main' => base_url().'catalog/'.$uri['catalog'],
			'brand' => (isset($uri['brand']))?'/brand/'.$uri['brand']:'',
			'filter' => (isset($uri['filter']))?'/filter/'.$uri['filter']:'',
			'page' => ''
		);

		$this->data['subCategories'] = $this->categories->handler_get_all(array(
			'parent' => $id_category, 
			'order_by' => 'category_title ASC'
		));

		if(0 === (int) $this->data['category']['category_parent']){
			$this->data['firstLevelCategories'] = $this->categories->handler_get_all(array(
				'parent' => (int) $this->data['category']['category_parent'], 
				'order_by' => 'category_title ASC'
			));
		} else{
			if(empty($this->data['subCategories'])){
				$parentCategory = $this->categories->handler_get((int) $this->data['category']['category_parent']);
				$this->data['firstLevelCategories'] = $this->categories->handler_get_all(array(
					'parent' => (int) @$parentCategory['category_parent'], 
					'order_by' => 'category_title ASC'
				));

				$this->data['subCategories'] = $this->categories->handler_get_all(array(
					'parent' => (int) $this->data['category']['category_parent'], 
					'order_by' => 'category_title ASC'
				));				
			} else{
				$this->data['firstLevelCategories'] = $this->categories->handler_get_all(array(
					'parent' => (int) $this->data['category']['category_parent'], 
					'order_by' => 'category_title ASC'
				));
			}
		}

		$products_params = array();

		$limit = $this->data['settings']['products_per_page']['setting_value'];
		$products_params['limit'] = $limit;

		$this->data['page_number'] = 1;
        if(isset($uri['page'])){
			$page_number = (int)$uri['page'];
			$this->data['page_number'] = ($page_number > 0)?$page_number:1;
            $start = ($uri['page']  == 1) ? 0 : ($uri['page'] * $limit) - $limit;
			$link_array['page'] = '/page/'.$uri['page'];
        } else{
            $start = 0;
        }
		$products_params['start'] = $start;
		$price_column = getPriceColumn();
		$products_params['item_price_column'] = $price_column['db_price_column'];
		$this->data['list_sort_by'] = array(
			'price-asc'  => array(
				'title' => 'Pret (A-Z)',
				'db_column' => "(
					IF( 
						item_temp_price > 0 AND 
						item_temp_price < item_price AND 
						item_temp_price < {$price_column['db_price_column']}, 
						item_temp_price , 
						IF(
							item_price < {$price_column['db_price_column']}, 
							item_price, 
							{$price_column['db_price_column']}
						) 
					)
				)-asc"
			),
			'price-desc' => array(
				'title' => 'Pret (Z-A)',
				'db_column' => "(
					IF( 
						item_temp_price > 0 AND 
						item_temp_price < item_price AND 
						item_temp_price < {$price_column['db_price_column']}, 
						item_temp_price , 
						IF(
							item_price < {$price_column['db_price_column']}, 
							item_price, 
							{$price_column['db_price_column']}
						) 
					)
				)-desc"
			),
			'date_update-asc'  => array(
				'title' => 'Data (A-Z)',
				'db_column' => 'item_last_update-asc'
			),
			'date_update-desc' => array(
				'title' => 'Data (Z-A)',
				'db_column' => 'item_last_update-desc'
			),
			'popular-desc' => array(
				'title' => 'Popularitate',
				'db_column' => 'item_popular-desc'
			)
		);
		
		$get_parameters = array();
		if (!empty($_SERVER['QUERY_STRING'])) {
			$this->data['get_params'] = array();
			foreach($_GET as $key => $one_param){
				$param_value = cut_str($this->input->get($key, true));
				$get_parameters[$key] = $param_value;
				$this->data['get_params'][$key] = $key.'='.$param_value;
			}
		}

		$this->data['selected_filters'] = $get_parameters;

		
		// KEYWORDS
		if(!empty($get_parameters['keywords'])){
			$products_params['keywords'] = $get_parameters['keywords'];
		}
		
		// SORT BY LINK
		$sort_by_link = $get_parameters;
		if(!empty($sort_by_link['sort_by']) && array_key_exists($sort_by_link['sort_by'], $this->data['list_sort_by'])){
			$this->data['sort_by'] = $sort_by_link['sort_by'];
			$products_params['sort_by'][] = $this->data['list_sort_by'][$sort_by_link['sort_by']]['db_column'];
			unset($sort_by_link['sort_by']);
		}else{
			$sort_by = array_keys($this->data['list_sort_by']);
			$this->data['sort_by'] = $sort_by[0];
			$products_params['sort_by'][] = $this->data['list_sort_by'][$sort_by[0]]['db_column'];
		}
		$this->data['get_sort_by'] = arrayToGET($sort_by_link);

		// GET FILTERS
		$properties_params = array();
		$properties_selected = array();

		if($this->input->get('priceFrom') && (float) $this->input->get('priceFrom') > 0){
			$priceFrom = getPriceInverse((float) $this->input->get('priceFrom'));
			$products_params['item_price_from'] = $priceFrom['db_price'];
			$properties_selected['priceFrom'] = formatPrice((float) $this->input->get('priceFrom'), true);
		}
		if($this->input->get('priceTo') && (float) $this->input->get('priceTo') > 0){
			$priceTo = getPriceInverse((float) $this->input->get('priceTo'));
			$products_params['item_price_to'] = $priceTo['db_price'];
			$properties_selected['priceTo'] = formatPrice((float) $this->input->get('priceTo'), true);
		}

		if(isset($uri['filter'])){
			$uri_filters = explode(':', $uri['filter']);
			$property_select = array();
			$property_range = array();
			foreach ($uri_filters as $uri_filter) {
				list($temp_key, $temp_values) = explode('_', $uri_filter);
					
				switch ($temp_key) {
					case 'fp':
						$values = explode('-', $temp_values);
						$values = array_filter($values);
						$properties_selected['fp'] = $values;
						if(isset($values[0])){
							$price_from = getPriceInverse($values[0]);
							$products_params['item_price_from'] = $price_from['db_price'];
						}
						if(isset($values[1])){
							$price_to = getPriceInverse($values[1]);
							$products_params['item_price_to'] = $price_to['db_price'];
						}
					break;
					case 'fb':
						$values = explode('-', $temp_values);
						$values = array_filter($values);
						if(!empty($values)){
							$properties_selected['fb'] = $values;
							$products_params['id_brand'] = $values;
						}
					break;
					case 'fstock':
						if($this->lauth->group_view_stocks()){
							$values = explode('-', $temp_values);
							$values = array_filter($values);
							if(!empty($values)){
								$properties_selected['fstock'] = $values;
								$products_params['stock_available'] = $values;
							}
						}
					break;
					case 'fsuppliers':
						if($this->lauth->have_right('filter_by_suppliers')){
							$values = explode('-', $temp_values);
							$values = array_filter($values);
							if(!empty($values)){
								$suppliers_is = array();
								foreach($this->data['suppliers'] as $suppliers_record){
									$suppliers_is[] = $suppliers_record['id_supplier'];
								}
								
								$suppliers_list_filter = array();
								$suppliers_list_selected = array();
								foreach ($values as $id_supplier) {
									if(isset($this->data['suppliers'][$id_supplier])){
										$suppliers_list_filter[] = $this->data['suppliers'][$id_supplier]['supplier_prog_id'];
										$suppliers_list_selected[] = $id_supplier;
									} elseif($id_supplier == 'no'){
										$suppliers_list_filter[] = 'no';
										$suppliers_list_selected[] = 'no';
									}
								}
	
								if(!empty($suppliers_list_filter)){
									$properties_selected['fsuppliers'] = $suppliers_list_selected;
									$products_params['id_supplier'] = $suppliers_list_filter;
								}
							}
						}
					break;
					case 'foptional':
						$values = explode('-', $temp_values);
						$values = array_filter($values);
						if(!empty($values)){
							$properties_selected['foptional'] = $values;
							$products_params['optional_filters'] = $values;
						}
					break;
					default:
						$values = explode('-', $temp_values);
						$values = array_filter($values);
						if(!empty($values)){
							if (preg_match('/^fs-/', $temp_key)) {
								$prop_id = id_from_link($temp_key);
								$property_select[$prop_id] = implode(',', $values);
								$properties_selected['fs'][$prop_id] = $values;
							}

							if (preg_match('/^fm-/', $temp_key)) {
								$prop_id = id_from_link($temp_key);
								$property_select[$prop_id] = implode(',', $values);
								$properties_selected['fm'][$prop_id] = $values;
							}
							
							if (preg_match('/^fr-/', $temp_key)) {
								$prop_id = id_from_link($temp_key);
								$properties_selected['fr'][$prop_id] = $values;
								
								if(!empty($values[0])){
									$property_range[$prop_id]['from'] = $values[0];
								}

								if(!empty($values[1])){
									$property_range[$prop_id]['to'] = $values[1];
								}
							}							
						}
					break;
				}
			}
			
			if (!empty($property_select)){
				$properties_params['properties_select'] = $property_select;
			}

			if (!empty($property_range)){
				$properties_params['properties_range'] = $property_range;
			}

			if(!empty($properties_params)){
				$items_by_properties = $this->properties->handler_get_items_by_properties_and($properties_params);
				if(!empty($items_by_properties)){
					foreach ($items_by_properties as $item_by_properties) {
						$items_list[$item_by_properties['id_item']] = $item_by_properties['id_item'];
					}
				}
		
				if(empty($items_list)){
					$items_list[] = 0;
				}
					
				$products_params['items_list'] = $items_list;
			}
		}
		
		$this->data['properties_selected'] = $properties_selected;
		$categories_list = explode(',', $this->data['category']['category_children']);
		$categories_list = array_filter($categories_list);
		$categories_list[] = $id_category;
		$products_params['id_category'] = $categories_list;

		// echo '<pre>';
		// print_r($products_params);
		// echo '</pre>';
		// GET PRODUCTS
		$this->data['total_products'] = $total = $this->items->handler_get_count($products_params);
		$this->data['products'] = $this->items->handler_get_all_query($products_params);
		
		$filter_counters_condition['category_children'] = $this->properties->_get_category_children($this->data['category']['category_id']);
		
		if(isset($products_params['id_brand'])){
			$filter_counters_condition['id_brand'] = $products_params['id_brand'];		
		}
		
		$all_products_params = $products_params;
		unset($all_products_params['limit']); 
		unset($all_products_params['start']);
		// if ($this->input->get('test')) {
		// 	echo '<pre>';
		// 	print_r($all_products_params);
		// 	echo '</pre>';
		// }
		$all_products = $this->items->handler_get_all_query($all_products_params);
		$all_products_list = array();
		foreach ($all_products as $all_products_record) {
			$all_products_list[] = $all_products_record['id_item'];
		}

		if(!empty($all_products_list)){
			$filter_counters_condition['items_list'] = $all_products_list;
		}

		$this->data['all_products_list'] = $all_products_list;
		
		if (!empty($properties_params)){
			$filter_counters_condition['properties_params'] = $properties_params;
		}
		
		// $available_properties_values_counters = array();
		if ($this->input->is_ajax_request()) {
			$available_properties_values_counters = $this->properties->handler_get_items_properties_values_counters($filter_counters_condition);
		}
		
		$this->data['brands'] = array();
		$brands_items_count = array();	
		$all_brand_products_params = $products_params;
		unset($all_brand_products_params['limit']); 
		unset($all_brand_products_params['start']);
		unset($all_brand_products_params['id_brand']);
		$all_brand_products_params['coulmns'] = 'id_item';
		$all_brands_products = $this->items->handler_get_all_query($all_brand_products_params);
		$all_brands_products_list = array();
		foreach ($all_brands_products as $all_brands_products_record) {
			$all_brands_products_list[] = $all_brands_products_record['id_item'];
		}
		
		if(!empty($all_brands_products_list)){
			$this->data['brands'] = $this->items->handler_get_search_brands(array('id_item' => $all_brands_products_list));
			foreach($this->data['brands'] as $brand){
				$brands_items_count[$brand['id_brand']] = $brand['items_count'];
			}

			if(!empty($brands_items_count) && $this->input->is_ajax_request()){
				$available_properties_values_counters['fb'] = $brands_items_count;
			}
		}

		if($this->lauth->group_view_stocks()){
			$fstock_product_params = $products_params;
			unset($fstock_product_params['limit']); 
			unset($fstock_product_params['start']);
			unset($fstock_product_params['stock_available']);
			$fstock_product_params['stock_available'] = array('yes');
			$available_properties_values_counters['fstock']['yes'] = $this->items->handler_get_count($fstock_product_params);
			$fstock_product_params['stock_available'] = array('no');
			$available_properties_values_counters['fstock']['no'] = $this->items->handler_get_count($fstock_product_params);
			$fstock_product_params['stock_available'] = array('ordering');
			$available_properties_values_counters['fstock']['ordering'] = $this->items->handler_get_count($fstock_product_params);
		}

		$foptional_product_params = $products_params;
		unset($foptional_product_params['limit']); 
		unset($foptional_product_params['start']);
		unset($foptional_product_params['optional_filters']);
		$foptional_product_params['optional_filters'] = array('actional');
		$available_properties_values_counters['foptional']['actional'] = $this->items->handler_get_count($foptional_product_params);
		$foptional_product_params['optional_filters'] = array('newest');
		$available_properties_values_counters['foptional']['newest'] = $this->items->handler_get_count($foptional_product_params);

		if($this->lauth->have_right('filter_by_suppliers')){
			$fsuppliers_product_params = $products_params;
			unset($fsuppliers_product_params['limit']); 
			unset($fsuppliers_product_params['start']);
			unset($fsuppliers_product_params['id_supplier']);
			foreach ($this->data['suppliers'] as $supplier) {
				$fsuppliers_product_params['id_supplier'] = $supplier['supplier_prog_id'];
				$counter_supplier = $this->items->handler_get_count($fsuppliers_product_params);
				if($counter_supplier > 0){
					$available_properties_values_counters['fsuppliers'][$supplier['id_supplier']] = $counter_supplier;
				}
			}
			$fsuppliers_product_params['id_supplier'] = 'no';
			$counter_supplier = $this->items->handler_get_count($fsuppliers_product_params);
			if($counter_supplier > 0){
				$available_properties_values_counters['fsuppliers']['no'] = $counter_supplier;
			}
		}
		
		$filter_url = $link_array;
		unset($filter_url['filter']);
		unset($filter_url['page']);
		$this->data['filter_url'] = implode('', $filter_url);

		$page_link = $link_array;
		$this->data['page_link'] = implode('', $page_link);

		// PAGINATION
		$settings = $this->pagination_lib->get_settings('products', $total, $limit);
        $this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();
		
		$this->db->cache_off();

        if (!$this->input->is_ajax_request()) {
			$this->data['available_properties_values_counters'] = $available_properties_values_counters;
			$this->data['max_price'] = $this->items->handler_max_price($products_params);
			$this->data['min_price'] = $this->items->handler_min_price($products_params);
			$this->data['categoryBreadcrumbs'] = $this->breadcrumbs;
			$this->data['stitle'] = 'Категория '.$this->data['category']['category_title'];
			$this->data['ftitle'] = 'Категория '.$this->data['category']['category_title'];
			$this->data['main_content'] = 'shop/catalog/index_view';
			$this->load->view($this->theme->public_view('index_view'), $this->data);
		} else{
			$catalog_content = $this->load->view($this->theme->public_view('shop/catalog/products_view'), $this->data, true);
			jsonResponse('', 'success', array('catalog_content' => $catalog_content, 'filters_available' => json_encode($available_properties_values_counters)));
		}
	}

	function products(){
		$uri = $this->uri->uri_string();
		// GET PRODUCT
		$id_item = id_from_link($uri, false);
		$this->data['product'] = $this->items->handler_get($id_item);
		if(empty($this->data['product'])){
			return_404($this->data);
		}

		if($this->data['product']['item_price'] <= 0){
			return_404($this->data);
		}

		$id_product = $this->data['product']['id_item'];

		// GET CATEGORY
		$this->data['category'] = $this->categories->handler_get($this->data['product']['id_category']);
		if(empty($this->data['category'])){
			return_404($this->data);
		}

		// GET BRAND
		$this->data['brand'] = $this->brands->handler_get($this->data['product']['id_brand']);

		$price_column = getPriceColumn();
		$db_price_column = $price_column['db_price_column'];
		if(
			$this->data['product']['item_temp_price'] > 0 && 
			$this->data['product']['item_temp_price'] < $this->data['product']['item_price'] &&
			$this->data['product']['item_temp_price'] < $this->data['product'][$price_column['db_price_column']]
		){
			$db_price_column = 'item_temp_price';
		} else if($this->data['product']['item_price'] < $this->data['product'][$price_column['db_price_column']]){
			$db_price_column = 'item_price';
		}
		
		// GET SAME PRODUCTS
		$same_products_params = array(
			'item_price_column' => $price_column['db_price_column'],
			'item_visible' => 1,
			'sort_by' => array($price_column['db_price_column'].'-DESC'),
			'not_items_list' => $id_product,
			'id_category' => $this->data['product']['id_category'],
			'display_price_to' => $this->data['product'][$db_price_column],
			'start' => 0,
			'limit' => 6
		);
		$same_products_before = $this->items->handler_get_all($same_products_params);
		$same_products_before_count = count($same_products_before);
		if($same_products_before_count < 6 && $this->data['category']['category_parent'] > 0){
			$parent_category = $this->categories->handler_get($this->data['category']['category_parent']);
			if(!empty($parent_category) && !empty($parent_category['category_children'])){
				$temp_categories = explode(',', $parent_category['category_children']);
				$temp_categories = array_flip($temp_categories);
				unset($temp_categories[$this->data['product']['id_category']]);
				$temp_categories = array_flip($temp_categories);
				if(!empty($temp_categories)){
					$same_products_params = array(
						'item_price_column' => $price_column['db_price_column'],
						'item_visible' => 1,
						'sort_by' => array($price_column['db_price_column'].'-DESC'),
						'not_items_list' => $id_product,
						'id_category' => $temp_categories,
						'display_price_to' => $this->data['product'][$db_price_column],
						'start' => 0,
						'limit' => 6 - $same_products_before_count
					);
					
					$same_products_before = array_merge($same_products_before, $this->items->handler_get_all($same_products_params));
				}
			}
		}

		if(!empty($same_products_before)){
			usort( $same_products_before , function($a, $b) use($price_column){
				$_a_column = $_b_column = $price_column['db_price_column'];
				if(
					$a['item_temp_price'] > 0 && 
					$a['item_temp_price'] < $a['item_price'] &&
					$a['item_temp_price'] < $a[$price_column['db_price_column']]
				){
					$_a_column = 'item_temp_price';
				} else if($a['item_price'] < $a[$price_column['db_price_column']]){
					$_a_column = 'item_price';
				}
				
				$_b_column = $price_column['db_price_column'];
				if(
					$b['item_temp_price'] > 0 && 
					$b['item_temp_price'] < $b['item_price'] &&
					$b['item_temp_price'] < $b[$price_column['db_price_column']]
				){
					$_b_column = 'item_temp_price';
				} else if($b['item_price'] < $b[$price_column['db_price_column']]){
					$_b_column = 'item_price';
				}
				
				return ($a[$_a_column] <= $b[$_b_column])?-1:1;
			} );
		}

		$same_products_params = array(
			'item_price_column' => $price_column['db_price_column'],
			'item_visible' => 1,
			'sort_by' => array($price_column['db_price_column'].'-ASC'),
			'not_items_list' => $id_product,
			'id_category' => $this->data['product']['id_category'],
			'display_price_from' => $this->data['product'][$db_price_column],
			'start' => 0,
			'limit' => 6
		);
		$same_products_after = $this->items->handler_get_all($same_products_params);
		$same_products_after_count = count($same_products_after);
		if($same_products_after_count < 6 && $this->data['category']['category_parent'] > 0){
			$parent_category = $this->categories->handler_get($this->data['category']['category_parent']);
			if(!empty($parent_category) && !empty($parent_category['category_children'])){
				$temp_categories = explode(',', $parent_category['category_children']);
				$temp_categories = array_flip($temp_categories);
				unset($temp_categories[$this->data['product']['id_category']]);
				$temp_categories = array_flip($temp_categories);
				if(!empty($temp_categories)){
					$same_products_params = array(
						'item_price_column' => $price_column['db_price_column'],
						'item_visible' => 1,
						'sort_by' => array($price_column['db_price_column'].'-ASC'),
						'not_items_list' => $id_product,
						'id_category' => $temp_categories,
						'display_price_from' => $this->data['product'][$db_price_column],
						'start' => 0,
						'limit' => 6 - $same_products_after_count
					);
					
					$same_products_after = array_merge($same_products_after, $this->items->handler_get_all($same_products_params));
				}
			}
		}

		if(!empty($same_products_after)){
			usort( $same_products_after , function($a, $b) use($price_column){
				$_a_column = $_b_column = $price_column['db_price_column'];
				if(
					$a['item_temp_price'] > 0 && 
					$a['item_temp_price'] < $a['item_price'] &&
					$a['item_temp_price'] < $a[$price_column['db_price_column']]
				){
					$_a_column = 'item_temp_price';
				} else if($a['item_price'] < $a[$price_column['db_price_column']]){
					$_a_column = 'item_price';
				}
				
				$_b_column = $price_column['db_price_column'];
				if(
					$b['item_temp_price'] > 0 && 
					$b['item_temp_price'] < $b['item_price'] &&
					$b['item_temp_price'] < $b[$price_column['db_price_column']]
				){
					$_b_column = 'item_temp_price';
				} else if($b['item_price'] < $b[$price_column['db_price_column']]){
					$_b_column = 'item_price';
				}
				
				return ($a[$_a_column] <= $b[$_b_column])?-1:1;
			} );
		}

		$this->data['same_products'] = array_merge($same_products_before, $same_products_after);
		
		$category_breadcrumbs = json_decode('['.$this->data['category']['category_breadcrumbs'].']');
		if(!empty($category_breadcrumbs)){
			foreach ($category_breadcrumbs as $category_breadcrumb) {
				$bread_category_url = (!empty($category_breadcrumb->special_url))?$category_breadcrumb->special_url:'catalog/'.$category_breadcrumb->url;
				$this->breadcrumbs[] = array(
					'title' => $category_breadcrumb->category_title,
					'link' => base_url($bread_category_url)
				);
			}
		}

		$this->breadcrumbs[] = array(
			'title' => clean_output($this->data['product']['item_title']),
			'link' => null
		);
		
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['stitle'] = (!empty($this->data['product']['mt'])) ? clean_output($this->data['product']['mt']) : clean_output($this->data['product']['item_title']);
		$this->data['skeywords'] = clean_output($this->data['product']['mk']);
		$this->data['sdescription'] = clean_output($this->data['product']['md']);

		$price = getPrice($this->data['product']);
		$product_ld = (object) [
			"@context" => "http://schema.org/",
			"@type" => "product",
			"name" => clean_output($this->data['product']['item_title']),
			"url" => base_url('products/'.$this->data['product']['item_url']),
			"image" => base_url(getImage('files/items/'.$this->data['product']['item_photo'])),
			"description" => clean_output($this->data['product']['md']),
			"brand" => (!empty($this->data['brand'])) ? $this->data['brand']['brand_title'] : "",
			"offers" => (object) [
				"@type" => "Offer",
				"itemCondition" => "http://schema.org/NewCondition",
				"availability" => "http://schema.org/InStock",
				"price" => $price['display_price'],
				"priceCurrency" => $price['currency_symbol']
			]
		];
		$this->data['product_jsonld'] = json_encode($product_ld, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

		$this->data['og_meta'] = array(
			'site_name' => @$this->data['settings']['default_title']['setting_value'],
			'title' => clean_output($this->data['product']['item_title']),
			'description' => clean_output($this->data['product']['md']),
			'url' => current_url(),
			'image' => base_url(getImage('files/items/'.$this->data['product']['item_photo']))
		);

		$this->data['stocks'] = arrayByKey($this->items->handler_get_stocks_all(), 'stock_prog_id');
		$this->data['suppliers'] = arrayByKey($this->items->handler_get_suppliers_all(), 'supplier_prog_id');
		$this->data['recaptcha_widget'] = $this->recaptcha->create_box();

		$this->data['main_content'] = 'shop/product/index_view';
		$this->load->view($this->theme->public_view('index_view'), $this->data);
	}

	function search(){
		$keywords = $this->input->get('keywords', true);
		$products_params = array();
		$products_params['keywords'] = clean_output($keywords);
		$start = 0;
		$limit = $this->data['settings']['products_per_page']['setting_value'];
		if ($this->input->is_ajax_request()) {
			if($this->input->get('mode', true) == 'simple'){
				$limit = $this->data['settings']['search_products_per_page']['setting_value'];
			}
		}
		$uri = $this->uri->uri_to_assoc(1);
		$this->data['page_number'] = 1;
		if(isset($uri['page'])){
			$page_number = (int)$uri['page'];
			$this->data['page_number'] = ($page_number > 0)?$page_number:1;
			$start = ($uri['page']  == 1) ? 0 : ($uri['page'] * $limit) - $limit;
		}

		$id_category = id_from_link($this->input->get('category', true));
		if($id_category){
			// GET CATEGORY
			$this->data['category'] = $this->categories->handler_get($id_category);
			if(empty($this->data['category'])){
				return_404($this->data);
			}
			
			$categories_list = explode(',', $this->data['category']['category_children']);
			$categories_list = array_filter($categories_list);
			$categories_list[] = $id_category;
			$products_params['id_category'] = $categories_list;
		}

		if (!empty($_SERVER['QUERY_STRING'])) {
			$this->data['get_params'] = array();
			foreach($_GET as $key => $one_param){
				$param_value = cut_str($this->input->get($key, true));
				$this->data['get_params'][$key] = $key.'='.$param_value;
			}
		}

		// GET FILTERS
		$properties_selected = array();
		$price_column = getPriceColumn();
		$products_params['item_price_column'] = $price_column['db_price_column'];
		if(isset($uri['filter'])){
			$uri_filters = explode(':', $uri['filter']);
			foreach ($uri_filters as $uri_filter) {
				list($temp_key, $temp_values) = explode('_', $uri_filter);
					
				switch ($temp_key) {
					case 'fp':
						$values = explode('-', $temp_values);
						$values = array_filter($values);
						$properties_selected['fp'] = $values;
						if(isset($values[0])){
							$price_from = getPriceInverse($values[0]);
							$products_params['item_price_from'] = $price_from['db_price'];
						}
						if(isset($values[1])){
							$price_to = getPriceInverse($values[1]);
							$products_params['item_price_to'] = $price_to['db_price'];
						}
					break;
					case 'fstock':
						if($this->lauth->group_view_stocks()){
							$values = explode('-', $temp_values);
							$values = array_filter($values);
							if(!empty($values)){
								$properties_selected['fstock'] = $values;
								$products_params['stock_available'] = $values;
							}
						}
					break;
				}
			}
		}
		$this->data['properties_selected'] = $properties_selected;
		$this->data['filter_url'] = 'search/catalog';
		
		$products_params['limit'] = $limit;
		$products_params['start'] = $start;
		$products_params['sort_by'] = array(
			'categories.category_parent_weight-ASC',
			'categories.category_weight-ASC'
		);
		
		// GET PRODUCTS
		$this->data['total_products'] = $total = $this->items->handler_get_search_count($products_params);
		$this->data['products'] = $this->items->handler_get_search($products_params);
		$categories = $this->items->handler_get_search_categories($products_params);

		$available_properties_values_counters = array();
		if($this->lauth->group_view_stocks()){
			$fstock_product_params = $products_params;
			unset($fstock_product_params['limit']); 
			unset($fstock_product_params['start']);
			unset($fstock_product_params['stock_available']);
			$fstock_product_params['stock_available'] = array('yes');
			$available_properties_values_counters['fstock']['yes'] = $this->items->handler_get_search_count($fstock_product_params);
			$fstock_product_params['stock_available'] = array('no');
			$available_properties_values_counters['fstock']['no'] = $this->items->handler_get_search_count($fstock_product_params);
			$fstock_product_params['stock_available'] = array('ordering');
			$available_properties_values_counters['fstock']['ordering'] = $this->items->handler_get_search_count($fstock_product_params);
		}
		
		$search_categories = array();
		foreach ($categories as $category) {
			$category_breadcrumbs = json_decode('['.$category['category_breadcrumbs'].']');
			$search_categories_list[] = $parent_key = $category_breadcrumbs[0]->category_id;
			
			$category_key = $category_breadcrumbs[0]->category_id;
			if(count($category_breadcrumbs) > 1){
				$category_key .= '_'.$category_breadcrumbs[1]->category_id;
				if(!isset($search_categories[$parent_key])){
					$search_categories[$parent_key] = array(
						'sub_categories' => array(
							$category_key => array(
								'category' => $category_breadcrumbs[1],
								'search_count' => $category['search_count']
							)
						),
						'detail' => $category_breadcrumbs[0],
						'search_count' => $category['search_count']
					);
				} else{
					if(!isset($search_categories[$parent_key]['sub_categories'][$category_key])){
						$search_categories[$parent_key]['sub_categories'][$category_key] = array(
							'category' => $category_breadcrumbs[1],
							'search_count' => $category['search_count']
						);
						$search_categories[$parent_key]['search_count'] += $category['search_count'];
					} else{
						$search_categories[$parent_key]['sub_categories'][$category_key]['search_count'] += $category['search_count'];
						$search_categories[$parent_key]['search_count'] += $category['search_count'];
					}
				}
			} else{
				if(!array_key_exists($category_key, $search_categories)){
					$search_categories[$category_key] = array(
						'detail' => $category_breadcrumbs[0],
						'sub_categories' => array(),
						'search_count' => $category['search_count']
					);
				} else{
					$search_categories[$category_key]['search_count'] += $category['search_count'];
				}

				if(!isset($search_categories[$parent_key])){
					$search_categories[$parent_key] = array(
						'detail' => $category_breadcrumbs[0],
						'sub_categories' => array(),
						'search_count' => $category['search_count']
					);
				} else{
					$search_categories[$parent_key]['search_count'] += $category['search_count'];
				}
			}
		}

		if(!empty($search_categories_list)){
			$original_categories = $this->categories->handler_get_all(array('id_category' => $search_categories_list, 'order_by' => 'category_weight ASC'));
			$search_categories_ordered = array();
			foreach ($original_categories as $original_category) {
				$search_categories_ordered[$original_category['category_id']] = $search_categories[$original_category['category_id']];
			}
			$search_categories = $search_categories_ordered;
		}
		
		$this->data['search_categories'] = $search_categories;
		$this->data['keywords'] = $keywords;

		// PAGINATION
		$settings = $this->pagination_lib->get_settings('products', $total, $limit);
        $this->pagination->initialize($settings);
        $this->data['pagination'] = $this->pagination->create_links();
		$this->data['available_properties_values_counters'] = $available_properties_values_counters;
		$this->data['max_price'] = $this->items->handler_max_price($products_params);
		$this->data['min_price'] = $this->items->handler_min_price($products_params);
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['stitle'] = 'Поиск '.$keywords;
		$this->data['ftitle'] = 'Поиск '.$keywords;

		$this->data['main_content'] = 'shop/catalog/search/index_view';
		$this->load->view($this->theme->public_view('index_view'), $this->data);
	}
	
	function catalog_mode(){
		$uri = $this->uri->uri_to_assoc(1);

		$products_params = array();
		$categories_products_params = array();
		$mode = $uri['catalog_mode'];
		switch ($mode) {
			case 'newest':
				$categories_products_params['item_newest'] = $products_params['item_newest'] = 1;
				$categories_products_params['sort_by'] = $products_params['sort_by'] = array(
					'items.item_created-DESC',
					'items.item_newest-DESC',
					'items.item_action-DESC',
					'items.item_hit-DESC',
					'categories.category_weight-ASC'
				);
				$this->data['mode_title'] = 'Новинки';
				$this->data['catalog_mode'] = $mode;
			break;			
			case 'actional':
				$categories_products_params['item_action'] = $products_params['item_action'] = 1;
				$products_params['sort_by'] = array(
					'item_order_price-ASC',
					'items.item_newest-DESC',
					'items.item_action-DESC',
					'items.item_hit-DESC',
					'categories.category_weight-ASC'
				);
				$categories_products_params['sort_by'] = array(
					'items.item_newest-DESC',
					'items.item_action-DESC',
					'items.item_hit-DESC',
					'categories.category_weight-ASC'
				);
				$this->data['mode_title'] = 'Акции';
				$this->data['catalog_mode'] = $mode;
			break;			
			default:
				return_404($this->data);
			break;
		}

		$start = 0;
		$limit = $this->data['settings']['products_per_page']['setting_value'];

		$price_column = getPriceColumn();
		$products_params['item_price_column'] = $price_column['db_price_column'];

		$this->data['page_number'] = 1;
		if(isset($uri['page'])){
			$page_number = (int)$uri['page'];
			$this->data['page_number'] = ($page_number > 0)?$page_number:1;
			$start = ($uri['page']  == 1) ? 0 : ($uri['page'] * $limit) - $limit;
		}

		$id_category = id_from_link($this->input->get('category', true));
		if($id_category){
			// GET CATEGORY
			$this->data['category'] = $this->categories->handler_get($id_category);
			if(empty($this->data['category'])){
				return_404($this->data);
			}
			
			$categories_list = explode(',', $this->data['category']['category_children']);
			$categories_list = array_filter($categories_list);
			$categories_list[] = $id_category;
			$products_params['id_category'] = $categories_list;
			$categories_products_params['id_category'] = $categories_list;
		}

		if (!empty($_SERVER['QUERY_STRING'])) {
			$this->data['get_params'] = array();
			foreach($_GET as $key => $one_param){
				$param_value = cut_str($this->input->get($key, true));
				$this->data['get_params'][$key] = $key.'='.$param_value;
			}
		}

		// GET FILTERS
		$properties_selected = array();
		if(isset($uri['filter'])){
			$uri_filters = explode(':', $uri['filter']);
			$categories_products_params['item_price_column'] = $price_column['db_price_column'];
			foreach ($uri_filters as $uri_filter) {
				list($temp_key, $temp_values) = explode('_', $uri_filter);
					
				switch ($temp_key) {
					case 'fp':
						$values = explode('-', $temp_values);
						$values = array_filter($values);
						$properties_selected['fp'] = $values;
						if(isset($values[0])){
							$price_from = getPriceInverse($values[0]);
							$products_params['item_price_from'] = $price_from['db_price'];
						}
						if(isset($values[1])){
							$price_to = getPriceInverse($values[1]);
							$products_params['item_price_to'] = $price_to['db_price'];
						}
					break;
					case 'fstock':
						if($this->lauth->group_view_stocks()){
							$values = explode('-', $temp_values);
							$values = array_filter($values);
							if(!empty($values)){
								$properties_selected['fstock'] = $values;
								$products_params['stock_available'] = $values;
							}
						}
					break;
				}
			}
		}
		$this->data['properties_selected'] = $properties_selected;
		$this->data['filter_url'] = 'catalog_mode/'.$mode;
		
		$products_params['limit'] = $limit;
		$products_params['start'] = $start;
		
		// GET PRODUCTS
		$this->data['total_products'] = $total = $this->items->handler_get_search_count($products_params);
		$this->data['products'] = $this->items->handler_get_search($products_params);
		$categories = $this->items->handler_get_search_categories($categories_products_params);

		$available_properties_values_counters = array();
		if($this->lauth->group_view_stocks()){
			$fstock_product_params = $products_params;
			unset($fstock_product_params['limit']); 
			unset($fstock_product_params['start']);
			unset($fstock_product_params['stock_available']);
			$fstock_product_params['stock_available'] = array('yes');
			$available_properties_values_counters['fstock']['yes'] = $this->items->handler_get_search_count($fstock_product_params);
			$fstock_product_params['stock_available'] = array('no');
			$available_properties_values_counters['fstock']['no'] = $this->items->handler_get_search_count($fstock_product_params);
			$fstock_product_params['stock_available'] = array('ordering');
			$available_properties_values_counters['fstock']['ordering'] = $this->items->handler_get_search_count($fstock_product_params);
		}

		$search_categories = array();
		foreach ($categories as $category) {
			$category_breadcrumbs = json_decode('['.$category['category_breadcrumbs'].']');
			$category_key = $category_breadcrumbs[0]->category_id;
			if(!array_key_exists($category_key, $search_categories)){
				$search_categories[$category_key] = array(
					'category' => $category_breadcrumbs[0],
					'parent' => array(),
					'search_count' => $category['search_count']
				);
			} else{
				$search_categories[$category_key]['search_count'] += $category['search_count'];
			}
		}
		
		$this->data['search_categories'] = $search_categories;
		// $this->data['keywords'] = $keywords;

		// PAGINATION
		$settings = $this->pagination_lib->get_settings('products', $total, $limit);
        $this->pagination->initialize($settings);
        $this->data['pagination'] = $this->pagination->create_links();
		
		$this->data['available_properties_values_counters'] = $available_properties_values_counters;
		$this->data['max_price'] = $this->items->handler_max_price($products_params);
		$this->data['min_price'] = $this->items->handler_min_price($products_params);
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['stitle'] = $this->data['mode_title'];
		$this->data['ftitle'] = $this->data['mode_title'];

		$this->data['main_content'] = 'shop/catalog/special/index_view';
		$this->load->view($this->theme->public_view('index_view'), $this->data);
	}
	
	function brand(){
		$uri = $this->uri->uri_to_assoc(1);
		checkURI($uri, array('brand', 'category', 'page'), $this->data);

		$id_brand = id_from_link($uri['brand']);
		if(!$id_brand){
			return_404($this->data);
		}

		// GET CATEGORY
		$this->data['brand'] = $this->brands->handler_get($id_brand);
		if(empty($this->data['brand'])){
			return_404($this->data);
		}

		$products_params = array();
		$products_params['id_brand'] = $id_brand;
		$price_column = getPriceColumn();
		$products_params['item_price_column'] = $price_column['db_price_column'];
		$start = 0;
		$limit = $this->data['settings']['products_per_page']['setting_value'];
		$this->data['page_number'] = 1;
		if(isset($uri['page'])){
			$page_number = (int)$uri['page'];
			$this->data['page_number'] = ($page_number > 0)?$page_number:1;
			$start = ($uri['page']  == 1) ? 0 : ($uri['page'] * $limit) - $limit;
		}
		$products_params['limit'] = $limit;
		$products_params['start'] = $start;
		
		if(isset($uri['category'])){
			$id_category = id_from_link($uri['category']);
			// GET CATEGORY
			$this->data['category'] = $this->categories->handler_get($id_category);
			if(empty($this->data['category'])){
				return_404($this->data);
			}
			
			$categories_list = explode(',', $this->data['category']['category_children']);
			$categories_list = array_filter($categories_list);
			$categories_list[] = $id_category;
			$products_params['id_category'] = $categories_list;
		}

		if (!empty($_SERVER['QUERY_STRING'])) {
			$this->data['get_params'] = array();
			foreach($_GET as $key => $one_param){
				$param_value = cut_str($this->input->get($key, true));
				$this->data['get_params'][$key] = $key.'='.$param_value;
			}
		}
		
		$products_params['sort_by'] = array(
			'categories.category_parent_weight-ASC',
			'categories.category_weight-ASC'
		);
		
		// GET PRODUCTS
		$this->data['total_products'] = $total = $this->items->handler_get_search_count($products_params);
		$this->data['products'] = $this->items->handler_get_search($products_params);
		$categories = $this->items->handler_get_search_categories($products_params);
		
		$search_categories = array();
		foreach ($categories as $category) {
			$category_breadcrumbs = json_decode('['.$category['category_breadcrumbs'].']');
			$search_categories_list[] = $parent_key = $category_breadcrumbs[0]->category_id;
			
			$category_key = $category_breadcrumbs[0]->category_id;
			if(count($category_breadcrumbs) > 1){
				$category_key .= '_'.$category_breadcrumbs[1]->category_id;
				if(!isset($search_categories[$parent_key])){
					$search_categories[$parent_key] = array(
						'sub_categories' => array(
							$category_key => array(
								'category' => $category_breadcrumbs[1],
								'search_count' => $category['search_count']
							)
						),
						'detail' => $category_breadcrumbs[0]
					);
				} else{
					if(!isset($search_categories[$parent_key]['sub_categories'][$category_key])){
						$search_categories[$parent_key]['sub_categories'][$category_key] = array(
							'category' => $category_breadcrumbs[1],
							'search_count' => $category['search_count']
						);
					} else{
						$search_categories[$parent_key]['sub_categories'][$category_key]['search_count'] += $category['search_count'];
					}
				}
			} else{
				if(!array_key_exists($category_key, $search_categories)){
					$search_categories[$category_key] = array(
						'detail' => $category_breadcrumbs[0],
						'sub_categories' => array(),
						'search_count' => $category['search_count']
					);
				} else{
					$search_categories[$category_key]['search_count'] += $category['search_count'];
				}

				if(!isset($search_categories[$parent_key])){
					$search_categories[$parent_key] = array(
						'detail' => $category_breadcrumbs[0],
						'sub_categories' => array(),
						'search_count' => $category['search_count']
					);
				} else{
					$search_categories[$parent_key]['search_count'] += $category['search_count'];
				}
			}
		}

		if(!empty($search_categories_list)){
			$original_categories = $this->categories->handler_get_all(array('id_category' => $search_categories_list, 'order_by' => 'category_weight ASC'));
			$search_categories_ordered = array();
			foreach ($original_categories as $original_category) {
				$search_categories_ordered[$original_category['category_id']] = $search_categories[$original_category['category_id']];
			}
			$search_categories = $search_categories_ordered;
		}
		
		$this->data['search_categories'] = $search_categories;

		// PAGINATION
		$settings = $this->pagination_lib->get_settings('products', $total, $limit);
		$this->pagination->initialize($settings);
		$this->data['pagination'] = $this->pagination->create_links();
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['stitle'] = 'Бренд '.$this->data['brand']['brand_title'];

		$this->data['main_content'] = 'shop/catalog/brand/index_view';
		$this->load->view($this->theme->public_view('index_view'), $this->data);
	}
}
