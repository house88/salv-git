<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Cities extends MX_Controller{
	
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model("Cities_model", "cities");
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		return_404($this->data);
	}

	function _get_cities(){
		$this->data['cities'] = $this->cities->handler_get_all();
		return $this->data['cities'];
	}

	function _get_city($id_city = 0){
		return $this->cities->handler_get($id_city);
	}
}
