<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/banners/';
		$this->active_menu = 'special_pages';
		$this->active_submenu = 'banners';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Banners_model", "banners");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_banners', '/admin');

		$this->data['page_header'] = 'Баннеры';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_banners');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_banners');

				$id_banner = (int) $this->uri->segment(5);
				$this->data['banner'] = $this->banners->handler_get($id_banner);
				if(empty($this->data['banner'])){
					jsonResponse('Баннер не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_banners');

                $this->form_validation->set_rules('banner_name', 'Название баннера', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$banner_data = $this->input->post('banner');
				if(!is_array($banner_data) || empty($banner_data)){
					jsonResponse('Вы не загрузили ни одну фотографию для баннера.');
				}

				$filtered_data = array();
				$banner_tocken = md5($this->input->post('banner_name').uniqid());
				$path = 'files/banners/'.$banner_tocken;
				create_dir($path);

				foreach($banner_data as $key_index => $banner){
					if (file_exists('files/temp/'.$banner['photo'])){
						copy('files/temp/'.$banner['photo'], $path.'/'.$banner['photo']);
						@unlink('files/temp/'.$banner['photo']);

						$filtered_data[$key_index] = array(
							'photo' 	=> $banner['photo'],
							'bg_color' 	=> !empty($banner['bg_color']) ? $banner['bg_color'] : '#ffffff',
							'title' 	=> !empty($banner['title']) ? $banner['title'] : '',
							'link' 		=> !empty($banner['link']) ? $banner['link'] : '',
							'text' 		=> !empty($banner['text']) ? $banner['text'] : '',
							'active' 	=> isset($banner['active']) ? 1 : 0
						);
					}
				}

				$remove_temp_photos = $this->input->post('remove_temp_photos');
				if(is_array($remove_temp_photos) && !empty($remove_temp_photos)){
					foreach($remove_temp_photos as $remove_temp_photo){
						@unlink('files/temp/'.$remove_temp_photo);
					}
				}

				$this->banners->set_banner(array(
					'banner_name' => $this->input->post('banner_name'),
					'banner_tocken' => $banner_tocken,
					'banner_data' => json_encode($filtered_data)
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_banners');

                $this->form_validation->set_rules('id_banner', 'Данные баннера', 'required|xss_clean');
                $this->form_validation->set_rules('banner_name', 'Название баннера', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$banner_data = $this->input->post('banner');
				if(!is_array($banner_data) || empty($banner_data)){
					jsonResponse('Вы не загрузили ни одну фотографию для баннера.');
				}

				$id_banner = (int) $this->input->post('id_banner');
				$banner_info = $this->banners->handler_get($id_banner);
				
				$path = 'files/banners/'. $banner_info['banner_tocken'];
				create_dir($path);

				$filtered_data = array();
				foreach($banner_data as $key_index => $banner){
					if (!isset($banner['is_old'])){
						if(file_exists('files/temp/'.$banner['photo'])){
							copy('files/temp/'.$banner['photo'], $path.'/'.$banner['photo']);
							@unlink('files/temp/'.$banner['photo']);
						} else{
							continue;
						}

					}

					$filtered_data[$key_index] = array(
						'photo' 	=> $banner['photo'],
						'bg_color' 	=> !empty($banner['bg_color']) ? $banner['bg_color'] : '#ffffff',
						'title' 	=> !empty($banner['title']) ? $banner['title'] : '',
						'link' 		=> !empty($banner['link']) ? $banner['link'] : '',
						'text' 		=> !empty($banner['text']) ? $banner['text'] : '',
						'active' 	=> isset($banner['active']) ? 1 : 0
					);
				}

				$remove_temp_photos = $this->input->post('remove_temp_photos');
				if(is_array($remove_temp_photos) && !empty($remove_temp_photos)){
					foreach($remove_temp_photos as $remove_temp_photo){
						@unlink('files/temp/'.$remove_temp_photo);
					}
				}

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/banners/'.$banner_info['banner_tocken'].'/'.$remove_photo);
					}
				}

				$banner = array(
					'banner_name' => $this->input->post('banner_name'),
					'banner_data' => json_encode($filtered_data)
				);

				$this->banners->update_banner($id_banner, $banner);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_banners');

                $this->form_validation->set_rules('banner', 'Баннер', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_banner = (int) $this->input->post('banner');
				$banner = $this->banners->handler_get($id_banner);
				if(empty($banner)){
					jsonResponse('Баннер не найден.');
				}
				
				$this->banners->update_banner($id_banner, array(
					'visible' => (int) !((int) $banner['visible'] === 1)
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				if(!$this->lauth->have_right('manage_banners')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('banner', 'Баннер', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_banner = (int)$this->input->post('banner');
				$banner = $this->banners->handler_get($id_banner);
				if(empty($banner)){
					jsonResponse('Баннер не существует.');
				}

				$path = 'files/banners/'.$banner['banner_tocken'];
				remove_dir($path);
				$this->banners->delete_banner($id_banner);

				jsonResponse('Сохранено.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_banners');

				$params = array(
					'limit' 	=> (int) $this->input->post('iDisplayLength'),
					'start' 	=> (int) $this->input->post('iDisplayStart')
				);

				$params['sort_by'] = flat_dt_ordering($_POST, array(
					'dt_id' => 'id_banner'
				));

				$records = $this->banners->handler_get_all($params);
				$records_total = $this->banners->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){			
						return array(
							'dt_id'			=> $record['id_banner'],
							'dt_name'		=> $record['banner_name'],
							'dt_folder' 	=> $record['banner_tocken'],
							'dt_active'		=> '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['visible'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-banner="'.$record['id_banner'].'"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/banners/popup/edit/' . $record['id_banner']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить баннер?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-banner="'.$record['id_banner'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
			case 'upload':
				checkPermisionAjax('manage_banners');

				$path = 'files/temp/';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['max_size']	= '2000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}
				$data = $this->upload->data();
				
				jsonResponse('', 'success', array("file" => (object) array(
					'filename' => $data['file_name']
				)));
			break;
		}
	}
}
