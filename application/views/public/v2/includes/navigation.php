
<?php $smallCartData = Modules::run('cart/_get');?>
<div class="bg-light">
	<div class="main-nav shadow-sm">
		<nav class="navbar navbar-expand-lg navbar-light bg-white pt-0 pb-0">
		<div class="container">
			<a class="navbar-brand" href="<?php  echo base_url();?>">
				<img src="<?php echo site_url($this->theme->public_assets('img/logo.png'));?>" alt="Logo">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto main-nav-left">
					<li class="nav-item dropdown mega-drop-main">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CATALOG</a>
						
						<div class="dropdown-menu mega-drop  shadow-sm border-0" aria-labelledby="navbarDropdown">
							<div class="row ml-0 mr-0">
								<?php echo Modules::run('static_block/_view', array('menu' => 'catalog_mega_menu'));?>
							</div>
						</div>
					</li>
				</ul>

				<ul class="navbar-nav ml-auto profile-nav-right">
					<li class="nav-item header-nav-social">
						<a class="btn-facebook" href="<?php echo @$settings['social_facebook']['setting_value'];?>" target="_blank"><i class="icofont-facebook"></i></a>
						<a class="btn-instagram" href="<?php echo @$settings['social_instagram']['setting_value'];?>" target="_blank"><i class="icofont-instagram"></i></a>
					</li>
					<?php if(true === $this->lauth->logged_in()){?>
						<li class="nav-item dropdown">
							<a class="nav-link ml-0 dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Profilul meu
							</a>
							<div class="dropdown-menu dropdown-menu-right shadow-sm border-0">
								<?php if($this->lauth->is_admin()){?>
									<a class="dropdown-item" href="<?php echo site_url('admin');?>"><i class="icofont-user-suited"></i> Administrare</a>
								<?php }?>
								<a class="dropdown-item call-popup" data-popup="#general_popup_form" data-href="<?php echo site_url('auth/popup/edit');?>" href="#"><i class="icofont-ui-edit"></i> Editeaza</a>
								<a class="dropdown-item" href="profile.html"><i class="icofont-list"></i> Lista de comenzi</a>
								<a class="dropdown-item" href="<?php echo site_url('logout');?>"><i class="icofont-logout"></i> Logout</a>
							</div>
						</li>
					<?php } else{?>
						<li class="nav-item">
							<a href="#" class="nav-link ml-0 call-popup" data-popup="#general_popup_form" data-href="<?php echo site_url('auth/popup/form')?>">
								<i class="icofont-ui-user"></i> Cont personal
							</a>
						</li>
					<?php }?>
					<li class="nav-item cart-nav">
						<a class="nav-link call-function" data-callback="cartToogle" href="#">
							<i class="icofont-basket"></i> Coș
							<span class="badge badge-danger" id="js-nav-cart-products-counter"><?php echo (int) $smallCartData['cartProductsCounter'];?></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		</nav>
	</div>
</div>

<div class="cart-sidebar" id="js-cart-sidebar-content">
	<?php echo $smallCartData['cartProductsContent'];?>
</div>