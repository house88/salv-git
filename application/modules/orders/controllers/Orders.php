<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->view_module_path = 'orders/';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model('Orders_model', 'orders');
		$this->load->model('cart/Cart_model', 'cart');
		$this->load->model('items/Items_Model', 'items');
		$this->load->model("delivery/Delivery_model", "delivery");
		$this->load->model("payment/Payment_model", "payment");
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function update(){
		$records = $this->orders->handler_get_all();
		foreach($records as $record){
			$ordered_items = $this->orders->handler_get_orders_items($record['id_order']);
			$update_order['order_ordered_items'] = json_encode($ordered_items);
			$this->orders->handler_update($record['id_order'], $update_order);
			$this->orders->handler_update_search_info($record['id_order']);
		}		
	}

	function index(){
		return_404($this->data);
	}

	function view(){
		$order_hash = $this->uri->segment(2);
		$this->data['order'] = $this->orders->handler_get_by_hash($order_hash);
		if(empty($this->data['order'])){
			return_404($this->data);
		}

		if($this->data['order']['order_user_viewed'] <= 1){
			$this->orders->handler_update($this->data['order']['id_order'], array('order_user_viewed' => 2));
			$this->data['order']['order_user_viewed'] += 1;
		}

		$this->breadcrumbs[] = array(
			'title' => 'Заказ номер ' . orderNumber($this->data['order']['id_order']),
			'link' => base_url('order/'.$order_hash)
		);

		$status = get_order_status($this->data['order']['order_status']);
		$this->data['show_payment_button'] = false;
		$this->data['status_payment_procesing'] = false;
		if($this->data['order']['order_payment_card'] == 1 && true === $status['is_payable']){
			$this->data['show_payment_button'] = true;
			$payment_info = Modules::run('payment/_get_paynet_payment', $this->data['order']['id_order']);

			if($payment_info->IsOk()){
				$this->data['show_payment_button'] = !in_array($payment_info->Data[0]['Status'], array(2,3));
				$this->data['status_payment_procesing'] = in_array($payment_info->Data[0]['Status'], array(2,3));
			}
		}

		if($this->input->get('payment') && in_array($this->input->get('payment'), array($this->data['order']['order_payment_card_success_token'], $this->data['order']['order_payment_card_cancel_token']))){
			$payment_token = $this->input->get('payment');
			if($payment_token === $this->data['order']['order_payment_card_success_token'] && in_array($this->data['order']['order_payment_card_status'], array('not_paid', 'paid'))){
				$this->session->set_flashdata('system_messages', '<li class="message-success zoomIn">Спасибо за оформление оплаты. <i class="ca-icon ca-icon_remove"></i></li>');
			} else if($payment_token === $this->data['order']['order_payment_card_cancel_token'] && $this->data['order']['order_payment_card_status'] === 'processing'){
				$this->session->set_flashdata('system_messages', '<li class="message-error zoomIn">Процесс оплаты был отменен. <i class="ca-icon ca-icon_remove"></i></li>');
				$this->orders->handler_update($this->data['order']['id_order'], array('order_payment_card_status' => 'not_paid'));
			}

			redirect('/order/' . $order_hash);
		}

		$ordered_items = json_decode($this->data['order']['order_ordered_items'], true);
		$id_delivery = (int) $this->data['order']['order_delivery_option'];
		$this->data['delivery_option'] = $this->delivery->handler_get($id_delivery);

		$id_payment = (int) $this->data['order']['order_payment_option'];
		$this->data['payment_option'] = $this->payment->handler_get($id_payment);

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		
		$this->data['main_content'] = 'orders/details_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}

	function ajax_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		if(!$this->lauth->logged_in()){
			jsonResponse('Ошибка! Пройдите авторизацию.');
		}

		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart']),
			'id_user' => $this->session->userdata('id_user')
        );
		
		$params['sort_by'] = flat_dt_ordering($_POST, array(
			'dt_name'     => 'id_order',
			'dt_amount'   => 'order_price_final',
			'dt_created'  => 'order_created'
		));
		
        $records = $this->orders->handler_get_all($params);
        $records_total = $this->orders->handler_get_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

        foreach ($records as $record) {
			$status = get_order_status($record['order_status']);
			$delivery_option = $this->delivery->handler_get($record['order_delivery_option']);	
			$id_payment = (int) $record['order_payment_option'];
			$payment_option = $this->payment->handler_get($record['order_payment_option']);

			$show_payment_button = false;
			$status_payment_procesing = false;
			if($record['order_payment_card'] == 1 && true === $status['is_payable']){
				$show_payment_button = true;
				if($record['order_payment_card_status'] !== 'paid'){
					$payment_info = Modules::run('payment/_get_paynet_payment', $record['id_order']);
	
					if($payment_info->IsOk()){
						$show_payment_button = !in_array($payment_info->Data[0]['Status'], array(2,3));
						$status_payment_procesing = in_array($payment_info->Data[0]['Status'], array(2,3));
					}
				}
			}

			$order_detail = $this->load->view($this->theme->public_view('orders/details_view'), array(
				'order' => $record, 
				'delivery_option' => $delivery_option, 
				'payment_option' => $payment_option,
				'show_payment_button' => $show_payment_button,
				'status_payment_procesing' => $status_payment_procesing
			), true);

            $output['aaData'][] = array(
                'dt_toggle'		=> '<a class="order_details" href="#"><span class="ca-icon ca-icon_plus"></span></a>',
                'dt_name'		=> orderNumber($record['id_order']),
                'dt_amount'		=> $record['order_price_final'] .' '. $record['order_currency'],
                'dt_created'	=> formatDate($record['order_created'], 'd.m.Y H:i'),
				'dt_status' 	=> '<div class="label label-'.$status['color_class'].'">'.$status['title'].'</div>',
				'dt_detail'		=> $order_detail
            );
        }
        jsonResponse('', 'success', $output);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'create':				
				$content = $this->load->view($this->theme->public_view($this->view_module_path . 'create_form_view'), $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));				
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('name', 'Cum sa ne adresăm Dvs.?', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('phone', 'Telefon', 'required|xss_clean|max_length[12]');
				$this->form_validation->set_rules('comment', 'Comentariu', 'xss_clean|max_length[500]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$basketHash = get_cookie('_cart_products');
				if(empty($basketHash) || empty($basket = $this->cart->handler_get($basketHash))){
					jsonResponse('Cosul este gol!');
				}

				$basketItems = json_decode($basket['basket_data'], true);
				$itemsList = array_keys($basketItems);
				if(empty($itemsList) || empty($items = $this->items->handler_get_all(array('items_list' => $itemsList)))){
					jsonResponse('Cosul este gol!');
				}

				$orderUserPhone = '';
				if($this->input->post('phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('phone'));
					if(false === $phoneFormatResult['is_valid']){
						jsonResponse('Numarul de telefon nu este valid.');
					}

					$orderUserPhone = $phoneFormatResult['formated'];
				}

				$insert_order = array(
					'order_user_name' => $this->input->post('name'),
					'order_user_email' => $this->input->post('email'),
					'order_user_phone' => $orderUserPhone,
					'order_user_comment' => $this->input->post('comment'),
					'order_delivery_option' => 0,
					'order_payment_option' => 0
				);

				if($this->lauth->logged_in()){
					$insert_order['id_user'] = $this->session->userdata('id_user');
				}

				$id_order = $this->orders->handler_insert($insert_order);

				$ordered_items = [];
				$original_order_price = 0;
				$total_order_price = 0;
				$total_bonus = 0;
				$group_view_decimals = $this->lauth->group_view_decimals();
				$total_cashback = $this->lauth->user_cashback_balance();
				foreach ($items as $item) {
					$price = getPrice($item);
					$quantity = (int) @$basketItems[$item['id_item']]['quantity'];
					$item_price = (float) $price['display_price'];
					$total_order_price += $item_price * $quantity;
					$original_order_price += $price['display_price'] * $quantity;
					$ordered_items[] = array(
						'id_order' => $id_order,
						'id_item' => $item['id_item'],
						'item_title' => $item['item_title'],
						'item_code' => $item['item_code'],
						'item_url' => $item['item_url'],
						'item_photo' => $item['item_photo'],
						'item_price' => $item_price,
						'item_price_final' => $item_price * $quantity,
						'item_currency' => $price['currency_symbol'],
						'item_quantity' => $quantity,
						'item_cashback_price' => ($this->lauth->group_view_cashback()) ? $item['item_cashback_price'] : 0
					);

					if($this->lauth->group_view_cashback()){
						$total_cashback += $item['item_cashback_price'] * $quantity;
						if($item['item_cashback_price'] == 0){
							$total_bonus += $price['display_price'] * $quantity;
						}
					}
				}

				$this->orders->handler_insert_orders_items($ordered_items);
				$ordered_items_bonus_amount = ($total_cashback <= $total_bonus)?$total_cashback:$total_bonus;

				$ordered_items = $this->orders->handler_get_orders_items($id_order);
				$update_order = array(
					'order_price' => $total_order_price,
					'order_price_final' => $total_order_price,
					'order_price_decimals' => $group_view_decimals,
					'order_currency' => $price['currency_symbol'],
					'order_currency_code' => $price['currency_code'],
					'order_currency_number' => $price['currency_number'],
					'order_ordered_items' => json_encode($ordered_items),
					'order_hash' => getHash(uniqid("order-{$id_order}-") . '-email-' . $this->input->post('email'))
				);

				$order_cashback = $ordered_items_bonus_amount;
				$update_order['order_cashback'] = ($order_cashback < $total_cashback) ? $order_cashback : $total_cashback;

				$this->orders->handler_update($id_order, $update_order);
				$this->orders->handler_update_search_info($id_order);

				$this->cart->handler_delete($basketHash);
				delete_cookie('_cart_products');

				$subject = 'Ati plasat comanda cu numarul '.orderNumber($id_order).' pe salv.md';
				$email_data = array(
					'title' => $subject,
					'order' => $this->orders->handler_get($id_order),
					'email_content' => 'order_detail_tpl'
				);

				Modules::run('email/send', array(
					'to' => $email_data['order']['order_user_email'],
					'subject' => $subject,
					'email_data' => $email_data
				));

				$subject = 'Clientul a plasat comanda cu numarul '.orderNumber($id_order).' pe salv.md';
				$email_data['title'] = $subject;
				$email_data['email_content'] = 'order_detail_admin_tpl';

				Modules::run('email/send', array(
					'to' => $this->data['settings']['order_admin_email']['setting_value'],
					'subject' => $subject,
					'email_data' => $email_data
				));

				jsonResponse('', 'success', [
					'orderNumber' => orderNumber($id_order)
				]);
			break;
		}
	}

	function show_email(){
		$delivery_option = $this->delivery->handler_get(3);
		$email_data = array(
			'order' => $this->orders->handler_get(8),
			'delivery' => $delivery_option,
			'title' => 'Вы оформили заказ номер '.orderNumber(8),
			'email_content' => 'order_detail_tpl'
		);
		$email_data['email_content'] = 'order_detail_tpl';
		$this->load->view('email_templates/email_tpl',$email_data);
	}

	function _get_by_hash($hash){
		return $this->orders->handler_get_by_hash($hash);
	}

	function _get($id_order){
		return $this->orders->handler_get($id_order);
	}

	function _update($id_order, $update){
		return $this->orders->handler_update($id_order, $update);
	}
	
	function _get_all($params = array()){
		return $this->orders->handler_get_all((array) $params);
	}
}