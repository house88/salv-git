<!-- DateTimeRangePicker -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/daterangepicker/daterangepicker.css'));?>">
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'));?>">
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/moment/moment.locale.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/daterangepicker/daterangepicker.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js'));?>"></script>

<script>
    var tooltips = {
        today: 'Сегодня',
        clear: 'Очистить',
        close: 'Закрыть',
        selectMonth: 'Выберите месяц',
        prevMonth: 'Предыдущий месяц',
        nextMonth: 'Следующий месяц',
        selectYear: 'Выберите год',
        prevYear: 'Предыдущий год',
        nextYear: 'Следующий год',
        selectDecade: 'Выберите десятилетие',
        prevDecade: 'Предыдущее десятилетие',
        nextDecade: 'Следующее десятилетие',
        prevCentury: 'Предыдущий век',
        nextCentury: 'Следующий век',
        pickHour: 'Выберите час',
        incrementHour: 'Добавить час',
        decrementHour:'Убавить час',
        pickMinute: 'Выберите минуту',
        incrementMinute: 'Добавить минуту',
        decrementMinute:'Убавить минуту',
        pickSecond: 'Выберите секунду',
        incrementSecond: 'Добавить секунду',
        decrementSecond:'Убавить секунду',
        togglePeriod: 'Переключить период',
        selectTime: 'Выберите время',
        selectDate: 'Select Date'
    }
</script>