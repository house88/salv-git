<p style="font: 16px Arial, sans-serif; color: #555555;margin:0 0 10px 0;padding:0;">Bine v-am găsit,</p>
<p style="font: 16px Arial, sans-serif; color: #555555;margin:0 0 10px 0;padding:0;"><?php echo $title;?></p>
<table width="100%" cellpadding="0" cellspacing="0" style="font: 12px Arial, sans-serif; color: #555555;margin:0 auto; padding:0;">
    <thead>
        <tr>
            <th style="width:75%;text-align:left;border-bottom:2px solid #d2d2d2;font-size:14px; padding-bottom:5px;">Produs</th>
            <th style="width:25%; text-align:right;border-bottom:2px solid #d2d2d2;font-size:14px; padding-bottom:5px;">Preț</th>
        </tr>
    </thead>
    <tbody>
        <?php $ordered_items = json_decode($order['order_ordered_items'], true);?>
        <?php foreach($ordered_items as $ordered_item){?>
            <tr>
                <td style="width:75%;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                    <a href="<?php echo base_url('products/'.$ordered_item['item_url']);?>" style="text-decoration:none;color:#555555;"><?php echo clean_output($ordered_item['item_title']);?></a>
                </td>
                <td style="width:25%; text-align:right;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                    <?php echo $ordered_item['item_quantity'];?> x <?php echo numberFormat($ordered_item['item_price'], $order['order_price_decimals']);?> <?php echo $ordered_item['item_currency'];?>
                </td>
            </tr>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td style="font-size:14px;text-align:right;padding-bottom:5px; padding-top:5px;">
                Suma:
            </td>
            <td style="font-size:14px;text-align:right;padding-bottom:5px; padding-top:5px;">
                <b><?php echo numberFormat($order['order_price_final'], $order['order_price_decimals']);?> <?php echo $order['order_currency'];?></b>
            </td>
        </tr>
        <?php if($order['order_cashback'] > 0){?>
            <tr>
                <td style="font-size:14px;text-align:right;padding-bottom:5px; padding-top:5px;">
                    Bonus:
                </td>
                <td style="font-size:14px;text-align:right;padding-bottom:5px; padding-top:5px;">
                    <b>- <?php echo numberFormat($order['order_cashback'], $order['order_price_decimals']);?> <?php echo $order['order_currency'];?></b>
                </td>
            </tr>                       
        <?php }?>
        <tr>
            <td style="font-size:14px;text-align:right;padding-bottom:5px; padding-top:5px;">
                Total către plată:
            </td>
            <td style="font-size:14px;text-align:right;padding-bottom:5px; padding-top:5px;">
                <b><?php echo numberFormat($order['order_price_final'] - $order['order_cashback'], $order['order_price_decimals']);?> <?php echo $order['order_currency'];?></b>
            </td>
        </tr>
    </tfoot>
</table>

<table width="100%" cellpadding="0" cellspacing="0" style="font: 12px Arial, sans-serif; color: #555555;margin:0 auto; padding:0;">
    <tbody>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Starea comenzii
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php $order_status = get_order_status($order['order_status']);?>
                <?php echo $order_status['title'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Data
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo formatDate($order['order_created'], 'd.m.Y H:i');?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Cumparator
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_name'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Еmail
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_email'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Telefon
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_phone'];?>
            </td>
        </tr>
        <tr>
            <td style="width:120px;text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                Comentariu
            </td>
            <td style="text-align:left;border-bottom:2px solid #d2d2d2;padding-bottom:5px; padding-top:5px;">
                <?php echo $order['order_user_comment'];?>
            </td>
        </tr>
    </tbody>
</table>
<?php if(!empty($changes)){?>
    <p style="font: 20px Arial, sans-serif; color: #555555;">Modificari:</p>
    <?php foreach($changes as $change){?>        
        <p style="font: 14px Arial, sans-serif; color: #555555;">&mdash; <?php echo $change;?></p>
    <?php }?>
<?php }?>