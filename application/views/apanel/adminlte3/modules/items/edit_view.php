<!-- Main content -->
<section class="content">
    <div class="card card-primary card-outline card-outline-tabs custom-margin-top-10">
        <div class="card-header p-0 border-bottom-0">
            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" title="Редактировать товар" id="js-edit-tab" data-toggle="pill" href="#js-edit-tab-content" role="tab" aria-controls="js-edit-tab-content" aria-selected="true">
                        <span class="fad fa-pencil"></span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="custom-tabs-three-tabContent">
                <div class="tab-pane fade show active" id="js-edit-tab-content" role="tabpanel" aria-labelledby="js-edit-tab">
                    <form role="form" id="edit_form">
                        <div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>Категория</label>
                                        <?php 
                                            $category_breadcrumbs = json_decode('['.$item_category['category_breadcrumbs'].']', true);
                                            $cbread = array();
                                            foreach ($category_breadcrumbs as $category_breadcrumb) {
                                                $cbread[] = $category_breadcrumb['category_title'];
                                            }
                                        ?>
                                        <div><?php echo implode(' / ', $cbread);?></div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-3">
                                    <div class="form-group">
                                        <label>Код</label>
                                        <div><?php echo $item['item_code'];?></div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>Название</label>
                                        <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo clean_output($item['item_title']);?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label>Маркетинг</label>
                                        <select name="marketting[]" class="form-control form-control-sm rounded-0 bs-select-multiple" data-live-search="true" data-style="btn btn-default btn-sm btn-flat" data-title="Выберите значение" multiple size="6">
                                            <option value="visible" <?php echo get_choice('selected', (int) $item['item_visible'] === 1);?>>Активный</option>
                                            <option value="hit" <?php echo get_choice('selected', (int) $item['item_hit'] === 1);?>>Хит</option>
                                            <option value="newest" <?php echo get_choice('selected', (int) $item['item_newest'] === 1);?>>Новинка</option>
                                            <option value="action" <?php echo get_choice('selected', (int) $item['item_action'] === 1);?>>Акция</option>
                                            <option value="popular" <?php echo get_choice('selected', (int) $item['item_popular'] === 1);?>>Популярный</option>
                                            <option value="on_home" <?php echo get_choice('selected', (int) $item['item_on_home'] === 1);?>>Показать на главной</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label>Цена</label>
                                        <div class="input-group input-group-sm">
                                            <div class="btn-group" role="group">
                                                <button id="js-item-price-variants" type="button" class="btn btn-default btn-sm btn-flat dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="d-none d-md-inline">Цены по группам</span>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="js-item-price-variants">
                                                    <label class="dropdown-item">Цены по группам</label>
                                                    <?php if(!empty($price_variants)){?>
                                                        <?php $index = 1;?>
                                                        <?php foreach($price_variants as $price_variant){?>
                                                            <div class="dropdown-item">
                                                                <div class="d-flex align-items-center">
                                                                    <input class="form-control form-control-sm rounded-0 custom-width-100 custom-margin-right-10" placeholder="Цена" name="vprice[<?php echo $price_variant['id_price_variant']?>]" value="<?php echo $item['item_price'.'_'.$price_variant['id_price_variant']];?>">
                                                                    <span><?php echo $index .'. '. $price_variant['price_variant_title']?></span>
                                                                </div>
                                                            </div>
                                                            <?php $index++;?>
                                                        <?php }?>
                                                    <?php } else{?>
                                                        <div class="dropdown-item">Нет груп.</div>
                                                    <?php }?>
                                                </div>
                                            </div>
                                            <input class="form-control form-control-sm rounded-0" placeholder="Цена" name="price" value="<?php echo $item['item_price'];?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label>Цена со скидкой</label>
                                        <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="temp_price" value="<?php echo $item['item_temp_price'];?>">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label>Кэшбэк</label>
                                        <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="cashback_price" value="<?php echo $item['item_cashback_price'];?>">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3">
                                    <div class="form-group">
                                        <label>Количество</label>
                                        <input class="form-control form-control-sm rounded-0" placeholder="0" name="quantity" value="<?php echo $item['item_quantity'];?>">
                                    </div>
                                </div>

                                <div class="col-12 col-md-3 col-md-offset-9">
                                    <?php if(!empty($brands_list)){?>
                                        <div class="form-group">
                                            <label>Брэнд <strong class="text-danger">*</strong></label>
                                            <select name="brand" class="form-control rounded-0 bs-select-element" data-live-search="true" data-style="btn btn-default btn-sm btn-flat bg-white">
                                                <option value="">Выберите brand</option>
                                                <?php foreach($brands_list as $brand){?>
                                                    <option value="<?php echo $brand['id_brand'];?>" <?php echo set_select('brand', $brand['id_brand'], $item['id_brand'] == $brand['id_brand']);?>><?php echo $brand['brand_title'];?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    <?php }?>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label>
                                            Главная фото товара
                                            <?php if(@$aditional_fields['item_photo']['active'] == 1){?>
                                                <strong class="text-danger">*</strong>
                                            <?php }?>
                                        </label>
                                        <p class="help-block">Min width: 500px, Min height: 500px</p>
                                        <div class="btn-group">
                                            <button class="btn btn-primary btn-flat btn-sm btn-file">
                                                <i class="fad fa-upload"></i>
                                                Загрузить фото <input type="file" id="inputLogo" name="file" accept=".jpg,.jpeg,.png,.gif">
                                            </button>
                                            <button class="btn btn-default btn-flat btn-sm btn-file call-function" data-callback="pasteIntoTextarea" data-wrapper="#paste-main-image-popup">
                                                <i class="fad fa-paste"></i>
                                                Вставить фото
                                            </button>
                                        </div>
                                        <div id="js-uploaded_photo" class="custom-margin-top-10">
                                            <?php if(!empty($item['item_photo'])){?>
                                                <div class="user-image-thumbnail-wr">
                                                    <img class="img-thumbnail" src="<?php echo base_url('files/items/'.$item['item_photo']);?>">
                                                    <div class="actions">
                                                        <a href="#" class="btn btn-danger btn-sm btn-flat btn-block confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger" data-photo="<?php echo $item['item_photo'];?>"><i class="fad fa-trash-alt"></i> Удалить</a>
                                                    </div>
                                                    <input type="hidden" name="photo" value="<?php echo $item['item_photo'];?>">
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>
                                            Дополнительные фото товара
                                            <?php if(@$aditional_fields['item_photos']['active'] == 1){?>
                                                <strong class="text-danger fs-18">*</strong>
                                            <?php }?>
                                        </label>
                                        <p class="help-block">Min width: 500px, Min height: 500px</p>
                                        <div class="btn-group">
                                            <button class="btn btn-primary btn-flat btn-sm btn-file">
                                                <i class="fad fa-upload"></i>
                                                Загрузить фото <input type="file" id="inputImages" name="file" accept=".jpg,.jpeg,.png,.gif">
                                            </button>
                                            <button class="btn btn-default btn-flat btn-sm btn-file call-function" data-callback="pasteIntoTextarea" data-wrapper="#paste-additional-image-popup">
                                                <i class="fad fa-paste"></i>
                                                Вставить фото
                                            </button>
                                        </div>

                                        <div id="js-uploaded_photos" class="d-flex flex-wrap custom-margin-top-10">
                                            <?php if(!empty($item['item_photos'])){?>
                                                <?php $photos = json_decode($item['item_photos']);?>
                                                <?php foreach($photos as $photo){?>
                                                    <div class="user-image-thumbnail-wr custom-margin-right-10">
                                                        <img class="img-thumbnail" src="<?php echo base_url('files/items/'.$photo);?>">
                                                        <div class="actions">
                                                            <a href="#" class="btn btn-danger btn-sm btn-flat btn-block confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger" data-photo="<?php echo $photo;?>"><i class="fad fa-trash-alt"></i> Удалить</a>
                                                        </div>
                                                        <input type="hidden" name="photos[]" value="<?php echo $photo;?>">
                                                    </div>
                                                <?php }?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    Описание
                                    <?php if(@$aditional_fields['full_description']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <textarea class="description" name="description"><?php echo $item['item_description'];?></textarea>
                            </div>
                            <div class="form-group">
                                <label>
                                    Видео
                                    <?php if(@$aditional_fields['video']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <input class="form-control" maxlength="250" placeholder="Видео" name="video" value="<?php echo $item['item_video_link'];?>">
                            </div>
                            <div class="form-group">
                                <label>
                                    Meta keywords
                                    <?php if(@$aditional_fields['mk']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <input class="form-control" placeholder="Meta keywords" name="mk" value="<?php echo $item['mk'];?>">
                                <p class="help-block">Не должно содержать более 250 символов.</p>
                            </div>
                            <div class="form-group">
                                <label>
                                    Meta description
                                    <?php if(@$aditional_fields['md']['active'] == 1){?>
                                        <strong class="text-danger fs-18">*</strong>
                                    <?php }?>
                                </label>
                                <input class="form-control" placeholder="Meta description" name="md" value="<?php echo $item['md'];?>">
                                <p class="help-block">Не должно содержать более 250 символов.</p>
                            </div>
                            <div id="properties_block"></div>
                        </div>
                        <input type="hidden" name="item" value="<?php echo $item['id_item'];?>">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>
</section>

<div class="modal fade" id="paste-main-image-popup" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body text-center">
				Для вставки картинки нажмите<br><strong>CTRL + V</strong>
				<textarea name="pasteMainImage" id="pasteMainImage" style="opacity: 0;width: 0;height: 0;position: absolute;"></textarea>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="paste-additional-image-popup" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body text-center">
				Для вставки картинки нажмите<br><strong>CTRL + V</strong>
				<textarea name="pasteAdditionalImage" id="pasteAdditionalImage" style="opacity: 0;width: 0;height: 0;position: absolute;"></textarea>
			</div>
		</div>
	</div>
</div>

<script type="text/template" id="js-image-cropper-template">
	<div class="modal-dialog modal-dialog-cropper">
		<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title">{{title}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
			</div>
			<div class="modal-body" id="js-uploaded_photo">
				<div class="cropper-modal-500x500">
					<img id="js-image-cropp" src="{{image_src}}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary call-function" data-file-name="{{file_name}}" data-file-type="{{file_type}}" data-callback="cropImage" data-settings="{{settings}}">Применить изминения</button>
			</div>
		</div>
	</div>
</script>

<script type="text/template" id="js-image-template">    
    <div class="user-image-thumbnail-wr custom-margin-right-10">
        <img class="img-thumbnail" src="{{image_src}}">
        <div class="actions">
            <a href="#" class="btn btn-danger btn-sm btn-flat btn-block confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger" data-photo="{{file_name}}"><i class="fad fa-trash-alt"></i> Удалить</a>
        </div>
		<input type="hidden" name="{{input_name}}" value="{{file_name}}">
    </div>
</script>