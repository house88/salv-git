<div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php if(!empty($group)){?>Редактировать<?php } else{?>Добавить<?php }?> группу
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="group_name" value="<?php echo !empty($group) ? $group['group_name'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Тип</label>
                            <select name="group_type" class="form-control form-control-sm rounded-0">
                                <option value="admin" <?php echo set_select('group_type', 'admin', !empty($group) && $group['group_type'] == 'admin');?>>Администратор</option>
                                <option value="moderator" <?php echo set_select('group_type', 'moderator', !empty($group) && $group['group_type'] == 'moderator');?>>Модератор</option>
                                <option value="content_menedger" <?php echo set_select('group_type', 'content_menedger', !empty($group) && $group['group_type'] == 'content_menedger');?>>Контент менеджер</option>
                                <option value="user" <?php echo set_select('group_type', 'user', !empty($group) && $group['group_type'] == 'user');?>>Клиент</option>
                            </select>
                        </div>                    
                    </div>                    
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Ценовая категория</label>
                            <select name="price_variant" class="form-control form-control-sm rounded-0">
                                <option value="static_0">Стандартная цена</option>
                                <?php 
									$price_params = array(
										'active' => 1
									);
									if(!empty($group)){
										$price_params['price_type'] = $group['group_price_type'];
										$price_params['selected_price_variant'] = $group['group_price_variant'];
									}
								?>
								<?php echo modules::run('items/_get_price_variants', $price_params);?>
							</select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="icheck-primary">
                            <input name="group_view_cashback" type="checkbox" id="js-checkbox-group_view_cashback-active" <?php echo set_checkbox('group_view_cashback', '', !empty($group) && (int) $group['group_view_cashback'] === 1);?>>
                            <label for="js-checkbox-group_view_cashback-active">
                                Показывать кэшбэк
                            </label>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="icheck-primary">
                            <input name="group_view_decimals" type="checkbox" id="js-checkbox-group_view_decimals-active" <?php echo set_checkbox('group_view_decimals', '', !empty($group) && (int) $group['group_view_decimals'] === 1);?>>
                            <label for="js-checkbox-group_view_decimals-active">
                                Показывать цену без округления
                            </label>
                        </div>
                    </div>
                    <div class="col-12">                        
                        <div class="icheck-primary">
                            <input name="group_view_stocks" type="checkbox" id="js-checkbox-group_view_stocks-active" <?php echo set_checkbox('group_view_stocks', '', !empty($group) && (int) $group['group_view_stocks'] === 1);?>>
                            <label for="js-checkbox-group_view_stocks-active">
                                Показывать количество по складам
                            </label>
                        </div>
                        <fieldset id="stocks-settings" <?php if(empty($group) || $group['group_view_stocks'] == 0){?>style="display:none;"<?php }?>>
                            <legend class="small">Настройки</legend>
                            <div id="stock-settings-wr">
                                <?php if(!empty($group)){?>
                                    <?php $stock_settings = json_decode($group['group_view_stocks_settings'], true);?>
                                    <?php if(!empty($stock_settings)){?>
                                        <?php foreach($stock_settings as $key_stock => $stock_setting){?>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="input-group input-group-sm photo_colorpicker">
                                                            <div class="input-group-prepend" title="От">
                                                                <span class="input-group-text rounded-0">от</span>
                                                            </div>
                                                            <input class="form-control float-right rounded-0" placeholder="ex. 1" name="stock_info[<?php echo $key_stock;?>][quantity_from]" value="<?php echo $stock_setting['from'];?>">
                                                            <div class="input-group-append rounded-0" title="до">
                                                                <span class="input-group-text">до</span>
                                                            </div>
                                                            <input class="form-control float-right rounded-0" placeholder="ex. 10" name="stock_info[<?php echo $key_stock;?>][quantity_to]" value="<?php echo $stock_setting['to'];?>">
                                                            <input type="hidden" class="form-control colorpicker-input" name="stock_info[<?php echo $key_stock;?>][color]" value="<?php echo $stock_setting['color'];?>"/>
                                                            <span class="input-group-append">
                                                                <span class="input-group-text colorpicker-input-addon"><i></i></span>
                                                            </span>
                                                            <select name="stock_info[<?php echo $key_stock;?>][lines]" class="form-control form-control-sm rounded-0">
                                                                <option value="1" <?php echo set_select('stock_info[][lines]', 1, (int) $stock_setting['lines'] === 1)?>>1</option>
                                                                <option value="2" <?php echo set_select('stock_info[][lines]', 2, (int) $stock_setting['lines'] === 2)?>>2</option>
                                                                <option value="3" <?php echo set_select('stock_info[][lines]', 3, (int) $stock_setting['lines'] === 3)?>>3</option>
                                                                <option value="4" <?php echo set_select('stock_info[][lines]', 4, (int) $stock_setting['lines'] === 4)?>>4</option>
                                                                <option value="5" <?php echo set_select('stock_info[][lines]', 5, (int) $stock_setting['lines'] === 5)?>>5</option>
                                                                <option value="6" <?php echo set_select('stock_info[][lines]', 6, (int) $stock_setting['lines'] === 6)?>>6</option>
                                                                <option value="7" <?php echo set_select('stock_info[][lines]', 7, (int) $stock_setting['lines'] === 7)?>>7</option>
                                                                <option value="8" <?php echo set_select('stock_info[][lines]', 8, (int) $stock_setting['lines'] === 8)?>>8</option>
                                                                <option value="9" <?php echo set_select('stock_info[][lines]', 9, (int) $stock_setting['lines'] === 9)?>>9</option>
                                                                <option value="10" <?php echo set_select('stock_info[][lines]', 10, (int) $stock_setting['lines'] === 10)?>>10</option>
                                                            </select>
                                                            <div class="input-group-append">
                                                                <span class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_settings">
                                                                    <i class="fad fa-trash-alt"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                    <?php }?>                                    
                                <?php }?>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="btn btn-default btn-sm btn-flat call-function" data-callback="add_settings" data-type="stock">Добавить настройку</span>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-12">                        
                        <div class="icheck-primary">
                            <input name="group_view_suppliers" type="checkbox" id="js-checkbox-group_view_suppliers-active" <?php echo set_checkbox('group_view_suppliers', '', !empty($group) && (int) $group['group_view_suppliers'] === 1);?>>
                            <label for="js-checkbox-group_view_suppliers-active">
                                Показывать количество по поставщикам
                            </label>
                        </div>
                        <fieldset id="suppliers-settings" <?php if(empty($group) || $group['group_view_suppliers'] == 0){?>style="display:none;"<?php }?>>
                            <legend class="small">Настройки</legend>
                            <div id="suppliers-settings-wr">
                                <?php if(!empty($group)){?>
                                    <?php $suppliers_settings = json_decode($group['group_view_suppliers_settings'], true);?>
                                    <?php if(!empty($suppliers_settings)){?>
                                        <?php foreach($suppliers_settings as $key_supplier => $supplier_settings){?>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="input-group input-group-sm photo_colorpicker">
                                                            <div class="input-group-prepend" title="От">
                                                                <span class="input-group-text rounded-0">от</span>
                                                            </div>
                                                            <input class="form-control float-right rounded-0" placeholder="ex. 1" name="suppliers_info[<?php echo $key_supplier;?>][quantity_from]" value="<?php echo $supplier_settings['from'];?>">
                                                            <div class="input-group-append rounded-0" title="до">
                                                                <span class="input-group-text">до</span>
                                                            </div>
                                                            <input class="form-control float-right rounded-0" placeholder="ex. 10" name="suppliers_info[<?php echo $key_supplier;?>][quantity_to]" value="<?php echo $supplier_settings['to'];?>">
                                                            <input type="hidden" class="form-control colorpicker-input" name="suppliers_info[<?php echo $key_supplier;?>][color]" value="<?php echo $supplier_settings['color'];?>"/>
                                                            <span class="input-group-append">
                                                                <span class="input-group-text colorpicker-input-addon"><i></i></span>
                                                            </span>
                                                            <select name="suppliers_info[<?php echo $key_supplier;?>][lines]" class="form-control form-control-sm rounded-0">
                                                                <option value="1" <?php echo set_select('stock_info[][lines]', 1, (int) $supplier_settings['lines'] === 1)?>>1</option>
                                                                <option value="2" <?php echo set_select('stock_info[][lines]', 2, (int) $supplier_settings['lines'] === 2)?>>2</option>
                                                                <option value="3" <?php echo set_select('stock_info[][lines]', 3, (int) $supplier_settings['lines'] === 3)?>>3</option>
                                                                <option value="4" <?php echo set_select('stock_info[][lines]', 4, (int) $supplier_settings['lines'] === 4)?>>4</option>
                                                                <option value="5" <?php echo set_select('stock_info[][lines]', 5, (int) $supplier_settings['lines'] === 5)?>>5</option>
                                                                <option value="6" <?php echo set_select('stock_info[][lines]', 6, (int) $supplier_settings['lines'] === 6)?>>6</option>
                                                                <option value="7" <?php echo set_select('stock_info[][lines]', 7, (int) $supplier_settings['lines'] === 7)?>>7</option>
                                                                <option value="8" <?php echo set_select('stock_info[][lines]', 8, (int) $supplier_settings['lines'] === 8)?>>8</option>
                                                                <option value="9" <?php echo set_select('stock_info[][lines]', 9, (int) $supplier_settings['lines'] === 9)?>>9</option>
                                                                <option value="10" <?php echo set_select('stock_info[][lines]', 10, (int) $supplier_settings['lines'] === 10)?>>10</option>
                                                            </select>
                                                            <div class="input-group-append">
                                                                <span class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_settings">
                                                                    <i class="fad fa-trash-alt"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                    <?php }?>                                    
                                <?php }?>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="btn btn-default btn-sm btn-flat call-function" data-callback="add_settings" data-type="suppliers">Добавить настройку</span>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
                    <?php if(!empty($group)){?>
                        <input type="hidden" name="group" value="<?php echo $group['id_group'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="templates d-none">
	<div data-template="settings-row-template">        
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <div class="input-group input-group-sm photo_colorpicker">
                        <div class="input-group-prepend" title="От">
                            <span class="input-group-text rounded-0">от</span>
                        </div>
                        <input class="form-control float-right rounded-0" placeholder="ex. 1" name="{{settings_type}}_info[{{key_index}}][quantity_from]" value="">
                        <div class="input-group-append rounded-0" title="до">
                            <span class="input-group-text">до</span>
                        </div>
                        <input class="form-control float-right rounded-0" placeholder="ex. 10" name="{{settings_type}}_info[{{key_index}}][quantity_to]" value="">
                        <input type="hidden" class="form-control colorpicker-input" name="{{settings_type}}_info[{{key_index}}][color]"/>
                        <span class="input-group-append">
                            <span class="input-group-text colorpicker-input-addon"><i></i></span>
                        </span>
                        <select name="{{settings_type}}_info[{{key_index}}][lines]" class="form-control form-control-sm rounded-0">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                        <div class="input-group-append">
                            <span class="btn btn-danger btn-sm btn-flat call-function" data-callback="delete_settings">
                                <i class="fad fa-trash-alt"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

<script>    
	function init_colorpicker(){
		$('.photo_colorpicker').colorpicker({
            input: '.colorpicker-input',
            format: 'hex'
        });
    }
    
    init_colorpicker();

	var add_settings = function(element){
        var $this = $(element);
		var template = $('div[data-template="settings-row-template"]').html();
		var setting_type = $this.data('type');
		var key_index = uniqid('key_');

		template = template.replace(/{{settings_type}}/g, setting_type);
		template = template.replace(/{{key_index}}/g, key_index);
		$('#'+setting_type+'-settings-wr').append(template);
		init_colorpicker();
    }
    
	var delete_settings = function(obj){
		var $this = $(obj);
		$this.closest('.row').remove();
	}

    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/groups/ajax_operations/<?php if(!empty($group)){?>edit<?php } else{?>add<?php }?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });

    $('input[name="group_view_stocks"]').on('change', function(e){
        if($(this).prop('checked') === true){
            $('#stocks-settings').show();
        } else{
            $('#stocks-settings').hide();
        }
    });

    $('input[name="group_view_suppliers"]').on('change', function(e){
        if($(this).prop('checked') === true){
            $('#suppliers-settings').show();
        } else{
            $('#suppliers-settings').hide();
        }
    });
</script>
