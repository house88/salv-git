<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends CI_Model{
	var $orders = "orders";
	var $orders_items = "orders_items";
	var $users = "users";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->orders, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_order, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_order', $id_order);
		$this->db->update($this->orders, $data);
	}

	function handler_delete($id_order){
		$this->db->where('id_order', $id_order);
		$this->db->delete($this->orders);
	}

	function handler_get($id_order){
		$this->db->where('id_order', $id_order);
		return $this->db->get($this->orders)->row_array();
	}

	function handler_get_by_hash($order_hash){
		$this->db->where('order_hash', $order_hash);
		return $this->db->get($this->orders)->row_array();
	}

	function handler_sum_all($conditions = array()){
		extract($conditions);
		
		$this->db->select("SUM(order_price_final+order_delivery_price) as orders_price, order_currency_code, order_currency");
		$this->db->from($this->orders);

		if(isset($id_user)){
			$this->db->where('id_user', $id_user);
        }

		if(isset($id_manager)){
			$this->db->where('id_manager', $id_manager);
        }

        if(isset($status)){
			$this->db->where('order_status', $status);
        }

        if(isset($status_client)){
			$this->db->where('order_client_not_responding', $status_client);
        }

        if(isset($delivery)){
			$this->db->where('order_delivery_option', $delivery);
        }

        if(isset($payment)){
			$this->db->where('order_payment_option', $payment);
        }

        if(isset($prog_order_id)){
			$this->db->where('prog_order_id', $prog_order_id);
        }

        if(isset($order_price_final_from)){
			$this->db->where('order_price_final >= ', $order_price_final_from);
        }

        if(isset($order_price_final_to)){
			$this->db->where('order_price_final <= ', $order_price_final_to);
        }

        if(isset($order_created_from)){
			$this->db->where("DATE(order_created) >= DATE('{$order_created_from}')");
        }

        if(isset($order_created_to)){
			$this->db->where("DATE(order_created) <= DATE('{$order_created_to}')");
        }

        if(isset($order_last_update_from)){
			$this->db->where("DATE(order_updated) >= DATE('{$order_last_update_from}')");
        }

        if(isset($order_last_update_to)){
			$this->db->where("DATE(order_updated) <= DATE('{$order_last_update_to}')");
        }

        if(isset($order_pickup_date_from)){
			$this->db->where("DATE(order_pickup_date) >= DATE('{$order_pickup_date_from}')");
        }

        if(isset($order_pickup_date_to)){
			$this->db->where("DATE(order_pickup_date) <= DATE('{$order_pickup_date_to}')");
        }

		if (isset($keywords)) {
			$this->db->group_start()
							->like('order_search_info', $keywords)
							->or_like('order_user_name', $keywords)
							->or_like('order_user_phone', $keywords)
							->or_like('order_user_phone_display', $keywords)
							->or_like('order_user_email', $keywords)
						->group_end();
		}

		$this->db->group_by('order_currency_code');

		$records = arrayByKey($this->db->get()->result_array(), 'order_currency_code');
		
		return $records;
	}

	function handler_get_all($conditions = array()){
		$order_by = " id_order DESC ";
		$coulmns = '*';

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if(!empty($multi_order_by)){
				$order_by = implode(',', $multi_order_by);
			}
		}

		$this->db->select($coulmns);
		$this->db->from($this->orders);

        if(isset($id_user)){
			$this->db->where('id_user', $id_user);
        }

		if(isset($id_manager)){
			$this->db->where('id_manager', $id_manager);
        }

        if(isset($status)){
			$this->db->where_in('order_status', $status);
        }

        if(isset($status_client)){
			$this->db->where('order_client_not_responding', $status_client);
        }

        if(isset($delivery)){
			$this->db->where('order_delivery_option', $delivery);
        }

        if(isset($payment)){
			$this->db->where('order_payment_option', $payment);
        }

        if(isset($prog_order_id)){
			$this->db->where('prog_order_id', $prog_order_id);
        }

        if(isset($order_price_final_from)){
			$this->db->where('order_price_final >= ', $order_price_final_from);
        }

        if(isset($order_price_final_to)){
			$this->db->where('order_price_final <= ', $order_price_final_to);
        }

        if(isset($order_created_from)){
			$this->db->where("DATE(order_created) >= DATE('{$order_created_from}')");
        }

        if(isset($order_created_to)){
			$this->db->where("DATE(order_created) <= DATE('{$order_created_to}')");
        }

        if(isset($order_last_update_from)){
			$this->db->where("DATE(order_updated) >= DATE('{$order_last_update_from}')");
        }

        if(isset($order_last_update_to)){
			$this->db->where("DATE(order_updated) <= DATE('{$order_last_update_to}')");
        }

        if(isset($order_pickup_date_from)){
			$this->db->where("DATE(order_pickup_date) >= DATE('{$order_pickup_date_from}')");
        }

        if(isset($order_pickup_date_to)){
			$this->db->where("DATE(order_pickup_date) <= DATE('{$order_pickup_date_to}')");
        }		

		if (isset($keywords)) {
			$this->db->group_start()
							->like('order_search_info', $keywords)
							->or_like('order_user_name', $keywords)
							->or_like('order_user_phone', $keywords)
							->or_like('order_user_phone_display', $keywords)
							->or_like('order_user_email', $keywords)
						->group_end();
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_user)){
			$this->db->where('id_user', $id_user);
        }

		if(isset($id_manager)){
			$this->db->where('id_manager', $id_manager);
        }

        if(isset($status)){
			$this->db->where_in('order_status', $status);
        }

        if(isset($status_client)){
			$this->db->where('order_client_not_responding', $status_client);
        }

        if(isset($delivery)){
			$this->db->where('order_delivery_option', $delivery);
        }

        if(isset($payment)){
			$this->db->where('order_payment_option', $payment);
        }

        if(isset($prog_order_id)){
			$this->db->where('prog_order_id', $prog_order_id);
        }

        if(isset($order_price_final_from)){
			$this->db->where('order_price_final >= ', $order_price_final_from);
        }

        if(isset($order_price_final_to)){
			$this->db->where('order_price_final <= ', $order_price_final_to);
        }

        if(isset($order_created_from)){
			$this->db->where("DATE(order_created) >= DATE('{$order_created_from}')");
        }

        if(isset($order_created_to)){
			$this->db->where("DATE(order_created) <= DATE('{$order_created_to}')");
        }

        if(isset($order_last_update_from)){
			$this->db->where("DATE(order_updated) >= DATE('{$order_last_update_from}')");
        }

        if(isset($order_last_update_to)){
			$this->db->where("DATE(order_updated) <= DATE('{$order_last_update_to}')");
        }

        if(isset($order_pickup_date_from)){
			$this->db->where("DATE(order_pickup_date) >= DATE('{$order_pickup_date_from}')");
        }

        if(isset($order_pickup_date_to)){
			$this->db->where("DATE(order_pickup_date) <= DATE('{$order_pickup_date_to}')");
        }

		if (isset($keywords)) {
			$this->db->group_start()
							->like('order_search_info', $keywords)
							->or_like('order_user_name', $keywords)
							->or_like('order_user_phone', $keywords)
							->or_like('order_user_phone_display', $keywords)
							->or_like('order_user_email', $keywords)
						->group_end();
		}

		return $this->db->count_all_results($this->orders);
	}

	function handler_update_search_info($id_order){
		$order = $this->handler_get($id_order);
		if(empty($order)){
			return false;
		}

		$search_info = array(
			$order['order_user_name'],
			$order['order_user_email'],
			$order['order_user_address'],
			orderNumber($id_order)
		);

		$ordered_items = json_decode($order['order_ordered_items'], true);
		foreach ($ordered_items as $ordered_item) {
			$search_info[] = $ordered_item['item_code'];
			$search_info[] = $ordered_item['item_title'];
		}

		$this->handler_update($id_order, array('order_search_info' => implode(' ', $search_info)));
	}

	// ORDERS ITEMS
	function handler_insert_orders_items($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert_batch($this->orders_items, $data);
	}

	function handler_update_orders_items($data = array(), $index_column = 'id_order_item'){
		if(empty($data)){
			return;
		}

		$this->db->update_batch($this->orders_items, $data, $index_column);
	}

	function handler_get_orders_items($id_order){
		$this->db->where('id_order', $id_order);
		return $this->db->get($this->orders_items)->result_array();
	}

	// USERS ORDERS 
	function handler_get_orders_users(){
		$order_by = " $this->users.user_nicename ASC ";
		$coulmns = " DISTINCT($this->users.id), $this->users.* ";

		$this->db->select($coulmns);
		$this->db->from($this->orders);
		$this->db->join($this->users, "$this->orders.id_user = $this->users.id", 'inner');
		$this->db->order_by($order_by);
		return $this->db->get()->result_array();
	}
}
