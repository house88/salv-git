<!DOCTYPE html>
<html lang="en" style="margin:0;padding:0;width:100%;height:100%;">
<head>
    <meta charset="UTF-8">
</head>

<body style="margin:0;padding:0;width:100%;height:100%;">
    <div style="background: #ededed; width: 100%;height:100%;">
        <table width="600px" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0">
            <tr>
                <td style="height: 10px;">
                    <div style="height: 10px; width: 100%;"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" height="45px" border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:15px;background:#ffffff">
                        <tr>
                            <td><a href="<?php echo base_url();?>"><img style="vertical-align: top;" src="<?php echo base_url('theme/public/salv/img/logo.png');?>" alt="SALV" border="0" width="110"></a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border:0px; padding: 15px; background: #ffffff;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0; padding: 0; background: #ffffff;">
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0;">
                                    <tr>
                                        <td>
                                            <?php $this->load->view('email_templates/'.$email_content);?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding: 15px 0; font-family: Arial,sans-serif; vertical-align: moddle; text-align:center; color: #a2a2a2; font-size: 11px; line-height: 20px;">
                    &copy; <?php echo date('Y');?> <a style="font-family: Arial,sans-serif; vertical-align: moddle; text-align:center; color: #a2a2a2; font-size: 11px; line-height: 20px; text-decoration: underline;" href="<?php echo base_url();?>">salv.md</a> | Toate drepturile rezervate<br>
                    Chișinău, Moldova
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
