<?php if(!empty($products)){?>
    <ul class="products-list">
        <?php foreach($products as $item){?>
            <li>
                <a href="<?php echo base_url('products/'.$item['item_url']);?>">
                    <span class="img-wr">
                        <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$item['item_photo']));?>">
                    </span>
                    <span class="title">
                        <?php echo clean_output($item['item_title']);?>
                    </span>
                    <span class="price">
                        <?php $price = getPrice($item);?>
                        <ins><span class="amount"><?php echo $price['display_price'] . ' <span class="currency">' . $price['currency_symbol'] . ' </span>';?></span></ins>
                        <?php if($price['display_price_old'] > 0){?>
                            <del>
                                <span class="amount"><?php echo $price['display_price_old'] . ' <span class="currency">' . $price['currency_symbol'] . ' </span>';?></span>
                            </del>                        
                        <?php }?>
                    </span>
                </a>
            </li>
        <?php }?>
    </ul>
<?php } else{?>
    <p>По вашему запросу ничего не найденно.</p>
<?php }?>