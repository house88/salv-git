<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($currency) ? 'Редактировать' : 'Добавить';?> валюту
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($currency) ? $currency['currency_name'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>ISO Код</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. USD" name="iso_code" value="<?php echo !empty($currency) ? $currency['currency_code'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Символ</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. $" name="symbol" value="<?php echo !empty($currency) ? $currency['currency_symbol'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label>Курс</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Курс" name="exchange_rate" value="<?php echo !empty($currency) ? $currency['currency_rate'] : '';?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="active" type="checkbox" id="js-checkbox-currency-active" <?php echo set_checkbox('active', 1, !empty($currency) ? (int) $currency['currency_active'] === 1 : false);?>>
                    <label for="js-checkbox-currency-active">
                        Активный
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($currency)){?>
                        <input type="hidden" name="currency" value="<?php echo $currency['id_currency'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/currency/ajax_operations/<?php echo !empty($currency) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
