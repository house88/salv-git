function systemMessages(mess_array, ul_class){
	typeM = typeof mess_array;
	var good_array = [];
	switch(typeM){
		case 'string':
			good_array = [mess_array];
		break;
		case 'array':
			good_array = mess_array;
		break;
		case 'object':
			for(var i in mess_array){
				good_array.push(mess_array[i]);
			}
		break;

	}

	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	var $systMessUl = $('.system-text-messages-b ul');
	$systMessUl.html('');
	for (var li in good_array ) {
		$systMessUl.prepend('<li  class="message-' + ul_class + '">'+ good_array[li] +' <i class="fa fa-times"></i></li>');
		$systMessUl.children('li').first().addClass('zoomIn').show().delay( 30000 ).slideUp('slow', function(){
			if($systMessUl.children('li').length == 1){
				$systMessUl.closest('.system-text-messages-b').slideUp();
				$(this).remove();
			}else
				$(this).remove();
		});
	}
}

function showLoader(parentId){
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.show();
    else {
        $(parentId).prepend('<div class="ajax-loader"><div class="wrapper"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
    }
}

function showAbsoluteLoader(parentId){
    var $this = $(parentId).children('.ajax-loader.absolute-loader');
    if ($this.length > 0)
        $this.show();
    else {
        $(parentId).prepend('<div class="ajax-loader absolute-loader"><div class="wrapper"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
    }
}

function hideLoader(parentId){
    var $this = $(parentId).children('.ajax-loader');
    if ($this.length > 0)
        $this.hide();
}

var cartToogle = function(element){
	$('body').toggleClass('toggled');
}

$(document).ready(function() {
    "use strict";

	$('body').on("click", '.system-text-messages-b li .fa-times', function(){
		var $li = $(this).closest('li');
		$li.clearQueue();
		$li.slideUp('slow',function(){
			if($('.system-text-messages-b li').length == 1){
				$('.system-text-messages-b').slideUp();
				$li.remove();
			}else
				$li.remove();
		});
	});

    // ===========Category Owl Carousel============
    var objowlcarousel = $(".owl-carousel-category");
    if (objowlcarousel.length > 0) {
        objowlcarousel.owlCarousel({
            items: 4,
            lazyLoad: true,
            pagination: false,
			 loop: true,
            autoPlay: 2000,
            navigation: true,
            stopOnHover: true,
			navigationText: ["<i class='icofont icofont-thin-left'></i>", "<i class='icofont icofont-thin-right'></i>"]
        });
    }

    // ===========Hover Nav============ 
    $('.navbar-nav li.dropdown').on('mouseenter', function(){ $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500); })
    $('.navbar-nav li.dropdown').on('mouseleave', function(){ $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500); })
    
	// ===========Select2============
    $('select').select2();
	
    // ===========Tooltip============
    $('[data-toggle="tooltip"]').tooltip();

    // ===========Single Items Slider============   
    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
    sync1.owlCarousel({
        singleItem: true,
        items: 1,
        slideSpeed: 1000,
        pagination: false,
        navigation: true,
        autoPlay: 2500,
		dots: false,
        nav: true,
        navigationText: ["<i class='icofont icofont-thin-left'></i>", "<i class='icofont icofont-thin-right'></i>"],
        afterAction: syncPosition,
        responsiveRefreshRate: 200,
    });
    sync2.owlCarousel({
        items: 5,
        navigation: true,
        dots: false,
        pagination: false,
        nav: true,
        navigationText: ["<i class='icofont icofont-thin-left'></i>", "<i class='icofont icofont-thin-right'></i>"],
        responsiveRefreshRate: 100,
        afterInit: function(el) {
            el.find(".owl-item").eq(0).addClass("synced");
        }
    });

    function syncPosition(el) {
        var current = this.currentItem;
        $("#sync2")
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")
        if ($("#sync2").data("owlCarousel") !== undefined) {
            center(current)
        }
    }
    $("#sync2").on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo", number);
    });

    function center(number) {
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for (var i in sync2visible) {
            if (num === sync2visible[i]) {
                var found = true;
            }
        }
        if (found === false) {
            if (num > sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", num - sync2visible.length + 2)
            } else {
                if (num - 1 === -1) {
                    num = 0;
                }
                sync2.trigger("owl.goTo", num);
            }
        } else if (num === sync2visible[sync2visible.length - 1]) {
            sync2.trigger("owl.goTo", sync2visible[1])
        } else if (num === sync2visible[0]) {
            sync2.trigger("owl.goTo", num - 1)
        }
    }
	
	// ===========Datatabel============
	// $('.datatabel').DataTable();

    $('body').on('click touchend', ".call-systmess", function(e){
		e.preventDefault();
		var $thisBtn = $(this);
		var mess = $thisBtn.data('message');
		var type = $thisBtn.data('type');
		systemMessages(mess, type);

		return false;
	});

	$('body').on('click touchend', ".call-function", function(e){
		e.preventDefault();
		var $thisBtn = $(this);
		if($thisBtn.hasClass('disabled')){
			return false;
		}
		
		var callBack = $thisBtn.data('callback');
		window[callBack]($thisBtn);
		return false;
	});
	

	$('body').on('click touchend', ".call-popup", function(e){
		e.preventDefault();
		var $this = $(this);
		if($this.hasClass('disabled')){
			return false;
		}

		var popup_url = $this.data('href');
		var $popup_parent = $($this.data('popup'));
		var formdata = {};
		if($this.data('formdata-callback') != undefined){
			formdata = window[$this.data('formdata-callback')]($this);
		}
		$.ajax({
			type: 'POST',
			url: popup_url,
			data: formdata,
			dataType: 'JSON',
			beforeSend: function(){},
			success: function(resp){
				if(resp.mess_type == 'success'){
					$popup_parent.html(resp.popup_content).modal('show');
				} else{
					systemMessages( resp.message, resp.mess_type );
				}
			}
		});
		return false;
	});
});

function intval(num){
	if (typeof num == 'number' || typeof num == 'string'){
		num = num.toString();
		var dotLocation = num.indexOf('.');
		if (dotLocation > 0){
			num = num.substr(0, dotLocation);
		}

		if (isNaN(Number(num))){
			num = parseInt(num);
		}

		if (isNaN(num)){
			return 0;
		}

		return Number(num);
	} else if (typeof num == 'object' && num.length != null && num.length > 0){
		return 1;
	} else if (typeof num == 'boolean' && num === true){
		return 1;
	}

	return 0;
}

function floatval(mixed_var) {
	return (parseFloat(mixed_var) || 0);
}

var addToCartIncreaseQuantity = function(element){
    var $this = $(element);
    var field = $($this.data('field'));
    var fieldValue = intval(field.val());

    fieldValue++;
    field.val(fieldValue);
}

var addToCartDecreaseQuantity = function(element){
    var $this = $(element);
    var field = $($this.data('field'));
    var fieldValue = intval(field.val());

    fieldValue--;
    if(0 >= fieldValue){
        fieldValue = 1;
    }
    field.val(fieldValue);
}

var sort_products = function(btn){
	var $this = $(btn);
	var sort_by = $this.data('sortby');
	window.location.search = '?sort_by=' + sort_by;
}

var addToCart = function(element){
	var $this = $(element);
	var form = $this.closest('form');
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/add',
		dataType: 'JSON',
		data: form.serialize(),
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
            hideLoader('body');

			if(resp.mess_type == 'success'){
				updateSmallCart(resp);
			} else{
				systemMessages(resp.message, resp.mess_type);
			}
		}
	});
}

function updateSmallCart(data){
    $('#js-nav-cart-products-counter').text(`${data.cartProductsCounter}`);
    $('#js-cart-sidebar-content').html(`${data.cartProductsContent}`);
}

var removeCartProduct = function(element){
	var $this = $(element);
	var item = $this.data('item');
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/delete',
		dataType: 'JSON',
		data:{item:item},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			hideLoader('body');

			if(resp.mess_type == 'success'){
				updateSmallCart(resp);
			}
		}
	});
}

var updateCartIncreaseQuantity = function(element){
    var $this = $(element);
	var item = $this.data('item');
    var field = $($this.data('field'));
    var fieldValue = intval(field.val());

    fieldValue++;
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/change_quantity',
		dataType: 'JSON',
		data:{item:item, quantity:fieldValue},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			hideLoader('body');

			if(resp.mess_type == 'success'){
				updateSmallCart(resp);
			}
		}
	});
}

var updateCartDecreaseQuantity = function(element){
    var $this = $(element);
	var item = $this.data('item');
    var field = $($this.data('field'));
    var fieldValue = intval(field.val());

    fieldValue--;
    if(0 >= fieldValue){
        fieldValue = 1;
    }
	$.ajax({
		type: 'POST',
		url: base_url+'cart/ajax_operations/change_quantity',
		dataType: 'JSON',
		data:{item:item, quantity:fieldValue},
		beforeSend: function(){
			showLoader('body');
		},
		success: function(resp){
			hideLoader('body');

			if(resp.mess_type == 'success'){
				updateSmallCart(resp);
			}
		}
	});
}