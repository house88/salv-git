<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . "third_party/paynet/PaynetAPI.php";
require_once APPPATH . "third_party/paynet/PaynetConfig.php";

class Paynet
{
    private $api;
    private $salt = '@tehn0Order5a17';

	public function __construct(){
        $this->api = new PaynetEcomAPI(MERCHANT_CODE,MERCHANT_SEC_KEY,MERCHANT_USER,MERCHANT_USER_PASS);
	}    
	
	public function registerOrder($order){
		$LinkSuccessToken = hash('sha1', $order['id_order'] . uniqid() . 'success' . $this->salt);
		$LinkCancelToken = hash('sha1', $order['id_order'] . uniqid() . 'cancel' . $this->salt);
		Modules::run('orders/_update', $order['id_order'], array(
			'order_payment_card_success_token' => $LinkSuccessToken,
			'order_payment_card_cancel_token' => $LinkCancelToken
		));

        $prequest = new PaynetRequest();
        $prequest->ExternalID =  $order['id_order'];
        $prequest->LinkSuccess = base_url('order/' . $order['order_hash'] . '?payment=' . $LinkSuccessToken);
        $prequest->LinkCancel =  base_url('order/' . $order['order_hash'] . '?payment=' . $LinkCancelToken);
        $prequest->Lang = 'ru-RU';

        $products = json_decode($order['order_ordered_items'], true);

		$prequest_products = [];
		$index = 0;
        foreach ($products as $product) {
			$prequest_products[] = array(
				"Amount" => (int) (100 * $product['item_price_final']),
				"Code" => "{$product['id_item']}",
				"LineNo" => $index, 
				"Name" => $this->normalizeProductTitle($product['item_title']),
				"Quantity" => (int) $product['item_quantity'] * 100,
				"UnitPrice" => (int) (100 * $product['item_price']),
				"UnitProduct" => 'шт'
			);
			
			$index++;
        }

        if($order['order_delivery_price'] > 0){
			$prequest_products[] = array( 
				'Amount'	=> (int) (100 * $order['order_delivery_price']),
				"Code" => "DO-{$order['id_order']}",
				"LineNo" => $index, 
				'Name' => 'Доставка',
				"Quantity" => 100,
				"UnitPrice" => (int) (100 * $order['order_delivery_price']),
				"UnitProduct" => 'шт'
			);
        }
            
        $prequest->Service = array ( 
            array ( 
                'Name'		 => 'Заказ № ' . orderNumber($order['id_order']),
                'Description'=> 'Заказ товаров на сайте ATEHNO - Basm, Молдова Бельцы',
                'Amount'	=> 0,
                'Products'	=> $prequest_products
            )
        );

        $prequest->Amount = (int) (100 * ($order['order_price_final'] + $order['order_delivery_price']));

        $prequest->Customer = array(
            "NameFirst" => $order['order_user_name'],
            "PhoneNumber" => $order['order_user_phone'],
            "email" => $order['order_user_email'],
            "Country" => "Молдова",
            "Address" => $order['order_user_address']
		);
		
        
		$paymentRegObj = $this->api->PaymentReg($prequest);
		
        return $paymentRegObj;
	}
	
	public function paymentGet($xternalId){
		return $this->api->PaymentGet($xternalId);
	}

	public function normalizeProductTitle($str){
		$rules = array(
			'Any-Latin;',
			'NFD;',
			'[:Nonspacing Mark:] Remove;',
			'NFC;'
		);

		$str = transliterator_transliterate(implode('', $rules), $str);
		if(strlen($str) > 100 )
		{
			$str = substr($str,0,strpos($str, ' ',100)) . ' ...';
		}

		return trim($str, ' ');
	}
}
