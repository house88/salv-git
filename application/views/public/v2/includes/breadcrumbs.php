<?php if(!empty($breadcrumbs)){?>
	<section class="py-2  inner-header">
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-lg-12">
					<div class="breadcrumbs">
						<p class="mb-0">
							<a href="<?php echo base_url();?>">
								<span class="icofont icofont-ui-home"></span> Home
							</a> 							
							<?php foreach($breadcrumbs as $breadcrumb){?>
								<span class="icofont icofont-thin-right"></span> 
								<?php if(!empty($breadcrumb['link'])){?>
									<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['title'];?></a>
								<?php } else{?>
									<span><?php echo $breadcrumb['title'];?></span>
								<?php }?>
							<?php }?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php }?>
