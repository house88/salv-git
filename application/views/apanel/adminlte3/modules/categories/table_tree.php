<?php if(!empty($category)){?>
		<?php 
			$breads = json_decode('['.$category['category_breadcrumbs'].']', true);
			$cat_parents = array();
			foreach($breads as $bread){
				$cat_parents[] = $bread['category_id'];
			}
		?>
		<tr class="parent_<?php echo $category['category_parent'];?>" data-category="<?php echo $category['category_id'];?>" data-parent="<?php echo $category['category_parent'];?>" data-parents="p<?php echo implode('-', $cat_parents);?>" <?php if($category['category_parent'] != 0){echo 'style="display:none;"';}?>>
			<td class="text-center">
				<?php if(!empty($category['category_children'])){?>
					<button type="button" class="btn btn-default btn-xs js-toggle-categories call-function" data-callback="toggle_one" data-toggle="p<?php echo implode('-', $cat_parents);?>"><i class="fad fa-plus"></i></button>
				<?php }?>
			</td>
			<td class="text-center">
				<?php echo $category['category_id'];?>
			</td>
			<td>
				<?php if((int) $category['category_level'] === 2){?>
					<span class="custom-padding-left-15">
						<i class="fad fa-arrow-from-left"></i>
					</span>
				<?php }?>
				<?php if($category['category_level'] > 2){?>
					<span class="custom-padding-left-<?php echo $category['category_level'] * 10;?>">
						<i class="fad fa-arrow-right"></i>
					</span>
				<?php }?>
				<?php echo $category['category_title'];?>
			</td>
			<td>
				<?php if(!empty($category_parent)){?>
					<?php echo $category_parent['category_title'];?>
				<?php } else{?>
					&mdash;
				<?php }?>
			</td>
			<td><?php echo $category['category_url'];?></td>
			<td>
				<div class="input-group input-group-sm js-category-weight">
					<input class="form-control rounded-0" name="category_weight" value="<?php echo (int) $category['category_weight'];?>">
					<span class="input-group-append">
						<button type="button" class="btn btn-success btn-flat rounded-0 call-function" data-callback="change_weight" title="Применить" data-category="<?php echo $category['category_id'];?>">
							<i class="fad fa-check"></i>
						</button>
					</span>
				</div>
			</td>
			<td class="text-center"><?php echo $category['category_items_count'];?></td>
			<td class="text-center">
				<div class="dropdown">
					<a data-toggle="dropdown" href="#" aria-expanded="false">
						<i class="fas fa-ellipsis-h"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
						<div class="dropdown-divider"></div>
						<?php if((int) $category['category_active'] === 1){?>
							<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите сделать категорию невидимой?" data-title="<i class=\'fad fa-eye\'></i> Сделать категорию невидимой" data-callback="change_status" data-category="<?php echo $category['category_id'];?>" data-type="bg-warning">
								<i class="fad fa-eye-slash"></i> Скрыть категорию
							</a>
						<?php } else{?>
							<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите сделать категорию видимой?" data-title="<i class=\'fad fa-eye-slash\'></i> Сделать категорию видимой" data-callback="change_status" data-category="<?php echo $category['category_id'];?>" data-type="bg-warning">
								<i class="fad fa-eye"></i> Показать категорию
							</a>
						<?php }?>
						
						<div class="dropdown-divider"></div>
						<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/categories/popup/edit/'.$category['category_id']);?>">
							<i class="fad fa-pencil"></i> Редактировать
						</a>
						
						<div class="dropdown-divider"></div>
						<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/categories/popup/product_options/'.$category['category_id']);?>">
							<i class="fad fa-pencil"></i> Правила редактирования товаров
						</a>
					</div>
				</div>
			</td>
		</tr>
<?php }?>
