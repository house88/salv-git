<?php if(!empty($properties)){?>
    <div class="item_properties-wr">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="custom-width-200">Proprietate</th>
                    <th>Valoare</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach($properties as $property){?>                
                    <?php $property_values = json_decode($property['property_values'], true);?>
                    <tr>
                        <td class="custom-width-200">
                            <?php echo $property['title_property']; if($property['type_property'] == 'range'){ echo ', '.$property_values['unit'];}?>
                        </td>
                        <td><?php echo implode(', ', $property['item_values']);?></td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
<?php }?>