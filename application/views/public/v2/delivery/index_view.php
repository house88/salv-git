<?php if(!empty($delivery_options)){?>
    <?php foreach($delivery_options as $delivery_option){?>
        <fieldset>
            <legend class="scheduler-border"><?php echo $delivery_option['delivery_title'];?></legend>
            <div class="col-xs-12">
                <?php echo $delivery_option['delivery_description'];?>
            </div>                    
        </fieldset>
    <?php }?>
<?php } else{?>
    <div class="alert alert-info mb-0" role="alert">
        Нет данных!
    </div>
<?php }?>