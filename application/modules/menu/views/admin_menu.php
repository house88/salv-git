<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<?php foreach($items as $item){?>
				<?php if($this->lauth->have_right_or($item->rights)){?>
					<li>
						<a href="<?php echo base_url('/admin/'.$item->url); ?>">
							<?php echo $item->name; ?>
							<?php if(!empty($item->submenu)){?><span class="fa arrow"></span><?php }?>
						</a>
						<?php if(!empty($item->submenu)){?>
							<ul class="nav nav-second-level">
							<?php foreach($item->submenu as $item_submenu){?>
								<?php if($this->lauth->have_right_or($item_submenu['rights'])){?>
									<li>
										<a href="<?php echo base_url('/admin/'.$item_submenu['url']); ?>"><?php echo $item_submenu['name']; ?></a>
									</li>
								<?php }?>
							<?php }?>
							</ul>
						<?php }?>
					</li>				
				<?php }?>
			<?php }?>
		</ul>
		<ul class="nav">			
			<?php if($this->lauth->have_right('manage_files')){?>
				<li>
					<a class="active" data-filemanager data-src="<?php echo site_url('tinymanager/init/default');?>/dialog.php?type=2&field_id=&fldr=" href="javascript:;">
						<i class="fa fa-folder-open"></i> Файловый менеджер
					</a>
					<script>
						$(function(){
							$("[data-filemanager]").fancybox({
								speed: 230,
								buttons: [
									"close"
								],
								iframe: {
									scrolling: 'auto'
								}
							});
						});
					</script>
				</li>
			<?php }?>
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>
