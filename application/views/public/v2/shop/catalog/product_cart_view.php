<?php $productPrice = getPrice($item);?>
<div class="card list-item bg-white rounded overflow-hidden position-relative shadow-sm">
    <a class="dark-link" href="<?php echo base_url('products/'.$item['item_url']);?>">
        <div class="product-labels">
            <?php if($item['item_action'] == 1){?>
                <span class="badge badge-warning">% REDUCERE</span>
            <?php }?>
            <?php if($item['item_newest'] == 1){?>
                <span class="badge badge-danger">NEW</span>           
            <?php }?>
            <?php if($item['item_hit'] == 1){?>
                <span class="badge badge-success">HIT VINZARI</span>
            <?php }?>
        </div>
        <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$item['item_photo']));?>" class="card-img-top" alt="<?php echo clean_output($item['item_title']);?>">
        <div class="card-body">
            <h6 class="card-title mb-1"><?php echo clean_output($item['item_title']);?></h6>
            <p class="mb-0 text-dark custom-font-size-20">
                <?php echo $productPrice['display_price'] . ' <span class="currency custom-font-size-12">' . $productPrice['currency_symbol'] . ' </span>';?>
                <?php if($productPrice['display_price_old'] > 0){?>
                    <span class="text-black-50 custom-font-size-14">
                        <del><?php echo $productPrice['display_price_old'] . '<span class="currency custom-font-size-12">' . $productPrice['currency_symbol'] . ' </span>';?></del>
                    </span>                       
                <?php }?>
            </p>
        </div>
    </a>
</div>