
<div class="bg-light">
    <?php $homeBanner = Modules::run('banners/_get_by', array('banner_tocken' => '7b3f500d6ddbfff9d9f67e60d6de206a'));?>
	<?php if(!empty($homeBanner)){?>
		<?php $bannerPhotos = !empty($homeBanner['banner_data']) ? json_decode($homeBanner['banner_data'], true) : array();?>
		<?php if(!empty($bannerPhotos)){?>
			<div class="pb-3 pt-3">
				<header>
					<div class="carousel" data-flickity='{ "wrapAround": true }'>
						<?php foreach($bannerPhotos as $bannerPhoto){?>
							<?php if((int) @$bannerPhoto['active'] == 1){?>
								<div class="carousel-cell">
									<?php if(!empty($bannerPhoto['link'])){?>
										<a href="<?php echo $bannerPhoto['link'];?>">
											<img class="img-fluid mx-auto rounded shadow-sm" src="<?php echo site_url('files/banners/' . $homeBanner['banner_tocken'] . '/' . $bannerPhoto['photo']);?>">
										</a>
									<?php } else{?>
										<img class="img-fluid mx-auto rounded shadow-sm" src="<?php echo site_url('files/banners/' . $homeBanner['banner_tocken'] . '/' . $bannerPhoto['photo']);?>">
									<?php }?>
								</div>
							<?php }?>
						<?php }?>
					</div>
				</header>
			</div>
		<?php }?>
	<?php }?>
</div>

<?php $promoBannerFour = Modules::run('banners/_get_by', array('banner_tocken' => '4ba63f7a0149dd88e4b7db16d3448b87'));?>
<?php if(!empty($promoBannerFour)){?>
    <?php $bannerPhotos = !empty($promoBannerFour['banner_data']) ? json_decode($promoBannerFour['banner_data'], true) : array();?>
    <?php if(!empty($bannerPhotos) && 4 === count($bannerPhotos)){?>
        <?php $bannerPhotos = array_values($bannerPhotos);?>
        <section class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <div class="offers-block">
                            <a href="<?php echo $bannerPhotos[0]['link'];?>">
                                <img class="img-fluid" src="<?php echo site_url('files/banners/' . $promoBannerFour['banner_tocken'] . '/' . $bannerPhotos[0]['photo']);?>" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="offers-block">
                            <a href="<?php echo $bannerPhotos[1]['link'];?>">
                                <img class="img-fluid mb-3" src="<?php echo site_url('files/banners/' . $promoBannerFour['banner_tocken'] . '/' . $bannerPhotos[1]['photo']);?>" alt="">
                            </a>
                        </div>
                        <div class="offers-block">
                            <a href="<?php echo $bannerPhotos[2]['link'];?>">
                                <img class="img-fluid" src="<?php echo site_url('files/banners/' . $promoBannerFour['banner_tocken'] . '/' . $bannerPhotos[2]['photo']);?>" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="offers-block">
                            <a href="<?php echo $bannerPhotos[3]['link'];?>">
                                <img class="img-fluid" src="<?php echo site_url('files/banners/' . $promoBannerFour['banner_tocken'] . '/' . $bannerPhotos[3]['photo']);?>" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php }?>
<?php }?>

<?php if($home_settings['newest_products']['setting_active']){?>
    <?php $newestProductsSettings = json_decode($home_settings['newest_products']['setting_data']);?>
    <?php $products = Modules::run('items/_get_all', [
            'item_newest' => 1,
            'item_visible' => 1,
            'order_by' => 'RAND()',
            'limit' => $newestProductsSettings->limit,
            'start' => 0
    ]);?>
    

    <?php if(!empty($products)){?>
        <section class="product-list pbc-5 pb-4 pt-5 bg-light">
            <div class="container">
                <h4 class="mt-0 mb-3 text-dark font-weight-normel"><?php echo $newestProductsSettings->block_name;?></h4>
                <div class="row">
                    <?php foreach($products as $item){?>
                        <!-- PRODUCT - START -->
                        <div class="col-6 col-md-3">
                            <?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $item));?>
                        </div>
                        <!-- PRODUCT - END -->
                    <?php }?>
                </div>
            </div>
        </section>
    <?php }?>
<?php }?>

<?php $promoBannerDouble = Modules::run('banners/_get_by', array('banner_tocken' => '408817904d8ce3265b8da2a50e444263'));?>
<?php if(!empty($promoBannerDouble)){?>
    <?php $bannerDoublePhotos = !empty($promoBannerDouble['banner_data']) ? json_decode($promoBannerDouble['banner_data'], true) : array();?>
    <?php if(!empty($bannerDoublePhotos) && 2 === count($bannerDoublePhotos)){?>
        <?php $bannerDoublePhotos = array_values($bannerDoublePhotos);?>
        <section class="offer-product py-5">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <div class="offers-block">
                            <a href="<?php echo $bannerDoublePhotos[0]['link'];?>">
                                <img class="img-fluid" src="<?php echo site_url('files/banners/' . $promoBannerDouble['banner_tocken'] . '/' . $bannerDoublePhotos[0]['photo']);?>" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="offers-block">
                            <a href="<?php echo $bannerDoublePhotos[1]['link'];?>">
                                <img class="img-fluid" src="<?php echo site_url('files/banners/' . $promoBannerDouble['banner_tocken'] . '/' . $bannerDoublePhotos[1]['photo']);?>" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php }?>
<?php }?>

<?php if($home_settings['popular_products']['setting_active']){?>
    <?php $popularProductsSettings = json_decode($home_settings['popular_products']['setting_data']);?>
    <?php $popularProducts = Modules::run('items/_get_all', [
            'item_popular' => 1,
            'item_visible' => 1,
            'order_by' => 'RAND()',
            'limit' => $newestProductsSettings->limit,
            'start' => 0
    ]);?>
    
    <?php if(!empty($popularProducts)){?>
        <section class="product-list pbc-5 pb-4 pt-5 bg-light">
            <div class="container">
                <h4 class="mt-0 mb-3 text-dark font-weight-normel"><?php echo $popularProductsSettings->block_name;?></h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel owl-carousel-category owl-theme">
                            <?php foreach($popularProducts as $item){?>
                                <!-- PRODUCT - START -->
                                <div class="item">
                                    <?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $item));?>
                                </div>
                                <!-- PRODUCT - END -->
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php }?>
<?php }?>