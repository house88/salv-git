<div class="row dashboard-page-wr">
	<div class="col-xs-12">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="<?php echo base_url('account');?>">
                    Мои заказы
                    <?php if($this->lauth->group_has_cashback()){?>
                        <span class="label label-primary">Бонусные балы: <?php echo numberFormat($this->lauth->user_cashback_balance(), $this->lauth->group_view_decimals());?> лей</span>
                    <?php }?>
                </a>
            </li>
            <li role="presentation"><a href="<?php echo base_url('account/settings');?>">Личные данные</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
                <div class="col-xs-12">
                    <?php $this->load->view($this->theme->public_view('orders/list_view'));?>
                </div>
            </div>
        </div>
	</div>
</div>