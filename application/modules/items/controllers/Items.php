<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Items extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->data = array();
		
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Items_model", "items");
		$this->load->model("Comments_model", "comments");
		$this->load->model("Prices_model", "prices");
		$this->load->model("Dynamic_prices_model", "dynamic_prices");
	}

	function index(){
		return_404($this->data);
	}

	function popup_forms(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'add_comment':
				// GET PRODUCT
				$item_id = (int)$this->uri->segment(4);
				$this->data['product'] = $this->items->handler_get($item_id);
				if(empty($this->data['product'])){
					messageInModal('Такого товара нет.', 'danger');
				}

				$this->data['recaptcha_widget'] = $this->recaptcha->create_box();
				$this->load->view($this->theme->public_view('shop/product/comments/form_view'), $this->data);
			break;
			default:
				messageInModal('Ошибка.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
		
		$action = $this->uri->segment(3);
		switch ($action) {
			case 'add_comment':
				$this->form_validation->set_rules('item', 'Товар', 'required|xss_clean');
				$this->form_validation->set_rules('author', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('comment', 'Коментарий', 'required|xss_clean|max_length[500]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				// GET PRODUCT
				$id_item = (int)$this->input->post('item');
				$product = $this->items->handler_get($id_item);
				if(empty($product)){
					jsonResponse('Такого товара нет.', 'danger');
				}
				
				// $is_valid = $this->recaptcha->is_valid();
				// if(!$is_valid['success']){
				// 	jsonResponse('Ошибка проверки данных.');
				// }

				// ADD COMMENT TO THE DATABASE
				$insert = array(
					'id_item' => $id_item,
					'comment_username' => $this->input->post('author'),
					'comment_user_email' => $this->input->post('email'),
					'comment_text' => $this->input->post('comment'),
				);
				$id_comment = $this->comments->handler_insert($insert);
				$this->data['comment'] = $this->comments->handler_get($id_comment);
				$comment_html = $this->load->view($this->theme->public_view('shop/product/comments/item_view'), $this->data, true);
				jsonResponse('Спасибо за ваш отзыв.', 'success', array('comment' => $comment_html));
			break;
			default:
				jsonResponse('Ошибка.');
			break;
		}
	}

	function _get_prices_variants_all($params = array()){
		return $this->prices->handler_get_all($params);
	}

	function _get_price_variants($conditions = array()){
		extract($conditions);
		$params = array();

		if(isset($active)){
			$params['active'] = 1;
		}

		if(isset($price_type)){
			$this->data['price_type'] = $price_type;
		}

		if(isset($selected_price_variant)){
			$this->data['selected_price_variant'] = $selected_price_variant;
		}

        $this->data['price_variants'] = $this->prices->handler_get_all($params);
        $this->data['dynamic_price_variants'] = $this->dynamic_prices->handler_get_all($params);
        return $this->load->view($this->theme->apanel_view('modules/items/price_variants_select_list'), $this->data, true);
	}

	function _get_comments($conditions = array()){
		extract($conditions);

        return $this->comments->handler_get_all(array(
			'id_item' => isset($id_item) ? (int) $id_item : 0
		));
	}

	function _get_all($conditions = array()){
		return $this->items->handler_get_all($conditions);
	}
}
