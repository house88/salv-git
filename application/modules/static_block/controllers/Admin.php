<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/static_block/';
		$this->active_menu = 'special_pages';
		$this->active_submenu = 'static_block';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Static_block_model", "static_block");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_static_block', '/admin');

		$this->data['page_header'] = 'Статические блоки';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_static_block');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_static_block');

				$id_block = (int) $this->uri->segment(5);
				$this->data['static_block'] = $this->static_block->handler_get($id_block);
				if(empty($this->data['static_block'])){
					jsonResponse('Статический блок не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_static_block');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('alias', 'Алиас', 'required|xss_clean');
				$this->form_validation->set_rules('text', 'Код страницы', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'block_title' => $this->input->post('title'),
					'block_alias' => $this->input->post('alias'),
					'block_text' => $this->input->post('text'),
					'is_page' => $this->input->post('is_page') ? 1 : 0,
					'is_landing' => $this->input->post('is_landing') ? 1 : 0
				);

				$breadcrumbs = $this->input->post('breadcrumbs');
				$filtered_breadcrumbs = array();
				if(is_array($breadcrumbs)){
					foreach ($breadcrumbs as $bread_key => $bread) {
						if(!empty($bread['title']) && !empty($bread['url'])){
							$filtered_breadcrumbs[$bread_key] = array(
								'link_title' => $bread['title'],
								'link_url' => $bread['url']
							);
						}
					}
				}

				$insert['block_breadcrumbs'] = json_encode($filtered_breadcrumbs);

				$this->static_block->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_static_block');

                $this->form_validation->set_rules('block', 'Блок', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('alias', 'Алиас', 'required|xss_clean');
				$this->form_validation->set_rules('text', 'Код страницы', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_block = (int) $this->input->post('block');
				$update = array(
					'block_title' => $this->input->post('title'),
					'block_alias' => $this->input->post('alias'),
					'block_text' => $this->input->post('text'),
					'is_page' => $this->input->post('is_page') ? 1 : 0,
					'is_landing' => $this->input->post('is_landing') ? 1 : 0
				);

				$breadcrumbs = $this->input->post('breadcrumbs');
				$filtered_breadcrumbs = array();
				if(is_array($breadcrumbs)){
					foreach ($breadcrumbs as $bread_key => $bread) {
						if(!empty($bread['title']) && !empty($bread['url'])){
							$filtered_breadcrumbs[$bread_key] = array(
								'link_title' => $bread['title'],
								'link_url' => $bread['url']
							);
						}
					}
				}

				$update['block_breadcrumbs'] = json_encode($filtered_breadcrumbs);

				$this->static_block->handler_update($id_block, $update);

				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_static_block');

                $this->form_validation->set_rules('block', 'Блок', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_block = (int) $this->input->post('block');
				$block = $this->static_block->handler_get($id_block);
				if(empty($block)){
					jsonResponse('Блок не существует.');
				}

				$this->static_block->handler_delete($id_block);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_static_block');

				$params = array(
					'limit' 	=> (int) $this->input->post('iDisplayLength'),
					'start' 	=> (int) $this->input->post('iDisplayStart')
				);

				$params['sort_by'] = flat_dt_ordering($_POST, array(
					'dt_id' 	=> 'id_block',
					'dt_name' 	=> 'block_title'
				));

				$records = $this->static_block->handler_get_all($params);
        		$records_total = $this->static_block->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){			
						return array(
							'dt_id'			=> $record['id_block'],
							'dt_name'		=> $record['block_title'],
							'dt_alias'		=> $record['block_alias'],
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/static_block/popup/edit/' . $record['id_block']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить блок?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-block="'.$record['id_block'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
