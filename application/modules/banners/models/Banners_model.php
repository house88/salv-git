<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Banners_model extends CI_Model{

	var $banners = "banners";

	function __construct(){
		parent::__construct();
	}

	function set_banner($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->banners, $data);
		return $this->db->insert_id();
	}

	function update_banner($id_banner, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_banner', $id_banner);
		$this->db->update($this->banners, $data);
	}

	function delete_banner($id_banner){
		$this->db->where('id_banner', $id_banner);
		$this->db->delete($this->banners);
	}

	function handler_get($id_banner){
		$this->db->where('id_banner', $id_banner);
		return $this->db->get($this->banners)->row_array();
	}

	function get_banner_by_name($banner_name){
		$this->db->where('banner_name', $banner_name);
		return $this->db->get($this->banners)->row_array();
	}

	function get_banner_by_tocken($banner_tocken){
		$this->db->where('banner_tocken', $banner_tocken);
		return $this->db->get($this->banners)->row_array();
	}

	function handler_get_by($conditions = array()){
		extract($conditions);

		if (isset($id_banner)) {
			$this->db->where('id_banner', $id_banner);
		}

		if (isset($banner_name)) {
			$this->db->where('banner_name', $banner_name);
		}

		if (isset($banner_tocken)) {
			$this->db->where('banner_tocken', $banner_tocken);
		}

		return $this->db->get($this->banners)->row_array();
	}

	function handler_get_all($conditions = array()){
		$order_by = " id_banner ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if(!empty($multi_order_by)){
				$order_by = implode(',', $multi_order_by);
			}
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->banners)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

		return $this->db->count_all_results($this->banners);
	}
}
