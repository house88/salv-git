<div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($delivery) ? 'Редактировать' : 'Добавить';?> метод доставки
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($delivery) ? $delivery['delivery_title'] : '';?>">
                        </div>
                    </div>
                    <?php $price_options = !empty($delivery) ? json_decode($delivery['delivery_price_options'], true) : array();?>
                    <?php foreach($currencies as $currency){?>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label>Цена, <?php echo $currency['currency_symbol'];?></label>
                                <input class="form-control form-control-sm rounded-0" placeholder="0.0" name="price[<?php echo $currency['currency_code'];?>]" value="<?php echo $price_options[$currency['currency_code']]['price'] ?? 0;?>">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label>Доплата за крупногабаритный товар</label>
                                <input class="form-control form-control-sm rounded-0" placeholder="0.0" name="price_for_oversize[<?php echo $currency['currency_code'];?>]" value="<?php echo $price_options[$currency['currency_code']]['price_for_oversize'] ?? 0;?>">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label>Бесплатно от</label>
                                <input class="form-control form-control-sm rounded-0" placeholder="0.0" name="free_price[<?php echo $currency['currency_code'];?>]" value="<?php echo $price_options[$currency['currency_code']]['price_free'] ?? 0;?>">
                            </div>
                        </div>
                    <?php }?>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Оплата</label>
                            <select class="form-control form-control-sm rounded-0 bs-select-element" data-style="btn btn-default btn-sm btn-flat" data-title="Привязанные мотоды оплаты" multiple>
                                <?php $payment_options = arrayByKey(Modules::run('payment/_get_all'), 'id_payment');?>
                                <?php foreach($payment_options as $payment_option){?>
                                    <option value="<?php echo $payment_option['id_payment'];?>" <?php echo get_choice('selected', !empty($payments_selected) && in_array($payment_option['id_payment'], $payments_selected))?>><?php echo $payment_option['payment_title'];?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div id="js-relation-text">
                            <?php if(!empty($payments_relations)){?>
                                <?php foreach($payments_relations as $payment_relation){?>
                                    <?php if(!isset($payment_options[$payment_relation['id_payment']])){ continue; }?>

                                    <fieldset id="js-relation-delivery-<?php echo $payment_relation['id_payment'];?>">
                                        <legend>
                                            <span><?php echo $payment_options[$payment_relation['id_payment']]['payment_title'];?></span>
                                            <div class="fieldset-tools">
                                                <a href="#" class="btn btn-tool call-function" data-callback="delete_relation_payment" data-payment="<?php echo $payment_relation['id_payment'];?>" tooltip="Удалить">
                                                    <i class="fad fa-times"></i> 
                                                    <span class="d-none d-md-inline">Удалить</span>
                                                </a>
                                            </div>
                                        </legend>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-sm rounded-0" placeholder="Текст для заказа" name="payments[<?php echo $payment_relation['id_payment'];?>][text]"><?php echo $payment_relation['relation_text'];?></textarea>
                                        </div>
                                    </fieldset>
                                <?php }?>
                            <?php }?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Краткое описание</label>
                            <textarea class="form-control form-control-sm rounded-0" name="stext"><?php echo !empty($delivery) ? $delivery['delivery_sdescription'] : '';?></textarea>
                            <p class="help-block">Не должно содержать более 250 символов.</p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Текст</label>
                            <textarea class="description" name="description"><?php echo !empty($delivery) ? $delivery['delivery_description'] : '';?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="active" type="checkbox" id="js-checkbox-delivery-active" <?php echo set_checkbox('active', 1, !empty($delivery) ? (int) $delivery['delivery_active'] === 1 : false);?>>
                    <label for="js-checkbox-delivery-active">
                        Активный
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($delivery)){?>
                        <input type="hidden" name="delivery" value="<?php echo $delivery['id_delivery'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/template" id="js-relation-payment-template">
    <fieldset id="js-relation-payment-{{id_payment}}">
        <legend>
            <span>{{payment_title}}</span>
            <div class="fieldset-tools">
                <a href="#" class="btn btn-tool call-function" data-callback="delete_relation_payment" data-payment="{{id_payment}}" tooltip="Удалить">
                    <i class="fad fa-times"></i> 
                    <span class="d-none d-md-inline">Удалить</span>
                </a>
            </div>
        </legend>
        <div class="form-group">
            <textarea class="form-control form-control-sm rounded-0" placeholder="Текст для заказа" name="payments[{{id_payment}}][text]"></textarea>
        </div>
    </fieldset>
</script>

<script>
    $(".bs-select-element").selectpicker()
    .on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        var option = $(e.target[clickedIndex]);
        if(isSelected){
            var template = $('#js-relation-payment-template').text();
            var context = {
                'payment_title': option.text(),
                'id_payment': option.val()
            };

			for (var key in context) {
				template = template.replace(new RegExp('{{' + key + '}}', 'g'), context[key]);
            }
            
            $('#js-relation-text').append(template);
        } else{
            $('fieldset#js-relation-payment-' + option.val()).remove();
        }
    });

    var delete_relation_payment = function(element){        
        $('.bs-select-element').find('[value="'+ $(element).data('payment') +'"]').prop('selected', false);
        $('.bs-select-element').selectpicker('refresh');
        $(element).closest('fieldset').remove();
    }
    
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        tinyMCE.triggerSave();
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/delivery/ajax_operations/<?php echo !empty($delivery) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });

    initTinyMce();
</script>
