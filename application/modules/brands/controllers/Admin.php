<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/brands/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'brands';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Brands_model", "brands");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_brands', '/admin');

		$this->data['page_header'] = 'Бренды';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'brands_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_brands');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_brands');

				$id_brand = (int) $this->uri->segment(5);
				$this->data['brand'] = $this->brands->handler_get($id_brand);
				if(empty($this->data['brand'])){
					jsonResponse('Бренд не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_brands');

				$this->form_validation->set_rules('photo', 'Логотип', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description', 'Текст', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/brands/'.$remove_photo);
					}
				}

				$id_brand = $this->brands->handler_insert(array(
					'brand_title' => $this->input->post('title'),
					'brand_description' => $this->input->post('description'),					
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'brand_photo' => $this->input->post('photo'),
					'visible' => $this->input->post('visible') ? 1 : 0
				));

				$this->brands->handler_update($id_brand, array(
					'brand_url' => strForURL($this->input->post('title', true).'-'.$id_brand)
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_brands');

                $this->form_validation->set_rules('brand', 'Бренд', 'required|xss_clean');
				$this->form_validation->set_rules('photo', 'Логотип', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description', 'Текст', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_brand = (int)$this->input->post('brand');
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/brands/'.$remove_photo);
					}
				}

				$this->brands->handler_update($id_brand, array(
					'brand_title' => $this->input->post('title', true),
					'brand_description' => $this->input->post('description'),
					'brand_url' => strForURL($this->input->post('title', true).'-'.$id_brand),
					'mk' => $this->input->post('mk', true),
					'md' => $this->input->post('md', true),
					'brand_photo' => $this->input->post('photo', true),
					'visible' => $this->input->post('visible') ? 1 : 0
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_brands');

				$this->form_validation->set_rules('brand', 'Бренд', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_brand = (int)$this->input->post('brand');
				$brand = $this->brands->handler_get($id_brand);
				if(empty($brand)){
					jsonResponse('Бренд не существует.');
				}

				@unlink('files/brands/'.$brand['brand_photo']);
				$this->brands->handler_delete($id_brand);

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_brands');

				$this->form_validation->set_rules('brand', 'Бренд', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_brand = (int) $this->input->post('brand');
				$brand = $this->brands->handler_get($id_brand);
				if(empty($brand)){
					jsonResponse('Бренд не существует.');
				}

				$this->brands->handler_update($id_brand, array(
					'visible' => (int) !((int) $brand['visible'] === 1)
				));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_brands');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'id_brand-desc' )
				);

				$params['sort_by'] = flat_dt_ordering($_POST, array(
					'dt_name'     => 'brand_title'
				));

				$records = $this->brands->handler_get_all($params);
				$records_total = $this->brands->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){			
						return array(
							'dt_photo'		=> '<img src="'.site_url(getImage('files/brands/'.$record['brand_photo'])).'" alt="'.clean_output($record['brand_title']).'" class="img-thumbnail custom-width-50">',
							'dt_name'		=> $record['brand_title'],
							'dt_url' 		=> '<a href="'.site_url('brand/'.$record['brand_url']).'">'.site_url('brand/'.$record['brand_url']).'</a>',
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['visible'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-brand="'.$record['id_brand'].'"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/brands/popup/edit/' . $record['id_brand']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить бренд?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-brand="'.$record['id_brand'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
			case "upload_photo":
				checkPermisionAjax('manage_brands');
				
				$path = 'files/brands';
				create_dir($path);

				$config['upload_path'] 		= FCPATH . $path;
				$config['allowed_types'] 	= 'jpg|jpeg|png|gif';
				$config['file_name'] 		= uniqid();
				$config['min_width']		= '200';
				$config['min_height']		= '200';
				$config['max_size']			= '2000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();

				jsonResponse('', 'success', array("file" => (object) array(
					'filename' => $data['file_name']
				)));
			break;
		}
	}
}
