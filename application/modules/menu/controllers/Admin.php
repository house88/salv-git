<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
        
		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/menu/';
		$this->active_menu = 'special_pages';
		$this->active_submenu = 'menu';

		$this->data = array();

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->load->model("menu/Menu_model", "menu");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

    function index(){
		checkPermision('manage_site_menu', '/admin');
        
		$this->data['page_header'] = 'Меню';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
    }

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_site_menu');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_site_menu');

				$id_menu = (int) $this->uri->segment(5);
                $this->data['menu_item'] = $this->menu->handler_get($id_menu);
				if(empty($this->data['menu_item'])){
					jsonResponse('Меню не найдено.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

    function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $option = $this->uri->segment(4);
        switch($option){
            case 'add':
				checkPermisionAjaxDT('manage_site_menu');

                $this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$menu_elements = $this->input->post('menu_elements');
				$valid_menu_elements = array();
				if(!empty($menu_elements)){
					foreach($menu_elements as $menu_element){
						if(empty($menu_element['title'])){
							jsonResponse('Названия елементов меню не могум быть пустыми.');
							break;
                        }
                        
						if(empty($menu_element['link'])){
							jsonResponse('Ссылки елементов меню не могум быть пустыми.');
							break;
                        }
                        
						$valid_menu_elements[] = $menu_element;
					}
                }
                
                $this->menu->handler_insert(array(
                    'menu_title' => $this->input->post('title'),
                    'menu_elements' => json_encode($valid_menu_elements),
                    'menu_visible' => $this->input->post('menu_visible') ? 1: 0
                ));

                jsonResponse('Сохранено.', 'success');
            break;
            case 'edit':
				checkPermisionAjaxDT('manage_site_menu');

                $this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
                $this->form_validation->set_rules('menu', 'Раздел', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_menu = (int) $this->input->post('menu');
				$menu_elements = $this->input->post('menu_elements');
				$valid_menu_elements = array();
				if(!empty($menu_elements)){
					foreach($menu_elements as $menu_element){
						if(empty($menu_element['title'])){
							jsonResponse('Названия елементов меню не могум быть пустыми.');
							break;
                        }
                        
						if(empty($menu_element['link'])){
							jsonResponse('Ссылки елементов меню не могум быть пустыми.');
							break;
                        }
                        
						$valid_menu_elements[] = $menu_element;
					}
                }
                
                $this->menu->handler_update($id_menu, array(
                    'menu_title' => $this->input->post('title'),
					'menu_elements' => json_encode($valid_menu_elements),
                    'menu_visible' => $this->input->post('menu_visible') ? 1: 0
                ));

                jsonResponse('Сохранено.', 'success');
            break;
            case 'delete':
				checkPermisionAjaxDT('manage_site_menu');

                $this->form_validation->set_rules('menu', 'Раздел', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_menu = (int) $this->input->post('menu');
                $this->menu->handler_delete($id_menu);

                jsonResponse('Операция прошла успешно.', 'success');
            break;
            case 'change_status':
				checkPermisionAjaxDT('manage_site_menu');

                $this->form_validation->set_rules('menu', 'Раздел', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_menu = (int) $this->input->post('menu');
        		$menu_item = $this->menu->handler_get($id_menu);
				if(empty($menu_item)){
					jsonResponse('Раздел не найден.');
                }
                
                $this->menu->handler_update($id_menu, array(
                    'menu_visible' => (int) !((int) $menu_item['menu_visible'] === 1)
                ));

                jsonResponse('Сохранено.', 'success');
            break;
			case 'list':
				checkPermisionAjaxDT('manage_site_menu');

				$records = $this->menu->handler_get_all();
				$records_total = $this->menu->handler_get_count();

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){			
						return array(
							'dt_name'		=> $record['menu_title'],
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['menu_visible'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-menu="'.$record['id_menu'].'"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/menu/popup/edit/' . $record['id_menu']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить раздел <strong>'. clean_output($record['menu_title']) .'</strong>?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-menu="'.$record['id_menu'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
        }
    }
}
