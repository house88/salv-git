<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		
        $this->data = array();
        $this->breadcrumbs = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		return_404($this->data);
	}
	
	function account(){
		if(!$this->lauth->logged_in()){
			redirect('/');
		}

		$this->breadcrumbs[] = array(
			'title' => 'Личный кабинет',
			'link' => base_url('account')
		);

		$this->data['stitle'] = 'Личный кабинет';

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['main_content'] = 'user/dashboard_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
		
	}

	function account_settings(){
		if(!$this->lauth->logged_in()){
			redirect('/');
		}
		
		$this->breadcrumbs[] = array(
			'title' => 'Личный кабинет',
			'link' => base_url('account')
		);

		$this->breadcrumbs[] = array(
			'title' => 'Личные данные',
			'link' => base_url('account/settings')
		);

		$this->data['stitle'] = 'Личные данные';

		$this->data['user'] = $this->lauth->user_data();
		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['main_content'] = 'user/settings_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'update':
				if(!$this->lauth->logged_in()){
					jsonResponse('Вы не авторизированы.');
				}

				$user = $this->lauth->user_data();
				$config = array(
					array(
						'field' => 'name',
						'label' => 'Nume/Prenume',
						'rules' => 'required|trim|xss_clean|max_length[50]',
					),
					array(
						'field' => 'phone',
						'label' => 'Telefon',
						'rules' => 'xss_clean|max_length[50]',
					)
				);

				if($user->user_email != $this->input->post('email')){
					$config[] = array(
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email|is_unique[users.user_email]|xss_clean',
					);
				} else{
					$config[] = array(
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|valid_email|xss_clean',
					);
				}

				if($this->input->post('password')){
					$config[] = array(
						'field' => 'password',
						'label' => 'Parola',
						'rules' => 'trim|required|xss_clean|min_length[6]',
					);
				}

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$update = array(
					'user_login' => $this->input->post('name',true),
					'user_nicename' => $this->input->post('name',true),
					'user_email' => $this->input->post('email',true),
					'user_phone' => $this->input->post('phone',true),
					'user_phone_display' => NULL
				);

				if($this->input->post('phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('phone'));
					if(true === $phoneFormatResult['is_valid']){
						$update['user_phone_display'] = $phoneFormatResult['formated'];
					}
				}

				if($this->input->post('password')){
					$update['user_pass'] = $this->lauth->hash_password($this->input->post('password',true));
				}
				
				$this->auth_model->handler_update($user->id, $update);

				jsonResponse('Datele au fost salvate.', 'success');
			break;
			case 'change_password':
				if(!$this->lauth->logged_in()){
					jsonResponse('Вы не авторизированы.');
				}

				$user = $this->lauth->user_data();
				$config = array(
					array(
						'field' => 'old_password',
						'label' => 'Нынешний пароль',
						'rules' => 'required|trim|xss_clean',
					),
					array(
						'field' => 'password',
						'label' => 'Новый пароль',
						'rules' => 'required|trim|xss_clean|min_length[6]',
					),
					array(
						'field' => 'confirm_password',
						'label' => 'Повторите новый пароль',
						'rules' => 'required|trim|xss_clean|matches[password]',
					)
				);

				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$old_password = $this->lauth->hash_password($this->input->post('old_password', true));
				if($old_password != $user->user_pass){
					jsonResponse('Нынешний пароль верный.');
				}

				$update = array(
					'reset_key' => md5($user->user_email.uniqid().'@ccreset'),
					'user_pass' => $this->lauth->hash_password($this->input->post('password',true))
				);
				$this->auth_model->handler_update($user->id, $update);

				jsonResponse('Пароль был изменен.', 'success');
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function _get($id_user){
		return $this->user_model->handler_get((int) $id_user);
	}

	function _get_all($params = array()){
		return $this->user_model->handler_get_all((array) $params);
	}

	function _update($id_user, $params = array()){
		return $this->auth_model->handler_update($id_user, (array) $params);
	}
}
