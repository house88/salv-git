<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Icons extends MX_Controller {
	public function index()
	{
		$homepage = file_get_contents(base_url('theme/css/ca-icons/demo.html'));
		$homepage = str_replace("demo-files/demo", base_url('theme/css/ca-icons/demo-files/demo'), $homepage);
		$homepage = str_replace("style.css", base_url('theme/css/ca-icons/style.css'), $homepage);
		echo $homepage;
	}
}
