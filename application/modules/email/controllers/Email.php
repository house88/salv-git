<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Email extends MX_Controller{
	function __construct(){
        parent::__construct();
        
        
		$this->data = array();
        $this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
        
        $this->load->library('email');
        
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.yandex.com';
        $config['smtp_port'] = 465;
        $config['smtp_crypto'] = 'ssl';
        $config['smtp_user'] = 'noreply@salv.md';
        $config['smtp_pass'] = 'leoncernjmjkewqn';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $this->email->initialize($config);
    }
    
    function send($params = array()){
        if(empty($params)){
            return;
        }
    
        $this->email->from('noreply@salv.md', $this->data['settings']['default_title']['setting_value']);
        $this->email->to($params['to']);					
        $this->email->subject($params['subject']);
        $this->email->message($this->load->view('email_templates/email_tpl', $params['email_data'], true));
        $this->email->set_mailtype("html");
        $this->email->send();

        $this->email->clear();
    }

    function test(){
        $order = Modules::run('orders/_get', 1);
        $email_data = array(
            'order' => $order,
            'title' => 'Ati plasat comanda cu numarul '.orderNumber(1).' pe salv.md',
            'changes' => [],
            'email_content' => 'order_detail_admin_tpl'
        );

        $this->send(array(
            'to' => 'cravciucandy@gmail.com',
            'subject' => 'Изминения в заказе номер '.orderNumber(1).' на сайте salv.md',
            'email_data' => $email_data
        ));
    }
}
