<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dynamic_prices extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/items/dynamic_prices/';
		$this->active_menu = 'catalog';
		$this->active_submenu = 'dynamic_prices';

		$this->data = array();

		$this->load->model("Prices_model", "prices");
		$this->load->model("Dynamic_prices_model", "dynamic_prices");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		checkPermision('manage_price_variants', '/admin');

		$this->data['page_header'] = 'Динамические цены';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_price_variants');

				$this->data['currencies'] = Modules::run('currency/_get_all', array(
					'active' => 1
				));
				
				$this->data['prices_variants'] = $this->prices->handler_get_all(array(
					'active' => 1
				));

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_price_variants');

				$id_dynamic_price = (int) $this->uri->segment(5);
				$this->data['dynamic_price'] = $this->dynamic_prices->handler_get($id_dynamic_price);
				if(empty($this->data['dynamic_price'])){
					jsonResponse('Ценовая категория не найдена.');
				}

				$this->data['currencies'] = Modules::run('currency/_get_all', array(
					'active' => 1
				));
				
				$this->data['prices_variants'] = $this->prices->handler_get_all(array('active' => 1));

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_price_variants');

				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[30]');
				$this->form_validation->set_rules('price_variant', 'Ценовая категория', 'required|xss_clean');
				$this->form_validation->set_rules('percent', '%', 'required|xss_clean');
				$this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'id_price_variant' => (int) $this->input->post('price_variant'),
					'dinamic_price_title' => $this->input->post('title'),
					'dinamic_price_add' => $this->input->post('percent'),
					'id_price_currency' => (int)$this->input->post('currency'),
					'dinamic_price_active' => $this->input->post('active') ? 1 : 0
				);

				$this->dynamic_prices->handler_insert($insert);

				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_price_variants');

				$this->form_validation->set_rules('price', 'Динамическая цена', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[30]');
				$this->form_validation->set_rules('price_variant', 'Ценовая категория', 'required|xss_clean');
				$this->form_validation->set_rules('percent', '%', 'required|xss_clean');
				$this->form_validation->set_rules('currency', 'Валюта', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_dynamic_price = (int) $this->input->post('price');
				$dynamic_price = $this->dynamic_prices->handler_get($id_dynamic_price);
				if(empty($dynamic_price)){
					jsonResponse('Ценовая категория не найдена.');
				}

				$this->dynamic_prices->handler_update($id_dynamic_price, array(
					'id_price_variant' => (int) $this->input->post('price_variant'),
					'dinamic_price_title' => $this->input->post('title'),
					'dinamic_price_add' => $this->input->post('percent'),
					'id_price_currency' => (int)$this->input->post('currency'),
					'dinamic_price_active' => $this->input->post('active') ? 1 : 0
				));

				jsonResponse('Сохранено.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_price_variants');

				$this->form_validation->set_rules('price', 'Ценовая категория', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_dynamic_price = (int) $this->input->post('price');
				$dynamic_price = $this->dynamic_prices->handler_get($id_dynamic_price);
				if(empty($dynamic_price)){
					jsonResponse('Ценовая категория не найдена.');
				}

				$this->dynamic_prices->handler_update($id_dynamic_price, array(
					'dinamic_price_active' => (int) $dynamic_price['dinamic_price_active'] === 1 ? 0 : 1
				));

				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_price_variants');
				
				$this->form_validation->set_rules('price', 'Ценовая категория', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_dynamic_price = (int) $this->input->post('price');
				$dynamic_price = $this->dynamic_prices->handler_get($id_dynamic_price);
				if(empty($dynamic_price)){
					jsonResponse('Ценовая категория не найдена.');
				}

				$this->dynamic_prices->handler_delete($id_dynamic_price);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list':
				checkPermisionAjaxDT('manage_price_variants');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart')
				);
		
				$records = $this->dynamic_prices->handler_get_all($params);
				$records_total = $this->dynamic_prices->handler_get_count($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_name'		=>  $record['dinamic_price_title'],
							'dt_category'	=>  $record['price_variant_title'],
							'dt_currency'	=>  $record['currency_symbol'],
							'dt_status'		=> '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['dinamic_price_active'] === 1, 'fad fa-minus-square text-gray') .' call-function" data-callback="change_status" data-price="'.$record['id_dynamic_price'].'"></a>',
							'dt_actions'	=> '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/dynamic_prices/popup/edit/' . $record['id_dynamic_price']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить тип цены?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-price="'.$record['id_dynamic_price'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
