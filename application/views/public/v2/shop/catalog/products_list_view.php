<?php foreach($products as $item){?>
    <!-- PRODUCT - START -->
    <div class="col-6 col-md-4">
		  <?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $item));?>
    </div>
    <!-- PRODUCT - END -->
<?php }?>