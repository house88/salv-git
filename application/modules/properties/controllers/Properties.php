<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Properties extends MX_Controller{
	
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("categories/Categories_model", "categories");
		$this->load->model("Properties_model", "properties");

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		return_404($this->data);
	}

	function _get_category_properties($params = array()){
		extract($params);
		
		if(!isset($id_category)){
			return;
		}

		$condition['category_children'] = $this->properties->_get_category_children((int) $id_category);
		$this->data['properties'] = $this->properties->handler_get_all($condition);
		if(empty($this->data['properties'])){
			return;
		}

		$this->data['properties_values_counters'] = $this->properties->handler_get_items_properties_values_counters($condition);
		
		if(!empty($items_list)){
			$condition['items_list'] = $items_list;
		}
		
		$this->data['available_properties_values_counters'] = $this->properties->handler_get_items_properties_values_counters($condition);
		return $this->load->view($this->theme->public_view('shop/catalog/properties_view'), $this->data, true);
	}

    function _get_product_properties($params = array()){
        extract($params);

        if(empty($id_item)){
            return false;
        }
        
        $this->data['item_properties_values'] = $this->properties->handler_get_items_properties_values($id_item);		
        if(!empty($this->data['item_properties_values'])){
            $prop_list = array();
            foreach ($this->data['item_properties_values'] as $property) {
                $prop_list[$property['id_property']] = $property['id_property'];
            }

            if(!empty($prop_list)){
                $this->data['properties'] = arrayByKey($this->properties->handler_get_all(array('properties_list' => implode(',', $prop_list), 'on_item' => 1, 'status_property' => 1)), 'id_property');
            }

            
            foreach($this->data['item_properties_values'] as $item_property_value){
				if(array_key_exists($item_property_value['id_property'], $this->data['properties'])){
					if(in_array($this->data['properties'][$item_property_value['id_property']]['type_property'], array('select', 'multiselect'))){
						$available_values = json_decode($this->data['properties'][$item_property_value['id_property']]['property_values'], true);
						foreach ($available_values as $available_value) {
							if($available_value['id_value'] == $item_property_value['value']){
								$this->data['properties'][$item_property_value['id_property']]['item_values'][] = $available_value['value'];
							}
						}
					}
					
					if($this->data['properties'][$item_property_value['id_property']]['type_property'] == 'range'){
						$this->data['properties'][$item_property_value['id_property']]['item_values'][] = $item_property_value['value'];
					}
					
					if($this->data['properties'][$item_property_value['id_property']]['type_property'] == 'simple'){
						$this->data['properties'][$item_property_value['id_property']]['item_values'][] = $item_property_value['value'];
					}
				}
            }
        }

		return $this->load->view($this->theme->public_view('shop/product/properties_view'), $this->data, true);
    }
}
