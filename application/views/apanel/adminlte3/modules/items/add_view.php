<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                Добавить товар
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="add_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <select name="id_category" class="form-control rounded-0 bs-select-categories" data-live-search="true" data-style="btn btn-default btn-sm btn-flat" data-title="Выберите категорию" data-show-content="false">
                                <option value="">Выберите категорию</option>
                                <?php if(!empty($categories)){?>
                                    <?php echo $categories;?>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Добавить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(".bs-select-categories").selectpicker();
    
    var add_form = $('#add_form');
    add_form.submit(function () {
        var fdata = add_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/items/ajax_operations/add',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    window.location.replace(resp.link);
                }
            }
        });
        return false;
    });
</script>
