<div class="row products-list products_block" id="products">
	<?php if(!empty($products)){?>
		<?php $this->load->view($this->theme->public_view('shop/catalog/products_list_view'));?>
	<?php } else{?>
		<div class="col-12">
			<div class="jumbotron jumbotron-white">
				<h1 class="display-4">Ooops!</h1>
				<p class="lead">Ne pare rău dar nu am gasit nici un produs dupa filtrele aplicate.</p>
				<hr class="my-4">
				<p>Vă rugăm sa vă asigurați că filtrele aplicate sunt corecte ori sa alegeți alte filtre și să repetați căutarea.</p>
			</div>
		</div>
	<?php }?>
</div>
<div class="row">
	<div class="col-12 pl-0 pr-0">
		<div class="pagination-wrapper">
			<?php if($pagination != ''){?>
					<?php echo $pagination;?>
			<?php }?>	
		</div>
	</div>
</div>