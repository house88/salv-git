<article class="cart-content">
    <?php if(!empty($items)){?>
        <form class="basket_form">
            <div class="products-order shopping-cart">
                <?php 
                    $total_price = 0;
                    $total_cashback = $this->lauth->user_cashback_balance();
                    $total_bonus = 0;
                    $can_apply_bonus = false;
                    $default_payment_method_id = 0;
                    $default_delivery_method_id = 0;
                ?>
                <?php foreach($items as $item){?>
                    <?php $price = getPrice($item);?>
                    <div class="row row-eq-height cart_product-wr">
                        <div class="col-xs-12 col-sm-2 col-md-1">
                            <img src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$item['item_photo']));?>" alt="" class="img-responsive">
                        </div>
                        <div class="col-xs-12 col-sm-10 col-md-7">
                            <a class="fs-13" href="<?php echo base_url('products/'.$item['item_url']);?>"><?php echo clean_output($item['item_title']);?></a>
                        </div>
                        <div class="col-sm-12 visible-sm hidden-md mb-15"></div>
                        <div class="col-xs-10 col-sm-9 col-sm-offset-2 col-md-3 col-md-offset-0 text-center">
                            <div class="w-45pr pull-left">
                                <div class="form-group product-quantity">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm pl-5 pr-5 call-function" data-callback="change_qty" data-action="minus" data-type="basket" data-item="<?php echo $item['id_item'];?>">
                                                <span class="ca-icon ca-icon_minus fs-10_i"></span>
                                            </button>
                                        </span>
                                        <input class="form-control input-sm text-center item_quantity" onchange="change_qty_input(this);" data-item="<?php echo $item['id_item'];?>" type="text" name="item_<?php echo $item['id_item'];?>_quantity" value="<?php echo $item['item_cart_qty'];?>"/>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-sm pl-5 pr-5 call-function" data-callback="change_qty" data-action="plus" data-type="basket" data-item="<?php echo $item['id_item'];?>">
                                                <span class="ca-icon ca-icon_plus fs-10_i"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="w-10pr pull-left h-30 lh-30">x</div>
                            <div class="w-45pr pull-left h-30 lh-30 text-left">
                                <strong><span class="item_price"><?php echo numberFormat($price['display_price'], $this->lauth->group_view_decimals());?></span> <span class="currency"><?php echo $price['currency_symbol'];?></span></strong>
                                <input type="hidden" name="item_price" value="<?php echo numberFormat($price['display_price'], $this->lauth->group_view_decimals());?>">
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-1 col-md-1 text-right h-30">
                            <a href="#" class="btn btn-danger btn-xs mt-4 call-function" data-callback="remove_from_big_cart" data-item="<?php echo $item['id_item'];?>">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                        <div class="col-xs-12"><hr class="mt-15 mb-15"></div>
                    </div>
                    <!-- CART ITEM - END -->
                    <?php 
                        $total_price += $price['display_price'] * $item['item_cart_qty'];
                        if($this->lauth->group_view_cashback()){
                            $total_cashback += $item['item_cashback_price'] * $item['item_cart_qty'];
                            if($item['item_cashback_price'] == 0){
                                $total_bonus += $price['display_price'] * $item['item_cart_qty'];
                                $can_apply_bonus = true;
                            }
                        }
                    ?>
                <?php }?>
                <?php $total_bonus_price = ($total_cashback <= $total_bonus)?$total_cashback:$total_bonus;?>
                <?php $cart_cashback = $total_cashback - $this->lauth->user_cashback_balance();?>
            </div>
            <div class="row cart-blocks-order">
                <div class="order-2 col-sm-7">
                    <div class="row">
                        <fieldset>
                            <legend class="scheduler-border">Контакты</legend>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputName">Имя</label>
                                    <input name="user_name" type="text" class="form-control" id="inputName" placeholder="Имя" value="<?php echo ((!empty($user_info))?$user_info->user_nicename:'');?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input name="user_email" type="text" class="form-control" id="inputEmail" placeholder="Email" value="<?php echo ((!empty($user_info))?$user_info->user_email:'');?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputPhone">Телефон</label>
                                    <input name="user_phone" type="text" class="form-control" id="inputPhone" placeholder="Телефон" value="<?php echo ((!empty($user_info))?$user_info->user_phone:'');?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputAddress">Адрес</label>
                                    <input name="user_address" type="text" class="form-control" id="inputAddress" placeholder="Адрес" value="<?php echo ((!empty($user_info))?$user_info->user_address:'');?>">
                                </div>
                            </div>                    
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="inputComment">Комментарий к заказу</label>
                                    <textarea name="user_comment" style="resize:none;" rows="5" class="form-control" id="inputComment" placeholder="Коментарий"></textarea>
                                </div>
                            </div>                    
                        </fieldset>
                        <fieldset>
                            <legend class="scheduler-border">Способ доставки</legend>
                            <div class="col-xs-12">
                                <?php if(!empty($delivery_options)){?>
                                    <?php foreach($delivery_options as $delivery_option){?>
                                        <?php
                                            $delivery_price_options = json_decode($delivery_option['delivery_price_options'], true);
                                            
                                            if(!isset($delivery_price_options[$price['currency_code']])){
                                                continue;
                                            }

                                            $delivery_price_option = $delivery_price_options[$price['currency_code']];
                                            $delivery_price = $total_price >= $delivery_price_option['price_free'] ? 0 : $delivery_price_option['price'];
                                            if(!empty($delivery_price_option['price_for_oversize'])){
                                                $delivery_price += $delivery_price_option['price_for_oversize'] * $count_oversize_items;
                                            }
                                        ?>
                                        <div class="form-group mt-10">
                                            <label class="cur-pointer">
                                                <input type="radio" name="delivery_method" value="<?php echo $delivery_option['id_delivery'];?>" data-delivery-price="<?php echo $delivery_price;?>" data-payment-available="<?php echo !empty($delivery_payment_methods[$delivery_option['id_delivery']]) ? implode(',', $delivery_payment_methods[$delivery_option['id_delivery']]) : '';?>"> 
                                                <span class="lh-22 vam">
                                                    <?php echo $delivery_option['delivery_title'];?> <?php if($delivery_price > 0){?><small>( +<?php echo numberFormat($delivery_price);?> <?php echo $price['currency_symbol'];?> )</small><?php }?>
                                                </span>
                                            </label>
                                        </div>
                                    <?php }?>
                                <?php }?>
                                <p class="text-right fs-12"><a href="<?php echo base_url('spage/delivery-menu');?>">Подробнее о доставке <span class="ca-icon ca-icon_arrow-right fs-8"></span></a></p>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend class="scheduler-border">Способ оплаты</legend>
                            <div class="col-xs-12">
                                <?php if(!empty($payment_options)){?>
                                    <?php foreach($payment_options as $payment_option){?>
                                        <?php
                                            $payment_add_options = json_decode($payment_option['payment_add_options'], true);
                                            $payment_add_options_percent = (isset($payment_add_options[$price['currency_code']]))?$payment_add_options[$price['currency_code']]:0;
                                        ?>
                                        <div class="form-group mt-10">
                                            <label class="cur-pointer">
                                                <input type="radio" class="input_payment_option" name="payment_method" value="<?php echo $payment_option['id_payment'];?>" data-delivery-available="<?php echo !empty($payment_delivery_methods[$payment_option['id_payment']]) ? implode(',', $payment_delivery_methods[$payment_option['id_payment']]) : '';?>" data-payment-add="<?php echo $payment_add_options_percent;?>"> 
                                                <span>
                                                    <?php echo $payment_option['payment_title'];?>
                                                    <?php if($payment_add_options_percent > 0){?><small>( +<?php echo numberFormat($total_price*$payment_add_options_percent/100);?> <?php echo $price['currency_symbol'];?> )</small><?php }?>
                                                </span>
                                            </label>
                                        </div>
                                    <?php }?>
                                <?php }?>
                                <div id="js-payment-delivery-relation-text"></div>
                                <p class="text-right fs-12"><a href="<?php echo base_url();?>payment">Подробнее о способах оплаты <span class="ca-icon ca-icon_arrow-right fs-8"></span></a></p>
                            </div>
                        </fieldset>
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-danger pull-right call-function" data-callback="create_order">Оформить заказ</button>
                        </div>
                    </div>
                </div>
                <div class="order-1 col-sm-5">
                    <div class="row">
                        <?php if($this->lauth->group_view_cashback() && $cart_cashback > 0){?>
                            <div class="col-xs-12 text-right">
                                <div class="cart-footer">
                                    <span class="cart-footer__text cashback-text"><sup>*</sup>Подарок на следующую покупку:</span>
                                    <span class="cart-footer__value">
                                        <span class="value cashback-text">
                                            + <?php echo numberFormat($cart_cashback, $this->lauth->group_view_decimals());?> лей
                                        </span>
                                    </span>
                                </div>                      
                            </div>                        
                        <?php }?>
                        <div class="col-xs-12 text-right">
                            <div class="cart-footer">
                                <span class="cart-footer__text">Доставка:</span>
                                <span class="cart-footer__value">
                                    <span class="value">
                                        <strong id="cart_price_delivery-wr">
                                            <span class="price"><?php echo numberFormat(0, $this->lauth->group_view_decimals());?></span>
                                            <span class="currency"><?php echo $price['currency_symbol'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right">
                            <div class="cart-footer">
                                <span class="cart-footer__text">Сумма заказа:</span>
                                <span class="cart-footer__value">
                                    <span class="value">
                                        <strong>
                                            <span class="final_order_price"><?php echo numberFormat($total_price, $this->lauth->group_view_decimals());?></span>                                
                                            <span class="currency"><?php echo $price['currency_symbol'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <?php if($can_apply_bonus && $total_bonus_price > 0){?>
                            <div class="col-xs-12 text-right">
                                <div class="cart-footer">
                                    <span class="cart-footer__text">Бонусные баллы:</span>
                                    <span class="cart-footer__value">
                                        <span class="value">
                                            <strong>
                                                - <span class="final_order_bonus"><?php echo numberFormat($total_bonus_price, $this->lauth->group_view_decimals());?></span>                                
                                                <span class="currency"><?php echo $price['currency_symbol'];?></span>
                                            </strong>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        <?php }?>
                        <div class="col-xs-12 text-right">
                            <div class="cart-footer cart-footer__border-top">
                                <span class="cart-footer__text">Итого к оплате:</span>
                                <span class="cart-footer__value">
                                    <span class="value">
                                        <strong>
                                            <span class="final_order_price_bonus"><?php echo numberFormat((numberFormat($total_price, $this->lauth->group_view_decimals()) - numberFormat($total_bonus_price, $this->lauth->group_view_decimals())), $this->lauth->group_view_decimals());?></span>                                
                                            <span class="currency"><?php echo $price['currency_symbol'];?></span>
                                        </strong>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <?php if($this->lauth->group_view_cashback() && $cart_cashback > 0){?>
                            <?php if(!$this->lauth->group_has_cashback()){?>
                                <div class="col-xs-12 text-right">
                                    <span class="cart-footer__info text-danger"><sup>*</sup>Чтобы воспользоваться остатком бонусных балов при следуюших покупках просим вас <a href="#" class="call-function" data-callback="show_auth_popup" data-target="#auth-modal" data-action=".login-form-link">авторизоваться</a> или <a href="#" class="call-function" data-callback="show_auth_popup" data-target="#auth-modal" data-action=".register-form-link">зарегистрироваться</a>!</span>
                                </div>                            
                            <?php } else{?>
                                <div class="col-xs-12 text-right">
                                    <span class="cart-footer__info text-primary"><sup>*</sup>Остаток бонусных балов попадет в личный кабинет и будет использован при следующих заказах.</span>
                                </div>
                            <?php }?>
                        <?php }?>
                    </div>
                </div>
            </div>
        </form>
        <div id="js-payment-form-component" style="display:none;"></div>
        
        <script>
            var order_price = floatval('<?php echo $total_price;?>');
            var bonus_price = floatval('<?php echo $total_bonus_price;?>');
            var total_cashback = floatval('<?php echo $total_cashback;?>');
            var delivery_price = 0;
            var payment_add_percent = 0;
            var payment_add_price = 0;
            var final_order_price = number_format((order_price + delivery_price + payment_add_price), view_decimals);
            var total_bonus_price = delivery_price + payment_add_price + bonus_price;
            var final_order_bonus = (total_bonus_price < total_cashback)?total_bonus_price:total_cashback;
            var final_order_price_bonus = number_format((order_price + delivery_price + payment_add_price - final_order_bonus), view_decimals);
            var payment_delivery_relation = <?php echo $payment_delivery_relation;?>;
            var payment_delivery_methods = <?php echo json_encode($payment_delivery_methods);?>;
            var delivery_payment_methods = <?php echo json_encode($delivery_payment_methods);?>;
            var payment_method_id = intval('<?php echo $default_payment_method_id;?>');
            var delivery_method_id = 0;            

            update_big_cart = true;
			
			var $input_payment_option;
			var $input_delivery_option;
            

            $(function(){
                $('#inputPhone').inputmask({"mask": "099999999"});
				
				$input_payment_option = $('.input_payment_option');
				$input_delivery_option = $('input[name="delivery_method"]');

                $input_payment_option.on('ifChecked', function(event){
                    payment_add_percent = floatval($(this).data('payment-add'));

                    order_price = 0;
                    var original_order_price = 0;
                    var $items = $('.shopping-cart > .cart_product-wr');
                    $items.each(function( index ) {
                        var quantity = intval($(this).find('.item_quantity').val());
                        var price = floatval($(this).find('input[name="item_price"]').val());
                        var final_item_price = (price + price*payment_add_percent/100) * quantity;
                        original_order_price += floatval(number_format((price * quantity), view_decimals));
                        order_price += floatval(number_format(final_item_price, view_decimals));
                        $(this).find('.item_price').text(number_format(final_item_price/quantity, view_decimals));
                    });

                    payment_add_price = order_price - original_order_price;
                    final_order_price = number_format((order_price + delivery_price), view_decimals);

                    total_bonus_price = delivery_price + payment_add_price + bonus_price;
                    final_order_bonus = (total_bonus_price < total_cashback)?total_bonus_price:total_cashback;

                    final_order_price_bonus = number_format((floatval(number_format(order_price, view_decimals)) + floatval(number_format(delivery_price, view_decimals)) - floatval(number_format(final_order_bonus, view_decimals))), view_decimals);
                    $('.final_order_price').text(final_order_price);
                    $('.final_order_price_bonus').text(final_order_price_bonus);
                    $('.final_order_bonus').text(number_format(final_order_bonus, view_decimals));

					payment_method_id = intval($(this).val());
					
					var delivery_available_str = $(this).data('delivery-available').toString();
					
					var delivery_available = delivery_available_str.split(',');
					$input_delivery_option.each(function(){
						var delivery_option_value = $(this).val();						
						
						if($.inArray( delivery_option_value, delivery_available ) === -1){
							$(this).prop('checked', false).closest('.form-group').hide();
						} else{
							$(this).closest('.form-group').show();
						}
					});

                    payment_delivery_text();
                });

                $input_delivery_option.on('ifChecked', function(event){
                    delivery_price = floatval($(this).data('delivery-price'));
                    order_price = 0;
                    var $items = $('.shopping-cart > .cart_product-wr');
                    $items.each(function( index ) {
                        var quantity = intval($(this).find('.item_quantity').val());
                        var price = floatval($(this).find('input[name="item_price"]').val());
                        var final_item_price = (price + price*payment_add_percent/100) * quantity;
                        order_price += parseFloat(number_format(final_item_price, view_decimals));
                        $(this).find('.item_price').text(number_format(final_item_price/quantity, view_decimals));
                    });

                    final_order_price = number_format((floatval(number_format(order_price, view_decimals)) + floatval(number_format(delivery_price, view_decimals))), view_decimals);

                    total_bonus_price = delivery_price + payment_add_price + bonus_price;
                    final_order_bonus = (total_bonus_price < total_cashback)?total_bonus_price:total_cashback;

                    final_order_price_bonus = number_format((floatval(number_format(order_price, view_decimals)) + floatval(number_format(delivery_price, view_decimals)) - floatval(number_format(final_order_bonus, view_decimals))), view_decimals);
                    $('.final_order_price').text(final_order_price);
                    $('.final_order_price_bonus').text(final_order_price_bonus);
                    $('.final_order_bonus').text(number_format(final_order_bonus, view_decimals));
                    $('#cart_price_delivery-wr .price').text(number_format(delivery_price, view_decimals));

                    delivery_method_id = intval($(this).val());
					
					var payments_available_str = $(this).data('payment-available').toString();
					var payments_available = payments_available_str.split(',');
					$input_payment_option.each(function(){
						var payment_option_value = $(this).val();						
						
						if($.inArray( payment_option_value, payments_available ) === -1){
							$(this).prop('checked', false).closest('.form-group').hide();
						} else{
							$(this).closest('.form-group').show();
						}
					});

                    payment_delivery_text();
                });
            });

            function payment_delivery_text(){
                var payment_delivery_relation_container = $('#js-payment-delivery-relation-text');
                var payment_delivery_relation_key = payment_method_id + '_' + delivery_method_id;

                if(payment_method_id === 0 || delivery_method_id === 0 || !(payment_delivery_relation_key in payment_delivery_relation)){
                    payment_delivery_relation_container.html('');
                    return;
                }

                payment_delivery_relation_container.html('<div class="alert alert-info">'+payment_delivery_relation[payment_delivery_relation_key]+'</div>');
            }
        </script>
    <?php } else{?>
        Ваша корзина пуста. Помогите ей наполниться, вернитесь к покупкам!
    <?php }?>
</article>