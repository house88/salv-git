<div class="static-form-container mb-15">
    <div class="collapsed in">
        <h1>Востановить пароль</h1><br>
        <form id="static_forgot_password_form">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="ca-icon ca-icon_envelope"></span>
                    </div>
                    <input type="text" name="email" class="form-control mb-0" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger btn-lg btn-block mb-15">Востановить</button>
            </div>
            <div class="wr-help">
                <a href="<?php echo base_url();?>" class="pull-left">Авторизация</a>
                <a href="<?php echo base_url('register');?>" class="pull-right">Регистрация</a>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $('#static_forgot_password_form').on('submit', function(e){
            var $form = $(this);
            var fdata = $form.serialize();
            
            $.ajax({
				type: 'POST',
				url: base_url+'auth/ajax_operations/forgot_password',
				data: fdata,
				dataType: 'JSON',
                beforeSend: function(){
                    showLoader('body');
                },
				success: function(resp){
					if(resp.mess_type == 'success'){
                        window.location.href = base_url;
					} else{
						systemMessages(resp.message, resp.mess_type);
                        hideLoader('body');
					}
				}
			});
            return false;
        });
    });
</script>