
<?php if(!empty($properties)){?>
	<?php foreach($properties as $property){?> 
		<?php $property_values = json_decode($property['property_values'], true);?>       
        <?php if($property['type_property'] == 'select' && !empty($properties_values_counters[$property['id_property']])){?>
            <li class="filter" data-property-type="select" data-property="<?php echo $property['id_property'];?>" <?php if(!isset($available_properties_values_counters[$property['id_property']])){echo 'style="display:none;"';}?>>
                <h3 class="filter-heading"><?php echo $property['title_property'];?></h3>
                <?php $_temp_available_counters = 0;?>
                <ul class="filter-list">
                    <?php foreach($property_values as $property_value){?>
                        <?php if(isset($properties_values_counters[$property['id_property']][$property_value['id_value']])){?>
                            <?php $_temp_available_counter = isset($available_properties_values_counters[$property['id_property']][$property_value['id_value']]) ? $available_properties_values_counters[$property['id_property']][$property_value['id_value']] : $properties_values_counters[$property['id_property']][$property_value['id_value']];?>
                            <?php if($_temp_available_counter > 0){ $_temp_available_counters++; }?>

                            <li data-property-value="<?php echo $property_value['id_value'];?>" <?php if($_temp_available_counter == 0 || $_temp_available_counters > 6){echo 'style="display:none;"';}?>>
                                <label>
                                    <input class="js_filter" type="checkbox" <?php echo set_checkbox('fs-'.$property['id_property'], $property_value['id_value'], isset($properties_selected['fs'][$property['id_property']]) && in_array($property_value['id_value'], $properties_selected['fs'][$property['id_property']]));?> name="fs-<?php echo $property['id_property'];?>" value="<?php echo $property_value['id_value'];?>" data-title="<?php echo $property['title_property'];?>" data-value-text="<?php echo $property_value['value'];?>"> 
                                    <?php echo $property_value['value'];?>
                                    <span class="filter-counter">(<?php echo $_temp_available_counter;?>)</span>
                                </label>
                            </li>                        
                        <?php }?>
                    <?php }?>
                </ul>
                <a href="#" class="show_more_filters call-function" data-callback="show_more_filters" <?php echo ($_temp_available_counters > 6) ? 'style="display:block;"' : '' ;?>><span class="text">Показать все</span> </a>
                <a href="#" class="show_less_filters call-function" data-callback="show_less_filters"><span class="text">Свернуть</span> </a>
            </li>
        <?php }?>

        <?php if($property['type_property'] == 'multiselect' && !empty($properties_values_counters[$property['id_property']])){?>
            <li class="filter" data-property-type="multiselect" data-property="<?php echo $property['id_property'];?>" <?php if(!isset($available_properties_values_counters[$property['id_property']])){echo 'style="display:none;"';}?>>
                <h3 class="filter-heading"><?php echo $property['title_property'];?></h3>
                <?php $_temp_available_counters = 0;?>
                <ul class="filter-list">
                    <?php foreach($property_values as $property_value){?>
                        <?php if(isset($properties_values_counters[$property['id_property']][$property_value['id_value']])){?>
                            <?php $_temp_available_counter = isset($available_properties_values_counters[$property['id_property']][$property_value['id_value']]) ? $available_properties_values_counters[$property['id_property']][$property_value['id_value']] : $properties_values_counters[$property['id_property']][$property_value['id_value']];?>
                            <?php if($_temp_available_counter > 0){ $_temp_available_counters++; }?>

                            <li data-property-value="<?php echo $property_value['id_value'];?>" <?php if($_temp_available_counter == 0 || $_temp_available_counters > 6){echo 'style="display:none;"';}?>>
                                <label>
                                    <input class="js_filter" type="checkbox" <?php echo set_checkbox('fm-'.$property['id_property'], $property_value['id_value'], isset($properties_selected['fm'][$property['id_property']]) && in_array($property_value['id_value'], $properties_selected['fm'][$property['id_property']]));?> name="fm-<?php echo $property['id_property'];?>" value="<?php echo $property_value['id_value'];?>" data-title="<?php echo $property['title_property'];?>" data-value-text="<?php echo $property_value['value'];?>"> 
                                    <?php echo $property_value['value'];?>
                                    <span class="filter-counter">(<?php echo $_temp_available_counter;?>)</span>
                                </label>
                            </li>
                        <?php }?>
                    <?php }?>
                </ul>
                <a href="#" class="show_more_filters call-function" data-callback="show_more_filters" <?php echo ($_temp_available_counters > 6) ? 'style="display:block;"' : '' ;?>><span class="text">Показать все</span> </a>
                <a href="#" class="show_less_filters call-function" data-callback="show_less_filters"><span class="text">Свернуть</span> </a>
            </li>
        <?php }?>

        <?php if($property['type_property'] == 'range'){?>
            <li class="filter" data-property-type="range" data-property="<?php echo $property['id_property'];?>">
                <h3 class="filter-heading"><?php echo $property['title_property'];?>, <?php echo $property_values['unit'];?></h3>
                <div class="pl-10_i jsfilter-range">
                    <input class="range-slider" type="text" name="br_<?php echo $property['id_property'];?>" value="" data-min="<?php echo $property_values['min_number'];?>" data-max="<?php echo $property_values['max_number'];?>" <?php echo ((!empty($properties_selected['fr'][$property['id_property']]))?'data-fvalue="['.implode(',', $properties_selected['fr'][$property['id_property']]).']"':'');?>/>
                    <span class="display-n">
                        <input class="js_filter" data-jsfilter-type="range" data-title="<?php echo $property['title_property'];?>" type="text" name="fr-<?php echo $property['id_property'];?>" value=""/>
                    </span>
                </div>
            </li>
        <?php }?>
    <?php }?>
<?php }?>
