<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TinyFileManager {
    private $_path;
    private $_config = array();

    public function __construct(){
        $this->_path  = APPPATH . 'libraries/TinyManager/';
        $this->_config= include $this->_path . 'config/config.php';
    }

    public function init($config = array(), $connectFile){
        $config = $this->get_config($config);

        $plaginURL = base_url('application/libraries/TinyManager/');
        $plaginURL .= '/';
        if (is_file($this->_path . $connectFile)){
            include $this->_path . $connectFile;
        } else {
            include $this->_path . 'dialog.php';
        }
    }

    public function get_config($params){
        if (!empty($params)){
            $this->_config = array_merge($this->_config, $params);
        }

        return array_merge(
            $this->_config,
            array(
                'MaxSizeUpload' => ((int)(ini_get('post_max_size')) < $this->_config['MaxSizeUpload'])
                    ? (int)(ini_get('post_max_size')) : $this->_config['MaxSizeUpload'],
                'ext'=> array_merge(
                    $this->_config['ext_img'],
                    $this->_config['ext_file'],
                    $this->_config['ext_misc'],
                    $this->_config['ext_video'],
                    $this->_config['ext_music']
                ),
                // For a list of options see: https://developers.aviary.com/docs/web/setup-guide#constructor-config
                'aviary_defaults_config' => array(
                    'apiKey'     => $this->_config['aviary_apiKey'],
                    'language'   => $this->_config['aviary_language'],
                    'theme'      => $this->_config['aviary_theme'],
                    'tools'      => $this->_config['aviary_tools'],
                    'maxSize'    => $this->_config['aviary_maxSize']
                ),
            )
        );
    }
}
