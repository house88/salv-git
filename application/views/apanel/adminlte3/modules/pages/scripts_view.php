<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/tinymce'));?>

<script>
	var dtTable;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/pages/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "vam text-left", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "vam text-left", "aTargets": ["dt_url"], "mData": "dt_url", "bSortable": false},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_active"], "mData": "dt_active", "bSortable": false},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	var change_status = function(btn){
		var $this = $(btn);
		var page = $this.data('page');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/pages/ajax_operations/change_status',
			data: {page:page},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var page = $this.data('page');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/pages/ajax_operations/delete',
			data: {page:page},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>