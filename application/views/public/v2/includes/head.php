<meta charset="utf-8">
<base href="<?php echo base_url();?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="yandex-verification" content="43d23373a3d31005" />
<meta name="keywords" content="<?php echo ((isset($skeywords))?clean_output($skeywords):clean_output($settings['mk']['setting_value']));?>">
<meta name="description" content="<?php echo ((isset($sdescription))?clean_output($sdescription):clean_output($settings['md']['setting_value']));?>">
<meta name="author" content="Cravciuc Andrei, cravciucandy@gmail.com">

<title><?php echo ((isset($stitle))?clean_output($stitle):clean_output($settings['default_title']['setting_value']));?></title>
<!-- Bootstrap core CSS -->
<link href="<?php echo file_modification_time($this->theme->public_assets('vendor/bootstrap/css/bootstrap.min.css'));?>" rel="stylesheet">
<!-- Slider CSS -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->public_assets('vendor/slider/slider.css'));?>">
<!-- Select2 CSS -->
<link href="<?php echo file_modification_time($this->theme->public_assets('vendor/select2/css/select2-bootstrap.css'));?>" />
<link href="<?php echo file_modification_time($this->theme->public_assets('vendor/select2/css/select2.min.css'));?>" rel="stylesheet" />
<!-- Font Awesome-->
<link href="<?php echo file_modification_time($this->theme->public_assets('vendor/fontawesome/css/all.min.css'));?>" rel="stylesheet">
<link href="<?php echo file_modification_time($this->theme->public_assets('vendor/icofont/icofont.min.css'));?>" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<?php echo file_modification_time($this->theme->public_assets('css/style.css'));?>" rel="stylesheet">
<link href="<?php echo file_modification_time($this->theme->public_assets('css/custom_sizes.css'));?>" rel="stylesheet">
<!-- Owl Carousel -->
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->public_assets('vendor/owl-carousel/owl.carousel.css'));?>">
<link rel="stylesheet" href="<?php echo file_modification_time($this->theme->public_assets('vendor/owl-carousel/owl.theme.css'));?>">

<?php if(!empty($product_jsonld)){?>
    <script type="application/ld+json">
        <?php echo $product_jsonld;?>
    </script>
<?php }?>

<?php if(isset($og_meta)){?>
    <!-- og_meta -->
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="<?php echo $og_meta['site_name'];?>">
    <meta property="og:title" content="<?php echo $og_meta['title'];?>" />
    <meta property="og:description" content="<?php echo $og_meta['description'];?>" />
    <meta property="og:url" content="<?php echo $og_meta['url'];?>" />
    <meta property="og:image" content="<?php echo $og_meta['image'];?>" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?php echo $og_meta['title'];?>">
    <meta name="twitter:description" content="<?php echo $og_meta['description'];?>">
    <meta name="twitter:url" content="<?php echo $og_meta['url'];?>">
    <meta name="twitter:domain" content="<?php echo base_url();?>">
    <meta name="twitter:site" content="@<?php echo @$settings['default_title']['setting_value'];?>">
    <meta name="twitter:image" content="<?php echo $og_meta['image'];?>">
    <!-- og_meta END -->
<?php }?>

<script>
    Object.defineProperty(window, 'base_url', { writable: false, value: "<?php echo base_url();?>" });
    Object.defineProperty(window, 'filter_url', { writable: false, value: "<?php echo ((!empty($filter_url))?$filter_url:current_url());?>" });
    Object.defineProperty(window, 'filter_title', { writable: false, value: "<?php if(isset($ftitle)) echo clean_output($ftitle);?>" });
    Object.defineProperty(window, 'filter_get_url', { writable: false, value: "<?php if(!empty($get_params)) echo "?".implode('&', $get_params);?>" });
    Object.defineProperty(window, 'view_decimals', { writable: false, value: Boolean(~~'<?php echo $this->lauth->group_view_decimals() ? 1 : 0;?>') });
</script>

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo file_modification_time($this->theme->public_assets('img/favicon/apple-touch-icon.png'));?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo file_modification_time($this->theme->public_assets('img/favicon/favicon-32x32.png'));?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo file_modification_time($this->theme->public_assets('img/favicon/favicon-16x16.png'));?>">
<link rel="manifest" href="<?php echo file_modification_time($this->theme->public_assets('img/favicon/site.webmanifest'));?>">
<link rel="mask-icon" href="<?php echo file_modification_time($this->theme->public_assets('img/favicon/safari-pinned-tab.svg'));?>" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">