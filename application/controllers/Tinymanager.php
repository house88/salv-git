<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tinymanager extends MX_Controller {
    private $_config = array(
        'default' => array(
            'upload_dir'        => '/files/uploads/',
            'current_path'      => 'files/uploads/',
            'thumbs_base_path'  => 'files/upload_thumbs/',
            'thumbs_image'      => false,
            'image_resizing'    => false,
            'image_resizing_mode'=> 3,
            'image_resizing_override'=> false,
            'java_upload'       => false,
            'MaxSizeUpload'     => 4,
            'fileFolderPermission'=> 0755,
            'edit_text_files'   => false,
            'create_text_files' => false,
            'previewable_text_file_exts' => array(),
            'previewable_text_file_exts_no_prettify' => array(),
            'editable_text_file_exts' => array(),
            'googledoc_file_exts' => array(),
            'viewerjs_file_exts' => array(),
            'ext_img'           => array('jpg', 'jpeg', 'png'),
            'ext_file'          => array(),
            'ext_video'         => array(),
            'ext_music'         => array(),
            'ext_misc'          => array(),
        )
    );


    public function init($request = '' , $connectFile = ''){
        if (!$this->lauth->is_admin()){
            exit('Access Denied!');
        }

        $this->load->library('TinyFileManager', null, 'tiny');

        $config = array();

        if (!empty($this->_config[$request])){
            $config = $this->_config[$request];
        }

        $this->tiny->init($config, $connectFile);
    }
}
