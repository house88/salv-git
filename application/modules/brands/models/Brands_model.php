<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Brands_model extends CI_Model{
	var $brands = "brands";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->brands, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_brand, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_brand', $id_brand);
		$this->db->update($this->brands, $data);
	}

	function handler_delete($id_brand){
		$this->db->where('id_brand', $id_brand);
		$this->db->delete($this->brands);
	}

	function handler_get($id_brand){
		$this->db->where('id_brand', $id_brand);
		return $this->db->get($this->brands)->row_array();
	}

	function handler_get_all($conditions = array()){
		$order_by = " id_brand DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}

			if(!empty($multi_order_by)){
				$order_by = implode(',', $multi_order_by);
			}
		}

        if(isset($id_brand)){
			$this->db->where('id_brand', $id_brand);
        }

        if(isset($active)){
			$this->db->where('active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->brands)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_brand)){
			$this->db->where('id_brand', $id_brand);
        }

        if(isset($active)){
			$this->db->where('active', $active);
        }

		return $this->db->count_all_results($this->brands);
	}
}
