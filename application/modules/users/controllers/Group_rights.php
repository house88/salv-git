<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Group_rights extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/users/group_rights/';
		$this->active_menu = 'users';
		$this->active_submenu = 'group_rights';

        $this->data = array();
        
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("User_model", 'user_model');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        checkPermision('manage_group_rights');

		$this->data['groups'] = $this->user_model->handler_get_groups_all();

		$this->data['page_header'] = 'Права по группам';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
            case 'change_status':
				checkPermisionAjax('manage_group_rights');

				$this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');
				$this->form_validation->set_rules('right', 'Право', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $group = (int) $this->input->post('group');
                $right = (int) $this->input->post('right');
                if($this->user_model->handler_get_group_right($group, $right)){
                    $this->user_model->handler_delete_group_right($group, $right);
                } else{
                    $this->user_model->handler_insert_group_right(array(
                        'id_group' => $group,
                        'id_right' => $right
                    ));
				}
				
				jsonResponse('Сохранено.', 'success');
            break;
            case 'list':
				checkPermisionAjaxDT('manage_group_rights');
								
				$groups = $this->user_model->handler_get_groups_all();
				$rights = $this->user_model->handler_get_rights_all();
				$groups_rights = arrayByKey($this->user_model->handler_get_groups_rights_all(), 'gr_key');

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($rights),
					"iTotalDisplayRecords" => count($rights),
					"aaData" => array_map(function($right) use ($groups, $groups_rights){
						$row = array(
							'dt_name'	=>  $right['right_name']
						);

						foreach($groups as $group){
							$gr_key = $group['id_group'].'_'.$right['id_right'];
							$row["dt_{$group['id_group']}"] = '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', isset($groups_rights[$gr_key]), 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-group="'. $group['id_group'] .'" data-right="'. $right['id_right'] .'"></a>';
						}

                        return $row;
					}, $rights)
				);

				jsonResponse('', 'success', $output);
			break;
		}
    }
}
