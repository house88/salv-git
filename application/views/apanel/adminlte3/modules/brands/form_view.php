<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($brand) ? 'Редактировать' : 'Добавить';?> бренд
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">                        
						<div class="form-group">
							<label>Фото</label>
							<p class="help-block">Мин высота: 200px, Мин ширина: 200px</p>
							<span class="btn btn-primary btn-flat btn-sm btn-file pull-left">
								<i class="fad fa-picture-o"></i>
								Добавить фото <input id="select_photo" type="file" name="userfile">
							</span>
							<div id="uploaded_photo" class="custom-margin-top-10">
                                <?php if(!empty($brand) && $brand['brand_photo'] != ''){?>
                                    <div class="user-image-thumbnail-wr">
                                        <img class="img-thumbnail" src="<?php echo base_url('files/brands/'.$brand['brand_photo']);?>"/>
                                        <div class="actions">
                                            <a href="#" class="btn btn-danger btn-sm btn-flat btn-block confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить"  data-title="<i class='fad fa-trash-alt'></i> Удалить" data-type="bg-danger" data-photo="<?php echo $brand['brand_photo'];?>"><i class="fad fa-trash-alt"></i> Удалить</a>
                                        </div>
                                        <input type="hidden" name="photo" value="<?php echo $brand['brand_photo'];?>">
                                    </div>
                                <?php }?>
							</div>
						</div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="title" value="<?php echo !empty($brand) ? $brand['brand_title'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Meta keywords</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Meta keywords" name="mk" value="<?php echo !empty($brand) ? $brand['mk'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Meta description</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Meta description" name="md" value="<?php echo !empty($brand) ? $brand['md'] : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Текст</label>
                            <textarea class="description" name="description"><?php echo !empty($brand) ? $brand['brand_description'] : '';?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="visible" type="checkbox" id="js-checkbox-brand-active" <?php echo set_checkbox('visible', 1, !empty($brand) ? (int) $brand['visible'] === 1 : false);?>>
                    <label for="js-checkbox-brand-active">
                        Активный
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($brand)){?>
                        <input type="hidden" name="brand" value="<?php echo $brand['id_brand'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        tinyMCE.triggerSave();
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/brands/ajax_operations/<?php echo !empty($brand) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });

    initTinyMce();

    $('#select_photo').fileupload({
        url: base_url+'admin/brands/ajax_operations/upload_photo',
        dataType: 'json',
        done: function (e, data) {
            if(data.result.mess_type == 'error'){
                systemMessages( data.result.message, data.result.mess_class );
            } else{
                if($('#uploaded_photo .user-image-thumbnail-wr').length > 0){
                    var unused_photo = $('#uploaded_photo .user-image-thumbnail-wr').find('input[name=photo]').val();
                    manage_form.append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
                }

                var uploaded_file = data.result.file;
                var template = '<div class="user-image-thumbnail-wr">\
                                    <img class="img-thumbnail" src="'+base_url+'files/brands/'+uploaded_file.filename+'"/>\
                                    <div class="actions">\
                                        <a href="#" class="btn btn-danger btn-sm btn-flat btn-block confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-photo="'+uploaded_file.filename+'" data-type="bg-danger"><i class="fad fa-trash-alt"></i> Удалить</a>\
                                    </div>\
                                    <input type="hidden" name="photo" value="'+uploaded_file.filename+'">\
                                </div>';
                $('#uploaded_photo').html(template);
            }
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
</script>
