<script>
	var save_popular_block = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form#popular_block');
		var fdata = $form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_popular_block',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}

	var save_newest_block = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form#newest_block');
		var fdata = $form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_newest_block',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}

	var settings_form = $('#settings_form');
	settings_form.submit(function () {
		var fdata = settings_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_settings',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});
</script>