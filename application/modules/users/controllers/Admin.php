<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/users/';
		$this->active_menu = 'users';
		$this->active_submenu = 'users';

        $this->data = array();
        
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("User_model", 'user_model');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        checkPermision('manage_users');

		$this->data['page_header'] = 'Пользователи';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function moderators(){
        checkPermision('manage_users');
        
		$this->active_submenu = 'moderators';

		$this->data['page_header'] = 'Модераторы';
		$this->data['is_staff'] = true;
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_users');

                $this->data['categories'] = Modules::run('categories/_get_tree_tokens');
                $this->data['groups'] = $this->user_model->handler_get_groups_all();
				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_users');

                $id_user = (int) $this->uri->segment(5);
                $this->data['user'] = $this->user_model->handler_get($id_user);
				if(empty($this->data['user'])){
					jsonResponse('Пользователь не найден.');
                }
                
                $selected_categories = array();
                $user_categories_access = array_filter(explode(',', $this->data['user']->user_categories_access));
                if(!empty($user_categories_access)){
                    $selected_categories = array_map('intval', $user_categories_access);
                }

                $this->data['categories'] = Modules::run('categories/_get_tree_tokens', array('selected_categories' => $selected_categories));
                $this->data['groups'] = arrayByKey($this->user_model->handler_get_groups_all(), 'id_group');
				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
            case 'add':
                if(!$this->lauth->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_phone', 'Телефон', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_address', 'Адрес', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('user_pass', 'Пароль', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('confirm_user_pass', 'Повторите пароль', 'required|xss_clean|max_length[50]|matches[user_pass]');
				$this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
                    'id_group' => (int) $this->input->post('group'),
					'user_login' => $this->input->post('user_name'),
					'user_nicename' => $this->input->post('user_name'),
					'user_email' => $this->input->post('user_email'),
					'user_pass' => $this->lauth->hash_password($this->input->post('user_pass',true)),
                    'user_phone' => $this->input->post('user_phone'),
                    'user_phone_display' => NULL,
					'user_address' => $this->input->post('user_address'),
					'registered_date' => date('Y-m-d H:i:s'),
					'activation_key' => md5($this->input->post('user_email').uniqid().'@cc@ctivate'),
                    'status' => ($this->input->post('status', true)) ? 1 : 0
                );
                
                if($this->input->post('user_phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('user_phone'));
					if(true === $phoneFormatResult['is_valid']){
						$insert['user_phone_display'] = $phoneFormatResult['formated'];
					}
				}

                $group = $this->user_model->handler_get_group($insert['id_group']);
                if(empty($group)){
                    jsonResponse('Ошибка: нет такой группы.');
                }

                if($group['group_type'] == 'content_menedger'){                    
                    $categories = $this->input->post('category_parent');
                    if(!empty($categories) && !empty($categories_info = Modules::run('categories/_get_all', array('id_category' => $categories)))){
                        $clist = array();
                        $clist_full = array();
                        foreach ($categories_info as $category) {
                            $clist[] = $category['category_id'];
                            $cbreads = json_decode('['.$category['category_breadcrumbs'].']');
                            foreach ($cbreads as $cbread) {
                                $clist_full[] = $cbread->category_id;
                            }
                        }
                        $insert['user_categories_access'] = implode(',', $clist);
                        $insert['user_categories_access_full'] = implode(',', array_unique($clist_full));
                    }
                }

				$this->auth_model->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'edit':
                checkPermisionAjax('manage_users');

                $this->form_validation->set_rules('user', 'Пользователь', 'required|xss_clean');
				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_address', 'Адрес', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('user_phone', 'Телефон', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');
                
                if($this->input->post('change_password')){
                    $this->form_validation->set_rules('user_pass', 'Пароль', 'required|min_length[6]');
                    $this->form_validation->set_rules('confirm_user_pass', 'Повторите пароль', 'required|matches[user_pass]');
                }

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_user = (int) $this->input->post('user');
                $user_data = $this->auth_model->handler_get_by_id($id_user);
                if(empty($user_data)){
                    jsonResponse('Пользователь не найден.');
                }

				$update = array(
					'user_login' => $this->input->post('user_name'),
					'user_nicename' => $this->input->post('user_name'),
					'user_email' => $this->input->post('user_email'),
                    'user_phone' => $this->input->post('user_phone'),
                    'user_phone_display' => NULL,
					'user_address' => $this->input->post('user_address'),
                    'id_group' => (int) $this->input->post('group'),
                    'callapi_sip_id' => $this->input->post('callapi_sip_id') ? $this->input->post('callapi_sip_id') : NULL,
                    'user_categories_access' => '',
                    'status' => ($this->input->post('status', true)) ? 1 : 0
                );
                
                if($this->input->post('user_phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('user_phone'));
					if(true === $phoneFormatResult['is_valid']){
						$update['user_phone_display'] = $phoneFormatResult['formated'];
					}
				}

                $group = $this->user_model->handler_get_group($update['id_group']);
                if(empty($group)){
                    jsonResponse('Ошибка: нет такой группы.');
                }

                if($group['group_type'] == 'content_menedger'){                    
                    $categories = $this->input->post('category_parent');
                    if(!empty($categories) && !empty($categories_info = Modules::run('categories/_get_all', array('id_category' => $categories)))){
                        $clist = array();
                        $clist_full = array();
                        foreach ($categories_info as $category) {
                            $clist[] = $category['category_id'];
                            $cbreads = json_decode('['.$category['category_breadcrumbs'].']');
                            foreach ($cbreads as $cbread) {
                                $clist_full[] = $cbread->category_id;
                            }
                        }
                        $update['user_categories_access'] = implode(',', $clist);
                        $update['user_categories_access_full'] = implode(',', array_unique($clist_full));
                    }
                }

                if($this->input->post('change_password')){
                    $update['reset_key'] = md5($user_data->user_email.uniqid().'@ccreset');
					$update['user_pass'] = $this->lauth->hash_password($this->input->post('user_pass'));
                }

				$this->auth_model->handler_update($id_user, $update);
				jsonResponse('Сохранено.', 'success');
            break;
			case 'change_status':
                checkPermisionAjax('manage_users');

                $this->form_validation->set_rules('user', 'Пользователь', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_user = (int) $this->input->post('user');
				$user = $this->user_model->handler_get($id_user);
				if(empty($user)){
					jsonResponse('Не могу завершить операцию.');
				}

				$this->auth_model->handler_update($id_user, array(
                    'user_banned' => ! (int) $user->user_banned ? 1: 0
                ));
				jsonResponse('Операция прошла успешно.', 'success');
            break;
            case 'list':
                checkPermisionAjaxDT('manage_users');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength', true),
					'start' => (int) $this->input->post('iDisplayStart', true),
                    'group_type' => array('user')
                );
                
                $uri_type = $this->uri->segment(5);
                if($uri_type === 'staff'){
                    $params['group_type'] = array('admin', 'moderator', 'content_menedger');
                }

				$params['sort_by'] = flat_dt_ordering($_POST, array(
					'dt_id'         => 'id',
					'dt_name'       => 'user_nicename',
					'dt_type'       => 'group_type',
					'dt_regdate'    => 'registered_date'
				));
		
				if($this->input->post('registered_on')){
					list($registered_date_from, $registered_date_to) = explode(' - ', $this->input->post('registered_on'));

					if(validateDate($registered_date_from, 'd.m.Y')){
						$params['registered_date_from'] = getDateFormat($registered_date_from, 'd.m.Y', 'Y-m-d');
					}

					if(validateDate($registered_date_to, 'd.m.Y')){
						$params['registered_date_to'] = getDateFormat($registered_date_to, 'd.m.Y', 'Y-m-d');
					}
                }
                
				if($this->input->post('keywords')){
					$params['keywords'] = $this->input->post('keywords', true);
				}
        
                $records = $this->user_model->handler_get_all($params);
                $records_total = $this->user_model->handler_get_count($params);

                $group_types = array(
                    'admin' => array(
                        'title' => 'Администратор'
                    ),
                    'moderator' => array(
                        'title' => 'Модератор'
                    ),
                    'content_menedger' => array(
                        'title' => 'Контент менеджер'
                    ),
                    'user' => array(
                        'title' => 'Пользователь'
                    )
                );

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($group_types){
                        return array(
							'dt_id'			=>  $record['id'],
                            'dt_name'		=>  $record['user_nicename'] . '<br>' . $record['user_email'] . get_choice('<br><span class="badge bg-warning">Не подтвержден</span>', (int) $record['status'] === 0),
                            'dt_phone'		=>  $record['user_phone_display'],
                            'dt_type'		=>  $group_types[$record['group_type']]['title'],
                            'dt_blocked'	=>  get_choice('<span class="badge bg-success">Нет</span>', (int) $record['user_banned'] === 0, '<span class="badge bg-danger">Да</span>'),
                            'dt_regdate'	=>  getDateFormat($record['registered_date']),
                            'dt_actions'	=> '<div class="dropdown">
                                                    <a data-toggle="dropdown" href="#" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
                                                        <a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/users/popup/edit/' . $record['id']).'">
                                                            <i class="fad fa-pencil"></i> Редактировать
                                                        </a>
                                                        <div class="dropdown-divider"></div>
                                                        <a href="#" class="dropdown-item call-function" data-callback="change_status" data-user="'.$record['id'].'">
                                                            '. get_choice('<i class="fad fa-lock-open-alt"></i> Разаблокировать', (int) $record['user_banned'] === 1, '<i class="fad fa-lock-alt"></i> Заблокировать') .'
                                                        </a>
                                                    </div>
                                                </div>'
						);
					}, $records)
				);

				jsonResponse('', 'success', $output);
            break;
		}
	}
}
