<div class="row products-page-wr">
	<div class="col-xs-12">
		<div class="catalog-header-wr">
			<div class="row">
				<div class="hidden-xs col-sm-6 col-md-6">
					<h1 class="catalog-title">
						Бренд: <?php echo $brand['brand_title'];?>
						<span class="catalog-count"><?php echo $total_products;?> моделей</span>
					</h1>
					<p class="page-number">страница <?php echo $page_number;?></p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="toggle-display_wr hidden-xs">
						<span class="ca-icon ca-icon_grid toggle_item call-function" data-callback="toggle_product_view" data-type="grid"></span>
						<span class="ca-icon ca-icon_list toggle_item call-function" data-callback="toggle_product_view" data-type="list"></span>
						<span class="ca-icon ca-icon_list-small toggle_item call-function" data-callback="toggle_product_view" data-type="small-list"></span>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<div class="col-xs-3 filters-wr">
		<aside class="sidebar">
            <?php $this->load->view($this->theme->public_view('shop/catalog/brand/filter_view'));?>
		</aside>
	</div>
	<div class="col-xs-12 products-wr">
		<div class="mt-15 mr-15 pr-15 pl-15" id="catalog-wr">
            <?php $this->load->view($this->theme->public_view('shop/catalog/products_view'));?>
        </div>
	</div>
</div>
