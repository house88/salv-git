<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Seo extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();

		$this->load->model('items/Items_Model', 'items');
		$this->load->model('categories/Categories_Model', 'categories');
		$this->load->model('brands/Brands_model', 'brands');
	}

	function index(){
		return_404($this->data);
	}

	function sitemap(){
		// $this->data['special_pages'] = Modules::run('static_block/_get_all', array());
		$this->data['categories'] = $this->categories->handler_get_all();
		$this->data['products_count'] = $this->items->handler_get_count();
		$this->data['limit'] = 5000;
		return $this->load->view('sitemap_view', $this->data, true);
	}
}
