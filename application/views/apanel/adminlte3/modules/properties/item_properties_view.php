<?php if(!empty($properties)){?>
	<h5>Свойства товара <?php if($properties_required){ echo '<strong class="text-danger">*</strong>';}?></h5>
	<?php foreach($properties as $property){?>
		<?php $property_values = json_decode($property['property_values'], true);?>
		<div class="form-group">
			<label><?php echo $property['title_property'];?><?php if($property['type_property'] == 'range'){ echo ', '.$property_values['unit'];}?></label>
			<?php if($property['type_property'] == 'select'){?>
				<select name="properties[select][<?php echo $property['id_property'];?>]" class="form-control form-control-sm rounded-0">
					<option value="">Выберите значение</option>
					<?php foreach($property_values as $property_value){?>
						<option value="<?php echo $property_value['id_value'];?>" <?php if(!empty($item_properties_values[$property['id_property']])){ echo set_select('properties[select]['.$property['id_property'].']', $property_value['id_value'], in_array($property_value['id_value'], $item_properties_values[$property['id_property']]));}?>><?php echo $property_value['value'];?></option>
					<?php }?>
				</select>
			<?php }?>
			<?php if($property['type_property'] == 'multiselect'){?>
				<select name="properties[multiselect][<?php echo $property['id_property'];?>][]" class="form-control form-control-sm rounded-0 bs-select-multiple" data-live-search="true" data-style="btn btn-default btn-sm btn-flat" data-title="Выберите значение" multiple size="<?php echo count($property_values);?>">
					<?php foreach($property_values as $property_value){?>
						<option value="<?php echo $property_value['id_value'];?>" <?php if(!empty($item_properties_values[$property['id_property']])){ echo set_select('properties[select]['.$property['id_property'].']', $property_value['id_value'], in_array($property_value['id_value'], $item_properties_values[$property['id_property']]));}?>><?php echo $property_value['value'];?></option>
					<?php }?>
				</select>
			<?php }?>
			<?php if($property['type_property'] == 'range'){?>
				<input class="form-control form-control-sm rounded-0" placeholder="" name="properties[range][<?php echo $property['id_property'];?>]" value="<?php if(!empty($item_properties_values[$property['id_property']])){ echo $item_properties_values[$property['id_property']][0];}?>">
				<p class="help-block">Мин. <?php echo $property_values['min_number']. ' ' .$property_values['unit'];?>; Макс. <?php echo $property_values['max_number']. ' ' .$property_values['unit'];?></p>
			<?php }?>
			<?php if($property['type_property'] == 'simple'){?>
				<input class="form-control form-control-sm rounded-0" placeholder="" name="properties[simple][<?php echo $property['id_property'];?>]" value="<?php if(!empty($item_properties_values[$property['id_property']])){ echo $item_properties_values[$property['id_property']][0];}?>">
			<?php }?>
		</div>
	<?php }?>
<?php }?>
