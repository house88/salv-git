<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-dark navbar-gray-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
            <i class="fas fa-th-large"></i>
            </a>
        </li> -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-ellipsis-h"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
                <a href="<?php echo site_url();?>" class="dropdown-item">
                    <i class="ca-icon ca-icon_home"></i> Перейти на сайт
                </a>
                <div class="dropdown-divider"></div>
                <a href="<?php echo site_url('logout');?>" class="dropdown-item">
                    <i class="fas fa-sign-out-alt"></i> Выход
                </a>
            </div>
            <!-- /.dropdown-user -->
        </li>
    </ul>
</nav>
<!-- /.navbar -->
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo site_url('admin');?>" class="brand-link">
        <img src="<?php echo site_url($this->theme->apanel_assets('img/AdminLTELogo.png'))?>" alt="A" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">Панель управления</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="custom-margin-top-2">
            <?php $dashboard_menu_items = Modules::run('menu/_get_apanel_menu');?>
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-legacy" data-widget="treeview" role="menu" data-accordion="false">
                <?php foreach($dashboard_menu_items as $key_menu => $item){?>
                    <?php if($this->lauth->have_right_or($item->rights)){?>                        
                        <li class="nav-item <?php if(!empty($item->submenu)){?>has-treeview<?php }?> <?php echo !empty($this->active_menu && $key_menu === $this->active_menu) ? 'menu-open' : '';?>">
                            <a href="<?php echo site_url("admin/{$item->url}"); ?>" class="nav-link">
                                <i class="nav-icon <?php echo $item->icon; ?>"></i>
                                <p>
                                    <?php echo $item->name; ?>
                                    <?php if(!empty($item->submenu)){?>
                                        <i class="right fas fa-angle-left"></i>
                                    <?php }?>
                                </p>
                            </a>
                            <?php if(!empty($item->submenu)){?>
                                <ul class="nav nav-treeview">
                                    <?php foreach($item->submenu as $key_submenu => $item_submenu){?>
                                        <?php if($this->lauth->have_right_or($item_submenu->rights)){?>
                                            <li class="nav-item">
                                                <a href="<?php echo site_url("admin/{$item_submenu->url}");?>" class="nav-link <?php echo !empty($this->active_submenu && $key_submenu === $this->active_submenu) ? 'active' : '';?>">
                                                    <i class="<?php echo $item_submenu->icon; ?> nav-icon"></i>
                                                    <p><?php echo $item_submenu->name; ?></p>
                                                </a>
                                            </li>
                                        <?php }?>
                                    <?php }?>
                                </ul>
                            <?php }?>
                        </li>
                    <?php }?>
                <?php }?>

                <?php if($this->lauth->have_right('manage_files')){?>
                    <li class="nav-item">
                        <a class="nav-link call-function" data-callback="openFilemanager" data-src="<?php echo site_url('tinymanager/init/default');?>/dialog.php?type=1&popup=2&relative_url=1" href="javascript:;">
                            <i class="nav-icon fad fa-folder-open"></i>
                            <p>Файловый менеджер</p>
                        </a>
                    </li>
                <?php }?>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>