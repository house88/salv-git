<?php if(!empty($comment)){?>
    <!-- COMMENT - START -->
    <div class="media">
        <div class="media-body">
            <h3 class="media-heading"><?php echo $comment['comment_username'];?></h3>
            <div class="meta">
                <span class="date"><?php echo formatDate($comment['comment_date']);?></span>
            </div>
            <p><?php echo $comment['comment_text'];?></p>
            <?php if(!empty($comment['comment_text_reply'])){?>
                <div class="media custom-padding-left-30">
                    <div class="media-body">
                        <h3 class="media-heading">SALV</h3>
                        <div class="meta">
                            <span class="date"><?php echo formatDate($comment['comment_date_reply']);?></span>
                        </div>
                        <p><?php echo $comment['comment_text_reply'];?></p>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
    <!-- COMMENT - END -->
<?php }?>