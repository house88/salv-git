<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function return_404($data) {
	$ci =& get_instance();
	$ci->output->set_status_header('404');
	echo $ci->load->view('errors/custom/404_view', $data, true);
	exit();
}