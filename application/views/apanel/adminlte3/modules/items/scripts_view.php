<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/fancybox'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/daterangepicker'));?>

<script>
    var dtTable; //obj of datatable
	var dtFilter;
    var current_category = '';
    var user_categories = false;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/items/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "vat text-center custom-width-100", "aTargets": ["dt_photo"], "mData": "dt_photo", "bSortable": false},
				{ "sClass": "vam text-left", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_price"], "mData": "dt_price"},
				{ "sClass": "vam text-center custom-width-100", "aTargets": ["dt_quantity"], "mData": "dt_quantity"},
				{ "sClass": "vam text-center custom-width-100 d-block d-md-none d-xl-table-cell", "aTargets": ["dt_create"], "mData": "dt_create"},
				{ "sClass": "vam text-center custom-width-100 d-block d-md-none d-xl-table-cell", "aTargets": ["dt_update"], "mData": "dt_update"},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions", "bSortable": false}
			],
			"aaSorting" : [[0,'desc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = initDtFilter('items');
				}

				aoData = aoData.concat(dtFilter.getDTFilter());

				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
	            $('[data-toggle="popover"]').popover();
			}
		});

		$('body').on('change', 'select[name="id_category"]', function(){
            current_category = $(this).val();
            $('#properties_block').html('').hide();
        });

        $('body').on('change', 'select#category_parent', function(){
            current_category = $(this).val();
            if(current_category != ''){
                getSubcategories();
            } else{
                $('#subcategories_block').html('');
            	$('#properties_block').html('').hide();
            }
		});

		$('#js-item-updated-date').daterangepicker({
			timePicker: false,
			parentEl: '.modal',
			locale: {
				format: 'DD.MM.YYYY'
			},
			autoUpdateInput: false,
			"autoApply": true,
			"opens": "left",
			"drops": "up"
		}).on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
		}).on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('').trigger('change');
		});
	});

	function onSetFilters(callerObj, filterObj){
		if(filterObj.name === 'id_category'){
			if(filterObj.value === filterObj.default){
				$('#subcategories_block').html('');
			} else{
				current_category = filterObj.value;

				if($(callerObj).hasClass('js-parent-category')){
					getSubcategories();
				}
			}
		}
	}

	function onDeleteFilters(filter){		
		if(filter.name == 'id_category'){
			$('#subcategories_block').html('');
		}
	}

	function getSubcategories(){
		$.ajax({
			type: 'POST',
			url: base_url+'admin/categories/ajax_operations/get_all_subcategories',
			data: {category:current_category},
			dataType: 'JSON',
			success: function(resp){
				if(resp.mess_type == 'success'){
					var subcategories_html = '<div class="form-group">\
												<div class="input-group input-group-sm">\
													<span class="input-group-prepend">\
														<button type="button" class="btn btn-default btn-flat custom-width-40"><i class="fad fa-folder-tree"></i></button>\
													</span>	\
													<select name="id_category" data-title="Категория" class="form-control form-control-sm rounded-0 dt_filter">\
														<option value="">Выберите категорию</option>\
														'+ resp.subcategories +'\
													</select>\
												</div>\
											</div>';
					$('#subcategories_block').html(subcategories_html);
				} else{
					systemMessages(resp.message, resp.mess_type);
				}
			}
		});
	}
	
	var getCategoryProperties = function(btn){
		var $this = $(btn);
		if(current_category != ''){
			$.ajax({
				type: 'POST',
				url: base_url+'admin/properties/ajax_operations/get_filter_category_properties',
				data: {category:current_category},
				dataType: 'JSON',
				success: function(resp){
					if(resp.mess_type == 'success'){
						$('#properties_block').html(resp.properties).show();
					} else{
						systemMessages(resp.message, resp.mess_type);
					}
				}
			});
		} else{
			$('#properties_block').html('').hide();
		}
	}
</script>