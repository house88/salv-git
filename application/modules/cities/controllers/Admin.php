<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/cities/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'cities';

		$this->data = array();
		$this->data['main_title'] = 'Города';

		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Cities_model", "cities");

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		checkPermision('manage_cities', '/admin');

		$this->data['page_header'] = 'Города';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'cities_scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function add(){
		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_city = (int)$this->uri->segment(4);
		$this->data['city'] = $this->cities->handler_get($id_city);
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_cities');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_cities');

				$id_city = (int) $this->uri->segment(5);
				$this->data['city'] = $this->cities->handler_get($id_city);
				if(empty($this->data['city'])){
					jsonResponse('Город не найден.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				checkPermisionAjax('manage_cities');

				$this->form_validation->set_rules('city_name', 'Название', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('city_delivery_days', 'Срок доставки', 'required|xss_clean|is_natural');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'city_name' => $this->input->post('city_name'),
					'city_delivery_days' => (int) $this->input->post('city_delivery_days'),
					'city_visible' => 0
				);

				if($this->input->post('active')){
					$insert['city_visible'] = 1;
				}

				$this->cities->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				checkPermisionAjax('manage_cities');

                $this->form_validation->set_rules('city', 'Город', 'required|xss_clean');
				$this->form_validation->set_rules('city_name', 'Название', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('city_delivery_days', 'Срок доставки', 'required|xss_clean|is_natural');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_city = (int) $this->input->post('city');

				$update = array(
					'city_name' => $this->input->post('city_name'),
					'city_delivery_days' => (int) $this->input->post('city_delivery_days'),
					'city_visible' => 0
				);

				if($this->input->post('active')){
					$update['city_visible'] = 1;
				}

				$this->cities->handler_update($id_city, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				checkPermisionAjax('manage_cities');

                $this->form_validation->set_rules('city', 'Город', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_city = (int)$this->input->post('city');
				$city = $this->cities->handler_get($id_city);
				if(empty($city)){
					jsonResponse('Город не существует.');
				}

				$this->cities->handler_delete($id_city);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				checkPermisionAjax('manage_cities');

                $this->form_validation->set_rules('city', 'Город', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_city = (int)$this->input->post('city');
				$city = $this->cities->handler_get($id_city);
				if(empty($city)){
					jsonResponse('Город не существует.');
				}

				$status = (int) !((int) $city['city_visible'] === 1);
				$this->cities->handler_update($id_city, array('city_visible' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			case 'list':
				checkPermisionAjaxDT('manage_cities');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength'),
					'start' => (int) $this->input->post('iDisplayStart'),
					'sort_by' => array(	'id_city-asc' )
				);

				$records = $this->cities->handler_get_all($params);
        		$records_total = $this->cities->handler_get_count($params);
				
				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record){
						return array(
							'dt_id'			=>  $record['id_city'],
							'dt_name'		=>  $record['city_name'],
							'dt_active'		=>  '<a href="#" class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['city_visible'] === 1, 'fad fa-minus-square text-danger') .' call-function" data-callback="change_status" data-city="'.$record['id_city'].'"></a>',
							'dt_actions'	=>  '<div class="dropdown">
													<a data-toggle="dropdown" href="#" aria-expanded="false">
														<i class="fas fa-ellipsis-h"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
														<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/cities/popup/edit/' . $record['id_city']).'">
															<i class="fad fa-pencil"></i> Редактировать
														</a>
														<div class="dropdown-divider"></div>
														<a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить город?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-city="'.$record['id_city'].'" data-type="bg-danger">
															<i class="fad fa-trash-alt"></i> Удалить
														</a>
													</div>
												</div>'
						);
					}, $records)
				);
				
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
