<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Currency_model extends CI_Model{
	var $currency = "currency";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->currency, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_currency, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_currency', $id_currency);
		$this->db->update($this->currency, $data);
	}

	function handler_delete($id_currency){
		$this->db->where('id_currency', $id_currency);
		$this->db->delete($this->currency);
	}

	function handler_get($id_currency){
		$this->db->where('id_currency', $id_currency);
		return $this->db->get($this->currency)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_currency ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('currency_active', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->currency)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('currency_active', $active);
        }

		return $this->db->count_all_results($this->currency);
	}

	function handler_get_default(){
		$this->db->where('currency_default', 1);
		return $this->db->get($this->currency)->row_array();
	}

	function handler_set_default($id_currency){
		$this->db->update($this->currency, array('currency_default' => 0));

		$this->db->where('id_currency', $id_currency);
		return $this->db->update($this->currency, array('currency_default' => 1));
	}

	function handler_set_default_admin($id_currency){
		$this->db->update($this->currency, array('currency_admin' => 0));

		$this->db->where('id_currency', $id_currency);
		return $this->db->update($this->currency, array('currency_admin' => 1));
	}

	function handler_set_default_user($id_currency){
		$this->db->update($this->currency, array('currency_user' => 0));

		$this->db->where('id_currency', $id_currency);
		return $this->db->update($this->currency, array('currency_user' => 1));
	}

	function handler_get_default_user(){
		$this->db->where('currency_user', 1);
		$this->db->limit(1);
		$currency = $this->db->get($this->currency)->row_array();
		if(empty($currency)){
			$currency = $this->handler_get_default();
		}

		return $currency;
	}

	function handler_get_default_admin(){
		$this->db->where('currency_admin', 1);
		$this->db->limit(1);
		$currency = $this->db->get($this->currency)->row_array();
		if(empty($currency)){
			$currency = $this->handler_get_default();
		}

		return $currency;
	}
}
