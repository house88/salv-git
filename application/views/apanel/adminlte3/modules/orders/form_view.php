<div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title">Заказ <?php echo orderNumber($order['id_order']);?></h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">                    
                    <?php $order_statuses = get_order_statuses();?>
                    <div class="col-12 col-sm-3 col-lg-2">
                        <div class="form-group custom-width-100pr">
                            <label class="text-gray custom-font-size-12">Статус</label>                            
                            <input type="hidden" name="order[status]" class="form-control input-sm" value="<?php echo $order['order_status'];?>">
                            <input type="hidden" name="opened_orders" value="<?php echo $opened_orders;?>">

                            <div class="dropdown custom-width-100pr">
                                <button class="btn btn-<?php echo $order_statuses[$order['order_status']]['color_class']?> btn-flat btn-sm custom-width-100pr text-truncate dropdown-toggle" type="button" id="js-dropdown-order-status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="<?php echo $order_statuses[$order['order_status']]['icon'];?>"></i> <?php echo $order_statuses[$order['order_status']]['title']?>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="js-dropdown-order-status">
                                    <?php foreach($order_statuses as $status_key => $order_status){?>
                                        <?php if($opened_orders > 0){?>
                                            <a 
                                                class="dropdown-item confirm-dialog" 
                                                href="#" 
                                                data-title="ВНИМАНИЕ!!!" 
                                                data-message="У данного пользователя есть другие незавершенные заказы. Просим вас проверить их состояние. Вы точно хотите изменить статус этого заказа на <strong><?php echo $order_status['title'];?></strong>?" 
                                                data-type="bg-danger" 
                                                data-callback="confirm_change_status" 
                                                data-status="<?php echo $status_key;?>" 
                                                data-color="<?php echo $order_status['color_class'];?>" 
                                                data-icon="<?php echo $order_status['icon'];?>" 
                                                data-text="<?php echo $order_status['title'];?>">
                                                <span class="badge bg-<?php echo $order_status['color_class'];?>"><i class="<?php echo $order_status['icon'];?>"></i> <?php echo $order_status['title'];?></span>
                                            </a>
                                        <?php } else if($order_status['is_final'] === true){?>
                                            <a 
                                                class="dropdown-item confirm-dialog" 
                                                href="#" 
                                                data-title="ВНИМАНИЕ!!!" 
                                                data-message="После применения данного статуса возможность менять статус для этого заказа будет заблокирована. Вы точно хотите изменить статус этого заказа на <strong><?php echo $order_status['title'];?></strong>?" 
                                                data-type="bg-danger" 
                                                data-callback="confirm_change_status" 
                                                data-status="<?php echo $status_key;?>" 
                                                data-color="<?php echo $order_status['color_class'];?>" 
                                                data-icon="<?php echo $order_status['icon'];?>" 
                                                data-text="<?php echo $order_status['title'];?>">
                                                <span class="badge bg-<?php echo $order_status['color_class'];?>"><i class="<?php echo $order_status['icon'];?>"></i> <?php echo $order_status['title'];?></span>
                                            </a>
                                        <?php } else{?>
                                            <a 
                                                class="dropdown-item call-function" 
                                                href="#" 
                                                data-callback="confirm_change_status" 
                                                data-status="<?php echo $status_key;?>" 
                                                data-color="<?php echo $order_status['color_class'];?>" 
                                                data-icon="<?php echo $order_status['icon'];?>" 
                                                data-text="<?php echo $order_status['title'];?>">
                                                <span class="badge bg-<?php echo $order_status['color_class'];?>"><i class="<?php echo $order_status['icon'];?>"></i> <?php echo $order_status['title'];?></span>
                                            </a>
                                        <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                        </div>          
                    </div>
                    <div class="col-12 col-sm-4 col-lg-2">
                        <div class="form-group">
                            <label class="text-gray custom-font-size-12">Покупатель</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-prepend">
                                    <button type="button" class="btn btn-default btn-flat"><i class="fad fa-user"></i></button>
                                </span>
                                <input class="form-control rounded-0" type="text" name="order[user_name]" value="<?php echo $order['order_user_name'];?>">
                            </div>
                        </div>          
                    </div>
                    <div class="col-12 col-sm-4 col-lg-3">
                        <div class="form-group">
                            <label class="text-gray custom-font-size-12">Еmail</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-prepend">
                                    <button type="button" class="btn btn-default btn-flat"><i class="fad fa-envelope"></i></button>
                                </span>
                                <input class="form-control rounded-0" type="text" name="order[user_email]" value="<?php echo $order['order_user_email'];?>">
                            </div>
                        </div>          
                    </div>
                    <div class="col-12 col-sm-4 col-lg-2">
                        <div class="form-group">
                            <label class="text-gray custom-font-size-12">Телефон</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-prepend">
                                    <button type="button" class="btn btn-default btn-flat call-function" data-callback="callClient" data-phone="<?php echo $order['order_user_phone'];?>"><i class="fad fa-phone"></i></button>
                                </span>
                                <input class="form-control rounded-0" type="text" name="order[user_phone]" value="<?php echo $order['order_user_phone'];?>">
                            </div>
                        </div>          
                    </div>
                    <div class="col-12 col-sm-4 col-lg-3">
                        <div class="form-group">
                            <label class="text-gray custom-font-size-12">Контакт с покупателем</label>
                            <div class="icheck-danger">
                                <input type="checkbox" name="order[order_client_not_responding]" <?php echo set_checkbox('order[order_client_not_responding]', '', $order['order_client_not_responding'] == 1);?> id="js-checkbox-order-contact-client">
                                <label class="font-weight-normal" for="js-checkbox-order-contact-client">Покупатель не отвечает</label>
                            </div>
                        </div>          
                    </div>
                </div>
                
                <div class="orders_items-wr order_<?php echo $order['id_order'];?>_detail">
                    <input type="hidden" name="order[cashback]" class="order_cashback" value="<?php echo $order['order_cashback'];?>">
                    <div class="ordered_item">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label class="text-gray custom-font-size-12">Скидка к заказу</label>
                                    <div class="input-group input-group-sm">
                                        <input class="form-control rounded-0 order_discount_price"  onkeyup="javascript: calc_order_price(event, <?php echo $order['id_order'];?>, 'by_discount_price');" title="Сумма дополнительной скидки" type="text" name="order[discount_by_currecy]" value="<?php echo $order['order_discount_by_currecy'];?>" placeholder="0.00">
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-default btn-flat"><?php echo $order['order_currency'];?></button>
                                        </span>
                                        <input class="form-control rounded-0 order_discount_percent"  onkeyup="javascript: calc_order_price(event, <?php echo $order['id_order'];?>, 'by_discount_percent');" title="Сумма дополнительной скидки, выраженная в процентах" type="text" name="order[discount_by_percent]" value="<?php echo $order['order_discount_by_percent'];?>" placeholder="0">
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-default btn-flat rounded-0">%</button>
                                        </span>
                                    </div>
                                </div>          
                            </div>
                            <div class="col-12 col-sm-6 col-lg-2">
                                <div class="form-group">
                                    <label class="text-gray custom-font-size-12">Товары на сумму</label>
                                    <div class="input-group input-group-sm">
                                        <input class="order_price" type="hidden" name="order[price]" value="<?php echo $order['order_price'];?>" placeholder="">
                                        <input class="form-control rounded-0 order_final_price" onkeyup="javascript: calc_order_price(event, <?php echo $order['id_order'];?>, 'by_order_final_price');" type="text" name="order[price_final]" value="<?php echo $order['order_price_final'];?>" placeholder="">
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-default btn-flat rounded-0"><?php echo $order['order_currency'];?></button>
                                        </span>
                                    </div>
                                </div>          
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="form-group">
                                    <label class="text-gray custom-font-size-12">Сумма заказа</label>
                                    <div class="text-left custom-height-30 d-flex <?php echo get_choice('flex-column', $order['order_cashback'] > 0, 'align-items-center');?> flex-fill">
                                        <?php if($order['order_cashback'] > 0){?>
                                            <strong class="text-danger custom-line-height-14 custom-font-size-10">- <?php echo numberFormat($order['order_cashback'], $order['order_price_decimals']);?> <?php echo $order['order_currency'];?> Бонус</strong>
                                        <?php }?>
                                    </div>
                                </div>          
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="form-group">
                                    <label class="text-gray">Итого к оплате</label>
                                    <div class="text-left custom-line-height-24 custom-height-30 d-flex align-items-center text-success">
                                        <strong class="custom-font-size-20"><span class="order_final_price_for_payment_bonus"><?php echo numberFormat($order['order_price_final'] + $order['order_delivery_price'] - $order['order_cashback'], $order['order_price_decimals']);?></span> <?php echo $order['order_currency'];?></strong>
                                    </div>
                                </div>          
                            </div>
                            <div class="col-12">
                                <hr class="row-delimiter">
                            </div>
                        </div>
                    </div>

                    <?php foreach($ordered_items as $ordered_item){?>
                        <div class="row ordered_item-b custom-margin-bottom-10">
                            <div class="col-12">
                                <div class="ordered-item-container">
                                    <div class="ordered-item-image">
                                        <img src="<?php echo site_url(getImage('files/items/' . $ordered_item['item_photo']))?>" class="img-thumbnail">
                                    </div>
                                    <a class="ordered-item-link" href="<?php echo base_url('products/'.$ordered_item['item_url']);?>"><?php echo clean_output($ordered_item['item_title']);?></a>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row order_<?php echo $order['id_order'];?>_item_<?php echo $ordered_item['id_order_item'];?>">
                                    <div class="col-6 col-md-4 col-lg-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Артикул</label>
                                            <div class="d-flex flex-column custom-font-size-10">
                                                <div class="">
                                                    <?php echo $ordered_item['item_code'];?> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Количество</label>
                                            <input class="form-control form-control-sm rounded-0 quantity" onkeyup="javascript: change_ordered_item_price(event, <?php echo $order['id_order'];?>,<?php echo $ordered_item['id_order_item'];?>,'quantity');" type="text" name="order[ordered_items][<?php echo $ordered_item['id_order_item'];?>][quantity]" value="<?php echo $ordered_item['item_quantity'];?>" placeholder="1">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Цена, <?php echo $ordered_item['item_currency'];?></label>
                                            <div class="form-control form-control-sm rounded-0">
                                                <span class="price" data-default-price="<?php echo $ordered_item['item_price'];?>"><?php echo $ordered_item['item_price'];?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Скидка, %</label>
                                            <input class="form-control form-control-sm rounded-0 discount" data-default-discount="<?php echo $ordered_item['item_discount'];?>" onkeyup="javascript: change_ordered_item_price(event, <?php echo $order['id_order'];?>,<?php echo $ordered_item['id_order_item'];?>,'discount');" type="text" name="order[ordered_items][<?php echo $ordered_item['id_order_item'];?>][discount]" value="<?php echo $ordered_item['item_discount'];?>" placeholder="0" title="Скидка на товар, выраженная в процентах">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Цена со скидкой</label>
                                            <input class="form-control form-control-sm rounded-0 final_price" data-default-final-price="<?php echo $ordered_item['item_price_final'];?>" onkeyup="javascript: change_ordered_item_price(event, <?php echo $order['id_order'];?>,<?php echo $ordered_item['id_order_item'];?>,'final_price');" type="text" name="order[ordered_items][<?php echo $ordered_item['id_order_item'];?>][price_final]" value="<?php echo $ordered_item['item_price_final'];?>" placeholder="0.00">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-2">
                                        <div class="form-group">
                                            <label class="custom-font-size-10 text-gray custom-margin-bottom-0">Подарок</label>
                                            <div class="custom-padding-5">
                                                <?php if(isset($ordered_item['item_cashback_price']) && $ordered_item['item_cashback_price'] > 0){?>
                                                    <span class="badge bg-danger"><?php echo $ordered_item['item_cashback_price'];?> <?php echo $ordered_item['item_currency'];?></span>
                                                <?php } else{?>
                                                    &mdash;
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>               
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label class="text-gray custom-font-size-12">Коментарий клиента</label>
                            <textarea name="order[user_comment]" rows="3" class="form-control rounded-0 custom-font-size-12"><?php echo $order['order_user_comment'];?></textarea>
                        </div>          
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label class="text-gray custom-font-size-12">Коментарий менеджера</label>
                            <textarea name="order[admin_comment]" rows="3" class="form-control rounded-0 custom-font-size-12"><?php echo $order['order_admin_comment'];?></textarea>
                        </div>          
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="notify_user" type="checkbox" id="js-checkbox-notify-buyer">
                    <label for="js-checkbox-notify-buyer" class="custom-font-size-12">Уведомить клиента<span class="d-none d-sm-inline"> об изминениях</span></label>
                </div>
                <div class="d-flex">
                    <input type="hidden" name="order[id_order]" value="<?php echo $order['id_order'];?>">
                    <button type="button" data-callback="updateOrder" data-close-popup="0" class="btn btn-primary btn-flat call-function custom-margin-right-10"><i class="fad fa-check"></i> Сохранить</button>
                    <button type="button" data-callback="updateOrder" data-close-popup="1" class="btn btn-success btn-flat call-function"><i class="fad fa-check"></i> Сохранить и закрыть</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    dtTable.fnDraw(false);

    // var manage_form = $('#manage_form');
    var updateOrder = function(element){
        var $this = $(element);
        var manage_form = $('#manage_form');
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/orders/ajax_operations/change',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    if($this.data('close-popup') == 1){
                        $popup_parent.modal('hide');
                    }

                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    }

	function change_ordered_item_price(event, order, item, changed_input){
		if ((typeof(event) == 'object') && (event != null)) {
			if ('keyCode' in event) {
				if ((event.keyCode < 32) && (event.keyCode != 8)) 
					return true;

				if ((event.keyCode == 37) || (event.keyCode == 38) || (event.keyCode == 39) || (event.keyCode == 40)) 
					return true;
			}
		}

		var $parent = $('.order_'+order+'_item_'+item);
		if($parent.length == 0){
			systemMessages('Ошибка!', 'error');
		}

		var $order_parent = $('.order_'+order+'_detail');
		if($order_parent.length == 0){
			systemMessages('Ошибка!', 'error');
		}

		var quantity = intval($parent.find('.quantity').val());
		var default_discount = floatval($parent.find('.discount').data('default-discount')) / 100;
		var default_price = floatval($parent.find('.price').data('default-price'));
		var default_price_final = floatval($parent.find('.final_price').data('default-final-price'));
		switch (changed_input) {
			case 'quantity':
				var discount = floatval($parent.find('.discount').val())/100;
				var final_price = floatval((default_price - (default_price * discount)) * quantity).toFixed(2);
				$parent.find('.final_price').val(final_price);				
			break;
			case 'discount':
				var discount = floatval($parent.find('.discount').val())/100;
                var final_price = floatval((default_price - (default_price * discount)) * quantity).toFixed(2);
				$parent.find('.final_price').val(final_price);
			break;
			case 'final_price':
				var final_price = floatval($parent.find('.final_price').val());
				var temp_price = default_price*quantity;
				var discount = (100 - final_price*100/temp_price).toFixed(2);			
				$parent.find('.discount').val(discount);
			break;
		}

		calc_order_price(event, order, 'by_items');
	}

	function calc_order_price(event, order, action){
		if ((typeof(event) == 'object') && (event != null)) {
			if ('keyCode' in event) {
				if ((event.keyCode < 32) && (event.keyCode != 8)) 
					return true;

				if ((event.keyCode == 37) || (event.keyCode == 38) || (event.keyCode == 39) || (event.keyCode == 40)) 
					return true;
			}
		}

		var $parent = $('.order_'+order+'_detail');
		if($parent.length == 0){
			systemMessages('Ошибка!', 'error');
		}		

		var lock_order_discount = false;
		var order_final_price = 0;
		var order_no_discount_price = 0;
		var $items = $parent.find('.ordered_item-b');
		var order_cashback = floatval($parent.find('.order_cashback').val());

		switch (action) {
			case 'by_items':
				$items.each(function( index ) {
					var discount = floatval($(this).find('.discount').val()) / 100;
					var quantity = intval($(this).find('.quantity').val());
					if(discount > 0 && $(this).find('.discount').prop('readonly') == false){
						lock_order_discount = true;
					}
					
					order_final_price += floatval($(this).find('.final_price').val());
                    order_no_discount_price += floatval($(this).find('.price').data('default-price')) * quantity;
				});

				var order_total_discount_percents_raw = order_no_discount_price > 0 ? 100 - (order_final_price*100/order_no_discount_price) : 0;
				var order_total_discount_percents = order_total_discount_percents_raw.toFixed(2);
				var order_total_discount_price = (order_no_discount_price - order_final_price).toFixed(2);
				if(lock_order_discount){
					$parent.find('.order_discount_price').prop('readonly', true).val(order_total_discount_price);
					$parent.find('.order_discount_percent').prop('readonly', true).val(order_total_discount_percents);
					$items.find('.discount').prop('readonly', false);
				} else{
					$parent.find('.order_discount_price').prop('readonly', false).val(order_total_discount_price);
					$parent.find('.order_discount_percent').prop('readonly', false).val(order_total_discount_percents);
					if(order_total_discount_price > 0){
						$items.find('.discount').prop('readonly', true);
					}
                }
                
				order_final_price = order_final_price.toFixed(2);
				$parent.find('.order_final_price').val(order_final_price);
				$parent.find('.order_price').val(order_no_discount_price.toFixed(2));
			break;
			case 'by_order_final_price':
				order_final_price = floatval($parent.find('.order_final_price').val());
				$items.each(function( index ) {
					var quantity = intval($(this).find('.quantity').val());
					order_no_discount_price += floatval($(this).find('.price').data('default-price')) * quantity;
				});

				var order_total_discount_percents_raw = 100 - (order_final_price*100/order_no_discount_price);
				var order_total_discount_percents = order_total_discount_percents_raw.toFixed(2);
				var order_total_discount_price = (order_no_discount_price - order_final_price).toFixed(2);
				$parent.find('.order_discount_price').prop('readonly', false).val(order_total_discount_price);
				$parent.find('.order_discount_percent').prop('readonly', false).val(order_total_discount_percents);
				if(order_total_discount_percents > 0){
					$items.find('.discount').prop('readonly', true).val(order_total_discount_percents);
				} else{
					$items.find('.discount').prop('readonly', false).val(order_total_discount_percents);
				}

				$items.each(function( index ) {
					var quantity = intval($(this).find('.quantity').val());
					var discount = order_total_discount_percents_raw/100;
                    var default_price = floatval($(this).find('.price').data('default-price'));
                    console.log(default_price,discount,quantity);
					var final_price = floatval((default_price - (default_price * discount)) * quantity).toFixed(2);
					$(this).find('.final_price').val(final_price);
				});
			break;
			case 'by_discount_price':
				var order_discount_price = floatval($parent.find('.order_discount_price').val());
				$items.each(function( index ) {
					var quantity = intval($(this).find('.quantity').val());
					order_no_discount_price += floatval($(this).find('.price').data('default-price')) * quantity;
				});

				order_final_price = (order_no_discount_price - order_discount_price).toFixed(2);
				$parent.find('.order_final_price').val(order_final_price);

                var order_total_discount_percents_raw = 100 - (order_final_price*100/order_no_discount_price);
				var order_total_discount_percents = order_total_discount_percents_raw.toFixed(2);
				$parent.find('.order_discount_percent').prop('readonly', false).val(order_total_discount_percents);
				if(order_total_discount_percents > 0){
					$items.find('.discount').prop('readonly', true).val(order_total_discount_percents);
				} else{
					$items.find('.discount').prop('readonly', false).val(order_total_discount_percents);
				}

				$items.each(function( index ) {
					var quantity = intval($(this).find('.quantity').val());
					var discount = order_total_discount_percents_raw/100;
					var default_price = floatval($(this).find('.price').data('default-price'));
					var final_price = floatval((default_price - (default_price * discount)) * quantity).toFixed(2);
					$(this).find('.final_price').val(final_price);
				});
			break;
			case 'by_discount_percent':
				var order_discount_percent = floatval($parent.find('.order_discount_percent').val());
				$items.each(function( index ) {
					var quantity = intval($(this).find('.quantity').val());
					order_no_discount_price += floatval($(this).find('.price').data('default-price')) * quantity;
				});
				
				var order_total_discount_price = (order_discount_percent*order_no_discount_price/100).toFixed(2);

				order_final_price = (order_no_discount_price - order_total_discount_price).toFixed(2);
				$parent.find('.order_final_price').val(order_final_price);

                $parent.find('.order_discount_price').prop('readonly', false).val(order_total_discount_price);
				if(order_discount_percent > 0){
					$items.find('.discount').prop('readonly', true).val(order_discount_percent);
				} else{
					$items.find('.discount').prop('readonly', false).val(order_discount_percent);
				}

				$items.each(function( index ) {
					var quantity = intval($(this).find('.quantity').val());
					var default_price = floatval($(this).find('.price').data('default-price'));
					var final_price = floatval((default_price - (default_price * order_discount_percent / 100)) * quantity).toFixed(2);
					$(this).find('.final_price').val(final_price);
				});
			break;
		}

		var order_final_price_for_payment_bonus = (floatval(order_final_price) + delivery_price - order_cashback).toFixed(2);
		$parent.find('.order_final_price_for_payment_bonus').text(order_final_price_for_payment_bonus);
    }

	function confirm_change_status($this){
		var order_status = $this.data('status');
		var color = $this.data('color');
		var text = $this.data('text');
		var icon = $this.data('icon');
		$this.closest('.form-group').find('input[name="order[status]"]').val(order_status);
		var template = '<button class="btn btn-'+ color +' btn-flat btn-sm custom-width-100pr text-truncate dropdown-toggle" type="button" id="js-dropdown-order-status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                            <i class="'+ icon +'"></i> '+ text +'\
                        </button>';
		$this.closest('.dropdown').find('.dropdown-toggle').replaceWith(template);
		$this.closest('.dropdown').find('.dropdown-toggle').dropdown('hide');
	}

    var callClient = function(element){
        var $this =$(element);
        var phone = $this.data('phone');

        if(phone == ''){
            systemMessages('Номер телефона не заполнен.', 'info');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: base_url+'notify/ajax_operations/callClient',
            data: {phone:phone},
            dataType: 'JSON',
			beforeSend: function(){
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
            }
        });

        return false;
    }
</script>
