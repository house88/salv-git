<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/users/groups/';
		$this->active_menu = 'users';
		$this->active_submenu = 'groups';

        $this->data = array();
        
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("User_model", 'user_model');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        checkPermision('manage_groups');

		$this->data['page_header'] = 'Группы пользователей';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('page'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_groups');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'edit':
				checkPermisionAjax('manage_groups');

				$id_group = (int) $this->uri->segment(5);
                $this->data['group'] = $this->user_model->handler_get_group($id_group);
				if(empty($this->data['group'])){
					jsonResponse('Группа не найдена.');
				}

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
            case 'add':
                checkPermisionAjax('manage_groups');

				$this->form_validation->set_rules('group_name', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('group_type', 'Тип', 'required');
				$this->form_validation->set_rules('price_variant', 'Ценовая категория', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                list($price_variant_type, $id_price_variant) = explode('_', $this->input->post('price_variant'));
				$insert = array(
					'group_name' => $this->input->post('group_name'),
					'group_type' => $this->input->post('group_type'),
                    'group_price_variant' => $id_price_variant,
                    'group_price_type' => $price_variant_type,
                    'group_view_decimals' => ($this->input->post('group_view_decimals')) ? 1 : 0,
                    'group_view_stocks' => 0,
                    'group_view_suppliers' => 0,
                    'group_view_cashback' => ($this->input->post('group_view_cashback')) ? 1 : 0
                );
                
                $stocks_settings = array();
                if ($this->input->post('group_view_stocks')) {
                    $stocks_info = $this->input->post('stock_info');
                    if(!empty($stocks_info)){
                        foreach ($stocks_info as $key => $stock_info) {
                            if(isset($stock_info['quantity_from'], $stock_info['quantity_to'],$stock_info['lines'],$stock_info['color'])){
                                $stocks_settings[] = array(
                                    'from' => (int) $stock_info['quantity_from'],
                                    'to' => (int) $stock_info['quantity_to'],
                                    'lines' => (int) $stock_info['lines'],
                                    'color' => check_valid_colorhex($stock_info['color']) ? $stock_info['color'] : '#000000'
                                );
                            }
                        }
                    }

                    $insert['group_view_stocks'] = 1;
                    if (empty($stocks_settings)) {
                        jsonResponse('Добавьте хотябы одну настройку для количества по складам.');
                    }
                }

                $insert['group_view_stocks_settings'] = json_encode($stocks_settings);
                
                $suppliers_settings = array();
                if ($this->input->post('group_view_suppliers')) {
                    $suppliers_info = $this->input->post('suppliers_info');
                    if(!empty($suppliers_info)){
                        foreach ($suppliers_info as $key => $supplier_info) {
                            if(isset($supplier_info['quantity_from'], $supplier_info['quantity_to'],$supplier_info['lines'],$supplier_info['color'])){
                                $suppliers_settings[] = array(
                                    'from' => (int) $supplier_info['quantity_from'],
                                    'to' => (int) $supplier_info['quantity_to'],
                                    'lines' => (int) $supplier_info['lines'],
                                    'color' => check_valid_colorhex($supplier_info['color']) ? $supplier_info['color'] : '#000000'
                                );
                            }
                        }
                    }

                    $insert['group_view_suppliers'] = 1;
                    if (empty($suppliers_settings)) {
                        jsonResponse('Добавьте хотябы одну настройку для количества по поставщикам.');
                    }
                }

                $insert['group_view_suppliers_settings'] = json_encode($suppliers_settings);

				$this->user_model->handler_insert_group($insert);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'edit':
                checkPermisionAjax('manage_groups');

                $this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');
				$this->form_validation->set_rules('group_name', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('group_type', 'Тип', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                list($price_variant_type, $id_price_variant) = explode('_', $this->input->post('price_variant'));
				$id_group = (int)$this->input->post('group');
				$update = array(
					'group_name' => $this->input->post('group_name'),
					'group_type' => $this->input->post('group_type'),
                    'group_price_variant' => $id_price_variant,
                    'group_price_type' => $price_variant_type,
                    'group_view_decimals' => ($this->input->post('group_view_decimals'))?1:0,
				    'group_view_stocks' => 0,
                    'group_view_suppliers' => 0,
                    'group_view_cashback' => ($this->input->post('group_view_cashback'))?1:0
                );
                
                $stocks_settings = array();
                if ($this->input->post('group_view_stocks')) {
                    $stocks_info = $this->input->post('stock_info');
                    if(!empty($stocks_info)){
                        foreach ($stocks_info as $key => $stock_info) {
                            if(isset($stock_info['quantity_from'], $stock_info['quantity_to'],$stock_info['lines'],$stock_info['color'])){
                                $stocks_settings[] = array(
                                    'from' => (int) $stock_info['quantity_from'],
                                    'to' => (int) $stock_info['quantity_to'],
                                    'lines' => (int) $stock_info['lines'],
                                    'color' => check_valid_colorhex($stock_info['color']) ? $stock_info['color'] : '#000000'
                                );
                            }
                        }
                    }

                    $update['group_view_stocks'] = 1;
                    if (empty($stocks_settings)) {
                        jsonResponse('Добавьте хотябы одну настройку для количества по складам.');
                    }
                }

                $update['group_view_stocks_settings'] = json_encode($stocks_settings);
                
                $suppliers_settings = array();
                if ($this->input->post('group_view_suppliers')) {
                    $suppliers_info = $this->input->post('suppliers_info');
                    if(!empty($suppliers_info)){
                        foreach ($suppliers_info as $key => $supplier_info) {
                            if(isset($supplier_info['quantity_from'], $supplier_info['quantity_to'],$supplier_info['lines'],$supplier_info['color'])){
                                $suppliers_settings[] = array(
                                    'from' => (int) $supplier_info['quantity_from'],
                                    'to' => (int) $supplier_info['quantity_to'],
                                    'lines' => (int) $supplier_info['lines'],
                                    'color' => check_valid_colorhex($supplier_info['color']) ? $supplier_info['color'] : '#000000'
                                );
                            }
                        }
                    }

                    $update['group_view_suppliers'] = 1;
                    if (empty($suppliers_settings)) {
                        jsonResponse('Добавьте хотябы одну настройку для количества по поставщикам.');
                    }
                }

                $update['group_view_suppliers_settings'] = json_encode($suppliers_settings);

				$this->user_model->handler_update_group($id_group, $update);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'delete':
                checkPermisionAjax('manage_groups');

                $this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_group = (int) $this->input->post('group');
                if($this->user_model->handler_get_group_rights($id_group)){
				    jsonResponse('Удалите все связи с правами для данной группы.');
                }
                
                if($this->user_model->handler_get_group_users($id_group)){
				    jsonResponse('Удалите все связи с пользователями для данной группы.');
                }

                $this->user_model->handler_delete_group($id_group);
                
				jsonResponse('Удалено.', 'success');
            break;
            case 'list':
                checkPermisionAjaxDT('manage_groups');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength', true),
					'start' => (int) $this->input->post('iDisplayStart', true)
				);
				
				$sort_by = flat_dt_ordering($_POST, array(
					'dt_id'	    => 'id_group',
					'dt_name'	=> 'group_name',
					'dt_type' 	=> 'group_type'
				));
				if(!empty($sort_by)){
					$params['sort_by'] = $sort_by;
				}
		
				$records = $this->user_model->handler_get_groups_all($params);
                $records_total = $this->user_model->handler_get_groups_count($params);

                $group_types = array(
                    'admin' => array(
                        'title' => 'Администратор'
                    ),
                    'moderator' => array(
                        'title' => 'Модератор'
                    ),
                    'content_menedger' => array(
                        'title' => 'Контент менеджер'
                    ),
                    'user' => array(
                        'title' => 'Пользователь'
                    )
                );

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array_map(function($record) use ($group_types){
                        return array(
							'dt_id'		    => $record['id_group'],
							'dt_name'		=> $record['group_name'],
							'dt_type'		=> $group_types[$record['group_type']]['title'],
                            'dt_actions'	=> '<div class="dropdown">
                                                    <a data-toggle="dropdown" href="#" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
                                                        <a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/groups/popup/edit/' . $record['id_group']).'">
                                                            <i class="fad fa-pencil"></i> Редактировать
                                                        </a>
                                                        <div class="dropdown-divider"></div>
                                                        <a href="#" class="dropdown-item confirm-dialog" data-message="Вы уверены что хотите удалить группу?" data-title="<i class=\'fad fa-trash-alt\'></i> Удалить" data-callback="delete_action" data-group="'.$record['id_group'].'" data-type="bg-danger">
                                                            <i class="fad fa-trash-alt"></i> Удалить
                                                        </a>
                                                    </div>
                                                </div>'
						);
					}, $records)
				);

				jsonResponse('', 'success', $output);
			break;
		}
	}
}
