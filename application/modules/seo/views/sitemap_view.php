<?php echo '<?xml version="1.0" encoding="UTF-8" ?>';?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo base_url();?></loc>
        <priority>1.0</priority>
    </url>
    <url>
        <loc><?php echo base_url('spage/delivery-menu');?></loc>
        <priority>1.0</priority>
    </url>
    <url>
        <loc><?php echo base_url('catalog_mode/newest');?></loc>
        <priority>1.0</priority>
    </url>
    <url>
        <loc><?php echo base_url('catalog_mode/actional');?></loc>
        <priority>1.0</priority>
    </url>
    <url>
        <loc><?php echo base_url('spage/contacts-menu');?></loc>
        <priority>1.0</priority>
    </url>

    <?php foreach($categories as $category){?>
        <?php $category_url = (!empty($category['category_special_url']))?$category['category_special_url']:'catalog/'.$category['category_url'];?>
        <url>
            <loc><?php echo base_url($category_url);?></loc>
            <lastmod><?php echo getDateFormat($category['category_last_update'], 'Y-m-d H:i:s' ,'Y-m-d');?></lastmod>
        </url>
    <?php }?>

    
    <?php $pages = ceil($products_count / $limit);?>
    <?php for ($page=1; $page <= $pages; $page++) {?>
        <?php 
            $params = array(
                'columns' =>'id_item,item_url,item_last_update',
                'limit' => $limit,
                'start' => ($page - 1) * $limit
            );
            $products = Modules::run('items/_get_all', $params);
        ?>
        <?php foreach ($products as $product) {?>
            <url>
                <loc><?php echo base_url('products/'.$product['item_url']);?></loc>
                <lastmod><?php echo getDateFormat($product['item_last_update'], 'Y-m-d H:i:s','Y-m-d');?></lastmod>
            </url>
        <?php }?>
    <?php }?>
</urlset>