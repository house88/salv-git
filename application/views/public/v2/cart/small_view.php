<?php if(!empty($items)){?>
	<div class="cart-sidebar-header">
		<h5>
			Cosul meu <span class="text-info"><?php echo (int) $cartProductsCounter;?> produs(e)</span> 
			<a class="float-right call-function" data-callback="cartToogle">
				<i class="icofont icofont-close-line"></i>
			</a>
		</h5>
	</div>
    <div class="cart-sidebar-body">
        <?php foreach($items as $item){?>
            <!-- CART ITEM - START -->
            <?php $productPrice = getPrice($item);?>
            <div class="cart-list-product">
                <a class="float-right remove-cart call-function" data-callback="removeCartProduct" data-item="<?php echo $item['id_item'];?>" href=""><i class="icofont icofont-close-circled"></i></a>
                <img class="img-fluid" src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$item['item_photo']));?>" alt="">
                <div class="cart-list-product__details">
                    <h5><a href="<?php echo base_url('products/'.$item['item_url']);?>"><?php echo clean_output($item['item_title']);?></a></h5>
                    <p class="f-14 mb-0 text-dark float-right">
                        <?php echo $productPrice['display_price'] . ' <span class="small text-secondary">' . $productPrice['currency_symbol'] . ' </span>';?>
                    </p>
                    <span class="count-number float-left">
                        <button class="btn btn-outline-secondary btn-sm left dec call-function" data-callback="updateCartDecreaseQuantity" data-item="<?php echo $item['id_item'];?>" data-field="#js-product-<?php echo $item['id_item'];?>-cart-quantity-input"> <i class="icofont-minus"></i> </button>
                        <input class="count-number-input" id="js-product-<?php echo $item['id_item'];?>-cart-quantity-input" type="text" value="<?php echo (int) $item['item_cart_qty'];?>" readonly="">
                        <button class="btn btn-outline-secondary btn-sm right inc call-function" data-callback="updateCartIncreaseQuantity" data-item="<?php echo $item['id_item'];?>" data-field="#js-product-<?php echo $item['id_item'];?>-cart-quantity-input"> <i class="icofont-plus"></i> </button>
                    </span>
                </div>
            </div>
            <!-- CART ITEM - END -->
        <?php }?>
    </div>
    <div class="cart-sidebar-footer">
        <button class="btn btn-primary btn-lg btn-block text-left call-popup" data-popup="#general_popup_form" data-href="<?php echo site_url('orders/popup/create')?>" type="button">
            <span class="float-left"><i class="icofont icofont-cart"></i> Plaseaza comanda </span>
            <span class="float-right">
                <strong><?php echo $cartAmount;?></strong> 
                <span class="icofont icofont-bubble-right"></span>
            </span>
        </button>
    </div>
<?php } else{?>
	<div class="cart-sidebar-header">
		<h5>
			Cosul meu <span class="text-info"><?php echo (int) $cartProductsCounter;?> produs(e)</span> 
			<a class="float-right call-function" data-callback="cartToogle">
				<i class="icofont icofont-close-line"></i>
			</a>
		</h5>
	</div>
    <div class="cart-sidebar-body cart-sidebar-body-empty">
        <div class="jumbotron">
            <h1 class="display-4">Ooops!</h1>
            <p class="lead">Cosul este gol :(</p>
            <hr class="my-4">
            <p>Vă rugăm sa adaugați cel puțin un produs în coș înainte de a plasa o comandă.</p>
        </div>
    </div>
<?php }?>