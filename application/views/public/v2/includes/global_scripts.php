<?php if(!empty($main_scripts)){?>
	<?php $this->load->view($main_scripts);?>
<?php }?>

<?php if(!empty($flash_message)){?>
	<script>
		var flash_message = JSON.parse('<?php echo $flash_message;?>');
		systemMessages(flash_message.message, flash_message.type);
	</script>
<?php }?>

<div class="system-text-messages-b">	
	<div class="text-b">
		<ul>
			<?php if(!empty($system_messages)){echo $system_messages;}?>
		</ul>
	</div>
</div>

<div class="modal fade" id="general_popup_form" data-keyboard="false" data-backdrop="static"></div>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo file_modification_time($this->theme->public_assets('vendor/jquery/jquery.min.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->public_assets('vendor/bootstrap/js/bootstrap.bundle.min.js'));?>"></script>
<!-- select2 Js -->
<script src="<?php echo file_modification_time($this->theme->public_assets('vendor/select2/js/select2.min.js'));?>"></script>
<!-- Owl Carousel -->
<script src="<?php echo file_modification_time($this->theme->public_assets('vendor/owl-carousel/owl.carousel.js'));?>"></script>
<!-- Slider Js -->
<script src="<?php echo file_modification_time($this->theme->public_assets('vendor/slider/slider.js'));?>"></script>
<!-- Custom scripts for all pages-->
<script src="<?php echo file_modification_time($this->theme->public_assets('js/custom.js'));?>"></script>