<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['apanel_theme'] = 'apanel/adminlte3/';
$config['apanel_assets'] = 'theme/apanel/adminlte3/';
$config['public_theme'] = 'public/v2/';
$config['public_assets'] = 'theme/public/salv/';
