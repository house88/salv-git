<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-body">
            <div class="login-modal">
                <div class="row">
                    <div class="col-lg-5 d-flex align-items-center">
                        <div class="login-modal-left text-center">
                            <img src="<?php echo site_url($this->theme->public_assets('img/login.jpg'));?>" alt="Login">
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <button type="button" class="close close-top-right position-absolute" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="icofont-close-line"></i></span>
                            <span class="sr-only">Închide</span>
                        </button>
                        <div class="position-relative">
                            <ul class="mt-4 mr-4 nav nav-tabs-login float-right position-absolute" role="tablist">
                                <li>
                                    <a class="nav-link-login active" data-toggle="tab" href="#login-form" role="tab"><i class="icofont-ui-lock"></i> Intră in cont</a>
                                </li>
                                <li>
                                    <a class="nav-link-login" data-toggle="tab" href="#register" role="tab"><i class="icofont icofont-pencil"></i> Înregistrare</a>
                                </li>
                            </ul>
                            <div class="login-modal-right p-4">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="login-form" role="tabpanel">
                                        <form id="js-popup-signin-form">
                                            <h5 class="heading-design-h5 text-dark">INTRĂ ÎN CONT</h5>
                                            <fieldset class="form-group mt-4">
                                                <label>Email</label>
                                                <input name="user_email" type="text" class="form-control" placeholder="ex. example@example.com">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Parola</label>
                                                <input name="password" type="password" class="form-control" placeholder="********">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn btn-lg btn-primary btn-block">Intră în cont</button>
                                            </fieldset>
                                            <div class="login-with-sites mt-4">
                                                <label class="label-line"><span>sau folosește</span></label>
                                                <div class="row text-center mt-4">
                                                    <div class="col-12">
                                                        <button class="btn-facebook btn-block login-icons btn-lg">
                                                            <i class="icofont icofont-facebook"></i> Facebook
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="register" role="tabpanel">
                                        <form id="js-popup-register-form">
                                            <h5 class="heading-design-h5 text-dark">ÎNREGISTRARE</h5>
                                            <fieldset class="form-group mt-4">
                                                <label>Cum sa ne adresăm Dvs.?</label>
                                                <input type="text" name="username" class="form-control" placeholder="ex. Nume">
                                            </fieldset>
                                            <fieldset class="form-group mt-4">
                                                <label>Email</label>
                                                <input type="text" name="email" class="form-control" placeholder="ex. example@example.com">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Parola</label>
                                                <input type="password" name="password" class="form-control" placeholder="********">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Confirmă Parola</label>
                                                <input type="password" name="confirm_password" class="form-control" placeholder="********">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn btn-lg btn-primary btn-block">Înregistrare</button>
                                            </fieldset>
                                            <div class="login-with-sites mt-4">
                                                <label class="label-line"><span>sau folosește</span></label>
                                                <div class="row text-center mt-4">
                                                    <div class="col-12">
                                                        <button class="btn-facebook btn-block login-icons btn-lg">
                                                            <i class="icofont icofont-facebook"></i> Facebook
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(function(){
        $('#js-popup-signin-form').on('submit', function(e){
            var $form = $(this);
            var fdata = $form.serialize();

            $.ajax({
				type: 'POST',
				url: base_url+'auth/ajax_operations/signin',
				data: fdata,
				dataType: 'JSON',
                beforeSend: function(){
                    showLoader('body');
                },
				success: function(resp){
					if(resp.mess_type == 'success'){
                        window.location.href = base_url + resp.redirect;
					} else{
						systemMessages(resp.message, resp.mess_type);
                        hideLoader('body');
					}
				}
			});
            return false;
        });

        $('#js-popup-register-form').on('submit', function(e){
            var $form = $(this);
            var fdata = $form.serialize();

            $.ajax({
				type: 'POST',
				url: base_url+'auth/ajax_operations/signup',
				data: fdata,
				dataType: 'JSON',
                beforeSend: function(){
                    showLoader('body');
                },
				success: function(resp){
                    systemMessages(resp.message, resp.mess_type);
					hideLoader('body');
                    if(resp.mess_type == 'success'){
                        $form[0].reset();
					}
				}
			});
            return false;
        });
    });

    var fb_login = function(obj){
        var $this = $(obj);

        FB.login(function(response) {
            if (response.authResponse) {
                statusChangeCallback(response);
            }
        }, {scope: 'email,public_profile', return_scopes: true});
    }

    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            $.ajax({
                type: 'POST',
                url: base_url+'auth/ajax_operations/fb_signin',
                data: response,
                dataType: 'JSON',
                beforeSend: function(){
                    showLoader('body');
                },
                success: function(resp){
                    if(resp.mess_type == 'success'){
                        location.reload(true);
                    } else{
                        systemMessages(resp.message, resp.mess_type);
                        hideLoader('body');
                    }
                }
            });
        } else{
            hideLoader('body');
        }
    }

    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1089252241573724',
            autoLogAppEvents : false,
            cookie     : true,  // enable cookies to allow the server to access the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v3.0' // use graph api version 2.12
        });
    };

    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/ru_RU/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>