<p style="font: 16px Arial, sans-serif; color: #555555;margin:0 0 10px 0;padding:0;">Bine v-am găsit,</p>
<p style="font: 16px Arial, sans-serif; color: #555555;margin:0 0 10px 0;padding:0;">
    Vă mulțumim pentru inregistrare pe <a href="<?php echo site_url();?>">salv.md</a>
</p>
<p style="font: 16px Arial, sans-serif; color: #555555;margin:0 0 10px 0;padding:0;">
    Pentru a activa profilul vă rugăm să apăsați <a href="<?php echo base_url('activate/'.@$activation_key);?>" style="color:#555555;">aici</a>.
</p>
<p style="font: 12px Arial, sans-serif; color: #555555;text-align:center;">
    (Dacă link-ul de mai sus nu lucrează, copiati linkul de mai jos:) <br>
    <?php echo base_url('activate/'.@$activation_key);?>
</p>