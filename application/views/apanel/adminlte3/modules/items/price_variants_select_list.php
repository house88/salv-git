<?php if(!empty($price_variants)){?>
    <optgroup label="Статические ценовые категорий">
    <?php foreach ($price_variants as $price_variant) {?>
        <option value="static_<?php echo $price_variant['id_price_variant'];?>" <?php echo ((!empty($price_type) && $price_type == 'static')?set_select('price_variant', $price_type.'_'.$price_variant['id_price_variant'], $price_variant['id_price_variant'] == $selected_price_variant):'');?>><?php echo $price_variant['price_variant_title'];?></option>		
    <?php }?>
    </optgroup>
<?php }?>
<?php if(!empty($dynamic_price_variants)){?>
    <optgroup label="Динамические ценовые категорий">
    <?php foreach ($dynamic_price_variants as $dynamic_price_variant) {?>
        <option value="dynamic_<?php echo $dynamic_price_variant['id_dynamic_price'];?>" <?php echo ((!empty($price_type) && $price_type == 'dynamic')?set_select('price_variant', $price_type.'_'.$dynamic_price_variant['id_dynamic_price'], $dynamic_price_variant['id_dynamic_price'] == $selected_price_variant):'');?>><?php echo $dynamic_price_variant['dinamic_price_title'];?></option>
    <?php }?>
    </optgroup>
<?php }?>