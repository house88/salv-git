<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model("menu_model");
        $this->data = array();
	}
	
	function index(){
		$this->load->view("menu", $this->data);
	}

	function _get_apanel_menu($menu_params = array()){
		return $this->menu_model->handler_apanel_menu();
	}

	function _get_by_key($menu_key = null){
		if(empty($menu_key)){
			return array();
		}

		$menu = $this->menu_model->handler_get_by_title($menu_key);
		return !empty($menu['menu_elements']) ? json_decode($menu['menu_elements'], true) : array();
	}	
}
