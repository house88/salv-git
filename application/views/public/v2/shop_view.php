<!DOCTYPE html>
<html lang="ru">
<head>
    <?php $this->load->view($this->theme->public_view('includes/head')); ?>
</head>
<body class="bg-grey sticky">
    <?php $this->load->view($this->theme->public_view('includes/snowflake')); ?>
    
	<div class="dimmer-parent">
		<div class="page-wrap">
            <?php $this->load->view($this->theme->public_view('includes/header')); ?>
            
			<section id="wr-body">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
                            <?php $this->load->view($this->theme->public_view('includes/breadcrumbs')); ?>
						</div>
						<div class="col-xs-12">
							<div class="bg-white float-b">
								<?php $this->load->view($this->theme->public_view($main_content));?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
    	<?php $this->load->view($this->theme->public_view('includes/footer')); ?>
	</div>
</body>
</html>
