<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($category) ? 'Редактировать' : 'Добавить';?> категорию
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <?php $aditional_fields = json_decode($category['category_aditional_fields'], true);?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <th>От родителя</th>
                            <?php }?>
                            <th class="custom-width-100">Обязательно?</th>
                            <th class="custom-width-100">Вознаграждение</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="vam">Главная фото</td>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <td class="text-center vam">
                                    <?php if(!empty($parent_aditional_fields['item_photo']) && $parent_aditional_fields['item_photo']['active'] == 1){?>
                                        <i class="fad fa-check text-success"></i> <span class="badge"><?php echo numberFormat($parent_aditional_fields['item_photo']['price'], true);?> лей</span>
                                    <?php } else{?>
                                        <i class="fad fa-times text-danger"></i>
                                    <?php }?>
                                </td>
                            <?php }?>
                            <td class="text-center vam">
                                <div class="icheck-primary">
                                    <input name="edit_item[item_photo]" id="js-checkbox-item_photo-required" type="checkbox" <?php echo set_checkbox('edit_item[item_photo]', 1, @$aditional_fields['item_photo']['active'] == 1);?>>
                                    <label class="no-content" for="js-checkbox-item_photo-required"></label>
                                </div>
                            </td>
                            <td class="">
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price[item_photo]" value="<?php echo @$aditional_fields['item_photo']['price'];?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="vam">Дополнительные фото</td>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <td class="text-center vam">
                                    <?php if(!empty($parent_aditional_fields['item_photos']) && $parent_aditional_fields['item_photos']['active'] == 1){?>
                                        <?php echo (int) @$parent_aditional_fields['item_photos']['count'];?>, <span class="badge"><?php echo numberFormat($parent_aditional_fields['item_photos']['price'], true);?> лей</span>
                                    <?php } else{?>
                                        <i class="fad fa-times text-danger"></i>
                                    <?php }?>
                                </td>
                            <?php }?>
                            <td class="text-center vam">
                                <input class="form-control form-control-sm rounded-0 text-center" placeholder="Количество" name="edit_item[item_photos_count]" value="<?php echo @$aditional_fields['item_photos']['count'];?>">
                            </td>
                            <td class="">
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price[item_photos]" value="<?php echo @$aditional_fields['item_photos']['price'];?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="vam">Краткое описание</td>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <td class="text-center vam">
                                    <?php if(!empty($parent_aditional_fields['small_description']) && $parent_aditional_fields['small_description']['active'] == 1){?>
                                        <i class="fad fa-check text-success"></i> <span class="badge"><?php echo numberFormat($parent_aditional_fields['small_description']['price'], true);?> лей</span>
                                    <?php } else{?>
                                        <i class="fad fa-times text-danger"></i>
                                    <?php }?>
                                </td>
                            <?php }?>
                            <td class="text-center vam">
                                <div class="icheck-primary">
                                    <input name="edit_item[small_description]" id="js-checkbox-small_description-required" type="checkbox" <?php echo set_checkbox('edit_item[small_description]', 1, @$aditional_fields['small_description']['active'] == 1);?>>
                                    <label class="no-content" for="js-checkbox-small_description-required"></label>
                                </div>
                            </td>
                            <td class="">
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price[small_description]" value="<?php echo @$aditional_fields['small_description']['price'];?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="vam">Полное описание</td>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <td class="text-center vam">
                                    <?php if(!empty($parent_aditional_fields['full_description']) && $parent_aditional_fields['full_description']['active'] == 1){?>
                                        <i class="fad fa-check text-success"></i> <span class="badge"><?php echo numberFormat($parent_aditional_fields['full_description']['price'], true);?> лей</span>
                                    <?php } else{?>
                                        <i class="fad fa-times text-danger"></i>
                                    <?php }?>
                                </td>
                            <?php }?>
                            <td class="text-center vam">
                                <div class="icheck-primary">
                                    <input name="edit_item[full_description]" id="js-checkbox-full_description-required" type="checkbox" <?php echo set_checkbox('edit_item[full_description]', 1, @$aditional_fields['full_description']['active'] == 1);?>>
                                    <label class="no-content" for="js-checkbox-full_description-required"></label>
                                </div>
                            </td>
                            <td class="">
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price[full_description]" value="<?php echo @$aditional_fields['full_description']['price'];?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="vam">Видео</td>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <td class="text-center vam">
                                    <?php if(!empty($parent_aditional_fields['video']) && $parent_aditional_fields['video']['active'] == 1){?>
                                        <i class="fad fa-check text-success"></i> <span class="badge"><?php echo numberFormat($parent_aditional_fields['video']['price'], true);?> лей</span>
                                    <?php } else{?>
                                        <i class="fad fa-times text-danger"></i>
                                    <?php }?>
                                </td>
                            <?php }?>
                            <td class="text-center vam">
                                <div class="icheck-primary">
                                    <input name="edit_item[video]" id="js-checkbox-video-required" type="checkbox" <?php echo set_checkbox('edit_item[video]', 1, @$aditional_fields['video']['active'] == 1);?>>
                                    <label class="no-content" for="js-checkbox-video-required"></label>
                                </div>
                            </td>
                            <td class="">
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price[video]" value="<?php echo @$aditional_fields['video']['price'];?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="vam">Свойства товаров, по категориям</td>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <td class="text-center vam">
                                    <?php if(!empty($parent_aditional_fields['properties']) && $parent_aditional_fields['properties']['active'] == 1){?>
                                        <i class="fad fa-check text-success"></i> <span class="badge"><?php echo numberFormat($parent_aditional_fields['properties']['price'], true);?> лей</span>
                                    <?php } else{?>
                                        <i class="fad fa-times text-danger"></i>
                                    <?php }?>
                                </td>
                            <?php }?>
                            <td class="text-center vam">
                                <div class="icheck-primary">
                                    <input name="edit_item[properties]" id="js-checkbox-properties-required" type="checkbox" <?php echo set_checkbox('edit_item[properties]', 1, @$aditional_fields['properties']['active'] == 1);?>>
                                    <label class="no-content" for="js-checkbox-properties-required"></label>
                                </div>
                            </td>
                            <td class="">
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price[properties]" value="<?php echo @$aditional_fields['properties']['price'];?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="vam">SEO keywords</td>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <td class="text-center vam">
                                    <?php if(!empty($parent_aditional_fields['mk']) && $parent_aditional_fields['mk']['active'] == 1){?>
                                        <i class="fad fa-check text-success"></i> <span class="badge"><?php echo numberFormat($parent_aditional_fields['mk']['price'], true);?> лей</span>
                                    <?php } else{?>
                                        <i class="fad fa-times text-danger"></i>
                                    <?php }?>
                                </td>
                            <?php }?>
                            <td class="text-center vam">
                                <div class="icheck-primary">
                                    <input name="edit_item[mk]" id="js-checkbox-mk-required" type="checkbox" <?php echo set_checkbox('edit_item[mk]', 1, @$aditional_fields['mk']['active'] == 1);?>>
                                    <label class="no-content" for="js-checkbox-mk-required"></label>
                                </div>
                            </td>
                            <td class="">
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price[mk]" value="<?php echo @$aditional_fields['mk']['price'];?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="vam">SEO description</td>
                            <?php if(!empty($parent_aditional_fields)){?>
                                <td class="text-center vam">
                                    <?php if(!empty($parent_aditional_fields['md']) && $parent_aditional_fields['md']['active'] == 1){?>
                                        <i class="fad fa-check text-success"></i> <span class="badge"><?php echo numberFormat($parent_aditional_fields['md']['price'], true);?> лей</span>
                                    <?php } else{?>
                                        <i class="fad fa-times text-danger"></i>
                                    <?php }?>
                                </td>
                            <?php }?>
                            <td class="text-center vam">
                                <div class="icheck-primary">
                                    <input name="edit_item[md]" id="js-checkbox-md-required" type="checkbox" <?php echo set_checkbox('edit_item[md]', 1, @$aditional_fields['md']['active'] == 1);?>>
                                    <label class="no-content" for="js-checkbox-md-required"></label>
                                </div>
                            </td>
                            <td class="">
                                <input class="form-control form-control-sm rounded-0" placeholder="0.00" name="price[md]" value="<?php echo @$aditional_fields['md']['price'];?>">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="category_aditional_fields_children" type="checkbox" id="js-checkbox-children-active" <?php echo set_checkbox('category_aditional_fields_children', 1, (int) $category['category_aditional_fields_children'] === 1);?>>
                    <label for="js-checkbox-children-active">
                        Передать значения вложенным категориям
                    </label>
                </div>
                <div class="d-flex">
                    <input type="hidden" name="category" value="<?php echo $category['category_id'];?>">
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/categories/ajax_operations/product_options',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                }
            }
        });
        return false;
    });

    initTinyMce();
</script>
