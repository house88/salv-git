<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_model extends CI_Model{
	var $delivery = "delivery";
	var $payment_delivery_relation = "payment_delivery_relation";

	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->delivery, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_delivery, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_delivery', $id_delivery);
		$this->db->update($this->delivery, $data);
	}

	function handler_delete($id_delivery){
		$this->db->where('id_delivery', $id_delivery);
		$this->db->delete($this->delivery);
	}

	function handler_get($id_delivery){
		$this->db->where('id_delivery', $id_delivery);
		return $this->db->get($this->delivery)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_delivery ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('delivery_active', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->delivery)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('delivery_active', $active);
        }

		return $this->db->count_all_results($this->delivery);
	}

	function handler_insert_payments_relation($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert_batch($this->payment_delivery_relation, $data);
		return $this->db->insert_id();
	}

	function handler_delete_payments_relation($id_delivery){
		$this->db->where('id_delivery', $id_delivery);
		$this->db->delete($this->payment_delivery_relation);
	}

	function handler_get_payments_relation($id_delivery){
		$this->db->where('id_delivery', $id_delivery);

		$records = $this->db->get($this->payment_delivery_relation)->result_array();

		return !empty($records) ? $records : array();
	}
}
