<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends CI_Model{
    var $categories_table = "categories";

    function __construct(){
        parent::__construct();
    }
	
	function handler_insert($data = array()){
        if(empty($data)){
            return;
        }

        $this->db->insert($this->categories_table, $data);
        return $this->db->insert_id();
	}
	
	function handler_insert_batch($data = array()){
        if(empty($data)){
            return;
        }

        return $this->db->insert_batch($this->categories_table, $data);
	}

	function handler_update($id_category, $data = array()){
        if(empty($data)){
            return;
        }

        $this->db->where('category_id', $id_category);
        $this->db->update($this->categories_table, $data);
	}

	function handler_update_batch($data = array(), $key){
        if(empty($data)){
            return;
        }

        $this->db->update_batch($this->categories_table, $data, $key);
	}

	function handler_update_same_data($conditions = array(), $data = array()){
        if(empty($data)){
            return;
		}
		
		extract($conditions);

		if(isset($id_category)){
			$this->db->where_in('category_id', $id_category);
		}

        $this->db->update($this->categories_table, $data);
	}

	function handler_import($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert_on_duplicate_update_batch($this->categories_table, $data);
	}
	
	function handler_delete($id_category){
        $this->db->where('category_id', $id_category);
        $this->db->delete($this->categories_table);
	}

	function handler_get_by_title($category_title){
        $this->db->where('category_title', $category_title);
        $this->db->limit(1);
        return $this->db->get($this->categories_table)->row_array();
	}

	function handler_get_old_by_title($category_title){
        $this->db->where('name', $category_title);
        return $this->db->get('categories_old')->result_array();
	}

	function handler_get($id_category){
        $this->db->where('category_id', $id_category);
        return $this->db->get($this->categories_table)->row_array();
	}

	function handler_get_old($id_category){
        $this->db->where('category_id', $id_category);
        return $this->db->get('categories_old')->row_array();
	}

	function handler_get_additional_fields($category){
		$aditional_fields = array();
		if(!empty($category['category_aditional_fields'])){
			$aditional_fields = json_decode($category['category_aditional_fields'], true);
			$aditional_fields = array_filter(array_map(function($option){
				if($option['active'] === 1){
					return $option;
				}
			}, $aditional_fields));
		}

		if(!empty($aditional_fields)){
			return $aditional_fields;
		}

		$category_bread = json_decode("[{$category['category_breadcrumbs']}]", true);
		$category_parents = array_column($category_bread, 'category_id');
		if(!empty($category_parents)){
			$id_category = array_map('intval', $category_parents);
	
			$this->db->where_in('category_id', $id_category);
			$this->db->where('category_aditional_fields_children', 1);
			$this->db->where("category_aditional_fields != ''");
			$this->db->order_by('category_level', 'desc');
			$this->db->limit(1);
			$category_aditional_fields = $this->db->get($this->categories_table)->row_array();

			$aditional_fields = json_decode($category_aditional_fields['category_aditional_fields'], true);
		}

		return $aditional_fields;
	}

	function handler_get_all($conditions = array()){
		$category_items_count = false;
		$category_children_title = false;

		extract($conditions);

		$this->db->select("c.*");
		
		if($category_children_title){
			$this->db->select("GROUP_CONCAT(cc.category_title SEPARATOR ' ') as category_children_title");
		}
		
		$this->db->from("{$this->categories_table} c");

		if($category_children_title){
			$this->db->join("{$this->categories_table} cc", "cc.category_parent = c.category_id", "left");
		}
		
		if($category_items_count){
			$this->db->where('category_items_count > ', 0);
		}

		if(isset($parent)){
			$this->db->where('c.category_parent', $parent);
		}

		if(isset($id_category)){
			$this->db->where_in('c.category_id', $id_category);
		}

		$this->db->group_by("c.category_id");

		if(isset($order_by)){
			$this->db->order_by($order_by);
		}

		$records = $this->db->get()->result_array();
		
		return $records;
	}

	function handler_get_all_old($conditions = array()){
		extract($conditions);
		
		if(isset($parent)){
			$this->db->where('category_parent', $parent);
		}
		
        return $this->db->get('categories_old')->result_array();
	}

	function handler_get_unsorted(){
        $this->db->like('category_title', '/');
        return $this->db->get($this->categories_table)->result_array();
	}

	function handler_get_tree($conditions = array()){
		$categories = $this->handler_get_all($conditions);
		
		$tree = array();
        foreach ($categories as $category) {			
            $tree[(int) $category['category_parent']][(int) $category['category_id']] = $category;
        }
        return $tree;
	}
	
	public function handler_count_children($category_parents){
		$sql = "SELECT 	c.category_id , 
						(
							SELECT COUNT(*) 
							FROM categories cs 
							WHERE cs.category_parent = c.category_id
						) as children
				FROM categories c
				WHERE category_id IN ($category_parents)";
        $query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function handler_count_subcategories($category_id){
		$sql = "SELECT COUNT(*) as counter
				FROM categories
				WHERE category_parent = $category_id";
        $result = $this->db->query($sql)->row_array();
		return $result['counter'];
	}
	
	function handler_actualize_all(){
		$categories_bc = arrayByKey($this->handler_get_all(), 'category_id');

		$bc_cat = array();
		foreach($categories_bc as $category)
			$this->update_category_breadcrumbs($category['category_id'], $bc_cat, $categories_bc);

		if(!empty($bc_cat))
			$this->update_breadcrumbs_batch($bc_cat);


		$child_cat = array();
		$cat_list = array();
		$categories_child = $this->handler_get_all(array('parent' => 0));
		foreach($categories_child as $category)
			$cat_list[] = $category['category_id'];

		$this->get_cats_children_recursive(implode(',', $cat_list), $child_cat);
		if(!empty($child_cat))
			$this->update_children_batch($child_cat);
	}

	// BREADCRUMBS
	function update_category_breadcrumbs($id, &$glob, &$categories){
		if(isset($glob[$id]))
			return;

		$category_array = array(
			'category_id' => $id,
			'category_title' => $categories[$id]['category_title'],
			'url' => $categories[$id]['category_url'],
			'special_url' => $categories[$id]['category_special_url']
		);

		if($categories[$id]['category_parent'] == 0){

			$glob[$id][] = json_encode($category_array);

		} elseif(isset($glob[$categories[$id]['category_parent']])){

			$glob[$id] = $glob[$categories[$id]['category_parent']];
			$glob[$id][] = json_encode($category_array);

		}else{
			if(in_array($categories[$id]['category_parent'], array_keys($categories))){
				$this->update_category_breadcrumbs($categories[$id]['category_parent'], $glob, $categories);
				$glob[$id] = $glob[$categories[$id]['category_parent']];
				$glob[$id][] = json_encode($category_array);
			}else{
				$parent = $this->handler_get($categories[$id]['category_parent']);
				if(!empty($parent)){
					$glob[$id][] = $parent['category_breadcrumbs'];
					$glob[$id][] = json_encode($category_array);
				}
			}
		}
	}
	
	function update_breadcrumbs_batch($categories){
		foreach($categories as $category_id => $breadcrumbs){
			$category_level = count($breadcrumbs);
			$this->handler_update($category_id,array(
				'category_breadcrumbs' => implode(',', $breadcrumbs),
				'category_level' => $category_level
			));
		}
	}
	
	// CATEGORY CHILDREN
	function get_cats_children_recursive($cat_list, &$global){
		$sql = "SELECT category_id, category_parent, category_breadcrumbs 
				FROM $this->categories_table 
				WHERE category_parent IN (" . $cat_list . ")";
		$rez = $this->db->query($sql)->result_array();
		if(count($rez) > 0){
			$new_list = array();
			foreach($rez as $child){
				$new_list[] = $child['category_id'];
				$parents = json_decode("[" . $child['category_breadcrumbs'] . "]", true);

				foreach($parents as $par){
					if($par['category_id'] == $child['category_id'])
						continue;

					$global[$par['category_id']][] = $child['category_id'];
				}
			}
			if(!empty($new_list)){
				$this->get_cats_children_recursive(implode(',',$new_list), $global);
			}
		}
	}

	function update_children_batch($categories){
		foreach($categories as $cat => $children)
			$this->handler_update($cat, array('category_children' => implode(',', $children)));
	}

	// CATEGORY HELPERS
	function tree_select($tree, $category_parent = 0, $categories = array()) {
        if (empty($tree[$category_parent]))
            return;

        $str = '';
        foreach ($tree[$category_parent] as $k => $row) {
            $bool = in_array($row['category_id'], $categories);
			$tokens = $row['category_title'].' '.$row['category_children_title'];

            $str .= '<option value="'.$row["category_id"].'" title="'.$row['category_title'].'" data-tokens="'.$tokens.'" '.set_select('categories', $row["category_id"],$bool).'>'.str_repeat("-", $row['category_level'] - 1).' '.$row['category_title'].'</option>';
            if (isset($tree[$row['category_id']])){
                $str .= $this->tree_select($tree, $row['category_id'],$categories);
			}
        }

        return $str;
	}
		
	function tree_select_tokens($tree, $category_parent = 0, $categories = array()) {
        if (empty($tree[$category_parent])){
            return;
		}
		
        $str = '';
        foreach ($tree[$category_parent] as $k => $row) {
            $bool = in_array($row['category_id'], $categories);
			
			$tokens = $row['category_title'].' '.$row['category_children_title'];

            $str .= '<option value="'.$row["category_id"].'" title="'.$row['category_title'].'" data-tokens="'.$tokens.'" '.set_select('categories', $row["category_id"],$bool).'>'.str_repeat("-", $row['category_level'] - 1).' '.$row['category_title'].'</option>';
            if (isset($tree[$row['category_id']])){
                $str .= $this->tree_select_tokens($tree, $row['category_id'], $categories);
			}
        }

        return $str;
    }
}
