<?php

class VideoThumb {

	var $config = array();

	function __construct($config = array()) {
		$this_ci =& get_instance();

		$this->config = array(
			'imagesPath' 	=> 'files/video/', 
            'imagesUrl' 	=> 'files/video/', 
            'emptyImage' 	=> ''
		);
	}

	/*
	 * Return error message from lexicon array
	 * @param string $msg Array key
	 * @return string Message
	 * */
	function lexicon($msg = '') {
		$array = array(
			'video_err_ns' => 'Ссылка на видео обязательно.',
			'video_err_nf' => 'Запрос на видео не выдал результатов, может быть, неправильная ссылка?'
		);

		return @$array[$msg];
	}


	/*
	 * Check and format video link, then fire download of preview image
	 * @param string $video Remote url on video hosting
	 * @return array $array Array with formatted video link and preview url
	 * */
	function process($video = '') {
		if (empty($video)) {
			return array('error' => $this->lexicon('video_err_ns'));
		}

		if (!preg_match('/^(http|https)\:\/\//i', $video)) {
			$video = 'http://' . $video;
		}

		$info = $this->getVID($video);
		if(!$info['v_id']){
			return array('error' => $this->lexicon('video_err_nf'));
		}
		
		switch($info['type']){
			case 'youtube':
				$video = 'http://www.youtube.com/embed/'.$info['v_id'];
				$image = 'http://img.youtube.com/vi/'.$info['v_id'].'/0.jpg';

				$array = array(
					'video' => $video,
					'image' => $this->getRemoteImage($image),
					'v_id' => $info['v_id'],
					'type' => 'youtube'
				);
			break;
			case 'vimeo':
				$video = 'http://player.vimeo.com/video/'.$info['v_id'];
				$image = '';
				if ($xml = simplexml_load_file('http://vimeo.com/api/v2/video/'.$info['v_id'].'.xml')) {
					$image = $xml->video->thumbnail_large ? (string) $xml->video->thumbnail_large: (string) $xml->video->thumbnail_medium;
					$image = $this->getRemoteImage($image);
				}
				$array = array(
					'video' => $video,
					'image' => $image,
					'v_id' => $info['v_id'],
					'type' => 'vimeo'
				);
			break;
			default:
				$array = array('error' => $this->lexicon('video_err_nf'));
			break;
		}
		return $array;
	}
	/*
	 * Return video's type and id
	 * @param string $video Remote url on video hosting
	 * @return array $array Array with type of video and its id
	 * */
	function getVID($video = ""){ 
		if($this->getVimeoID($video)){
			return array( 
				'type' => 'vimeo',
				'v_id' =>	$this->getVimeoID($video)
			);
		} elseif($this->getYoutubeID($video)){
			return array( 
				'type' => 'youtube',
				'v_id' =>	$this->getYoutubeID($video)
			);
		} else{
			return false;
		}
	}
	/*
	 * Check and return video's id from Vimeo URL
	 * @param string $video Remote url on video hosting
	 * @return string $matches[1] with id of video
	 * */
	function getVimeoID($video){
		if(	preg_match('/[http|https]+:\/\/(?:www\.|)vimeo\.com\/([a-zA-Z0-9_\-]+)(&.+)?/i', $video, $matches) || 
			preg_match('/[http|https]+:\/\/player\.vimeo\.com\/video\/([a-zA-Z0-9_\-]+)(&.+)?/i', $video, $matches))
		{
			return $matches[1];
		}
	}

	/*
	 * Check and return video's id from YouTube URL
	 * @param string $video Remote url on video hosting
	 * @return string $matches[1] with id of video
	 * */
	function getYoutubeID($video){
		if(	preg_match('/[http|https]+:\/\/(?:www\.|)youtube\.com\/watch\?(?:.*)?v=([a-zA-Z0-9_\-]+)/i', $video, $matches) || 
			preg_match('/[http|https]+:\/\/(?:www\.|)youtube\.com\/embed\/([a-zA-Z0-9_\-]+)/i', $video, $matches) || 
			preg_match('/[http|https]+:\/\/(?:www\.|)youtu\.be\/([a-zA-Z0-9_\-]+)/i', $video, $matches))
		{ 
			return $matches[1];
		}
	}

	/*
	 * Download ans save image from remote service
	 * @param string $url Remote url
	 * @return string $image Url to image or false
	 * */
	function getRemoteImage($url = '') {
		if (empty($url)) {
			return false;
		}

		$image = '';
		$response = $this->_curl($url);
		if (!empty($response)) {
			$tmp = explode('.', $url);
			$ext = '.' . end($tmp);

			$filename = md5($url) . uniqid(rand()) . $ext;
			if (file_put_contents($this->config['imagesPath'] . $filename, $response)) {
				$image = $this->config['imagesUrl'] . $filename;
			}

		}
		if (empty($image)) {
			$image = $this->config['emptyImage'];
		}

		return $image;
	}

	/*
	 * Method for loading remote url
	 * @param string $url Remote url
	 * @return mixed $data Results of an request
	 * */
	function _curl($url = '') {
		if (empty($url)) {return false;}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);

		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
}