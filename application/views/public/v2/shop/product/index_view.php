<?php $productPrice = getPrice($product);?>
<?php $productGallery = json_decode($product['item_photos']);?>

<section class="py-5 shop-single bg-light">
    <div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="shop-detail-left">
                <div class="shop-detail-slider position-relative">
                    <div id="sync1" class="border rounded shadow-sm bg-white mb-2 owl-carousel text-center">
                        <div class="item">
                            <img alt="" src="<?php echo base_url(getImage('files/items/'.$product['item_photo']));?>" class="img-fluid img-center">
                        </div>
                        <?php if(!empty($productGallery)){?>
                            <?php foreach($productGallery as $key_photo => $productGalleryImage){?>
                                <div class="item">
                                    <img alt="" src="<?php echo base_url(getImage('files/items/'.$productGalleryImage));?>" class="img-fluid img-center">
                                    </a>
                                </div>
                            <?php }?>
                        <?php }?>
                    </div>
                    <div id="sync2" class="owl-carousel">
                        <div class="item">
                            <img alt="" src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$product['item_photo']));?>" class="img-fluid img-center">
                        </div>
                        <?php if(!empty($productGallery)){?>
                            <?php foreach($productGallery as $key_photo => $productGalleryImage){?>
                                <div class="item">
                                    <img alt="" src="<?php echo base_url(getImage('files/items/thumb_500x500_'.$productGalleryImage));?>" class="img-fluid img-center">
                                    </a>
                                </div>
                            <?php }?>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="shop-detail-right">
                <div class="border rounded shadow-sm bg-white p-4">
                    <div class="product-name">
                        <h2><?php echo clean_output($product['item_title']);?></h2>
                        <span>Articol: <b><?php echo $product['item_code'];?></b></span>
                    </div>
                    <div class="price-box">
                        <h5>
                            <?php if($productPrice['display_price_old'] > 0){?>  
                                <span class="product-desc-price"><?php echo $productPrice['display_price_old'] . ' <span class="currency custom-font-size-12">' . $productPrice['currency_symbol'] . ' </span>';?></span>
                                <span class="product-price text-danger"><?php echo $productPrice['display_price'] . '<span class="currency custom-font-size-12">' . $productPrice['currency_symbol'] . ' </span>';?></span>
                                <small class="text-success">Economisesti : <?php echo ($productPrice['display_price_old'] - $productPrice['display_price']) . '<span class="currency custom-font-size-12">' . $productPrice['currency_symbol'] . ' </span>';?></small>
                            <?php } else{?>
                                <span class="product-price"><?php echo $productPrice['display_price'] . '<span class="currency custom-font-size-12">' . $productPrice['currency_symbol'] . ' </span>';?></span>
                            <?php }?>
                        </h5>
                    </div>
                    <div class="clearfix"></div>
                    <div class="product-variation">
                        <form>
                            <div class="mt-1 pt-2 float-left mr-2">Cantitate: </div>
                            <div class="input-group quantity-input"> 
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-outline-secondary btn-number btn-lg call-function" data-callback="addToCartDecreaseQuantity" data-field="#js-add-to-cart-quantity-input">
                                        <span class="fa fa-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="quantity" class="text-center form-control border-form-control form-control-sm input-number" value="1" id="js-add-to-cart-quantity-input"> 
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-outline-secondary btn-number btn-lg call-function" data-callback="addToCartIncreaseQuantity" data-field="#js-add-to-cart-quantity-input">
                                        <span class="fa fa-plus"></span>
                                    </button>
                                </span>
                            </div>
                            <span class="float-right">
                                <input type="hidden" name="id_item" value="<?php echo $product['id_item'];?>">
                                <button type="button" class="btn btn-primary btn-lg call-function" data-callback="addToCart"><i class="icofont icofont-shopping-cart"></i> <span class="d-none d-lg-inline">Adauga in cos</span></button>
                            </span>
                        </form>
                    </div>
                    <div class="short-description border-bottom">
                        <h6 class="mb-3">
                            <span class="text-dark font-weight-bold">Descriere</span>  
                            <small class="float-right">Disponibilitate: <?php echo get_choice('<strong class="badge badge-danger">In stoc</strong>', 0 < (int) $product['item_quantity'], '<strong class="badge badge-secondary">Nu este</strong>')?></small>
                        </h6>
                        <div class="short-description-text">
                            <?php echo $product['item_description_cleaned'];?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<section id="det" class="pb-5 pt-0 shop-single-detail bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="rounded shadow-sm bg-white">
                    <ul class="nav nav-pills p-3" id="pills-tab" role="tablist">
                        <li class="nav-item"> 
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">SPECIFICATII</a>
                        </li>
                        <li class="nav-item"> 
                            <a class="nav-link" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews" role="tab" aria-controls="pills-reviews" aria-selected="false">RECENZII</a>
                        </li>
                    </ul>
                    <div class="tab-content p-4 border-top" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <?php echo Modules::run("properties/_get_product_properties",array('id_item' => $product['id_item'])); ?>

                            <?php if(!empty($product['item_video_code'])){?>
                                <div class="custom-margin-top-20">
                                    <h2 class="heading-title"><span>VIDEO</span></h2>
                                    <div class="video-wr">
                                        <?php echo generate_video_html($product['item_video_code'],940,529);?>
                                    </div>
                                </div>                   
                            <?php }?>
                        </div>
                        <div class="tab-pane fade" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">
                            <div class="card-body p-0 reviews-card">
                                <?php $comments = Modules::run('items/_get_comments', array('id_item' => $product['id_item']))?>
                                <?php if(!empty($comments)){?>
                                    <?php foreach($comments as $comment){?>
                                        <?php $this->load->view($this->theme->public_view('shop/product/comments/item_view'), array('comment' => $comment));?>
                                    <?php }?>
                                <?php } else{?>
                                    <p class="empty-comments fs-14">Fii primul care va lasa o recenzie!</p>
                                <?php }?>
                            </div>
                            <div class="p-4 bg-light rounded mt-4">
                                <h5 class="card-title mb-4">Scrie o recenzie</h5>
                                <p class="comment-notes"><span id="email-notes">Adresa de Email nu va fi publicata.</span> Cimpurile marcate cu <span class="text-danger">*</span> sunt obligatorii.</p>
                                <form name="sentMessage">
                                    <div class="row">
                                        <div class="control-group form-group col-6">
                                            <div class="controls">
                                                <label>Nume/Prenume <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="author">
                                            </div>
                                        </div>
                                        <div class="control-group form-group col-6">
                                            <div class="controls">
                                                <label>Email <span class="text-danger">*</span></label>
                                                <input type="email" class="form-control" name="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <label>Mesaj <span class="text-danger">*</span></label>
                                            <textarea rows="3" cols="100" class="form-control" name="comment"></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group comment-form-comment">
                                        <label>Dovedeste ca nu esti robot<span class="required">*</span></label>                            
                                        <?php echo $recaptcha_widget;?>
                                    </div>    
                                    <input type="hidden" name="item" value="<?php echo $product['id_item'];?>">
                                    <div class="text-right">
                                        <button type="button" class="btn btn-primary call-function" data-callback="add_comment">Trimite</button>
                                    </div>
                                </form>
                                <script src='https://www.google.com/recaptcha/api.js'></script>
                                <script>
                                    var add_comment = function(btn){
                                        var $this = $(btn);
                                        var $form = $this.closest('form');
                                        $.ajax({
                                            type: 'POST',
                                            url: base_url+'items/ajax_operations/add_comment',
                                            data: $form.serialize(),
                                            dataType: 'JSON',
                                            beforeSend: function(){
                                                showLoader($form);
                                            },
                                            success: function(resp){
                                                systemMessages(resp.message, resp.mess_type);
                                                hideLoader($form);
                                                if(resp.mess_type == 'success'){
                                                    $('.tab-pane#pills-reviews .reviews-card').append(resp.comment);
                                                    if($('.tab-pane#pills-reviews .reviews-card > .empty-comments').length > 0){
                                                        $('.tab-pane#pills-reviews .reviews-card > .empty-comments').remove();
                                                    }
                                                    $form[0].reset();
                                                }
                                            }
                                        });
                                        return false;
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if(!empty($same_products)){?>
    <section class="product-list pt-5 bg-light pb-4 pbc-5 border-top">
        <div class="container">
        <h4 class="mt-0 mb-3 text-dark">PRODUSE ASEMANATOARE</h4>
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel owl-carousel-category owl-theme">
                    <?php foreach($same_products as $same_product){?>
                        <!-- PRODUCT - START -->
                        <div class="item">
                            <?php $this->load->view($this->theme->public_view('shop/catalog/product_cart_view'), array('item' => $same_product));?>
                        </div>
                        <!-- PRODUCT - END -->
                    <?php }?>
                </div>
            </div>
        </div>
        </div>
    </section>
<?php }?>