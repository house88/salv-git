<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = "shop";
$route['404_override'] = '';

$route['signin'] 	        	= "auth/signin";
$route['register'] 	        	= "auth/register";
$route['forgot_password']   	= "auth/forgot_password";
$route['logout'] 	        	= "auth/logout";
$route['activate/(:any)']   	= "auth/activate/$1";
$route['reset_password/(:any)'] = "auth/reset_password/$1";

$route['account'] 	        = "users/account";
$route['account/settings'] 	= "users/account_settings";

$route['order/(:any)'] 	= "orders/view/$1";
$route['page/(:any)'] 	= "pages/view/$1";

$route['spage/(:any)'] 	= "static_block/page/$1";

//region Landings
$route['trade-in'] 	= "static_block/landing/trade-in";
//endregion Landings

$route['tinymanager/init/(:any)'] 	= "tinymanager/init/$1";

// ADMIN
$route['admin/([a-zA-Z0-9\/\-:._]+)'] = function ($module){
	$full_route = explode('/', $module);
	
	$module_name = array_shift($full_route);
	switch ($module_name) {
		case 'settings':
		case 'ajax_operations':
			return 'admin/'.$module;
		break;
		case 'trade-in':
			return 'admin/tradein';
		break;
		case 'groups':
		case 'rights':
		case 'group_rights':
			$url[] = 'users';
			$url[] = $module_name;
		break;
		case 'moderators':
			$url[] = 'users/admin';
			$url[] = $module_name;
		break;
		case 'prices':
		case 'dynamic_prices':
		case 'comments':
			$url[] = 'items';
			$url[] = $module_name;
		break;
		case 'dynamic_prices':
		case 'moderate':
		case 'moderator_report':
		case 'moderator_report_new':
			$url[] = 'items/admin';
			$url[] = $module_name;
		break;
		case 'moderation_reports':
			$url[] = 'items/reports';
		break;
		default:
			$url[] = $module_name;
			$url[] = 'admin';			
		break;
	}
	
	if(!empty($full_route)){
		$url[] = implode('/', $full_route);
	}
	
	return implode('/', $url);
};

// SHOP
$route['catalog/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'shop/catalog/' . strtolower($categories);
};
$route['testcatalog/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'shop/catalog/' . strtolower($categories);
};

$route['products/([a-zA-Z0-9\/\-:._]+)'] = function ($products){
	return 'shop/products/' . strtolower($products);
};

$route['search'] = 'shop/search';
$route['search/(:any)'] = 'shop/search/$1';
$route['search/catalog/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'shop/search/' . strtolower($categories);
};

$route['catalog_mode/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'shop/catalog_mode/' . strtolower($categories);
};

$route['brand/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'shop/brand/' . strtolower($categories);
};

$route['concurs/upload_photo'] 	= "concurs/upload_photo";
$route['concurs/ajax_operations'] 	= "concurs/ajax_operations";
$route['concurs/ajax_operations/(:any)'] 	= "concurs/ajax_operations/$1";
$route['concurs/(:any)'] 	= "concurs/index/$1";

$route['payments/(:any)'] 	= "payment/$1";


//$route['translate_uri_dashes'] = FALSE;
$route['scaffolding_trigger'] = "";
