<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Properties_model extends CI_Model{
    var $items_table = "items";
    var $categories_table = "categories";
    var $properties_table = "properties";
    var $properties_in_categories_table = "properties_in_categories";
    var $properties_values_table = "properties_values";
    var $items_properties_values_table = "items_properties_values";

    function __construct(){
        parent::__construct();
    }
	
	function handler_insert($data = array()){
        if(empty($data)){
            return;
        }

        $this->db->insert($this->properties_table, $data);
        return $this->db->insert_id();
	}

	function handler_update($id_property, $data = array()){
        if(empty($data)){
            return;
        }

        $this->db->where('id_property', $id_property);
        $this->db->update($this->properties_table, $data);
	}
	
	function handler_delete($id_property){
        $this->db->where('id_property', $id_property);
        $this->db->delete($this->properties_table);
	}

	function handler_get_property_values($id_property){
        $this->db->where('id_property', $id_property);
        $this->db->order_by('value_weight', 'ASC');
        return $this->db->get($this->properties_values_table)->result_array();
	}

	function handler_get($id_property){
        $this->db->where('id_property', $id_property);
        return $this->db->get($this->properties_table)->row_array();
	}

	function handler_get_all($conditions = array()){
		$order_by = " pc.property_category_weight DESC ";
		$where = array();
		$params = array();
		$admin_select = false;

		extract($conditions);
		
		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}
		
		if(isset($id_category)){
	    	$where[] = " pc.id_category = ? ";
	        $params[] = $id_category;
		}
		
		if(isset($category_children) && !empty($category_children)){
	    	$where[] = " pc.id_category IN ({$category_children}) ";
		}
		
		if(isset($properties_list)){
	    	$where[] = " p.id_property IN ({$properties_list}) ";
		}
		
		if(isset($on_item)){
	    	$where[] = " p.on_item = ? ";
	    	$params[] = $on_item;
		}
		
		if(isset($status_property)){
	    	$where[] = " p.status_property = ? ";
	    	$params[] = $status_property;
		}

		if(!$admin_select){
			$sql = "SELECT DISTINCT p.id_property,p.*, pc.property_category_weight, pc.property_category_to_children
					FROM $this->properties_table p
					INNER JOIN $this->properties_in_categories_table pc ON p.id_property = pc.id_property";
		
			if(!empty($where)){
				$sql .= " WHERE " . implode(" AND", $where);
			}
		} else{
			$sql = "SELECT p.*, pc.property_category_weight, pc.property_category_to_children
					FROM $this->properties_table p
					LEFT JOIN $this->properties_in_categories_table pc ON p.id_property = pc.id_property";
			
			if(!empty($where)){
				$sql .= " WHERE " . implode(" AND", $where);
			}
			
			$sql .= " GROUP BY p.id_property ";
		}
		
		$sql .= " ORDER BY " . $order_by;
		
		if(isset($limit, $start)){
			$sql .= " LIMIT $start, $limit";
		}
		
		$query = $this->db->query($sql,$params);
        return $query->result_array();
	}

	function handler_get_count($conditions = array()){
		$where = array();
        $params = array();
		$admin_select = false;

		extract($conditions);
		
		if(isset($id_category)){
	    	$where[] = " pc.id_category = ? ";
	        $params[] = $id_category;
		}
		
		if(isset($category_children) && !empty($category_children)){
	    	$where[] = " pc.id_category IN ({$category_children}) ";
		}
		
		if(isset($properties_list)){
	    	$where[] = " p.id_property IN ({$properties_list}) ";
		}
		
		if(isset($on_item)){
	    	$where[] = " p.on_item = ? ";
	    	$params[] = $on_item;
		}
		
		if(isset($status_property)){
	    	$where[] = " p.status_property = ? ";
	    	$params[] = $status_property;
		}

				
		if(!$admin_select){
			$sql = "SELECT COUNT(DISTINCT p.id_property) as counter
					FROM $this->properties_table p
					INNER JOIN $this->properties_in_categories_table pc ON p.id_property = pc.id_property";
		} else{
			$sql = "SELECT COUNT(DISTINCT p.id_property) as counter
					FROM $this->properties_table p
					LEFT JOIN $this->properties_in_categories_table pc ON p.id_property = pc.id_property";
		}

		if(!empty($where)){
			$sql .= " WHERE " . implode(" AND", $where);
		}
		
		$query = $this->db->query($sql,$params)->row_array();
        return $query['counter'];
	}
	
	function handler_insert_items_properties_values($data = array()){
        if(empty($data)){
            return;
        }

        return $this->db->insert_batch($this->items_properties_values_table, $data);
	}

	function handler_get_items_properties_values($id_item){
        $this->db->where('id_item', $id_item);
        return $this->db->get($this->items_properties_values_table)->result_array();
	}

	function _get_category_children($id_category = 0){
		$category = $this->db->where('category_id', $id_category)->get($this->categories_table)->row_array();
		$category_children = array();
		if(!empty($category['category_children'])){
			$category_children = explode(',', $category['category_children']);
		}
		$category_children[] = $id_category;
		return implode(',', $category_children);
	}

	function handler_get_items_properties_values_counters2($conditions = array()){
		extract($conditions);

		$properties_values_counters = array();

		$only_items = "";
		if(!empty($items_list)){
			$view_items_list = implode(',', $items_list);
			$only_items = " AND i.id_item IN ({$view_items_list}) ";
		}

		$only_brands = "";
		if(!empty($id_brand)){
			$view_brands_list = implode(',', $id_brand);
			$only_brands = " AND i.id_brand IN ({$view_brands_list}) ";
		}

		if(isset($properties_params)){
			if(!empty($category_children)){
				$properties_params['id_category'] = $category_children;
			}

			if(!empty($id_brand)){
				$properties_params['id_brand'] = $view_brands_list;
			}
			
			$properties = array();
			if(!empty($properties_params['properties_select'])){
				$properties = array_merge($properties, array_keys($properties_params['properties_select']));
			}
			if(!empty($properties_params['properties_range'])){
				$properties = array_merge($properties, array_keys($properties_params['properties_range']));
			}
			if(!empty($properties)){
				foreach ($properties as $property_id) {
					$properties_params['not_property'] = $property_id;
					$items_by_properties = $this->handler_get_items_by_properties_and($properties_params);
					$items_list = array();
					if(!empty($items_by_properties)){
						foreach ($items_by_properties as $item_by_properties) {
							$items_list[$item_by_properties['id_item']] = $item_by_properties['id_item'];
						}
					}
			
					if(!empty($items_list)){
						$items_list = implode(',', $items_list);
					} else{
						$items_list = 0;
					}
						
					$products_params['items_list'] = $items_list;
					$sql = "SELECT ipv.id_property, ipv.value, COUNT(DISTINCT(ipv.id_item)) as items_count
							FROM {$this->items_properties_values_table} ipv
							INNER JOIN {$this->properties_table} p on ipv.id_property = p.id_property
							INNER JOIN {$this->items_table} i ON ipv.id_item = i.id_item
							WHERE 
								p.id_property = {$property_id} AND 
								i.id_category IN ({$category_children}) AND 
								i.item_visible = 1 AND
								i.id_item IN ({$items_list})
							GROUP BY ipv.id_property, ipv.value";
							
					$records = $this->db->query($sql)->result_array();
					if(!empty($records)){
						foreach ($records as $record) {
							if(!isset($properties_values_counters[$record['id_property']][$record['value']])){
								$properties_values_counters[$record['id_property']][$record['value']] = $record['items_count'];
							}
						}
					}
				}
			}
		} else{		
			$sql = "SELECT ipv.id_property, ipv.value, COUNT(DISTINCT(ipv.id_item)) as items_count
					FROM {$this->items_properties_values_table} ipv
					INNER JOIN {$this->properties_table} p on ipv.id_property = p.id_property
					INNER JOIN {$this->items_table} i ON ipv.id_item = i.id_item
					WHERE 
						p.status_property = 1 AND 
						p.in_filter = 1 AND 
						p.type_property IN ('select','multiselect') AND 
						i.id_category IN ({$category_children}) AND 
						i.item_visible = 1 
						{$only_items} 
						{$only_brands}
					GROUP BY ipv.id_property, ipv.value";
			$records = $this->db->query($sql)->result_array();
			foreach ($records as $record) {
				$properties_values_counters[$record['id_property']][$record['value']] = $record['items_count'];
			}

		}
		return $properties_values_counters;
	}

	function handler_get_items_properties_values_counters($conditions = array()){
		extract($conditions);

		$properties_values_counters = array();

		$only_items = "";
		if(!empty($items_list)){
			$view_items_list = implode(',', $items_list);
			$only_items = " AND i.id_item IN ({$view_items_list}) ";
		}

		$only_brands = "";
		if(!empty($id_brand)){
			$view_brands_list = implode(',', $id_brand);
			$only_brands = " AND i.id_brand IN ({$view_brands_list}) ";
		}
		
		$sql = "SELECT ipv.id_property, ipv.value, COUNT(DISTINCT(ipv.id_item)) as items_count
				FROM {$this->items_properties_values_table} ipv
				INNER JOIN {$this->properties_table} p on ipv.id_property = p.id_property
				INNER JOIN {$this->items_table} i ON ipv.id_item = i.id_item
				INNER JOIN {$this->properties_in_categories_table} pc ON p.id_property = pc.id_property
				WHERE 
					p.status_property = 1 AND 
					p.in_filter = 1 AND 
					p.type_property IN ('select','multiselect') AND 
					i.id_category IN ({$category_children}) AND 
					i.item_visible = 1 
					{$only_items} 
					{$only_brands}
				GROUP BY ipv.id_property, ipv.value";
		$records = $this->db->query($sql)->result_array();
		foreach ($records as $record) {
			$properties_values_counters[$record['id_property']][$record['value']] = $record['items_count'];
		}

		if(isset($properties_params)){
			if(!empty($category_children)){
				$properties_params['id_category'] = $category_children;
			}

			if(!empty($id_brand)){
				$properties_params['id_brand'] = $view_brands_list;
			}
			
			$properties = array();
			if(!empty($properties_params['properties_select'])){
				$properties = array_merge($properties, array_keys($properties_params['properties_select']));
			}
			if(!empty($properties_params['properties_range'])){
				$properties = array_merge($properties, array_keys($properties_params['properties_range']));
			}
			if(!empty($properties)){
				foreach ($properties as $property_id) {
					$properties_params['not_property'] = $property_id;
					$items_by_properties = $this->handler_get_items_by_properties_and($properties_params);
					$items_list = array();
					if(!empty($items_by_properties)){
						foreach ($items_by_properties as $item_by_properties) {
							$items_list[$item_by_properties['id_item']] = $item_by_properties['id_item'];
						}
					}
			
					if(!empty($items_list)){
						$items_list = implode(',', $items_list);
					} else{
						$items_list = 0;
					}
						
					$sql = "SELECT ipv.id_property, ipv.value, COUNT(DISTINCT(ipv.id_item)) as items_count
							FROM {$this->items_properties_values_table} ipv
							INNER JOIN {$this->properties_table} p on ipv.id_property = p.id_property
							INNER JOIN {$this->items_table} i ON ipv.id_item = i.id_item
							INNER JOIN {$this->properties_in_categories_table} pc ON p.id_property = pc.id_property
							WHERE 
								p.id_property = {$property_id} AND 
								i.id_category IN ({$category_children}) AND 
								i.item_visible = 1 AND
								i.id_item IN ({$items_list})
							GROUP BY ipv.id_property, ipv.value";
							
					$records = $this->db->query($sql)->result_array();
					if(!empty($records)){
						foreach ($records as $record) {
							if(!isset($properties_values_counters[$record['id_property']][$record['value']])){
								$properties_values_counters[$record['id_property']][$record['value']] = $record['items_count'];
							}
						}
					}
				}
			}
		}
		return $properties_values_counters;
	}

	function handler_get_items_by_properties_and($conditions = array()){
		$where = array();

		extract($conditions);

		if(!empty($id_category)){
			$where[] = " i.id_category IN ({$id_category}) ";
		}

		if(!empty($id_brand)){
			$where[] = " i.id_brand IN ({$id_brand}) ";
		}

		if(isset($properties_select)){
			foreach($properties_select as $property_select => $values){
				if(isset($not_property) && $not_property == $property_select){
					continue;
				}

				$str = array();
				$values = explode(',',$values);
				foreach($values as $val){
					$val = trim($val);
					$str[] = $val;
				}

				if(!empty($str)){
					$str = implode(',', $str);
					$where[] = "(
								  SELECT ip.value 
								  FROM $this->items_properties_values_table ip 
								  INNER JOIN {$this->properties_table} p ON ip.id_property = p.id_property
								  WHERE ip.id_item = i.id_item AND ip.id_property = $property_select AND ip.value IN ($str) AND p.in_filter = 1 AND p.status_property = 1
								  GROUP BY ip.id_item
								)";
				}
			}			
		}

		if(isset($properties_range)){
			foreach($properties_range as $property_range => $values){
				if(isset($not_property) && $not_property == $property_select){
					continue;
				}

				if(array_key_exists('from', $values)){
					$where[] = "(
								  SELECT ip.value 
								  FROM $this->items_properties_values_table ip 
								  INNER JOIN {$this->properties_table} p ON ip.id_property = p.id_property
								  WHERE ip.id_item = i.id_item AND ip.id_property = $property_range AND p.in_filter = 1 AND p.status_property = 1
								  GROUP BY ip.id_item
								) >= " . $values['from'];
				}
				if(array_key_exists('to', $values)){
					$where[] = "(
								  SELECT ip.value 
								  FROM $this->items_properties_values_table ip 
								  INNER JOIN {$this->properties_table} p ON ip.id_property = p.id_property
								  WHERE ip.id_item = i.id_item AND ip.id_property = $property_range AND p.in_filter = 1 AND p.status_property = 1
								  GROUP BY ip.id_item
								) <= " . $values['to'];
				}
			}			
		}

		$sql = "SELECT i.id_item
				FROM $this->items_table i";
		
		if(!empty($where)){
			$sql .= " WHERE " . implode(" AND ", $where);
		}
		
		return $this->db->query($sql)->result_array();
	}

	function handler_delete_items_properties_values($id_item){
        $this->db->where('id_item', $id_item);
        return $this->db->delete($this->items_properties_values_table);
	}

	function handler_insert_property_values($data = array()){
        if(empty($data)){
            return;
        }

        return $this->db->insert_batch($this->properties_values_table, $data);
	}
	
	function handler_update_property_values($data = array()){
        if(empty($data)){
            return;
        }

        return $this->db->update_batch($this->properties_values_table, $data, 'id_value');
	}

	function handler_remove_property_values($id_property, $values = array()){
		$this->db->where_in('id_property', $id_property);

        if(!empty($values)){
            $this->db->where_in('id_value', $values);
        }

        return $this->db->delete($this->properties_values_table);
	}
	
	function handler_get_relation_categories($id_property = 0){
		$sql = "SELECT pc.*, c.*
				FROM $this->properties_in_categories_table pc
				INNER JOIN $this->categories_table c ON pc.id_category = c.category_id
				WHERE pc.id_property = {$id_property}";		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function handler_get_category_relation_properties($id_category = 0, $conditions = array()){
		$this->db->where('id_category', $id_category);
		
		extract($conditions);
		
		if(isset($property_category_to_children)){
			$this->db->where('property_category_to_children', $property_category_to_children);			
		}

        return $this->db->get($this->properties_in_categories_table)->result_array();
	}
	
	function handler_exist_relation_category($id_property = 0, $id_category = 0){
		$this->db->where('id_property', $id_property);
		$this->db->where('id_category', $id_category);
		return $this->db->count_all_results($this->properties_in_categories_table);
	}
	
	function handler_get_relation_category($id_property = 0, $id_category = 0){
		$this->db->where('id_property', $id_property);
		$this->db->where('id_category', $id_category);
		return $this->db->get($this->properties_in_categories_table)->row_array();
	}

	function handler_insert_relation_category($data = array()){
        if(empty($data)){
            return;
        }

        return $this->db->insert_batch($this->properties_in_categories_table, $data);
	}

	function handler_update_relation_category($id_property = 0, $id_category = 0, $data = array()){
        if(empty($data)){
            return;
		}
		
		$this->db->where('id_property', $id_property);
		$this->db->where('id_category', $id_category);
        return $this->db->update($this->properties_in_categories_table, $data);
	}
	
	function handler_delete_relation_category($id_property, $id_category = 0){
		$this->db->where('id_property', $id_property);

		if($id_category > 0){
			$this->db->where('id_category', $id_category);
		}
        return $this->db->delete($this->properties_in_categories_table);
	}
}
