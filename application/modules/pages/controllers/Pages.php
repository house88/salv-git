<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];

		$this->load->model('Pages_model', 'pages');
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		return_404($this->data);
	}

	function view(){
		$url = $this->uri->segment(2);
		$this->data['page'] = $this->pages->handler_get_by_url($url);
		if(empty($this->data['page'])){
			return_404($this->data);
		}

        if($this->data['page']['page_visible'] == 0){
            return_404($this->data);
        }

		$this->breadcrumbs[] = array(
			'title' => $this->data['page']['page_title'],
			'link' => base_url('page/'.$this->data['page']['url'])
		);

		$this->data['stitle'] = $this->data['page']['page_title'];
		$this->data['skeywords'] = $this->data['page']['mk'];
		$this->data['sdescription'] = $this->data['page']['md'];

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'pages/index_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}
}
