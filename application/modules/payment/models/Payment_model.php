<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model{
	var $payments = "payments";
	var $payments_logs = "payments_cards_log";
	var $payment_delivery_relation = "payment_delivery_relation";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->payments, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_payment, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_payment', $id_payment);
		$this->db->update($this->payments, $data);
	}

	function handler_delete($id_payment){
		$this->db->where('id_payment', $id_payment);
		$this->db->delete($this->payments);
	}

	function handler_get($id_payment){
		$this->db->where('id_payment', $id_payment);
		return $this->db->get($this->payments)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_payment ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('payment_active', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->payments)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('payment_active', $active);
        }

		return $this->db->count_all_results($this->payments);
	}

	function handler_insert_delivery_relation($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert_batch($this->payment_delivery_relation, $data);
		return $this->db->insert_id();
	}

	function handler_delete_delivery_relation($id_payment){
		$this->db->where('id_payment', $id_payment);
		$this->db->delete($this->payment_delivery_relation);
	}

	function handler_get_delivery_relation($id_payment){
		$this->db->where('id_payment', $id_payment);

		$records = $this->db->get($this->payment_delivery_relation)->result_array();

		return !empty($records) ? $records : array();
	}

	function handler_get_delivery_relations(){
		$records = $this->db->get($this->payment_delivery_relation)->result_array();

		return !empty($records) ? $records : array();
	}

	//region PAYMENTS LOGS
	function handler_insert_log($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->payments_logs, $data);
		return $this->db->insert_id();
	}
	//endregion PAYMENTS LOGS
}
